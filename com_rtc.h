/*  (c) Copyright:  2005..2019  Patrn, Confidential Data
 *
 *  Workfile:           com_rtc.h
 *  Purpose:            Headerfile for all real-time clock functions
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    20 Aug 2005       Created
 *    26 Jan 2019       Add DST_AUTO
 *    07 Oct 2019       Add TIME_FORMAT_WW_DD_MMM_YYYY_HH_MM_SS
 *    05 Jul 2021:      Add TIME_FORMAT_PIKRELLCAM
 *    19 Nov 2023:      Add MAX_TIME_LENZ
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _GEN_COM_H_
#define _GEN_COM_H_

#define  DATE_TIME_SIZE             32          //"Mo 31-12-2003 15:59"
#define  DST_AUTO                   2           // Handle daylight saving time automatically 
#define  DST_YES                    1           // Handle daylight saving time
#define  DST_NO                     0           // No     daylight saving time

#define  ES_QUARTERS_PER_DAY        (4 * 24)    // Quarters per day
#define  ES_SECONDS_PER_MINUTE      60          // seconds per minute
#define  ES_SECONDS_PER_HOUR        3600        // seconds per hour
#define  ES_SECONDS_PER_DAY         (24*3600)   // seconds per day
#define  ES_SECONDS_PER_WEEK        (604800L)   // seconds per week

#define  ES_SECONDS_TIMEZONE        3600        // Timezone UTC/GMT + 1 Hr
#define  ES_MJD_1970_OFFSET         40587
#define  ES_SECS_TO_1970            (2208988800LL)

#define  ES_SECS_DST_2004_START     (1080442800LL) // 1970
#define  ES_SECS_DST_2004_END       (1099191600LL) // 1970
//
#define  MAX_TIME_LEN               31             // Maximum buffer size used for all formats
#define  MAX_TIME_LENZ              MAX_TIME_LEN+1 // AsciiZ

//
// TIME formats
//
typedef enum
{
   TIME_FORMAT_WW_DD_MM_YYYY_HH_MM = 0,
   TIME_FORMAT_WW_DD_MM_YYYY_HH_MM_SS,
   TIME_FORMAT_WW_S_DD_MM_YYYY_HH_MM,
   TIME_FORMAT_WW_DD_MMM_YYYY_HH_MM_SS,
   TIME_FORMAT_WW_DD_MMM_YYYY,
   TIME_FORMAT_WW_DD_MMM_YYYY_HH_MM,
   TIME_FORMAT_WW_DD_MM_YYYY,
   TIME_FORMAT_DD_MM_YYYY_HH_MM,
   TIME_FORMAT_DD_MM_YYYY,
   TIME_FORMAT_HH_MM_SS,
   TIME_FORMAT_HH_MM,
   TIME_FORMAT_MM_SS,
   TIME_FORMAT_WW_YYYY_MM_DD,
   TIME_FORMAT_AMM_YYYY,
   TIME_FORMAT_YYYY_MM,
   TIME_FORMAT_YYYY_MM_DD,
   TIME_FORMAT_YYYY_MM_DD_WW,
   TIME_FORMAT_YYYY_MM_DD_HH_MM_WW,
   TIME_FORMAT_YYYY_MM_DD_HH_MM_SS,
   TIME_FORMAT_CSV_YYYY_MM_DD_HH_MM_SS,
   TIME_FORMAT_CSV_YYYY_MM_DD,
   TIME_FORMAT_CSV_HH_MM_SS,
   TIME_FORMAT_CSV_HH_MM,
   TIME_FORMAT_MAIL,
   TIME_FORMAT_DAY_OF_WEEK,
   TIME_FORMAT_PIKRELLCAM,
   TIME_FORMAT_WW_S_DD_MM_YYYY_HH_MM_SS,
   //
   NUM_TIME_FORMATS
}  TIMEFORMAT;
//
// Either check actual format size or use :
//
#define TIME_FORMAT_MAX_SIZE     32

//
// Structures and enum type definitions
//
typedef struct CLOCK_DAY_TIME
{
   u_int8 ubHour;
   u_int8 ubMin;
   u_int8 ubSec;
   u_int8 ubDay;
   u_int8 ubMonth;
   u_int8 ubYear;       // Year since 1900 !!!
   u_int8 ubWeekDay;
}  CLOCK_DAY_TIME;

bool     RTC_DaylightSavingTime     (u_int32);
u_int32  RTC_GetSecs                (int, int, int);
u_int32  RTC_GetSystemSecs          (void);
void     RTC_SetTimeZone            (int iGmtOffset);
u_int32  RTC_GetCurrentQuarter      (u_int32);
u_int32  RTC_GetDateTime            (char *pcDateTime);
u_int32  RTC_GetDateTimeSecs        (char *pcDateTime);
int      RTC_GetDaysPerMonth        (u_int32 ulSecs);
void     RTC_ConvertDateTime        (TIMEFORMAT tFormat, char *pcDateTime, u_int32 ulSecs);
int      RTC_ConvertDateTimeSize    (TIMEFORMAT tFormat);
u_int32  RTC_GetMidnight            (u_int32 ulSecs);
int      RTC_GetYear                (u_int32 ulSecs);
int      RTC_GetMonth               (u_int32 ulSecs);
int      RTC_GetDay                 (u_int32 ulSecs);
int      RTC_GetHour                (u_int32 ulSecs);
int      RTC_GetMinute              (u_int32 ulSecs);
int      RTC_GetSecond              (u_int32 ulSecs);

#endif  /*_COM_RTC_H_ */

