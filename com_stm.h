/*  (c) Copyright:  2017..2018  Patrn, Confidential Data 
 *
 *  Workfile:           com_stm.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            HTTP GET/PUT/POST acquisition state machine 
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:            
 *    02 Mar 2018:      Made common from rpi_net.*
 *    31 Mar 2018:      Add timestamp to NETCL
 *    29 Jun 2019:      Add ISO Transport Service over TCP (ignore)
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

//
// Macro:       a         
//                       
EXTRACT_HTTP(NETSTM_IDLE,     "Idle",                    http_StmIdle            )     // -1
EXTRACT_HTTP(NETSTM_START,    "Start",                   http_StmStart           )     //  0
EXTRACT_HTTP(NETSTM_GET_1,    "Get 1",                   http_StmGet1            )     //  1
EXTRACT_HTTP(NETSTM_GET_2,    "Get 2",                   http_StmGet2            )     //  2
EXTRACT_HTTP(NETSTM_SPACE,    "Space",                   http_StmSpace           )     //  3
EXTRACT_HTTP(NETSTM_SLASH,    "Slash",                   http_StmSlash           )     //  New
EXTRACT_HTTP(NETSTM_POST1,    "Post 1",                  http_StmPost1           )     //  9
EXTRACT_HTTP(NETSTM_POST2,    "Post 2",                  http_StmPost2           )     // 10
EXTRACT_HTTP(NETSTM_POST3,    "Post 3",                  http_StmPost3           )     // 11
EXTRACT_HTTP(NETSTM_ESC_1,    "Escape 1",                http_StmEscape1         )     //  5
EXTRACT_HTTP(NETSTM_ESC_2,    "Escape 2",                http_StmEscape2         )     //  6 and 7
EXTRACT_HTTP(NETSTM_URL,      "URL",                     http_StmUrl             )     //  8 and 12
EXTRACT_HTTP(NETSTM_CHDRS,    "Content headers",         http_StmContentHeaders  )     // 13
EXTRACT_HTTP(NETSTM_PAYLD,    "Payload",                 http_StmPayload         )     // 14
EXTRACT_HTTP(NETSTM_ISO,      "TPKT Reserved",           http_StmTpktRes         )     // 15
EXTRACT_HTTP(NETSTM_ISOLM,    "TPKT Size1",              http_StmTpktLenMsb      )     // 16
EXTRACT_HTTP(NETSTM_ISOLL,    "TPKT Size2",              http_StmTpktLenLsb      )     // 17
EXTRACT_HTTP(NETSTM_TPKT,     "TPKT",                    http_StmTpktPacket      )     // 18
EXTRACT_HTTP(NETSTM_RAW,      "RAW",                     http_StmRawPacket       )     // Raw data
