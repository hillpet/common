/*  (c) Copyright:  2012..2019  Patrn, Confidential Data 
 *
 *  Workfile:           com_func.c
 *  Purpose:            Offer the API as an abstraction layer for library calls
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *  Note:               
 *
 *  Author:             Peter Hillen
 *  Changes:       
 *    17 Jul 2012       Created
 *    03 Jul 2019       popen() check fp
 *    24 Aug 2019       Add GEN_GetPidByCommand() alternative to GEN_GetPidByName()
 *    03 Sep 2019       Add GEN_RemovePair()
 *    27 Sep 2019       Fix TrimLeft() bug
 *    29 Oct 2019       Redo GEN_WaitSemaphore()
 *    17 Nov 2019       Add GEN_SimpleWildcards()
 *    22 Oct 2021:      Add DELIM_SQRB,_SQRE,_CURB,_CURE
 *    19 Nov 2021:      Add Variadic macro's for PRINTF()
 *    01 Dec 2021:      Add Gen_Upper/LowerCase()
 *    07 Mar 2022:      Replace atoi() by strtol()
 *    25 Jan 2024:      Add List data to file
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <malloc.h>
#include <signal.h>
#include <errno.h>
#include <stdarg.h>
#include <time.h>
#include <semaphore.h>
#include <pwd.h>
#include <grp.h>

#include "typedefs.h"
#include "config.h"
#include "com_safe.h"
#include "com_rtc.h"
#include "com_log.h"
#include "com_func.h"

//#define USE_PRINTF
#include "printx.h"
//
// Local prototypes
//
static char   *gen_FindDir       (char *, int);
static char    gen_GetChar       (char *, int);
static void    gen_ListData      (int, const char *, bool, int, u_int8 *, int);

//
//                                      0         1         2         3         4         5         6
//                                      0123456789012345678901234567890123456789012345678901234567890123
static const char   cMiscFunctions[] = "01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.";


#ifdef   FEATURE_SHOW_PROCESS_STATUS
#define  CMD_SHOWPROCESSSTATUS(x)      GEN_ShowProcessStatus(x)
#else  //FEATURE_SHOW_PROCESS_STATUS
#define  CMD_SHOWPROCESSSTATUS(x)
#endif //FEATURE_SHOW_PROCESS_STATUS

#define  ONEMILLION           1000000L
#define  ONEBILLION           1000000000L

/* ======   Local Functions separator ===========================================
void ___OS_FUNCTIONS(){}
==============================================================================*/

// 
//  Function:  GEN_BatchProcess
//  Purpose:   Start a process through the SH or BASH shell
// 
//  Parms:     Process name, process parms, process identification, timeout (secs)
//  Returns:   Process pid
// 
// 
pid_t GEN_BatchProcess(const char *pcProcess, const char *pcParms, const char *pcPidPath, int iTimeout)
{
   int      iCc, iSize;
   pid_t    tPid;
   char    *pcShell;

   //==========================================================================
   // Split process : 
   //       CMND thread returns with tPid of the EXEC thread
   //       EXEC thread returns with tPid=0
   //
   tPid = fork();
   //
   //==========================================================================
   switch(tPid)
   {
      case 0:
         //
         // This is the new process thread
         //
         iSize   = (int) GEN_STRLEN(pcProcess) + (int) GEN_STRLEN(pcParms) + 8;
         pcShell = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, iSize);
         //
         GEN_SPRINTF(pcShell, "%s %s", pcProcess, pcParms);
         PRINTF("GEN_BatchProcess(): Exec system(%s)" CRLF, pcShell);
         iCc = system(pcShell);
         PRINTF("GEN_BatchProcess(): Exec completion: cc=%d" CRLF, iCc);
         if(iCc < 0)
         {
            // error completion
            LOG_Report(errno, "GEN", "GEN_BatchProcess(): ERROR starting %s", pcShell);
            iCc = EXIT_CC_GEN_ERROR;
         }
         SAFEFREE(__PRETTY_FUNCTION__ , pcShell);
         exit(iCc);

      case -1:
         //
         // Error
         //
         tPid = -1;
         PRINTF("GEN_BatchProcess(): Error: %s" CRLF, strerror(errno));
         break;
            
      default:
         //
         // This is the parent with the thread of the child
         //
         PRINTF("GEN_BatchProcess(): Startup-pid=%d Process=%s" CRLF, tPid, pcPidPath);
         do
         {
            GEN_Sleep(1000);
            tPid = GEN_GetPidByCommand(pcPidPath);
            PRINTF("GEN-BatchProcess(): Pid=%d by name(%s) " CRLF, tPid, pcPidPath);
            if(tPid > 0)         LOG_Report(0, "GEN", "GEN_BatchProcess(): %s started.", pcPidPath);
            else if (tPid < 0)   LOG_Report(errno, "GEN", "GEN_BatchProcess(): ERROR starting %s. ", pcPidPath);
            else                 LOG_Report(0, "GEN", "GEN_BatchProcess(): Waiting for %s to start...", pcPidPath);
         }
         while((--iTimeout > 0) && (tPid == 0));
         break;
   }
   return(tPid);
}

// 
// Function:   GEN_CheckBgndTerminated
// Purpose:    Check the status of the exec thread
// 
// Parameters: Thread name, Pid
// Returns:    TRUE if Exec terminated
// 
bool GEN_CheckBgndTerminated(const char *pcName, pid_t tPid)
{
   bool     fCc=FALSE;
   int      iStatus=0;
   pid_t    tActPid;

   if(tPid)
   {
      tActPid = waitpid(tPid, &iStatus, WNOHANG);
      //
      // tActPid = 0       : Exec is active
      //           ExecPid : Exec has terminated
      //           -1      : Error
      //
      if(tActPid == tPid)
      {
         PRINTF("GEN_CheckBgndTerminated(): %s has terminated" CRLF, pcName);
         fCc = TRUE;
      }
      else if(tActPid == -1)
      {
         PRINTF("GEN_CheckBgndTerminated(): %s:ERROR=%d(%s)" CRLF, pcName, errno, strerror(errno));
         if(iStatus) CMD_SHOWPROCESSSTATUS(iStatus);
         fCc = TRUE;
      }
   }
   else fCc = TRUE;
   //
   return(fCc);   
}

// 
// Function:   GEN_CalculateCrc16
// Purpose:    Compute the CRC-16 for the data buffer
//             x^16 + x^15 + x^2 + 1
// 
// Parms:      Data pointer, number of bytes in the buffer
//
// Returns:    The CRC value.
// 
u_int16 GEN_CalculateCrc16(const u_int8 *pubBuffer, int iLen)
{
   u_int8   ubX;
   u_int16  usCrc=0xFFFF;

   while (iLen--)
   {
      ubX  = usCrc >> 8 ^ *pubBuffer++;
      ubX ^= ubX >> 4;
      usCrc = (usCrc << 8) ^ ((u_int16)(ubX << 12)) ^ ((u_int16)(ubX << 5)) ^ ((u_int16)ubX);
   }
   return(usCrc);
}

//
// Function:   GEN_GetGroupIdByName
// Purpose:    Retrieve  a Group ID by name
//
// Parms:      Username
// Returns:    GID, -1 if not-found Username
// Note:       
//
gid_t GEN_GetGroupIdByName(const char *pcName)
{
    struct group *pstGrp;

    if((pstGrp = getgrnam(pcName)) == NULL) 
    {
        return(-1);
    }
    return(pstGrp->gr_gid);
}


//
// Function:   GEN_GetPidByCommand
// Purpose:    Retrieve  a process ID by Command line name
//
// Parms:      Process name
// Returns:    tPid, -1 on error, 0 on not-found PID
// Note:       The command line name needs to be straight forward !
//
pid_t GEN_GetPidByCommand(const char *pcName)
{
   bool     fSearching=TRUE;
   pid_t    tPid=0;
   char     cLine[READ_LENZ];
   char     cName[NAME_LENZ];
   char    *pcRes;
   char    *pcDef;
   FILE    *ptFp;

   GEN_SNPRINTF(cName, NAME_LEN, "ps aux|grep \"%s\"", pcName);
   if(GEN_STRLEN(cName) >= NAME_LEN)
   {
      PRINTF("GEN-GetPidByCommand():%s too long=%d" CRLF, cName, strlen(cName));
      return(-1);
   }
   //
   if( (ptFp = popen(cName, "r")) == NULL)
   {
      // Error
      LOG_Report(errno, "GEN", "GEN-GetPidByCommand():Pipe %s open ERROR:", cName);
      PRINTF("GEN-GetPidByCommand():Pipe %s open ERROR %s" CRLF, cName, strerror(errno));
      return(-1);
   }
   //
   while(fSearching)
   {
      pcRes = fgets(cLine, READ_LEN, ptFp);
      if(pcRes)
      {
         GEN_RemoveChar(pcRes, 0x0a);
         pcDef = GEN_STRSTR(pcRes, "defunct");
         if(!pcDef)
         {
            pcDef = GEN_STRSTR(pcRes, "grep");
            if(!pcDef)
            {
               PRINTF("GEN-GetPidByCommand():--> %s" CRLF, pcRes);
               //
               // result is:
               //    "root     8420  0.0  0.2   4260  1876 pts/0    S+   19:28   0:00 /usr.local.bin/lcdproc K"
               //    "root     8421  0.0  0.2   4260  1876 pts/0    S+   19:28   0:00 grep lcdproc"
               //
               pcRes = GEN_FindDelimiter(pcRes, DELIM_SPACE);        // Skip user
               pcRes = GEN_FindDelimiter(pcRes, DELIM_NOSPACE);      // Find PID
               if(pcRes) 
               {
                  tPid = (int)strtol(pcRes, NULL, 0);                // Rework PID
                  PRINTF("GEN-GetPidByCommand():Found %s at %s" CRLF, pcName, pcRes);
               }
               fSearching = FALSE;
            }
         }
      }
      else fSearching = FALSE;
   }
   pclose(ptFp);
   return(tPid);
}

//
// Function:   GEN_GetPidByName
// Purpose:    Retrieve a process ID by name
//
// Parms:      Process name
// Returns:    tPid, -1 on error, 0 on not-found PID
// Note:       
//
pid_t GEN_GetPidByName(const char *pcName)
{
   pid_t    tPid=0;
   PSAUX    stInfo;

   if( GEN_GetThreadInfo(pcName, &stInfo) )
   {
      tPid = stInfo.tPid;
      PRINTF("GEN-GetPidByName():Found Pid=%d" CRLF, tPid);
   }
   return(tPid);
}


//
// Function:   GEN_GetUserIdByName
// Purpose:    Retrieve  a User ID by Username
//
// Parms:      Username
// Returns:    UID, -1 if not-found Username
// Note:       
//
uid_t GEN_GetUserIdByName(const char *pcUsername)
{
    struct passwd *pstPsw;

    if((pstPsw = getpwnam(pcUsername)) == NULL) 
    {
        return(-1);
    }
    return(pstPsw->pw_uid);
}

//
// Function:   GEN_GetUserPidByName
// Purpose:    Retrieve  a process ID by (Optional) User and name
//
// Parms:      User, Process name
// Returns:    tPid, -1 on error, 0 on not-found PID
// Note:       
//
pid_t GEN_GetUserPidByName(const char *pcUser, const char *pcName)
{
#ifdef DEPRICATED_VERSION
   bool     fSearching=TRUE;
   pid_t    tPid=0;
   char     cLine[READ_LENZ];
   char     cName[NAME_LENZ];
   char    *pcRes;
   char    *pcDef;
   FILE    *ptFp;

   if(pcUser)  GEN_SNPRINTF(cName, NAME_LEN, "ps aux|grep \"^%s .*%s\"", pcUser, pcName);
   else        GEN_SNPRINTF(cName, NAME_LEN, "ps aux|grep \"%s\"", pcName);

   if(GEN_STRLEN(cName) >= NAME_LEN)
   {
      //PRINTF("GEN_GetUserPidByName():%s too long=%d" CRLF, cName, strlen(cName));
      return(-1);
   }
   //
   if( (ptFp = popen(cName, "r")) == NULL)
   {
      // Error
      LOG_Report(errno, "GEN", "GEN-GetUserPidByName():Pipe %s open ERROR:", cName);
      PRINTF("GEN-GetUserPidByName():Pipe %s open ERROR %s" CRLF, cName, strerror(errno));
      return(-1);
   }
   //
   while(fSearching)
   {
      pcRes = fgets(cLine, READ_LEN, ptFp);
      if(pcRes)
      {
         GEN_RemoveChar(pcRes, 0x0a);
         pcDef = GEN_STRSTR(pcRes, "defunct");
         if(!pcDef)
         {
            pcDef = GEN_STRSTR(pcRes, "grep");
            if(!pcDef)
            {
               //PRINTF("GEN-GetUserPidByName():--> %s" CRLF, pcRes);
               //
               // result is:
               //    "root     8420  0.0  0.2   4260  1876 pts/0    S+   19:28   0:00 /usr.local.bin/lcdproc K"
               //    "root     8421  0.0  0.2   4260  1876 pts/0    S+   19:28   0:00 grep lcdproc"
               //
               pcRes = GEN_FindDelimiter(pcRes, DELIM_SPACE);        // Skip user
               pcRes = GEN_FindDelimiter(pcRes, DELIM_NOSPACE);      // Find PID
               if(pcRes) 
               {
                  tPid = (int)strtol(pcRes, NULL, 0);                // Rework PID
                  //PRINTF("GEN-GetUserPidByName():Found %s at %s" CRLF, pcName, pcRes);
               }
               fSearching = FALSE;
            }
         }
      }
      else fSearching = FALSE;
   }
   pclose(ptFp);

#else    //DEPRICATED_VERSION

   pid_t    tPid=0;
   PSAUX    stInfo;

   if( GEN_GetThreadInfo(pcName, &stInfo) )
   {
      if( GEN_STRCMP(pcUser, stInfo.cUser) == 0)
      {
         tPid = stInfo.tPid;
         PRINTF("GEN-GetUserPidByName():Found Pid=%d" CRLF, tPid);
      }
   }
   
#endif   //DEPRICATED_VERSION

   return(tPid);
}

//
//  Function:  GEN_GetPidInfo
//  Purpose:   Retrieve process info by PID
//
//  Parms:     Dest, Pid, tInfo
//  Returns:   +1 if PID found, -1 on error, 0 on not-found PID
//  Note:      Retrieve process info  using /proc/<pid>/status
//
int GEN_GetPidInfo(PIDINFO *pstPinfo, pid_t tPid, PIDST tInfo)
{
   bool     fSearching=TRUE;
   int      iCc=1;
   char     cLine[READ_LENZ];
   char     cFile[NAME_LENZ];
   char    *pcRes;
   FILE    *ptFp;

   GEN_SNPRINTF(cFile, NAME_LEN, "/proc/%d/status", tPid);
   //
   if( (ptFp = fopen(cFile, "r")) == NULL) 
   {
      PRINTF("GEN_GetPidInfo():File (%s) open ERROR:%s" CRLF, cFile, strerror(errno));
      return(0);
   }
   else if(pstPinfo)
   {
      iCc = -1;
      //
      while(fSearching)
      {
         pcRes = fgets(cLine, READ_LEN, ptFp);
         if(pcRes)
         {
            GEN_RemoveChar(pcRes, TAB);
            GEN_RemoveChar(pcRes, LF);
            GEN_TrimRight(pcRes);
            //PRINTF("GEN_GetPidInfo(%s):(%s)" CRLF, cFile, pcRes);
            switch(tInfo)
            {
               default:
                  fSearching = FALSE;
                  break;

               case PIDST_EXISTS:
                  iCc = 1;
                  fSearching = FALSE;
                  break;

               case PIDST_NAME:
                  if( (pcRes = GEN_STRSTR(pcRes, "Name:")) )
                  {
                     //
                     // result is:
                     // "Name:   xxxxxx"
                     //
                     pcRes = GEN_FindDelimiter(pcRes, DELIM_SPACE);           // Skip "Name:"
                     pcRes = GEN_FindDelimiter(pcRes, DELIM_NOSPACE);         // 
                     if(pcRes) 
                     {
                        // Copy name
                        GEN_STRNCPY(pstPinfo->cName, pcRes, NAME_LEN); 
                        PRINTF("GEN_GetPidInfo():Name=(%s)" CRLF, pstPinfo->cName);
                        iCc = 1;
                        fSearching = FALSE;
                     }
                  }
                  break;

               case PIDST_STATE:
                  if( (pcRes = GEN_STRSTR(pcRes, "State:")) )
                  {
                     //
                     // result is:
                     // "State:  R (running)"
                     //
                     pcRes = GEN_FindDelimiter(pcRes, DELIM_SPACE);       // Skip State:
                     pcRes = GEN_FindDelimiter(pcRes, DELIM_NOSPACE);     // 
                     if(pcRes) 
                     {
                        switch(*pcRes)
                        {
                           case 'S':
                              pstPinfo->tState = PIDST_IS_SLEEPING;
                              break;

                           case 'R':
                              pstPinfo->tState = PIDST_IS_RUNNING;
                              break;

                           case 'Z':
                              pstPinfo->tState = PIDST_IS_ZOMBIE;
                              break;

                           default:
                              pstPinfo->tState = PIDST_IS_OTHER;
                              break;

                        }
                        iCc = 1;
                        fSearching = FALSE;
                     }
                  }
                  break;

               case PIDST_PPID:
                  if( (pcRes = GEN_STRSTR(pcRes, "PPid:")) )
                  {
                     //
                     // result is:
                     // "PPid:   xxxxxx"
                     //
                     pcRes = GEN_FindDelimiter(pcRes, DELIM_NUMERIC);      // Skip PPid:
                     if(pcRes) 
                     {  
                        PRINTF("GEN_GetPidInfo():PPid=(%s)", pcRes);
                        pstPinfo->tPPid = (pid_t)strtol(pcRes, NULL, 0);   // Rework PID
                        iCc = 1;
                     }
                     fSearching = FALSE;
                  }
                  break;

               case PIDST_OPID:
                  if( (pcRes = GEN_STRSTR(pcRes, "NSpgid:")) )
                  {
                     //
                     // result is:
                     // "NSpgid:   xxxxxx"
                     //
                     pcRes = GEN_FindDelimiter(pcRes, DELIM_NUMERIC);      // Skip PPid:
                     if(pcRes) 
                     {  
                        PRINTF("GEN_GetPidInfo():NSpgid=(%s)", pcRes);
                        pstPinfo->tOPid = (pid_t)strtol(pcRes, NULL, 0);   // Rework PID
                        iCc = 1;
                     }
                     fSearching = FALSE;
                  }
                  break;
            }
         }
         else fSearching = FALSE;
      }
      if(!pcRes) PRINTF("GEN_GetPidInfo():Info (%d) not found" CRLF, tInfo);
   }
   fclose(ptFp);
   return(iCc);
}

//
// Function:   GEN_GetThreadInfo
// Purpose:    Retrieve all process info
//
// Parms:      Thread name, Result struct
// Returns:    TRUE if thread info found
// Note:       pstInfo->tPid is:
//             -1:Error reading thread command pipe
//              0:Thread not found
//             xx:Thread found, status is in Info block
//
bool GEN_GetThreadInfo(const char *pcName, PSAUX *pstInfo)
{
   bool     fOKee=FALSE;
   bool     fSearching=TRUE;
   int      iNr, iCc, iRead=0;
   char     cLine[READ_LENZ];
   char     cName[NAME_LENZ];
   char    *pcRes;
   FILE    *ptFp;

   GEN_SNPRINTF(cName, NAME_LEN, "ps --no-headers -o user,pid,pcpu,pmem,vsz,rss,stat,time,cmd -C %s", pcName);
   GEN_MEMSET(pstInfo, 0x00, sizeof(PSAUX));
   //
   // Preset PID Error
   //
   pstInfo->tPid = -1;
   //
   if(GEN_STRLEN(cName) < NAME_LEN)
   {
      if( (ptFp = popen(cName, "r")) == NULL)
      {
         // Error
         LOG_Report(errno, "GEN", "GEN-GetThreadInfo():Pipe %s open ERROR:", pcName);
         PRINTF("GEN-GetThreadInfo():Pipe %s open ERROR %s" CRLF, pcName, strerror(errno));
         return(FALSE);
      }
      //
      // Preset PID Not Found
      //
      pstInfo->tPid = 0;
      while(fSearching)
      {
         pcRes = fgets(cLine, READ_LEN, ptFp);
         if(pcRes)
         {
            iRead++;
            GEN_RemoveChar(pcRes, 0x0a);
            //
            // result is: (no header)
            //    USER     PID   %CPU  %MEM  VSZ   RSS   STAT  TIME        COMMAND
            //    "root    8420  0.0   0.2   4260  1876  S+    2-01:19:28  /usr.local.bin/lcdproc K"
            //
            iNr = GEN_SSCANF(pcRes, "%31s %d %f %f %d %d %31s %31s %62s %254[\n^]",
                     pstInfo->cUser,
                    &pstInfo->tPid,
                    &pstInfo->flCpu,
                    &pstInfo->flMem,
                    &pstInfo->iVsz,
                    &pstInfo->iRss,
                     pstInfo->cStat,
                     pstInfo->cTime,
                     pstInfo->cCommand,
                     pstInfo->cCmdLine);
            //
            PRINTF("GEN-GetThreadInfo():%d items:[%s]" CRLF, iNr, pcRes);
            if(iNr == 9) 
            {
               pstInfo->fTimeOKee = TRUE;
               iNr = GEN_SSCANF(pstInfo->cTime, "%d-%d:%d:%d", &pstInfo->iDays, &pstInfo->iHours, &pstInfo->iMins, &pstInfo->iSecs);
               if(iNr != 4)
               {
                  pstInfo->iDays = 0;
                  iNr = GEN_SSCANF(pstInfo->cTime, "%d:%d:%d", &pstInfo->iHours, &pstInfo->iMins, &pstInfo->iSecs);
                  if(iNr != 3)
                  {
                     pstInfo->iHours = 0;
                     iNr = GEN_SSCANF(pstInfo->cTime, "%d:%d", &pstInfo->iMins, &pstInfo->iSecs);
                     if(iNr != 2)
                     {
                        pstInfo->iMins = 0;
                        pstInfo->iSecs = 0;
                        PRINTF("GEN-GetThreadInfo():Info OKee, time not found (%d items)" CRLF, pstInfo->cTime, iNr);
                        pstInfo->fTimeOKee = FALSE;
                     }
                  }
               }
               fOKee      = TRUE;
               fSearching = FALSE;
            }
            else
            {
               PRINTF("GEN-GetThreadInfo():Bad info format in %s (%d items)" CRLF, pcRes, iNr);
            }
         }
         else fSearching = FALSE;
      }
      iCc = pclose(ptFp);
      if(iCc == -1) 
      {
         LOG_Report(0, "GEN", "GEN-GetThreadInfo():%s close error, Read=%d: [%s]", pcName, iRead, strerror(errno));
         PRINTF("GEN-GetThreadInfo():%s close error, Read=%d: [%s]" CRLF, pcName, iRead, strerror(errno));
         GEN_ReportProcessStatus(iCc);
      }
   }
   else
   {
      // Error
      PRINTF("GEN-GetThreadInfo():Name ERROR" CRLF);
   }
   return(fOKee);
}

// 
// Function:   GEN_KillProcess
// Purpose:    Kill an ongoing process (if any)
// 
// Parameters: Pid, friendly YESNO, wait-completion timeout in mSecs
// Returns:     0 PID killed
//             +1 PID Still running
//             -1 PID Zombie or error
// Note:       Timeout granularity is 100 mSecs !
// 
int GEN_KillProcess(pid_t tPid, bool fFriendly, int iWaitMsecs)
{
   int      iCc=0;
   char     cShell[CMD_LENZ];

   if(tPid > 0)
   {
      //
      // Ongoing action: terminate friendly or forced
      //
      if(fFriendly)
      {
         GEN_SignalPid(tPid, SIGINT);
         //
         //LOG_Report(0, "GEN", "GEN_KillProcess(): Friendly ask pid=%d to exit", tPid);
         PRINTF("GEN_KillProcess(): Friendly ask pid=%d to exit" CRLF, tPid);
      }
      else
      {
         //LOG_Report(0, "GEN", "GEN_KillProcess(): Terminate thread %d", tPid);
         PRINTF("GEN_KillProcess(): Force pid=%d to exit" CRLF, tPid);
         GEN_SNPRINTF(cShell, CMD_LEN, "kill -kill %d", (int)tPid);
         iCc  = system(cShell);
      }
      iCc = GEN_WaitProcessTimeout(tPid, iWaitMsecs);
   }
   return(iCc);
}

// 
// Function:   GEN_RunBatch
// Purpose:    Run a BASH command
// 
// Parms:      Command name, parms
// Returns:    See below
// Not:        The system() library function uses fork(2) to create a child process
//             that executes the shell command specified in command using execl(3)
//             as follows: execl("/bin/sh", "sh", "-c", command, (char *) NULL);
//            system() returns after the command has been completed.
//     
//            During execution of the command, SIGCHLD will be blocked, and SIGINT
//            and SIGQUIT will be ignored, in the process that calls system().
//            (These signals will be handled according to their defaults inside the
//            child process that executes command.)
//     
//            The return value of system() is one of the following:
//            *  If command is NULL, then a nonzero value if a shell is available,
//               or 0 if no shell is available.
//     
//            *  If a child process could not be created, or its status could not
//               be retrieved, the return value is -1 and errno is set to indicate
//               the error.
//     
//            *  If a shell could not be executed in the child process, then the
//               return value is as though the child shell terminated by calling
//               _exit(2) with the status 127.
//     
//            *  If all system calls succeed, then the return value is the
//               termination status of the child shell used to execute command.
//               (The termination status of a shell is the termination status of
//               the last command it executes.)
//     
//            In the last two cases, the return value is a "wait status" that can
//            be examined using the macros described in waitpid(2).  (i.e.,
//            WIFEXITED(), WEXITSTATUS(), and so on).
// 
int GEN_RunBatch(const char *pcCmd, const char *pcArgs)
{
   int      iCc, iSize;
   char    *pcShell;

   iSize   = (int) GEN_STRLEN(pcCmd) + (int) GEN_STRLEN(pcArgs) + 8;
   pcShell = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, iSize);
   //
   GEN_SPRINTF(pcShell, "%s %s", pcCmd, pcArgs);
   PRINTF("GEN_RunBatch(): Exec system(%s)" CRLF, pcShell);
   iCc = system(pcShell);
   PRINTF("GEN_RunBatch(): Exec completion: cc=%d" CRLF, iCc);
   if(iCc < 0)
   {
      // error completion
      LOG_Report(errno, "GEN", "GEN_RunBatch(): ERROR starting %s", pcShell);
      GEN_ReportProcessStatus(iCc);
   }
   else iCc = WEXITSTATUS(iCc);
   SAFEFREE(__PRETTY_FUNCTION__ , pcShell);
   return(iCc);
}

// 
// Function:   GEN_Printf
// Purpose:    Printf abstraction
// 
// Parameters: 
// Returns:    Nr written, -1 on error
// 
int GEN_Printf(const char *pcFormat, ...)
{
   va_list  tArg;
   int      iNr;

   va_start(tArg, pcFormat);
   iNr = GEN_VPRINTF(pcFormat, tArg);
   va_end(tArg);
   if( GEN_FFLUSH(stdout) == EOF) iNr = -1;
   return(iNr);
}

//
//  Function:  GEN_HexPrintf
//  Purpose:   Put out a single char as hex ('A'-->0x41)
//             
//  Parms:     Buffer, PrintAsciiToo
//  Returns:   
//
//
void GEN_HexPrintf(char *pcBuffer, bool fAsciiToo)
{
   GEN_Printf("%02X ", *pcBuffer);
   //
   if(fAsciiToo)
   {
      if( (*pcBuffer==0x0d) || (*pcBuffer==0x0a)) GEN_Printf(".");
      else                                        GEN_Printf("%c", *pcBuffer);
   }
}

//
// Function:   GEN_ShowProcessStatus
// Purpose:    Show process status
//             
// Parms:      Status
// Returns:    
// Note:       
//
void GEN_ShowProcessStatus(int iStatus)
{
   if(WIFEXITED(iStatus))     LOG_printf("cmd_ShowProcessStatus():the process terminated normally (%d)" CRLF, WEXITSTATUS(iStatus));
   if(WIFSIGNALED(iStatus)) 
   {
                              LOG_printf("cmd_ShowProcessStatus():the process was terminated by signal %d" CRLF, WTERMSIG(iStatus));
      if(WCOREDUMP(iStatus))  LOG_printf("cmd_ShowProcessStatus():the process did a core dump" CRLF);
   }
   if(WIFSTOPPED(iStatus))    LOG_printf("cmd_ShowProcessStatus():the process was stopped by delivery of signal %d" CRLF, WSTOPSIG(iStatus) );
   if(WIFCONTINUED(iStatus))  LOG_printf("cmd_ShowProcessStatus():the process was resumed by delivery of SIGCONT" CRLF);
}

//
// Function:   GEN_ReportProcessStatus
// Purpose:    Report process status
//             
// Parms:      Status
// Returns:    
// Note:       
//
void GEN_ReportProcessStatus(int iStatus)
{
   if(WIFEXITED(iStatus))     LOG_Report(0, "GEN", "GEN-ReportProcessStatus():the process terminated normally (%d)", WEXITSTATUS(iStatus));
   if(WIFSIGNALED(iStatus)) 
   {
                              LOG_Report(0, "GEN", "GEN-ReportProcessStatus():the process was terminated by signal %d", WTERMSIG(iStatus));
      if(WCOREDUMP(iStatus))  LOG_Report(0, "GEN", "GEN-ReportProcessStatus():the process did a core dump");
   }
   if(WIFSTOPPED(iStatus))    LOG_Report(0, "GEN", "GEN-ReportProcessStatus():the process was stopped by delivery of signal %d", WSTOPSIG(iStatus) );
   if(WIFCONTINUED(iStatus))  LOG_Report(0, "GEN", "GEN-ReportProcessStatus():the process was resumed by delivery of SIGCONT");
}

// 
// Function:   GEN_Timeprintf
// Purpose:    Printf abstraction with time stamp
// 
// Parameters: 
// Returns:    
// 
int GEN_Timeprintf(char *pcFormat, ...)
{
  va_list      tArgs;
  struct tm   *pstTime;
  time_t       tSec;
  int          iRet;

  tSec = (time_t) RTC_GetSystemSecs();
  pstTime = localtime(&tSec);

  GEN_PRINTF("%02d:%02d:%02d %5d| ",
       pstTime->tm_hour,
       pstTime->tm_min,
       pstTime->tm_sec,
       getpid());

  va_start(tArgs, pcFormat);
  iRet = GEN_VPRINTF(pcFormat, tArgs);
  va_end(tArgs);
  return(iRet);
}

// 
// Function:   GEN_WaitProcess
// Purpose:    Wait for an ongoing process to finish
// 
// Parameters: pid
// Returns:    
// 
void GEN_WaitProcess(pid_t tPid)
{
   pid_t    tPidAct;
   int      iStatus;

   if(tPid)
   {
      //
      // Wait till thread has exited
      //
      while( ((tPidAct = waitpid(tPid, &iStatus, 0)) == -1) && (errno == EINTR) )
        continue;
      //
      PRINTF("GEN_WaitProcess(): Thread (pid=%d, act=%d) finished" CRLF, tPid, tPidAct);
   }
}

// 
// Function:   GEN_WaitProcessTimeout
// Purpose:    Wait for an ongoing process to finish
// 
// Parameters: pid, timeout in mSecs
// Returns:     0 PID not found
//             +1 PID Still running
//             -1 PID Zombie or error
// Note:       Timeout granularity is 100 mSecs !
// 
int GEN_WaitProcessTimeout(pid_t tPid, int iWaitMsecs)
{
   int      iStatus=0;
   bool     fChecking=TRUE;
   PIDINFO  stInfo;

   if(tPid > 0)
   {
      while(fChecking)
      {
         switch(GEN_GetPidInfo(&stInfo, tPid, PIDST_STATE))
         {
            default:
            case 0:
               // PID not found
               iStatus   = 0;
               fChecking = FALSE;
               break;

            case 1:
               switch(stInfo.tState)
               {
                  default:
                  case PIDST_IS_SLEEPING:
                  case PIDST_IS_RUNNING:
                     iStatus = 1;
                     if(iWaitMsecs > 0)
                     {
                        GEN_Sleep(100);
                        iWaitMsecs -= 100;
                        if(iWaitMsecs <= 0) fChecking = FALSE;
                     }
                     else fChecking = FALSE;
                     break;

                  case PIDST_IS_OTHER:
                  case PIDST_IS_ZOMBIE:
                     iStatus   = -1;
                     fChecking = FALSE;
                     break;
               }
               break;
         }
      }
   }
   return(iStatus);
}

//
// Function:   GEN_WaitSemaphore
// Purpose:    Wait till Semaphore has been unlocked, then LOCK and proceed
//
// Parms:      Semaphore ptr, Timeout (or -1 if no timeout)
// Returns:     0 if sem unlocked
//             +1 if sem timeout
//             -1 on error (errno)
// Note:       sem_wait() decrements (locks) the semaphore pointed to by sem. 
//             If the semaphore's value is greater than zero, then the decrement proceeds, 
//             and the function returns, immediately. If the semaphore currently has the 
//             value zero, then the call blocks until either it becomes possible to perform
//             the decrement (i.e., the semaphore value rises above zero), 
//             or a signal handler interrupts the call. 
//
//             sem_timedwait() is the same as sem_wait(), except that abs_timeout specifies a 
//             limit on the amount of time that the call should block if the decrement cannot 
//             be immediately performed. The abs_timeout argument points to a structure that
//             specifies an absolute timeout in seconds and nanoseconds since the Epoch, 
//             1970-01-01 00:00:00 +0000 (UTC). This structure is defined as follows:
//
//             struct timespec 
//             {
//                time_t tv_sec;      /* Seconds */
//                long   tv_nsec;     /* Nanoseconds [0 .. 999999999] */
//             };
//             If the timeout has already expired by the time of the call, and the semaphore 
//             could not be locked immediately, then sem_timedwait() fails with a timeout error 
//             (errno set to ETIMEDOUT). If the operation can be performed immediately, then 
//             sem_timedwait() never fails with a timeout error, regardless of the value of 
//             abs_timeout. Furthermore, the validity of abs_timeout is not checked in this case.
//
//
int GEN_WaitSemaphore(sem_t *ptSem, int iMsecs)
{
   int               iCc=0;
   bool              fWait=TRUE;
   long              lTime;
   struct timespec   stTime;

   if(iMsecs > 0)
   {
      clock_gettime(CLOCK_REALTIME, &stTime);
      stTime.tv_sec += iMsecs / 1000;
      lTime          = stTime.tv_nsec;
      lTime         += ONEMILLION * (long) (iMsecs % 1000);
      //
      // Check secs overflow
      //
      if(lTime >= ONEBILLION) 
      {
         lTime -= ONEBILLION;
         stTime.tv_sec++;
      }
      stTime.tv_nsec = lTime;
      //PRINTF("GEN_WaitSemaphore():CLOCK time=%ld - %ld" CRLF, stTime.tv_sec, stTime.tv_nsec);
      do
      {
         iCc = sem_timedwait(ptSem, &stTime);
         if(iCc == -1)
         {
            switch(errno)
            {
               case EINTR:
                  // Interrupted: continue waiting 
                  break;

               case ETIMEDOUT: 
                  fWait = FALSE;
                  iCc   = 1;
                  break;

               default:
               case EINVAL:
                  fWait = FALSE;
                  //PRINTF("sup_WaitSemaphore():INVALID time=%ld - %ld" CRLF, stTime.tv_sec, stTime.tv_nsec);
                  break;
            }
         }
         else fWait = FALSE;
      }
      while(fWait);
   }
   else
   {
      //
      // if    sem == 0 : wait
      // else  sem-- (LOCK)   
      //
      while( ((iCc = sem_wait(ptSem)) < 0) && (errno == EINTR) ) continue;
   }
   //
   // iCc= 0: Normal completion: Sem=UNLOCKED
   // iCc= 1: Timeout:           Sem=LOCKED
   // iCc=-1: Error:             Sem=LOCKED
   //
   return(iCc);
}

// 
// Function:   GEN_Sleep
// Purpose:    Sleep for x msecs
// 
// Parameters: msecs
// Returns:    Number of interruptions
// 
int GEN_Sleep(int iMsecs)
{
   int   iCc=0;

   if(iMsecs)
   {
      struct timespec stWait, stRem;
   
      stWait.tv_sec = iMsecs / 1000;
      stWait.tv_nsec = (iMsecs % 1000) * 1000000L;
      //
      while( (nanosleep(&stWait, &stRem) == -1) && (errno == EINTR) ) 
      {
         stWait = stRem;
         iCc++;
         continue; 
      }
   }
   return(iCc);
}

// 
// Function:   GEN_SignalPid
// Purpose:    Signal a PID
// 
// Parameters: Pid, signal
// Returns:    
// Note:       
// 
void GEN_SignalPid(pid_t tPid, int iSignal)
{
   if(tPid > 0) kill(tPid, iSignal);
}


/* ======   Local Functions separator ===========================================
void ___GLOBAL_FUNCTIONS(){}
==============================================================================*/

// 
// Function:   GEN_CheckBadCharacters
// Purpose:    Check if strig contains bad characters
// 
// Parameters: String, Bad Chars
// Returns:    Pointer to first bad char or NULL if all OKee
// 
const char *GEN_CheckBadCharacters(char *pcSrc, const char *pcBad)
{
   int         i;
   const char *pcBadOne=NULL;

   for (i=0; i<GEN_STRLEN(pcBad); i++) 
   {
      if (GEN_STRCHR(pcSrc, pcBad[i]) != NULL) 
      {
         pcBadOne = &pcBad[i];
         break;
      }
   }
   return(pcBadOne);
}  

// 
// Function:   GEN_CopyToDelimiter
// Purpose:    This function copies until the required delimiter
// 
// Parameters: Delimiter, Dest, Src, max size
// Returns:    String Len if copied data
// 
int GEN_CopyToDelimiter(char cDelimiter, char *pcDest, char *pcSrc, int iMaxLen)
{
  bool   fCopy;
  int    iSize=0;

  if(pcSrc == NULL) fCopy = FALSE;
  else              fCopy = TRUE;

  while(fCopy == TRUE)
  {
     if(iSize < iMaxLen)
     {
        if( (*pcSrc == '\0') || (*pcSrc == cDelimiter) ) fCopy = FALSE;
        else
        {
           *pcDest++ = *pcSrc++;
           iSize++;
        }
     }
     else fCopy = FALSE;
  }
  *pcDest = '\0';
  return(iSize);
}

// 
// Function:   GEN_FindDelimiter
// Purpose:    This function looks for the required delimiter
// 
// Parameters: Record, Delimiter
// Returns:    Buffer pointer on delimiter or NULL if not found
// 
char *GEN_FindDelimiter(char *pcRecord, DELIM tDelimiter)
{
  if(pcRecord == NULL) return(NULL);

  switch(tDelimiter)
  {
     case DELIM_NOSPACE:
        // Skip to next non-space char
        while(*pcRecord && *pcRecord == ' ') pcRecord++;
        break;

     case DELIM_SPACE:
        // Skip to next space char
        while(*pcRecord && *pcRecord != ' ') pcRecord++;
        break;

     case DELIM_COLON:
        // Skip to next : char
        while(*pcRecord && *pcRecord != ':') pcRecord++;
        break;

     case DELIM_NUMERIC:
        // Skip to next 0-9 char
        while(*pcRecord && (*pcRecord < '0' || *pcRecord > '9')) pcRecord++;
        break;

     case DELIM_INTEGER:
        // Skip to next 0-9 char
        while(*pcRecord && (*pcRecord != '+') && (*pcRecord != '-') && (*pcRecord < '0' || *pcRecord > '9')) pcRecord++;
        break;

     case DELIM_COMMA:
        // Skip to next , char
        while(*pcRecord && *pcRecord != ',') pcRecord++;
        break;

     case DELIM_PERIOD:
        // Skip to next . char
        while(*pcRecord && *pcRecord != '.') pcRecord++;
        break;

     case DELIM_NEXTFIELD:
        // Skip to the beginning of the next word
        while(*pcRecord && *pcRecord != ' ') pcRecord++;
        while(*pcRecord && *pcRecord == ' ') pcRecord++;
        break;

     case DELIM_ASTERIX:
        // Skip to next * char
        while(*pcRecord && *pcRecord != '*') pcRecord++;
        break;

     case DELIM_AMPAND:
        // Skip to next & char
        while(*pcRecord && *pcRecord != '&') pcRecord++;
        break;

     case DELIM_DQUOTE:
        // Skip to next " char
        while(*pcRecord && *pcRecord != '\"') pcRecord++;
        break;

     case DELIM_QUOTE:
        // Skip to next ' char
        while(*pcRecord && *pcRecord != '\'') pcRecord++;
        break;

     case DELIM_EQU:
        // Skip to next = char
        while(*pcRecord && *pcRecord != '=') pcRecord++;
        break;

     case DELIM_STATIC:
        // Skip to next @ char
        while(*pcRecord && *pcRecord != '@') pcRecord++;
        break;

     case DELIM_CR:
        // Skip to next \r char
        while(*pcRecord && *pcRecord != '\r') pcRecord++;
        break;

     case DELIM_LF:
        // Skip to next \n char
        while(*pcRecord && *pcRecord != '\n') pcRecord++;
        break;

     case DELIM_TAB:
        // Skip to next \t char
        while(*pcRecord && *pcRecord != '\t') pcRecord++;
        break;

     case DELIM_SQRB:
        // Skip to next [ char
        while(*pcRecord && *pcRecord != '[') pcRecord++;
        break;

     case DELIM_SQRE:
        // Skip to next ] char
        while(*pcRecord && *pcRecord != ']') pcRecord++;
        break;

     case DELIM_CURB:
        // Skip to next { char
        while(*pcRecord && *pcRecord != '{') pcRecord++;
        break;

     case DELIM_CURE:
        // Skip to next } char
        while(*pcRecord && *pcRecord != '}') pcRecord++;
        break;

     default:
        pcRecord = NULL;
        break;
  }
  //
  // return NULL if no delimiter found
  //
  if(pcRecord)
  {
     if(*pcRecord =='\0') pcRecord = NULL;
  }
  return(pcRecord);
}

// 
// Function:   GEN_FindDelimiters
// Purpose:    Find parameter value delimiter
// 
// Parameters: Parameter, delimiter(s)
// Returns:    Value ptr
// Note:          pcParm  -> "Filter 20130723"
//                returns -> "20130723"
//  
char *GEN_FindDelimiters(char *pcParm, char *pcDelim)
{
   int   x, iNumDelims=GEN_STRLEN(pcDelim);

   while(*pcParm)
   {
      for(x=0; x<iNumDelims; x++)
      {
         if(*pcParm == pcDelim[x])
         {
            return(++pcParm);
         }
      }
      pcParm++;
   }
   return(NULL);
}

// 
// Function:   GEN_SizeToDelimiters
// Purpose:    Determine the nr of chars until the required delimiter
// 
// Parameters: String, delimiter(s)
// Returns:    Size of output
// Note:       Delimiter '\0' is always a valid delimiter for size
// 
int GEN_SizeToDelimiters(char *pcSrc, char *pcDelims)
{
   int   iSize=0;
   char *pcEnd;

   pcEnd = GEN_FindDelimiters(pcSrc, pcDelims);
   if(pcEnd)
   {
      iSize = pcEnd - pcSrc - 1;
   }
   else
   {
      //
      // No delimiter found: take string length
      //
      iSize = GEN_STRLEN(pcSrc);
   }
   return(iSize);
}

// 
// Function:   GEN_GetPublicKey
// Purpose:    
// 
// Parameters: Buffer, size
// Returns:    void
// 
void GEN_GetPublicKey(u_int8 *pubKey, int iSize)
{
  int   i, j;

  for(i=0; i<iSize; i++)
  {
     j = (((i+3)<<3)+7) & 0x3f;
     *pubKey++ = cMiscFunctions[j];
  }
}

//
//  Function:   GEN_Remalloc
//  Purpose:    Re-allocated memory
//
//  Parms:      Buffer, size
//  Returns:    New Buffer
//
char *GEN_Remalloc(char *pcOld, int iNewSize)
{
   char *pcNew;

   iNewSize += BUF_SPARE;
   pcNew = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, iNewSize);
   if(pcOld)
   {
      GEN_MEMCPY(pcNew, pcOld, iNewSize);
      SAFEFREE(__PRETTY_FUNCTION__ , pcOld);
   }
   return(pcNew);
}

// 
// Function:   GEN_RemoveChar
// Purpose:    This function removes all chars from the record
// 
// Parameters: Buffer, char to remove (and replace into space)
// Returns:    Number of removed chars
// 
int GEN_RemoveChar(char *pcDest, char cRemove)
{
   int iNr = 0;

   if(pcDest)
   {
      while(*pcDest)
      {
         if(*pcDest == cRemove)
         {
            *pcDest = ' ';
            iNr++;
         }
         pcDest++;
      }
   }
   return(iNr);
}

// 
// Function:   GEN_RemovePair
// Purpose:    Remove leading and tailing pair of chars
// 
// Parameters: Text, pair(s)
// Returns:    TRUE if a pair has been removed
// Note:          pcText  -> "{aap noot mies}"
//                pcPairs -> "{}[]<>||"  etc
//                returns -> "aap noot mies"
//  
bool GEN_RemovePair(char *pcText, const char *pcPairs)
{
   bool  fCc=FALSE;
   int   i=0, x, iLen, iPairs;
   char  cPairL, cPairR;

   iLen     = GEN_STRLEN(pcText) - 1;
   iPairs   = GEN_STRLEN(pcPairs);
   cPairL   = pcText[0];
   cPairR   = pcText[iLen];

   while(i < iPairs)
   {
      if( (cPairL == pcPairs[i]) && (cPairR == pcPairs[i+1]) )
      {
         // Matching pairs: remove
         pcText[iLen] = '\0';
         for(x=0; x<iLen; x++) pcText[x] = pcText[x+1];
         fCc = TRUE;
         break;
      }
      i += 2;
   }
   return(fCc);
}

// 
// Function:   GEN_ReplaceChar
// Purpose:    This function replaces all chars from the record
// 
// Parameters: Buffer, char to replace, replace char
// Returns:    Number of removed chars
// 
int GEN_ReplaceChar(char *pcDest, char cRemove, char cReplace)
{
   int   iNr = 0;

   if(pcDest)
   {
      while(*pcDest)
      {
         if(*pcDest == cRemove)
         {
            *pcDest = cReplace;
            iNr++;
         }
         pcDest++;
      }
   }
   return(iNr);
}

// 
//  Name:         GEN_SimpleWildcards
//  Description:  Perform simple wildcard filter on a string
//  Arguments:    String, filter, flags
// 
//  Returns:      TRUE if filter matches
// 
bool GEN_SimpleWildcards(char *pcStr, char *pcWcs, int iFlags)
{
   bool  fOKee=FALSE;
   bool  fChck=TRUE;
   bool  fAstx=FALSE;
   char  cStr, cWcs;
   char *pcWc2=NULL;

   if(*pcStr == '\0') return(FALSE);
   //
   if     (iFlags & WCF_DIR)  pcStr = gen_FindDir(pcStr, 2);
   else if(iFlags & WCF_FILE) pcStr = gen_FindDir(pcStr, 1);
   //LOG_printf("GEN-SimpleWildcards():Filter[%s]:[%s]" CRLF, pcWcs, pcStr);
   //
   while(fChck)
   {
      cWcs = gen_GetChar(pcWcs, iFlags);
      switch(cWcs)
      {
         case '\0':
            fChck = FALSE;
            break;

         case '?':
            fAstx = FALSE;
            pcStr++;
            pcWcs++;
            break;

         case '*':
            fAstx = TRUE;
            pcWcs++;
            pcWc2 = pcWcs;
            break;

         default:
            cStr = gen_GetChar(pcStr, iFlags);
            //LOG_printf("GEN-SimpleWildcards(%d-%d-%d):[ %c == %c ]", fAstx, fChck, fOKee, cStr, cWcs);
            if(cStr == cWcs)
            {
               fOKee = TRUE;
               pcWcs++;
               pcStr++;
               //LOG_printf("  Yes --> (%d-%d-%d)" CRLF, fAstx, fChck, fOKee);
            }
            else
            {
               if(fAstx) pcWcs = pcWc2;
               fOKee = FALSE;
               pcStr++;
               //LOG_printf("  Noo --> (%d-%d-%d)" CRLF, fAstx, fChck, fOKee);
            }
            break;
      }
      if(*pcStr == '\0') fChck = FALSE;
   }
   //if(fOKee) LOG_printf(" Match" CRLF);
   //else      LOG_printf(" No Match" CRLF);
   return(fOKee);
}

// 
//  Name:         GEN_TrimAll
//  Description:  This function trims leading and trailing spaces from the record
//  Arguments:    Buffer
// 
//  Returns:      Trimmed buffer ^
// 
char *GEN_TrimAll(char *pcBuffer)
{
   char *pcTemp;

   pcTemp = GEN_TrimLeft(pcBuffer);
   return(GEN_TrimRight(pcTemp));
}

// 
//  Name:         GEN_TrimLeft
//  Description:  This function trims leading spaces from the record
//  Arguments:    Buffer
// 
//  Returns:      Trimmed buffer ^
// 
char *GEN_TrimLeft(char *pcBuffer)
{
   bool  fChecking=TRUE;

   if(pcBuffer)
   {
      while(fChecking)
      {
         switch(*pcBuffer)
         {
            case ' ':   
               pcBuffer++;
               break;
   
            default:
               fChecking = FALSE;
               break;
         }
      }
   }
   return(pcBuffer);
}

// 
//  Name:         GEN_TrimRight
//  Description:  This function trims trailing spaces from the record
//  Arguments:    Buffer
// 
//  Returns:      Trimmed buffer ^
// 
char *GEN_TrimRight(char *pcBuffer)
{
   int   x;

   if(pcBuffer)
   {
      x = GEN_STRLEN(pcBuffer);
      while(x--)
      {
         switch(pcBuffer[x])
         {
            default :
               return(pcBuffer);

            case ' ':   
               pcBuffer[x] = 0;
               break;
         }
      }
   }
   return(pcBuffer);
}


//
// Function:   GEN_DisplayData
// Purpose:    Display data hex and ascii
//
// Parms:      Filepath, Title, Buffer, size
// Returns:   
// Note:       iAllZero =
//             0: Init
//             1: Non-zero item in buffer
//             2: Zero buffer has been displayed
//
#define GEN_DUMP_COLS            32
#define GEN_DUMP_COLS_LAST       GEN_DUMP_COLS - 1
#define GEN_DUMP_TEXT          ((GEN_DUMP_COLS * 3) + 4)
#define GEN_DUMP_SIZE_HEX      ((GEN_DUMP_COLS * 4) + 16)
#define GEN_DUMP_SIZE_ASC      ((GEN_DUMP_COLS * 1) + 16)
//
bool GEN_DisplayData(const char *pcPath, const char *pcTitle, char *pcData, int iLength)
{
   int      iFd=STDOUT_FILENO;
   int      iIs00ff=0;
   int      iAllZero=0;
   int      iAllOnes=0;
   int      x=0, y=0, i, iNr=0;
   char     cChar;
   char    *pcBuffer;
   char    *pcAscii;

   if(pcPath)
   {
      iFd = safeopen(pcPath, O_RDWR|O_CREAT|O_TRUNC);
      if(iFd < 0) return(FALSE);
   }
   GEN_DPRINTF(iFd, "%s" CRLF, pcTitle);
   //
   // Alloc and clear
   //
   pcBuffer = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, GEN_DUMP_SIZE_HEX);
   pcAscii  = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, GEN_DUMP_SIZE_ASC);
   //
   GEN_MEMSET(pcBuffer, 0x20, GEN_DUMP_COLS * 3);
   GEN_MEMSET(pcAscii,  0x20, GEN_DUMP_COLS * 1);
   //
   for(i=0; i<iLength; i++)
   {
      cChar = pcData[i];
      if(cChar != 0x00) iAllZero = 1;
      if(cChar != 0xFF) iAllOnes = 1;
      GEN_SNPRINTF(&pcBuffer[x], 4, "%02X", cChar);
      pcBuffer[x+2] = ' ';
      //
      // Print ASCII 
      //
      if(isprint((int)cChar)) pcAscii[y] = cChar;
      else                    pcAscii[y] = '.';
      //
      if(i % GEN_DUMP_COLS == GEN_DUMP_COLS_LAST) 
      {
         // Last char in this row
         iNr = (i / GEN_DUMP_COLS) * GEN_DUMP_COLS;
         switch(iAllZero)
         {
            case 0:
               // This row has all 0x00
               GEN_DPRINTF(iFd, "%06x 00 -->" CRLF, iNr);
               iAllZero = 2;
               break;

            default:
            case 1:
               // This row has data NOT 0x00
               //pwjh GEN_DPRINTF(iFd, "%06x %s  (%s)" CRLF, iNr, pcBuffer, pcAscii);
               iIs00ff |= 0x01;
               iAllZero = 0;
               break;

            case 2:
               // Previous content was all 0x00: summary is enough for now
               break;
         }
         switch(iAllOnes)
         {
            case 0:
               // This row has all 0xFF
               GEN_DPRINTF(iFd, "%06x FF -->" CRLF, iNr);
               iAllOnes = 2;
               break;

            default:
            case 1:
               // This row has data NOT 0xFF
               //pwjh GEN_DPRINTF(iFd, "%06x %s  (%s)" CRLF, iNr, pcBuffer, pcAscii);
               iIs00ff |= 0x02;
               iAllOnes = 0;
               break;

            case 2:
               // Previous content was all 0xFF: summary is enough for now
               break;
         }
         if(iIs00ff == 0x03)
         {
            // Line has data other than only 0x00 or 0xff
            GEN_DPRINTF(iFd, "%06x %s  (%s)" CRLF, iNr, pcBuffer, pcAscii);
         }
         // Restart new row
         iIs00ff = 0;
         GEN_MEMSET(pcBuffer, 0x20, GEN_DUMP_COLS * 3);
         GEN_MEMSET(pcAscii,  0x20, GEN_DUMP_COLS * 1);
         x = 0;
         y = 0;
      }
      else 
      {
         x += 3;
         y += 1;
      }
   }
   if(x) 
   {
      GEN_DPRINTF(iFd, "%06x %s  (%s)" CRLF, iNr, pcBuffer, pcAscii);
   }
   SAFEFREEX(__PRETTY_FUNCTION__, pcAscii);
   SAFEFREEX(__PRETTY_FUNCTION__, pcBuffer);
   //
   if(iFd > STDOUT_FILENO) safeclose(iFd);
   return(TRUE);
}

/**
 **  Name:         GEN_DumpData
 **  Description:  Dump a block of data in lines/cols to STDOUT
 **  Arguments:    pcHeader, Nr of Columns, pubData, iSize
 **
 **  Returns:      void
 **/
void GEN_DumpData(char *pcHeader, int iCols, u_int8 *pubBuffer, int iLength)
{
   gen_ListData(STDOUT_FILENO, pcHeader, FALSE, iCols, pubBuffer, iLength);
}

/**
 **  Name:         GEN_ListAddrData
 **  Description:  List a block of address and data in lines/cols to file
 **  Arguments:    Filepath, pcHeader, Nr of Columns, pubData, iSize
 **
 **  Returns:      void
 **/
bool GEN_ListAddrData(const char *pcPath, const char *pcHeader, int iCols, u_int8 *pubBuffer, int iLength)
{
   bool  fCc=FALSE;
   int   iFd;

   if(pcPath)
   {
      iFd = safeopen(pcPath, O_RDWR|O_CREAT|O_APPEND);
      if(iFd > 0)
      {
         gen_ListData(iFd, pcHeader, TRUE, iCols, pubBuffer, iLength);
         safeclose(iFd);
         fCc = TRUE;
      }
   }
   return(fCc);
}

/**
 **  Name:         GEN_ListData
 **  Description:  List a block of data in lines/cols to file
 **  Arguments:    Filepath, pcHeader, Nr of Columns, pubData, iSize
 **
 **  Returns:      void
 **/
bool GEN_ListData(const char *pcPath, const char *pcHeader, int iCols, u_int8 *pubBuffer, int iLength)
{
   bool  fCc=FALSE;
   int   iFd;

   if(pcPath)
   {
      iFd = safeopen(pcPath, O_RDWR|O_CREAT|O_APPEND);
      if(iFd > 0)
      {
         gen_ListData(iFd, pcHeader, FALSE, iCols, pubBuffer, iLength);
         safeclose(iFd);
         fCc = TRUE;
      }
   }
   return(fCc);
}

// 
// Function:   GEN_UpperCase
// Purpose:    This function converts all chars from the record to uppercase
// 
// Parameters: Buffer
// Returns:    
// 
void GEN_UpperCase(char *pcRecord)
{
   if(pcRecord)
   {
      while(*pcRecord)
      {
         *pcRecord = toupper(*pcRecord);
         pcRecord++;
      }
   }
}

// 
// Function:   GEN_LowerCase
// Purpose:    This function converts all chars from the record to lowercase
// 
// Parameters: Buffer
// Returns:    
// 
void GEN_LowerCase(char *pcRecord)
{
   if(pcRecord)
   {
      while(*pcRecord)
      {
         *pcRecord = tolower(*pcRecord);
         pcRecord++;
      }
   }
}

/**
 **  Name:         GEN_ubyte_get
 **
 **  Description:  Gets a ubyte from a byte array
 **
 **  Arguments:    array ^, mask, shift
 **
 **  Returns:      u_int8
 **/
u_int8 GEN_ubyte_get(u_int8 *pubSrc, u_int8 ubMask, u_int8 ubShift)
{
   u_int8 ubTemp;

   ubTemp   = *pubSrc;
   ubTemp >>= ubShift;
   ubTemp  &= ubMask;

   return (ubTemp);
}

/**
 **  Name:         GEN_ulong_get
 **
 **  Description:  Gets a ulong from a byte array
 **
 **  Arguments:    array ^, mask, shift
 **
 **  Returns:      u_int32
 **/
u_int32 GEN_ulong_get(u_int8 *pubSrc, u_int32 ulMask, u_int8 ubShift)
{
   u_int32 ulTemp;

   ulTemp = ((u_int32)*pubSrc       << 24) +
            ((u_int32)*(pubSrc + 1) << 16) +
            ((u_int32)*(pubSrc + 2) <<  8) +
             (u_int32)*(pubSrc + 3);
   ulTemp >>= ubShift;
   ulTemp &= ulMask;

   return (ulTemp);
}

/**
 **  Name:         GEN_ulong_put
 **
 **  Description:  Puts a ulong into a byte array
 **
 **  Arguments:    array ^, value
 **
 **  Returns:      Updated array ptr.
 **/

u_int8 *GEN_ulong_put(u_int8 *ubPtr, u_int32 ulValue)
{
   *ubPtr++ = (u_int8) (ulValue >> 24);
   *ubPtr++ = (u_int8) (ulValue >> 16);
   *ubPtr++ = (u_int8) (ulValue >> 8);
   *ubPtr++ = (u_int8) (ulValue);

   return(ubPtr);
}

/**
 **  Name:         GEN_ulong_to_BCD
 **
 **  Description:  Converts a u_int32 value into it's BCD representation.
 **
 **  Arguments:    Value to convert
 **
 **  Returns:      Converted value
 **/

u_int32 GEN_ulong_to_BCD(u_int32 ulValue)
{
   u_int8  ubIndex, ubShift = 0;
   u_int32 ulBcdDigit, ulRet = 0;

   for (ubIndex = 0; ubIndex < 8; ubIndex++)
   {
      ulBcdDigit   = ulValue % 10;
      ulBcdDigit <<= ubShift;
      ulRet       |= ulBcdDigit;
      ulValue     /= 10;
      ubShift     += 4;
   }

   return(ulRet);
}

/**
 **  Name:         GEN_ushort_get
 **
 **  Description:  Get's ushort value out of byte array
 **
 **  Arguments:    array ^, mask, shift
 **
 **  Returns:      value
 **/

u_int16 GEN_ushort_get(u_int8 *ubPtr, u_int16 usMask, u_int8 ubShift)
{
   u_int16 usTemp;

   usTemp = (*ubPtr << 8) + *(ubPtr + 1);
   usTemp >>= ubShift;
   usTemp  &= usMask;

   return (usTemp);
}

/**
 **  Name:         GEN_ushort_put
 **
 **  Description:  Puts a ushort into a byte array
 **
 **  Arguments:    array ^, value
 **
 **  Returns:      Updated array ptr.
 **/

u_int8 *GEN_ushort_put(u_int8 *ubPtr, u_int16 usValue)
{
   *ubPtr++ = (u_int8) (usValue >> 8);
   *ubPtr++ = (u_int8) (usValue);

   return(ubPtr);
}


/**
 **  Name:         GEN_ushort_to_BCD
 **
 **  Description:  Converts a u_int16 value into it's BCD representation.
 **
 **  Arguments:    Value to convert
 **
 **  Returns:      Converted value
 **/

u_int16 GEN_ushort_to_BCD(u_int16 usValue)
{
   u_int8  ubIndex, ubShift = 0;
   u_int16 usBcdDigit, usRet = 0;

   for (ubIndex = 0; ubIndex < 4; ubIndex++)
   {
      usBcdDigit   = usValue % 10;
      usBcdDigit <<= ubShift;
      usRet       |= usBcdDigit;
      usValue     /= 10;
      ubShift     += 4;
   }

   return(usRet);
}

/**
 **  Name:         GEN_ushort_update
 **
 **  Description:  Updates a ushort in a byte array with an offset
 **
 **  Arguments:    array ^, offset
 **
 **  Returns:      None
 **/

void GEN_ushort_update(u_int8 *ubPtr, u_int16 usOffset)
{
   u_int16 usValue;

   //
   // Get value from array
   //
   usValue  = GEN_ushort_get(ubPtr, 0xFFFF, 0);

   // 
   // Update the value
   //
   usValue += usOffset;

   //
   // Put updated value back into array
   //
   GEN_ushort_put(ubPtr, usValue);
}

/* ======   Local Functions separator ===========================================
void ___GLOBAL_FUNCTIONS(){}
==============================================================================*/

// 
//  Name:         gen_FindDir
//  Description:  Skip dir level
//  Arguments:    String, number of dirs
// 
//  Returns:      Char *
// 
static char *gen_FindDir(char *pcStr, int iCount)
{
   int   iIdx=GEN_STRLEN(pcStr);
   char *pcRet=pcStr;

   while(iCount && iIdx)
   {
      iIdx--;
      if(pcStr[iIdx] == '/') 
      {
         if(--iCount == 0) pcRet = &pcStr[iIdx+1];
      }
   }
   return(pcRet);
}

// 
//  Name:         gen_GetChar
//  Description:  Get a char from the buffer
//  Arguments:    String, flags
// 
//  Returns:      Char or uppercase(Char) if case-insensitive
// 
static char gen_GetChar(char *pcStr, int iFlags)
{
   char  cChar;

   cChar = *pcStr;
   if(iFlags & WCF_CASE) return(cChar);
   else                  return(toupper(cChar));
}

/**
 **  Name:         gen_ListData
 **  Description:  List a block of data in lines/cols to file 
 **  Arguments:    Fd, pcHeader, Nr of Columns, pubData, iSize
 **
 **  Returns:      void
 **/
static void gen_ListData(int iFd, const char *pcHeader, bool fAddr, int iCols, u_int8 *pubBuffer, int iLength)
{
   bool  fNewline=TRUE;
   int   i, x;
  
   GEN_DPRINTF(iFd, "%s (%d)"CRLF, pcHeader, iLength);
   //
   for(x=0, i=0; i<iLength; i++)
   {
      if(fNewline)
      {
         GEN_DPRINTF(iFd, "%06X=", i);
         fNewline = FALSE;
      }
      GEN_DPRINTF(iFd, "%02X", pubBuffer[i]);
      if(i%iCols == (iCols-1)) 
      {
         GEN_DPRINTF(iFd, "  ");
         while(x<=i)
         {
            if( isprint((int)pubBuffer[x])) GEN_DPRINTF(iFd, "%c", pubBuffer[x]);
            else                            GEN_DPRINTF(iFd, ".");
            x++;
         }
         GEN_DPRINTF(iFd, "\n");
         fNewline = TRUE;
      }
      else
      {
         GEN_DPRINTF(iFd, " ");
      }
   }
   GEN_DPRINTF(iFd, "\n");
}

