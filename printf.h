/*  (c) Copyright:  2021  Patrn ESS, Confidential Data
 *
 *  Workfile:           printf.h
 *  Purpose:            Generic printer redirections
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    09 Oct 2021:      Ported from rpisense
 *    19 Nov 2021:      Use Variadic Macro's (backwards comp)
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef  _PRINTF_H_
#define  _PRINTF_H_

//
// Define CRLF string : PRINTF("This is the text" CRLF);
//
#define CR                    '\r'
#define LF                    '\n'
//
#define NEWLINE               "\r\n"
#define CRLF                  "\r\n"
#define CRLF2                 "\r\n\r\n"
#define TAB                   '\t'
#define FF                    0x0c
//
#define  ERR_PRINTF(...)               fprintf(stderr, __VA_ARGS__)
//
#define  UNC_PRINTF(a)                 printf(...)
#define  UNC_PRINTF1(a,b)              printf(...)
#define  UNC_PRINTF2(a,b,c)            printf(...)
#define  UNC_PRINTF3(a,b,c,d)          printf(...)
//
//  Makefile should specify one of:
//
//    FEATURE_USE_PRINTF   - LOG_printf  trace data to stdout for debug on the spot
//    FEATURE_LOG_PRINTF   - LOG_printf  trace data to stdout for debug on the spot
//    FEATURE_USE_STDOUT   - printf      trace data to stdout for debug on the spot
//    FEATURE_TRACE_PRINTF - LOG_Trace   trace data to persistent file system for debug lockups which
//                                       require reboots or hard kills which would erase the TMPFS log files.
//
#if defined (USE_PRINTF)

#if defined (FEATURE_USE_PRINTF)
   #warning FEATURE_USE_PRINTF
   #define  PRINTF(a)                  LOG_printf(a)
   #define  PRINTF1(a,b)               LOG_printf(a,b)
   #define  PRINTF2(a,b,c)             LOG_printf(a,b,c)
   #define  PRINTF3(a,b,c,d)           LOG_printf(a,b,c,d)
   #define  PRINTF4(a,b,c,d,e)         LOG_printf(a,b,c,d,e)
   #define  PRINTF5(a,b,c,d,e,f)       LOG_printf(a,b,c,d,e,f)
   #define  PRINTF6(a,b,c,d,e,f,g)     LOG_printf(a,b,c,d,e,f,g)
   #define  PRINTF7(a,b,c,d,e,f,g,h)   LOG_printf(a,b,c,d,e,f,g,h)
   #define  PRINTF8(a,b,c,d,e,f,g,h,i) LOG_printf(a,b,c,d,e,f,g,h,i)
#elif defined (FEATURE_LOG_PRINTF)
   #warning FEATURE_LOG_PRINTF
   #define  PRINTF(...)                LOG_printf(__VA_ARGS__)
   #define  PRINTF1(...)               LOG_printf(__VA_ARGS__)
   #define  PRINTF2(...)               LOG_printf(__VA_ARGS__)
   #define  PRINTF3(...)               LOG_printf(__VA_ARGS__)
   #define  PRINTF4(...)               LOG_printf(__VA_ARGS__)
   #define  PRINTF5(...)               LOG_printf(__VA_ARGS__)
   #define  PRINTF6(...)               LOG_printf(__VA_ARGS__)
   #define  PRINTF7(...)               LOG_printf(__VA_ARGS__)
   #define  PRINTF8(...)               LOG_printf(__VA_ARGS__)
#elif defined (FEATURE_USE_STDOUT)
   #warning FEATURE_USE_STDOUT
   #define  PRINTF(a)                  printf(a)
   #define  PRINTF1(a,b)               printf(a,b)
   #define  PRINTF2(a,b,c)             printf(a,b,c)
   #define  PRINTF3(a,b,c,d)           printf(a,b,c,d)
   #define  PRINTF4(a,b,c,d,e)         printf(a,b,c,d,e)
   #define  PRINTF5(a,b,c,d,e,f)       printf(a,b,c,d,e,f)
   #define  PRINTF6(a,b,c,d,e,f,g)     printf(a,b,c,d,e,f,g)
   #define  PRINTF7(a,b,c,d,e,f,g,h)   printf(a,b,c,d,e,f,g,h)
   #define  PRINTF8(a,b,c,d,e,f,g,h,i) printf(a,b,c,d,e,f,g,h,i)
#elif defined (FEATURE_TRACE_PRINTF)
   #warning FEATURE_TRACE_PRINTF
   #define  PRINTF(a)                  LOG_Trace(a)
   #define  PRINTF1(a,b)               LOG_Trace(a,b)
   #define  PRINTF2(a,b,c)             LOG_Trace(a,b,c)
   #define  PRINTF3(a,b,c,d)           LOG_Trace(a,b,c,d)
   #define  PRINTF4(a,b,c,d,e)         LOG_Trace(a,b,c,d,e)
   #define  PRINTF5(a,b,c,d,e,f)       LOG_Trace(a,b,c,d,e,f)
   #define  PRINTF6(a,b,c,d,e,f,g)     LOG_Trace(a,b,c,d,e,f,g)
   #define  PRINTF7(a,b,c,d,e,f,g,h)   LOG_Trace(a,b,c,d,e,f,g,h)
   #define  PRINTF8(a,b,c,d,e,f,g,h,i) LOG_Trace(a,b,c,d,e,f,g,h,i)
#else
// #warning NO FEATURE_PRINTF
   #define  PRINTF(a)
   #define  PRINTF1(a,b)
   #define  PRINTF2(a,b,c)
   #define  PRINTF3(a,b,c,d)
   #define  PRINTF4(a,b,c,d,e)
   #define  PRINTF5(a,b,c,d,e,f)
   #define  PRINTF6(a,b,c,d,e,f,g)
   #define  PRINTF7(a,b,c,d,e,f,g,h)
   #define  PRINTF8(a,b,c,d,e,f,g,h,i)

#endif

#else
// #warning NO USE_PRINTF
   #define  PRINTF(...)
   #define  PRINTF1(...)
   #define  PRINTF2(...)
   #define  PRINTF3(...)
   #define  PRINTF4(...)
   #define  PRINTF5(...)
   #define  PRINTF6(...)
   #define  PRINTF7(...)
   #define  PRINTF8(...)
#endif   // USE_PRINTF

#endif   // _PRINTF_H_

