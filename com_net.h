/*  (c) Copyright:  2017..2018 Patrn, Confidential Data 
 *
 *  Workfile:           com_net.h
 *  Revision:        
 *  Modtime:         
 *
 *  Purpose:            Library headerfile for:
 *                      general networking
 *                      sockets, pipes, etc.
 *                      unbuffered I/O
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    02 Mar 2018:      Made common from rpi_net.*
 *    03 Sep 2019:      Add Net VERBOSE level (0=OFF)
 *    25 Apr 2022:      Add connected IP address to NETCL
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef __COM_NET_H__
#define __COM_NET_H__

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string.h>
#include <fcntl.h>
#include <stdio.h>
#include <malloc.h>
#include <errno.h>                        /* errno global variable */

#ifndef  INADDR_NONE
#define  INADDR_NONE                      0xffffffff
#endif
//
// Global debug filter
//
#define  DEBUG_NET                        0x00000001L
//
#define  HTTP_PROTOCOL                    "tcp"
#define  HTTP_DEFAULT_DATA_SIZE           1000
#define  HTTP_CONNECTION_TIMEOUT_MSECS    2000
#define  HTTP_CONNECTION_MAX_SECS         60

//    struct hostent
//  
//    This data type is used to represent an entry in the hosts database. It has the following members:
//  
//    char   *h_name         This is the �official� name of the host.
//    char  **h_aliases      These are alternative names for the host, represented as a null-terminated vector of strings.
//    int     h_addrtype     This is the host address type; in practice, its value is always either AF_INET or AF_INET6, 
//                           with the latter being used for IPv6 hosts. In principle other kinds of addresses could be 
//                           represented in the database as well as Internet addresses; if this were done, you might 
//                           find a value in this field other than AF_INET or AF_INET6. See Socket Addresses.
//    int     h_length       This is the length, in bytes, of each address.
//    char  **h_addr_list    This is the vector of addresses for the host. (Recall that the host might be connected to 
//                           multiple networks and have different addresses on each one.) The vector is terminated by a 
//                           null pointer.
//    char   *h_addr         This is a synonym for h_addr_list[0]; in other words, it is the first host address. 
//
typedef struct hostent      *LPHOSTENT;
typedef const char          *LPCSTR;

//  #include <netinet/in.h>
//  
//   struct sockaddr_in 
//   {
//      short            sin_family;   // e.g. AF_INET
//      unsigned short   sin_port;     // e.g. htons(3490)
//      struct in_addr   sin_addr;     // see struct in_addr, below
//      char             sin_zero[8];  // zero this if you want to
//   };
//  
//   struct in_addr 
//   {
//      unsigned long s_addr;  // load with inet_aton()
//   };
//
typedef struct sockaddr      SOCKADDR;
typedef struct sockaddr     *LPSOCKADDR;
typedef struct sockaddr_in   SOCKADDR_IN;
typedef struct sockaddr_in  *LPSOCKADDR_IN;
typedef char                *LPSTR;

#ifndef COPY_BUFSIZE
#define COPY_BUFSIZE          10*1024           // Buffer size for copies
#endif

#define  NUM_CONNECTIONS      5
#define  RCV_BUFFER_SIZE      2048
#define  RCV_BUFFER_MASK      (RCV_BUFFER_SIZE- 1)
#define  URL_BUFFER_SIZE      2048
#define  HDR_BUFFER_SIZE      2048
#define  XMT_BUFFER_SIZE      2048
#define  XMT_BUFFER_MAX       (XMT_BUFFER_SIZE-10)
#define  XMT_BUFFER_MASK      (XMT_BUFFER_SIZE- 1)
//
#define  NET_DUMP_ASCII       0x0001
#define  NET_DUMP_HEX         0x0002
#define  NET_DUMP_FILE        0x0004
//
// Net Client flags
//
#define  NET_CONT_HDR_END     0x00010000        // End of content header
#define  NET_SESSION_FRAG     0x00020000        // Session fragmented
//
typedef enum _netstart_
{
   NET_COLDSTART = 0,
   NET_WARMSTART
}  NETSTART;

//
// NET Status options
//
#define  NET_CHECK_COUNT      0x0000            // Count connections only
#define  NET_CHECK_LOG        0x0001            // LOG NET status to logfile
#define  NET_CHECK_FREE       0x0002            // Free unused connections
//
// Netwerk type: 
//    - HTTP
//    - RAW DATA
//
typedef enum _nettype_
{
   NET_HTTP = 0,
   NET_RAW
}  NETTYPE;
//
typedef enum _ftype_
{
   HTTP_GEN = 0,
   HTTP_SHTML,
   HTTP_HTML,
   HTTP_JS,
   HTTP_CGI,
   HTTP_GIF,
   HTTP_JPG,
   HTTP_BMP,
   HTTP_PNG,
   HTTP_TXT,
   HTTP_JSON,
   HTTP_ZIP,
   HTTP_CSV,
   HTTP_CSS,
   HTTP_MP3,
   HTTP_MP4,
   HTTP_H264,
   HTTP_ICO,
   HTTP_ISO
}  FTYPE;

typedef enum _rtype_
{
   HTTP_MORE = 0,
   HTTP_CHAR,
   HTTP_GET,
   HTTP_PUT,
   HTTP_POST,
   HTTP_DONE,
   HTTP_ERROR,
   HTTP_TPKT,
   HTTP_PAYL,
   HTTP_RAW
}  RTYPE;

typedef struct _netcl_
{
   int      iReqState;                          // State handler state
   int      iSubState;                          // State handler state substate (1 level nested)
   int      fFlags;                             // Net Client flags
   int      iVerbose;                           // Verbose LOG levels
   int      iContLength;                        // Content length
   int      iSocket;                            // Session Socket
   int      iPort;                              // Port
   char    *pcWww;                              // HTTP www path
   char    *pcCache;                            //      cache path
   char    *pcRcvBuffer;                        // RCV data buffer
   int      iRcvBufferGet;                      //     index get
   int      iRcvBufferPut;                      //     index put
   char    *pcUrl;                              // RCV URL buffer
   int      iUrlIdx;                            //     index URL
   int      iHdrIdx;                            //     index request header 
   int      iPaylIdx;                           //     index first payload  
   int      iNextIdx;                           //     index next  payload  
   int      iFileIdx;                           //     index filename       
   int      iExtnIdx;                           //     index extension      
   char    *pcHdr;                              // RCV HDR buffer
   char    *pcXmtBuffer;                        // XMT data buffer
   int      iXmtBufferGet;                      //     index get
   int      iXmtBufferPut;                      //     index put
   //
   u_int32  ulTimestamp;                        // Last usage timestamp
   //
   RTYPE    tSessionType;                       // HTTP_MORE/ERROR/DONE/TPKT
   RTYPE    tHttpType;                          // HTTP_GET/POST/PUT
   FTYPE    tContType;                          // Content type
   //
   char     cIpAddr[INET_ADDRSTRLEN];           // IP Address
}  NETCL;

//
// Define flags for dynamic URLs
//
#define  DYN_FLAG_NONE  0
#define  DYN_FLAG_PORT  0x0001
//
typedef bool (*PFNET)(NETCL *);
typedef bool (*PFNETI)(NETCL *, int);
//
typedef struct _rpi_dynpage_
{
   int         tUrl;
   FTYPE       tType;
   int         iTimeout;
   int         iFlags;
   const char *pcUrl;
   PFNETI      pfDynCb;
}  NETDYN;
//
// Verbose log flags
//
#define NET_VERBOSE_SESSION         0x00000001  // Verbose log Session data
#define NET_VERBOSE_SERVER          0x00000002  // Verbose log Server  data
#define NET_VERBOSE_HTTP_GET        0x00000004  // Verbose log HTTP GETs
#define NET_VERBOSE_HTTP_PUT        0x00000008  // Verbose log HTTP PUTs
#define NET_VERBOSE_DUMP_RCV        0x00000010  // Verbose log Session receive data
#define NET_VERBOSE_DUMP_XMT        0x00000020  // Verbose log Session xmit    data
#define NET_VERBOSE_TPKT            0x00000040  // Verbose log Session TPKT    data
//
#define NET_VERBOSE_NONE            0x00000000  // Verbose log NO      data
#define NET_VERBOSE_ALL             0x0000000F  // Verbose log all     data
//
// Connection based API
//
typedef struct NETCON
{
   int      iConnections;                       // Number of possible sessions
   int      iSocketPort;                        // Socket port
   int      iSocketConnect;                     // FD for connection socket
   int      iSocketMax;                         // FD highest socket number
   NETCL    stClient[NUM_CONNECTIONS];          // Session client state handler data
   //                                           
   fd_set   tFdRead;                            // FD Set Read
   fd_set   tFdWrite;                           //        Write 
   fd_set   tFdOoB;                             //        Out of Band data
   //
   NETTYPE  tRequest;                           // Request type HTTP. RAW, ...
   int      iVerbose;                           // Verbose log level (0=None)
   struct   timeval stTimeout;                  // Time out structure
}  NETCON;


NETCON  *NET_Init                   (char *, char *);
int      NET_BuildConnectionList    (NETCON *);
int      NET_WaitForConnection      (NETCON *, int);
int      NET_HandleSessions         (NETCON *, PFNET, char *, int);

/* Basic reading and writing */
int      NET_ReadAll                (int, char *, int); 
int      NET_Read                   (int, char *, int);
int      NET_Write                  (int, char *, int);

/* String/delimited reading and writing */

int      NET_WriteString            (int, char *);
int      NET_ReadString             (int, char *, int);
int      NET_ReadDelimString        (int, char *, int, char);

/* Integer reading and writing */

int      NET_ReadUlong              (int, u_int32 *);
int      NET_WriteUlong             (int, u_int32);

/* Data copy */

int      NET_Copy                   (int, int, int);

/* Reverse DNS lookups and friends */

char    *NET_GetMyFqdn              (char *, int);
char    *NET_GetFqdn                (char *, int);
char    *NET_ResolveHostName        (char *);
                                 
/* Network initialization */     
                                 
int      NET_ServerConnect          (NETCON *, const char *port, const char *proto);
int      NET_ServerDisconnect       (NETCL *);
int      NET_ServerAccept           (NETCON *, char *, int);
NETCON  *NET_ServerTerminate        (NETCON *);
int      NET_FlushCache             (NETCL *);
//
int      NET_RcvGet                 (NETCL *, char *, int);
int      NET_RcvPut                 (NETCL *, char *, int);
int      NET_XmtGet                 (NETCL *, char *, int);
int      NET_XmtPut                 (NETCL *, char *, int);
//
int      NET_ClientConnect          (const char *, int, const char *);
int      NET_ClientDisconnect       (int);
//                                 
bool     NET_GetIpInfo              (char *, int, char *, int);
int      NET_ReportServerStatus     (NETCON *, int);
//
int      NET_Connections            (NETCON *);
char    *NET_ConnectedIp            (NETCON *, int);
#endif   // __COM_NET_H__
