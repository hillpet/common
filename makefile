#################################################################################
# 
# makefile:
#	Common files for Raspberry Pi HTTP server
#
#	Copyright (c) 2012-2018 Peter Hillen
#################################################################################
BOARD=$(shell cat ../../RPIBOARD)

ifeq ($(BOARD),1)
#
# RPi#1 (Rev-1): Metal case with CAM
#
	BOARD_OK=yes
	TARGET=rpicam
endif

ifeq ($(BOARD),2)
#
# RPi#2 (Rev-1): Black box with PiFace CAD LCD
#
	BOARD_OK=yes
	TARGET=rpicam
endif

ifeq ($(BOARD),3)
#
# RPi#3 (Rev-1): PiFace Digital
#
	BOARD_OK=yes
	TARGET=rpicam
endif

ifeq ($(BOARD),4)
#
# RPi#4 (Rev-1): 
#
	BOARD_OK=yes
	TARGET=rpicam
endif

ifeq ($(BOARD),6)
#
# RPi#6 (Rev-1): Rpi TFT Camera
#
	BOARD_OK=yes
	TARGET=rpicam
endif

ifeq ($(BOARD),7)
#
# RPi#7 (Rev-2): PopKodi and Apache web server
#
	BOARD_OK=yes
	TARGET=popkodi
endif

ifeq ($(BOARD),8)
#
# RPi#8 (Rev-2): CNC Controller
#
	BOARD_OK=yes
	TARGET=rpicnc
endif

ifeq ($(BOARD),9)
#
# RPi#9 (Rev-3): VPN Server/Smart meter
#
	BOARD_OK=yes
	TARGET=rpislim
endif

ifeq ($(BOARD),10)
#
# RPi#10 (Rev-3): SpiCam HTTP Server
#
	BOARD_OK=yes
	TARGET=spicam
endif

ifeq ($(BOARD),11)
#
# RPi#11 (Rev-3+): Dev board
#
	BOARD_OK=yes
	TARGET=rpislim
endif

ifndef BOARD_OK
#
# All other RPi's: 
#
	TARGET=rpicam
endif

CC		= gcc
DEFS	+= -D BOARD_RPI$(BOARD)
LIB		= libcommon.a
LIBCOM 	=
DEFS	=
INCDIR	=
CFLAGS	= -O2 -Wall -g -ggdb
DESTDIR	= .
DEPS	=
SRC		=	com_func.c com_log.c com_rtc.c com_file.c com_json.c com_safe.c	com_net.c com_http.c com_juice.o com_nmap.o
OBJ		=	$(SRC:.c=.o)

debug:	LIB = libcommondebug.a
debug:	CFLAGS = -Wall -g -ggdb
debug: 	LIBCOM = -rdynamic
debug:	DEFS += -D DEBUG_ASSERT -D FEATURE_LOG_PRINTF
debug:	all

all:	$(LIB)

lib:	clean $(LIB)

libd:	clean debug $(LIB)

$(LIB):	$(OBJ)
	@echo "Build $(LIB) RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	ar rcs $(LIB) $(OBJ)
	ranlib $(LIB)

sync:
	@echo "RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	cp -u /mnt/rpi/softw/common/*.c ./
	cp -u /mnt/rpi/softw/common/*.h ./
	cp -u /mnt/rpi/softw/common/makefile ./

forcesync:
	@echo "RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	cp /mnt/rpi/softw/common/*.c ./
	cp /mnt/rpi/softw/common/*.h ./
	cp /mnt/rpi/softw/common/makefile ./

clean:
	rm -rf *.o

cleanall:
	rm -rf *.o
	rm -rf *.a

.c.o: $(DEPS)
	$(CC) -c $(CFLAGS) $(INCDIR) $(LIBCOM) $(DEFS) $< 
