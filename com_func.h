/*  (c) Copyright:  2012..2019  Patrn, Confidential Data
 *
 *  Workfile:           com_func.h
 *  Purpose:            Generic functions and OS abstraction API
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *  Note:               
 * 
 *  Author:             Peter Hillen
 *    17 Jul 2012       Created
 *    24 Aug 2019       Add GEN_GetPidByCommand() alternative to GEN_GetPidByName()
 *    03 Sep 2019       Add GEN_RemovePair()
 *    23 Sep 2019       Add GEN_CheckBadCharacters()
 *    17 Nov 2019       Add GEN_SimpleWildcards()
 *    22 Oct 2021:      Add DELIM_SQRB, _SQRE, _CURB, _CURE
 *    12 Nov 2021:      Add GEN_GetUserIdByName()
 *    01 Dec 2021:      Add Gen_Upper/LowerCase()
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/
#ifndef _COM_FUNC_H_
#define _COM_FUNC_H_

#include <semaphore.h>

#define CMD_LEN                  31
#define CMD_LENZ                 (CMD_LEN+1)
#define NAME_LEN                 255
#define NAME_LENZ                (NAME_LEN+1)
#define READ_LEN                 255
#define READ_LENZ                (READ_LEN+1)
#define BUF_SPARE                32

typedef enum _delimiters_
{
    DELIM_NOSPACE = 0,          // All but space
    DELIM_SPACE,                // ' '
    DELIM_COLON,                // ':'
    DELIM_NUMERIC,              // '0'..'9'
    DELIM_INTEGER,              // +/- '0'...'9'
    DELIM_COMMA,                // ','
    DELIM_PERIOD,               // '.'
    DELIM_ASTERIX,              // '*'
    DELIM_AMPAND,               // '&'
    DELIM_DQUOTE,               // '"'
    DELIM_QUOTE,                // '''
    DELIM_EQU,                  // '='
    DELIM_SQRB,                 // '['
    DELIM_SQRE,                 // ']'
    DELIM_CURB,                 // '{'
    DELIM_CURE,                 // '}'
    DELIM_CR,                   // '\r'
    DELIM_LF,                   // '\n'
    DELIM_TAB,                  // '\t'
    DELIM_NEXTFIELD,            // 'aap     noot" skip to next space separated field
    DELIM_STATIC,               // '@'      static file marker

    NUM_DELIMS
}   DELIM;
//
typedef enum _pid_status_
{
   PIDST_EXISTS = 0,             // PID exists
   PIDST_NAME,                   // PID Name
   PIDST_STATE,                  // PID State
   PIDST_PPID,                   // Parent PID
   PIDST_OPID,                   // Owner PID
   //
   PIDST_IS_RUNNING,             // Thread found running
   PIDST_IS_SLEEPING,            //              sleeping
   PIDST_IS_ZOMBIE,              //              zombie
   PIDST_IS_NOT,                 // Thread not found
   PIDST_IS_OTHER,               // Unknown error
   PIDST_IS_OTL,                 // Out To Lunch (not being scheduled)
}  PIDST;
//
typedef struct _pid_infos_
{
   PIDST tState;
   char  cName[NAME_LEN];
   pid_t tPPid;
   pid_t tOPid;
}  PIDINFO;
//
typedef struct _thread_infos_
{
   char     cUser[32];
   pid_t    tPid;
   float    flCpu;
   float    flMem;
   int      iVsz;
   int      iRss;
   char     cTty[32];
   char     cStat[32];
   char     cStart[32];
   char     cTime[32];
   char     cCommand[63];
   char     cCmdLine[255];
   //
   bool     fTimeOKee;
   int      iDays;
   int      iHours;
   int      iMins;
   int      iSecs;
}  PSAUX;
//
#define  WCF_CASE                1           // Case sensitive
#define  WCF_FILE                2           // Treat filename only
#define  WCF_DIR                 4           // Treat final dir/filename only
//
#define  DO_WAIT                 TRUE
#define  NO_WAIT                 FALSE
//
#define  DO_FRIENDLY             TRUE
#define  NO_FRIENDLY             FALSE
//
#define  NO_TIMEOUT              -1
//
#define  KILL_TIMEOUT_FRIENDLY   5000
#define  KILL_TIMEOUT_FORCED     5000
//
u_int8      GEN_ubyte_get           (u_int8 *, u_int8, u_int8);
u_int32     GEN_ulong_get           (u_int8 *, u_int32, u_int8);
u_int8     *GEN_ulong_put           (u_int8 *, u_int32);
u_int32     GEN_ulong_to_BCD        (u_int32);
u_int16     GEN_ushort_get          (u_int8 *, u_int16, u_int8);
u_int8     *GEN_ushort_put          (u_int8 *, u_int16);
u_int16     GEN_ushort_to_BCD       (u_int16);
void        GEN_ushort_update       (u_int8 *, u_int16);
//
pid_t       GEN_BatchProcess        (const char *, const char *, const char *, int);
u_int16     GEN_CalculateCrc16      (const u_int8 *, int);
const char *GEN_CheckBadCharacters  (char *, const char *);
bool        GEN_CheckBgndTerminated (const char *, pid_t);
int         GEN_CopyToDelimiter     (char, char *, char *, int);
bool        GEN_DisplayData         (const char *, const char *, char *, int);
void        GEN_DumpData            (char *, int, u_int8 *, int);
bool        GEN_ListAddrData        (const char *, const char *, int, u_int8 *, int);
bool        GEN_ListData            (const char *, const char *, int, u_int8 *, int);
char       *GEN_FindDelimiter       (char *, DELIM);
char       *GEN_FindDelimiters      (char *, char *);
gid_t       GEN_GetGroupIdByName    (const char *);
pid_t       GEN_GetPidByCommand     (const char *);
pid_t       GEN_GetPidByName        (const char *);
pid_t       GEN_GetUserPidByName    (const char *, const char *);
uid_t       GEN_GetUserIdByName     (const char *);
bool        GEN_GetThreadInfo       (const char *, PSAUX *);
int         GEN_GetPidInfo          (PIDINFO *, pid_t, PIDST);
void        GEN_GetPublicKey        (u_int8 *, int);
void        GEN_HexPrintf           (char *, bool);
int         GEN_KillProcess         (pid_t, bool, int);
int         GEN_Printf              (const char *, ...);
char       *GEN_Remalloc            (char *, int);
int         GEN_RemoveChar          (char *, char);
bool        GEN_RemovePair          (char *, const char *);
int         GEN_ReplaceChar         (char *, char, char);
int         GEN_RunBatch            (const char *, const char *);
void        GEN_ShowProcessStatus   (int);
void        GEN_ReportProcessStatus (int);
void        GEN_SignalPid           (pid_t, int);
bool        GEN_SimpleWildcards     (char *, char *, int);
int         GEN_SizeToDelimiters    (char *, char *);
int         GEN_Sleep               (int);
void        GEN_LowerCase           (char *);
void        GEN_UpperCase           (char *);
char       *GEN_TrimAll             (char *);
char       *GEN_TrimLeft            (char *);
char       *GEN_TrimRight           (char *);
int         GEN_Timeprintf          (char *, ...);
void        GEN_WaitProcess         (pid_t);
int         GEN_WaitProcessTimeout  (pid_t, int);
int         GEN_WaitSemaphore       (sem_t *, int);

#endif   //_COM_FUNC_H_
