/*  (c) Copyright:  2017..2018  Patrn, Confidential Data 
 *
 *  Workfile:           com_http.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Header file for HTTP web server helper files
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       2 Mar 2018 Made common from rpi_net.*
 *
 *  Revisions:
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _COM_HTTP_H_
#define _COM_HTTP_H_

#include "com_net.h"

typedef enum
{
   HTTP_ATTR_HTML    =  0x0001,
   HTTP_ATTR_TABLE   =  0x0002,
   HTTP_ATTR_TBODY   =  0x0004,
   HTTP_ATTR_BODY    =  0x0008,
   HTTP_ATTR_TR      =  0x0010,
   HTTP_ATTR_TD      =  0x0011
}  HTTP_ATTR;

typedef struct HTTPTYPE
{
   const char *pcExt;
   FTYPE       tType;
   const char *pcType;
   PFNET       pfExt;   
}  HTTPTYPE;

typedef RTYPE (*PFHTTP)(NETCL *, char *, int);
//
typedef struct HTTPREQ
{
   const char *pcHeader;
   PFHTTP      pfHttpCb;
}  HTTPREQ;

typedef RTYPE (*PFNETSTM)(NETCL *);
//
typedef struct HTTPSTM
{
   int         tState;
   const char *pcName;
   PFNETSTM    pfHttpStm;
}  HTTPSTM;
//
#define  IP_LEN         15
#define  IP_LENZ        IP_LEN+1
#define  MAC_LEN        17
#define  MAC_LENZ       MAC_LEN+1
//
typedef struct _rpiloc_ RPILOC;
typedef struct _rpiloc_
{
   char        cIp[IP_LENZ];
   char        cMac[MAC_LENZ];
   RPILOC     *pstNext;
}  RPILOC;


//
// Global prototypes
//
bool     HTTP_RequestStaticPage        (NETCL *);
RTYPE    HTTP_CollectRequest           (NETCL *);
bool     HTTP_CollectGet               (NETCL *, char);
char    *HTTP_CollectGetParameter      (NETCL *);
char    *HTTP_GetParameter             (NETCL *, int, char *, int);
bool     HTTP_BuildGeneric             (NETCL *, const char *, ...);
bool     HTTP_GetFromNetwork           (char *, char *, char *, int, int);
RPILOC  *HTTP_RetrieveRpi              (void);
RPILOC  *HTTP_ReleaseRpi               (RPILOC  *);
void     HTTP_SendData                 (NETCL *, char *, int);
void     HTTP_SendString               (NETCL *, char *);
bool     HTTP_SendTextFile             (NETCL *, char *);

#endif   //_COM_HTTP_H_
