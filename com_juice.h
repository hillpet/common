/*  (c) Copyright:  2018..2019 Patrn, Confidential Data
 *
 *  Workfile:           com_juice.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Library com_juice.c header file for:
 *                      pijuice functions
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    10 Dec 2018       Created
 * 
 *
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _COM_JUICE_H_
#define _COM_JUICE_H_

typedef enum _pijuice_type_
{
   PJ_TYPE_STR = 0,
   PJ_TYPE_INT,
   PJ_TYPE_BOOL,
   PJ_TYPE_FLT,
   //
   NUM_PJ_TYPES
}  PJTYPE;
//
typedef enum _pijuice_info_
{
   PJ_NONE = 0,
   PJ_BATTERY,
   PJ_CHARGE,
   PJ_5V,
   PJ_FAULT,
   PJ_BUTTON,
   PJ_POWER,
   PJ_ERROR,
   //
   NUM_PJS
}  PJINF;

//
// Global prototypes
//
bool PJ_GetStatus       (PJINF, void *, int);
 
#endif  /*_COM_JUICE_H_ */

