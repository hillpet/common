/*  (c) Copyright:  2012..2021  Patrn, Confidential Data 
 *
 *  Workfile:           com_log.c
 *  Purpose:            Offer the API as an abstraction layer for library calls
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *  Note:               Some functions CANNOT Use some of the LOG-functions due to the use 
 *                      of the G_tmutex by GLOBAL_GetTraceCode(). The mutex is stored in the 
 *                      GLOBAL area and the call to it must be defined in the main body.
 *                      Also, keep in mind that safe-calls also use LOG_Report(), so beware
 *                      not to use them exept safemalloc() and SAFEFREEX(__PRETTY_FUNCTION__, ).
 *
 *                         LOG_Report() 
 *                         LOG_Trace() 
 *                         LOG_printf()
 *                         LOG_debug() 
 *                         (so also the PRINTFx() macro's)
 *
 *  Author:             Peter Hillen
 *  Changes:       
 *    17 Jul 2012:      Created
 *    01 Jul 2019:      On Log full redirect to null
 *    26 Oct 2019       Prevent trailing CRLF insertion using iError = -1
 *    28 Oct 2019       Make LOG file Global
 *    09 Nov 2019       Change Global LOG struct
 *    22 Nov 2020:      Add LOG_DumpData() to stdout
 *    29 May 2021:      Add LOG_ListData() to log-file
 *    10 Nov 2021:      Lock LOG printf file during printing
 *    19 Nov 2021:      Add Variadic macro's for PRINTF()
 *    22 Nov 2021:      Fix LOG_ListData() bug
 *    14 Sep 2023:      LOG_Report() is const char *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <errno.h>
#include <unistd.h>
#include <ifaddrs.h>
#include <pthread.h>
#include <sys/types.h>
#include <semaphore.h>
#include <execinfo.h>

#include "typedefs.h"
#include "config.h"
#include "com_rtc.h"
#include "com_func.h"
#include "com_safe.h"
#include "com_file.h"
#include "com_log.h"

//#define USE_PRINTF
#include "printx.h"


//
// Externals
//
int   GLOBAL_GetTraceCode     (void);
int   GLOBAL_GetDebugMask     (void);
GLOG *GLOBAL_GetLog           (void);
//
static FILE *ptLogfile           = NULL;
static FILE *ptTracefile         = NULL;
//
// local prototypes
//
static FILE *log_RedirectLog         ();
static FILE *log_RedirectTrace       ();

/* ======   Local Functions separator ===========================================
void ___main_functions(){}
==============================================================================*/

//
// Function:   LOG_Assert
// Purpose:    Debug assert
//             
// Parms:      Module, true = exit
// Returns:    
// Note:
//
void LOG_Assert(char *pcMod, bool fAbort)
{
  if(fAbort)
  {
     LOG_Report(0, pcMod, "Assert !!!");
     exit(EXIT_CC_GEN_ERROR);
  }
}

//
// Function:   LOG_Init
// Purpose:    Init LOG and trace
//             
// Parms:      
// Returns:    True if OKee
// Note:
//
bool LOG_Init()
{
   bool  fCc=TRUE;
   GLOG *pstLog=GLOBAL_GetLog();

   if(pthread_mutex_init(&pstLog->tLogMutex,   NULL)) fCc = FALSE;
   if(pthread_mutex_init(&pstLog->tTraceMutex, NULL)) fCc = FALSE;
   //
   pstLog->fLogOpen   = FALSE;
   pstLog->fTraceOpen = FALSE;
   return(fCc);
}

//
// Function:   LOG_SegmentationFault
// Purpose:    Handle the SIGSEGV
//
// Parms:      File ptr, line nr
// Returns:    Stringlist ptr
// Note:
//
//#define FEATURE_SEGV_TO_STDERR
#define BT_SIZE 20
//
char **LOG_SegmentationFault(const char *pcFile, int iLineNr)
{
   char   **ppcList=NULL;

   #ifdef FEATURE_SEGV_TO_STDERR
   int      iNr;
   void    *pvBuffer[BT_SIZE];

   iNr = backtrace(pvBuffer, BT_SIZE);

   GEN_FPRINTF(stderr, "LOG-SegmentationFault(): Backtrace() (%s-%d) returned %d addresses" CRLF, pcFile, iLineNr, iNr);

   // The call backtrace_symbols_fd(buffer, nptrs, STDOUT_FILENO)
   // would produce similar output to the following:

   backtrace_symbols_fd(pvBuffer, iNr, STDOUT_FILENO);
   
   #else //FEATURE_SEGV_TO_STDERR
   
   int      j;
   int      iNr;
   void    *pvBuffer[BT_SIZE];
   char   **ppcStrings;

   if(ptLogfile == NULL) ptLogfile = log_RedirectLog();
   //
   iNr = backtrace(pvBuffer, BT_SIZE);
   //
   GEN_FPRINTF(stderr, "LOG-SegmentationFault(): Backtrace() (%s-%d) returned %d addresses" CRLF, pcFile, iLineNr, iNr);
   ppcStrings = backtrace_symbols(pvBuffer, iNr);
   if(ppcStrings) 
   {
      ppcList = ppcStrings;
      //
      for (j=0; j<iNr; j++)
      {
         GEN_FPRINTF(stderr, "LOG-SegmentationFault(): Backtrace():%s" CRLF, ppcStrings[j]);
         GEN_FPRINTF(ptLogfile, "%s", ppcStrings[j]);
      }
      SAFEFREE(__PRETTY_FUNCTION__ , ppcStrings);
   }
   else
   {
      GEN_FPRINTF(ptLogfile, "LOG-SegmentationFault(): No backtraces !!");
      backtrace_symbols_fd(pvBuffer, iNr, STDOUT_FILENO);
   }
   #endif   //FEATURE_SEGV_TO_STDERR

   return(ppcList);
}

//
// Function:   LOG_ReportOpenFile
// Purpose:    Open a new logfile
//             
// Parms:      Filename
// Returns:    
// Note:
//
bool LOG_ReportOpenFile(char *pcFilename) 
{
   bool  fCc=FALSE;
   GLOG *pstLog=GLOBAL_GetLog();

   LOG_ReportCloseFile();
   //
   ptLogfile = safefopen(pcFilename, "a+");
   if(ptLogfile) 
   {
      pstLog->fLogOpen = TRUE;
      fCc = TRUE;
   }
   return(fCc);
}

//
// Function:   LOG_ReportFilePosition
// Purpose:    Return the file position of the log file, if any
//             
// Parms:      
// Returns:    File position
// Note:
//
int64 LOG_ReportFilePosition()
{
   int64 llPos=0;

   if(ptLogfile) 
   {
      llPos = ftell(ptLogfile);
   }
  return(llPos);
}

//
// Function:   LOG_ReportFlushFile
// Purpose:    Flush logfile
//             
// Parms:      
// Returns:    
// Note:
//
void LOG_ReportFlushFile(void) 
{
   if(ptLogfile)
   {
      if(GEN_FFLUSH(ptLogfile) == EOF)  
      {
         PRINTF("LOG-ReportFlushFile() Flush signals EOF (disk full)!" CRLF);
         ptLogfile = log_RedirectLog();
      }
   }
}

//
// Function:   LOG_ReportCloseFile
// Purpose:    Close logfile
//             
// Parms:      
// Returns:    
// Note:
//
void LOG_ReportCloseFile(void) 
{
   GLOG *pstLog=GLOBAL_GetLog();

   if(ptLogfile)
   {
      pstLog->fLogRedirected = FALSE;
      safefclose(ptLogfile);
      ptLogfile = NULL;
   }
   pstLog->fLogOpen = FALSE;
}

//
// Function:   LOG_ReportLockFile
// Purpose:    Lock logfile
//             
// Parms:      
// Returns:    
// Note:       Lock file to prevent writes to the log file while it is
//             being reduced in size
//
void LOG_ReportLockFile(void) 
{
   GLOG *pstLog=GLOBAL_GetLog();

   while(TRUE)
   {
      if(pthread_mutex_trylock(&pstLog->tLogMutex))
      {
         //LOG_Trace("LOG-ReportLockFile():Locked!" CRLF);
         //PRINTF("LOG-ReportLockFile():Locked!" CRLF);
         GEN_Sleep(100);
      }
      else break;
   }
}

//
// Function:   LOG_ReportUnlockFile
// Purpose:    Unlock logfile
//             
// Parms:      
// Returns:    
// Note:       
//
void LOG_ReportUnlockFile(void) 
{
   GLOG *pstLog=GLOBAL_GetLog();

   pthread_mutex_unlock(&pstLog->tLogMutex);
}

//
// Function:   LOG_Report
// Purpose:    Report formatted string to the logfile
//             
// Parms:      
// Returns:    Nr put out, -1 on error
// Note:       CANNOT be used if GLOBAL_MutexLock() is in effect !
//             iError generally has the thread <errno> value (and thus always > 0).
//             iError may be set to -1 to prevent a trailing CRLF to the record !
//
int LOG_Report(int iError, const char *pcCaller, const char *pcFormat, ...) 
{
   int      iNr, iSize;
   int      iSeq;
   char    *pcTemp;
   va_list  tArgs;
   GLOG    *pstLog=GLOBAL_GetLog();

// #define  FEATURE_SKIP_IF_NOT_FREE
   #ifdef   FEATURE_SKIP_IF_NOT_FREE
   if(pthread_mutex_trylock(&pstLog->tLogMutex))
   {
      return(0);
   }
   #else    //ABORT_IF_NOT_FREE
   while(pthread_mutex_trylock(&pstLog->tLogMutex))
   {
      // Sit here till mutex is free
      GEN_Sleep(100);
   }
   #endif   //ABORT_IF_NOT_FREE

   iSeq = GLOBAL_GetTraceCode();
   RTC_GetDateTimeSecs(pstLog->cLog);
   //
   // make sure the LOG fp refers to something valid:
   //    the LOG file
   //    stderr if all fails
   //
   if(ptLogfile == NULL) 
   {
      //LOG_Trace("LOG-Report():No FP!" CRLF);
      ptLogfile = log_RedirectLog();
   }
   //
   // Put out LOG message
   // Put file ptr back to the EOF, since the LOG file could have been truncated along the route
   // by any of the threads !
   //
   GEN_FSEEK(ptLogfile, 0, SEEK_END);
   iNr = GEN_FPRINTF(ptLogfile, "%5d-%s-%5d|[%s]:", iSeq, pstLog->cLog, getpid(), pcCaller);
   //
   // Allocate log buffer: reserve extra space for expansion of formatted data
   // Small strings have fixed length, larger strings double their size.
   //
   iSize = GEN_STRLEN(pcFormat);
   if(iSize < LOG_DEF_RECORD_LEN) iSize  = LOG_MAX_RECORD_LENZ;
   else                           iSize *= 2;
   //
   pcTemp = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, iSize);
   va_start(tArgs, pcFormat);
   GEN_VSNPRINTF(pcTemp, iSize-1, pcFormat, tArgs);
   iNr += GEN_FPRINTF(ptLogfile, pcTemp);
   va_end(tArgs);
   SAFEFREE(__PRETTY_FUNCTION__, pcTemp);
   //
   if(iError > 0) 
   {
      iNr += GEN_FPRINTF(ptLogfile, "(%s)", strerror(iError));
   }
   //
   // iError = -1 prevents trailing CRLF insertion :
   //
   if(iError != -1) iNr += GEN_FPRINTF(ptLogfile, CRLF);
   //
   if(GEN_FFLUSH(ptLogfile) == EOF)  
   {
      PRINTF("LOG-Report() Flush signals EOF (disk full)!" CRLF);
      iNr = -1;
      ptLogfile = log_RedirectLog();
   }
   PRINTF("LOG-Report() Done." CRLF);
   pthread_mutex_unlock(&pstLog->tLogMutex);
   return(iNr);
}

//
// Function:   LOG_debug
// Purpose:    Log debug levels with a time stamp
//             
// Parms:      Debug mask, Formatted string
// Returns:    Nr put out, -1 on error
// Note:       CANNOT be used if GLOBAL_MutexLock() is in effect !
//
int  LOG_debug(u_int32 ulDebugMask, const char *pcFormat, ...)
{
   int         iNr=0, iSeq;
   va_list     tArgs;
   u_int32     ulCurrentMask=GLOBAL_GetDebugMask();
   GLOG       *pstLog=GLOBAL_GetLog();

   if(ulCurrentMask)
   {
      iSeq = GLOBAL_GetTraceCode();
      //
      RTC_GetDateTimeSecs(pstLog->cLog);
      //
      va_start(tArgs, pcFormat);
      if(ulDebugMask & DEBUG_FILE)
      {
         ulDebugMask &= ~DEBUG_FILE;
         if(ulDebugMask & ulCurrentMask)
         {
            if(ptLogfile == NULL) ptLogfile = log_RedirectLog();
            //
            iNr = GEN_FPRINTF(ptLogfile, "%5d-%s-%5d|[DBG]:", iSeq, pstLog->cLog, getpid());
            GEN_VPRINTF(pcFormat, tArgs);
            iNr += GEN_VFPRINTF(ptLogfile, pcFormat, tArgs);
            if(GEN_FFLUSH(ptLogfile) == EOF)  
            {
               PRINTF("LOG-debug():Flush signals EOF (disk full)!" CRLF);
               iNr = -1;
               ptLogfile = log_RedirectLog();
            }
         }
      }
      else
      {
         if(ulDebugMask & ulCurrentMask)
         {
            RTC_GetDateTimeSecs(pstLog->cLog);
            GEN_Printf("%5d-%s-%5d|[TRC]:", iSeq, pstLog->cLog, getpid());
            iNr = GEN_VPRINTF(pcFormat, tArgs);
         }
      }
      va_end(tArgs);
   }
   return(iNr);
}

//
// Function:   LOG_printf
// Purpose:    Log to stdout with a time stamp
//             
// Parms:      Formatted string
// Returns:    Nr put out, -1 on error
// Note:       CANNOT be used if GLOBAL_MutexLock() is in effect !
//
int  LOG_printf(const char *pcFormat, ...)
{
   int         iNr;
   va_list     tArgs;
   int         iSeq=GLOBAL_GetTraceCode();
   GLOG       *pstLog=GLOBAL_GetLog();

   LOG_ReportLockFile();
   //
   RTC_GetDateTimeSecs(pstLog->cLog);
   iNr = GEN_Printf("%4d-%s %5d| [LOG] ", iSeq, pstLog->cLog, getpid());
   va_start(tArgs, pcFormat);
   iNr += GEN_VPRINTF(pcFormat, tArgs);
   va_end(tArgs);
   //
   LOG_ReportUnlockFile();
   return(iNr);
}

//
// Function:   LOG_TraceOpenFile
// Purpose:    Open a new trace file
//             
// Parms:      Filename
// Returns:    
// Note:
//
bool LOG_TraceOpenFile(char *pcFilename) 
{
   bool     fCc = FALSE;
   GLOG    *pstLog=GLOBAL_GetLog();

   LOG_TraceCloseFile();
   //
   ptTracefile = safefopen(pcFilename, "a+");
   if(ptTracefile)
   {
      pstLog->fTraceOpen = TRUE;
      fCc = TRUE;
   }
   return(fCc);
}

//
// Function:   LOG_Trace
// Purpose:    Trace PRINTF macro's of formatted string to the tracefile
//             
// Parms:      Formatted string
// Returns:    Nr put out, -1 on error
// Note:       CANNOT be used if GLOBAL_MutexLock() is in effect !
//
int LOG_Trace(const char *pcFormat, ...) 
{
   int      iNr, iSize;
   int      iSeq;
   char    *pcTemp;
   va_list  tArgs;
   GLOG    *pstLog=GLOBAL_GetLog();

   if(pthread_mutex_trylock(&pstLog->tTraceMutex))
   {
      PRINTF("LOG-Trace():Locked!" CRLF);
      return(0);
   }

   if(ptTracefile == NULL) ptTracefile = log_RedirectTrace();
   //
   iSeq = GLOBAL_GetTraceCode();
   RTC_GetDateTimeSecs(pstLog->cLog);
   //
   iNr = GEN_FPRINTF(ptTracefile, "%5d-%s-%5d|", iSeq, pstLog->cLog, getpid());
   //
   // Allocate log buffer: reserve extra space for expansion of formatted data
   // Small strings have fixed length, larger strings double their size.
   //
   iSize = GEN_STRLEN(pcFormat);
   if(iSize < LOG_DEF_RECORD_LEN) iSize  = LOG_MAX_RECORD_LENZ;
   else                           iSize *= 2;
   //
   pcTemp = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, iSize);
   va_start(tArgs, pcFormat);
   GEN_VSNPRINTF(pcTemp, iSize-1, pcFormat, tArgs);
   iNr += GEN_FPRINTF(ptTracefile, pcTemp);
   va_end(tArgs);
   SAFEFREE(__PRETTY_FUNCTION__, pcTemp);
   //
   if(GEN_FFLUSH(ptTracefile) == EOF)  
   {
      PRINTF("LOG-Trace():Flush signals EOF (disk full)!" CRLF);
      iNr = -1;
      ptTracefile = log_RedirectTrace();
   }
   pthread_mutex_unlock(&pstLog->tTraceMutex);
   return(iNr);
}

//
// Function:   LOG_TraceFlushFile
// Purpose:    Flush trace file
//             
// Parms:      
// Returns:    
// Note:
//
void LOG_TraceFlushFile(void) 
{
   if(ptTracefile)
   {
      if(GEN_FFLUSH(ptTracefile) == EOF)  
      {
         PRINTF("LOG-TraceFlushFile():Flush signals EOF (disk full)!" CRLF);
         ptTracefile = log_RedirectTrace();
      }
   }
}

//
// Function:   LOG_TraceCloseFile
// Purpose:    Close tracefile
//             
// Parms:      
// Returns:    
// Note:
//
void LOG_TraceCloseFile(void) 
{
   GLOG *pstLog=GLOBAL_GetLog();

   if(ptTracefile)
   {
      safefclose(ptTracefile);
      ptTracefile = NULL;
      pstLog->fTraceOpen = FALSE;
   }
}

//
// Function:   LOG_DumpData
// Purpose:    Dump data hex and ascii to stdout
//
// Parms:      Title, Buffer, size
// Returns:   
// Note:       iAllZero =
//             0: Init
//             1: Non-zero item in buffer
//             2: Zero buffer has been displayed
//
#define LOG_DUMP_COLS            32
#define LOG_DUMP_COLS_LAST       LOG_DUMP_COLS - 1
#define LOG_DUMP_TEXT          ((LOG_DUMP_COLS * 3) + 4)
#define LOG_DUMP_SIZE_HEX      ((LOG_DUMP_COLS * 4) + 16)
#define LOG_DUMP_SIZE_ASC      ((LOG_DUMP_COLS * 1) + 16)
//
void LOG_DumpData(const char *pcTitle, char *pcData, int iLength)
{
   int      iAllZero=0;
   int      x=0, y=0, i, iNr;
   char     cChar;
   char    *pcBuffer;
   char    *pcAscii;

   LOG_printf("%s" CRLF, pcTitle);
   //
   // Alloc and clear
   //
   pcBuffer = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, LOG_DUMP_SIZE_HEX);
   pcAscii  = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, LOG_DUMP_SIZE_ASC);
   GEN_MEMSET(pcBuffer, 0x20, LOG_DUMP_COLS * 3);
   GEN_MEMSET(pcAscii,  0x20, LOG_DUMP_COLS * 1);
   //
   for(i=0; i<iLength; i++)
   {
      if( (cChar = pcData[i]) != 0) iAllZero = 1;
      GEN_SNPRINTF(&pcBuffer[x], 4, "%02X", cChar);
      pcBuffer[x+2] = ' ';
      //
      // Print ASCII 
      //
      if(isprint((int)cChar)) pcAscii[y] = cChar;
      else                    pcAscii[y] = '.';
      //
      if(i % LOG_DUMP_COLS == LOG_DUMP_COLS_LAST) 
      {
         iNr = (i / LOG_DUMP_COLS) * LOG_DUMP_COLS;
         switch(iAllZero)
         {
            case 0:
               // This row has all zeros
               LOG_printf("%04d 00 -->" CRLF, iNr);
               iAllZero = 2;
               break;

            default:
            case 1:
               // This row has actual data
               LOG_printf("%04d %s  (%s)" CRLF, iNr, pcBuffer, pcAscii);
               iAllZero = 0;
               break;

            case 2:
               // Previous content was all zeros: summary is enough for now
               break;
         }
         // Restart new row
         GEN_MEMSET(pcBuffer, 0x20, LOG_DUMP_COLS * 3);
         GEN_MEMSET(pcAscii,  0x20, LOG_DUMP_COLS * 1);
         x = 0;
         y = 0;
      }
      else 
      {
         x += 3;
         y += 1;
      }
   }
   if(x) 
   {
      LOG_printf("%s  (%s)" CRLF, pcBuffer, pcAscii);
   }
   SAFEFREEX(__PRETTY_FUNCTION__, pcAscii);
   SAFEFREEX(__PRETTY_FUNCTION__, pcBuffer);
}

//
// Function:   LOG_ListData
// Purpose:    List data hex and ascii to log-file
//
// Parms:      Title, Buffer, size
// Returns:   
// Note:       iAllZero =
//             0: Init
//             1: Non-zero item in buffer
//             2: Zero buffer has been displayed
//
void LOG_ListData(const char *pcTitle, char *pcData, int iLength)
{
   int      iAllZero=0;
   int      x=0, y=0, i, iNr;
   char     cChar;
   char    *pcBuffer;
   char    *pcAscii;

   LOG_Report(0, "LOG", "%s (%d bytes)", pcTitle, iLength);
   //
   // Alloc and clear
   //
   pcBuffer = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, LOG_DUMP_SIZE_HEX);
   pcAscii  = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, LOG_DUMP_SIZE_ASC);
   GEN_MEMSET(pcBuffer, 0x20, LOG_DUMP_COLS * 3);
   GEN_MEMSET(pcAscii,  0x20, LOG_DUMP_COLS * 1);
   //
   for(i=0; i<iLength; i++)
   {
      if( (cChar = pcData[i]) != 0) iAllZero = 1;
      GEN_SNPRINTF(&pcBuffer[x], 4, "%02X", cChar);
      pcBuffer[x+2] = ' ';
      //
      // Print ASCII 
      //
      if(isprint((int)cChar)) pcAscii[y] = cChar;
      else                    pcAscii[y] = '.';
      //
      if(i % LOG_DUMP_COLS == LOG_DUMP_COLS_LAST) 
      {
         iNr = (i / LOG_DUMP_COLS) * LOG_DUMP_COLS;
         switch(iAllZero)
         {
            case 0:
               // This row has all zeros
               LOG_Report(0, "LOG", "%04d 00 -->", iNr);
               iAllZero = 2;
               break;

            default:
            case 1:
               // This row has actual data
               LOG_Report(0, "LOG", "%04d %s  (%s)", iNr, pcBuffer, pcAscii);
               iAllZero = 0;
               break;

            case 2:
               // Previous content was all zeros: summary is enough for now
               break;
         }
         // Restart new row
         GEN_MEMSET(pcBuffer, 0x20, LOG_DUMP_COLS * 3);
         GEN_MEMSET(pcAscii,  0x20, LOG_DUMP_COLS * 1);
         x = 0;
         y = 0;
      }
      else 
      {
         x += 3;
         y += 1;
      }
   }
   if(x) 
   {
      //
      // The are a few bytes to display at the end
      //
      iLength -= (x / 3);
      LOG_Report(0, "LOG", "%04d %s  (%s)", iLength, pcBuffer, pcAscii);
   }
   SAFEFREEX(__PRETTY_FUNCTION__, pcAscii);
   SAFEFREEX(__PRETTY_FUNCTION__, pcBuffer);
}

/*------  Local functions separator ------------------------------------
__Local_functions_(){};
----------------------------------------------------------------------------*/

//
//  Function:  log_RedirectLog
//  Purpose:   Redirect log file to NULL
//              
//  Parms:     
//  Returns:   FILE ptr   
//  Note:
//
static FILE *log_RedirectLog() 
{
   GLOG *pstLog=GLOBAL_GetLog();

   if(pstLog->fLogOpen)
   {
      if(!pstLog->fLogRedirected)
      {
         //
         // Not yet redirected to stderr: do it now
         //
         RTC_GetDateTimeSecs(pstLog->cLog);
         PRINTF("log-RedirectLog():Redirect to strerr" CRLF);
         LOG_ReportCloseFile();
         GEN_FPRINTF(stderr, "    0-%s-%5d|[LOG]:Redirect Log to NULL" CRLF, pstLog->cLog, getpid());
         pstLog->fLogRedirected = TRUE;
      }
      else PRINTF("log-RedirectLog():Already redirected" CRLF);
   }
   return(stderr);
}

//
//  Function:   log_RedirectTrace
//  Purpose:    Redirect trace file to NULL
//              
//  Parms:     
//  Returns:   FILE ptr   
//  Note:
//
static FILE *log_RedirectTrace() 
{
   GLOG *pstLog=GLOBAL_GetLog();

   if(pstLog->fLogOpen)
   {
      if(!pstLog->fTraceRedirected)
      {
         //
         // Not yet redirected to stderr: do it now
         //
         RTC_GetDateTimeSecs(pstLog->cLog);
         PRINTF("log-RedirecTrace():Redirect to strerr" CRLF);
         LOG_TraceCloseFile();
         GEN_FPRINTF(stderr, "    0-%s-%5d|[LOG]:Redirect TRACE to NULL" CRLF, pstLog->cLog, getpid());
         pstLog->fTraceRedirected = TRUE;
      }
      else PRINTF("log-RedirectTrace():Already redirected" CRLF);
   }
   return(stderr);
}


