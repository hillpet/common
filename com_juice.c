/*  (c) Copyright:  2018...2019  Patrn, Confidential Data 
 *
 *  Workfile:           com_juice.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Library for:
 *                      pijuice functions
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    10 Dec 2018:      Created
 *    14 Dec 2021:      Switch to printx
 * 
 * 
 *
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include "typedefs.h"
#include "config.h"
#include "com_func.h"
#include "com_rtc.h"
#include "com_log.h"
#include "com_json.h"
#include "com_safe.h"
#include "com_juice.h"

//#define USE_PRINTF
#include "printx.h"

static bool pj_GetChargeLevel    (void *, int);
static bool pj_GetStatusBattery  (void *, int);
static bool pj_Get5V             (void *, int);
static bool pj_GetFault          (void *, int);
static bool pj_GetButton         (void *, int);
static bool pj_GetPower          (void *, int);
static bool pj_GetError          (void *, int);
//
static bool pj_GetJsonData       (char *, PJTYPE, const char *, void *, int);
//
static const char *pcTrue        = "True";
static const char *pcFalse       = "False";
static const char *pcData        = "data";
static const char *pcBattery     = "battery";
static const char *Input5vIo     = "powerInput5vIo";
static const char *pcFault       = "isFault";
static const char *pcButton      = "isButton";
static const char *pcPower       = "powerInput";
static const char *pcError       = "error";
//
static const char  cDelimiter    = '\'';
static const char *pcPyCmdBat    = "/usr/local/bin/pj_bat.py";
static const char *pcPyCmdStat   = "/usr/local/bin/pj_stat.py";

/* ======   Local Functions separator ===========================================
void ___GLOBAL_FUNCTIONS(){}
==============================================================================*/

//
// Function:   PJ_GetStatus
// Purpose:    Retrieve the status of the battery
//
// Parms:      Info type, void buffer ^, buffer size
// Returns:    TRUE if OKee
// Note:       
//
bool PJ_GetStatus(PJINF tInfo, void *pvResBuf, int iResLen)
{
   bool     fCc=FALSE;

   switch(tInfo)
   {
      default:
         break;

      case PJ_BATTERY:
         fCc = pj_GetStatusBattery(pvResBuf, iResLen);
         break;

      case PJ_CHARGE:
         fCc = pj_GetChargeLevel(pvResBuf, iResLen);
         break;

      case PJ_5V:
         fCc = pj_Get5V(pvResBuf, iResLen);
         break;

      case PJ_FAULT:
         fCc = pj_GetFault(pvResBuf, iResLen);
         break;

      case PJ_BUTTON:
         fCc = pj_GetButton(pvResBuf, iResLen);
         break;

      case PJ_POWER:
         fCc = pj_GetPower(pvResBuf, iResLen);
         break;

      case PJ_ERROR:
         fCc = pj_GetError(pvResBuf, iResLen);
         break;

   }
   return(fCc);
}


/* ======   Local Functions separator ===========================================
void ___LOCAL_FUNCTIONS(){}
==============================================================================*/

//
// Function:   pj_GetChargeLevel
// Purpose:    Retrieve the chargelevel of the battery
//
// Parms:      void buffer ^, buffer size
// Returns:    TRUE if OKee
// Note:       JSON Object:
//             {'data':78, 'error':'NO_ERROR'}
//
static bool pj_GetChargeLevel(void *pvResBuf, int iResLen)
{
   bool     fCc=FALSE;
   char    *pcObj;
   FILE    *ptFp;

   pcObj = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, 1024);
   ptFp  = popen(pcPyCmdBat, "r");
   //
   if(ptFp)
   {
      PRINTF("pj_GetChargeLevel():Pipe open:%s" CRLF, pcPyCmdBat);
      pcObj = fgets(pcObj, 1023, ptFp);
      fCc   = pj_GetJsonData(pcObj, PJ_TYPE_INT, pcData, pvResBuf, iResLen);
      pclose(ptFp);
   }
   else
   {
      PRINTF("pj_GetChargeLevel():ERROR Pipe open:%s" CRLF, pcPyCmdStat);
   }
   safefree(pcObj);
   return(fCc);
}

//
// Function:   pj_GetStatusBattery
// Purpose:    Retrieve the battery status
//
// Parms:      void buffer ^, buffer size
// Returns:    TRUE if OKee
// Note:       JSON Object:
//             {'data': {'battery': 'CHARGING_FROM_IN', 'powerInput5vIo': 'NOT_PRESENT', 'isFault': True, 
//             'isButton': False, 'powerInput': 'PRESENT'}, 'error': 'NO_ERROR'}
//
static bool pj_GetStatusBattery(void *pvResBuf, int iResLen)
{
   bool     fCc=FALSE;
   char    *pcObj;
   FILE    *ptFp;

   pcObj = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, 1024);
   ptFp  = popen(pcPyCmdStat, "r");
   //
   if(ptFp)
   {
      PRINTF("pj_GetStatusBattery():Pipe open:%s" CRLF, pcPyCmdStat);
      pcObj = fgets(pcObj, 1023, ptFp);
      fCc   = pj_GetJsonData(pcObj, PJ_TYPE_STR, pcBattery, pvResBuf, iResLen);
      pclose(ptFp);
   }
   else
   {
      PRINTF("pj_GetStatusBattery():ERROR Pipe open:%s" CRLF, pcPyCmdStat);
   }
   safefree(pcObj);
   return(fCc);
}

//
// Function:   pj_Get5V
// Purpose:    Retrieve the battery 5V IO input
//
// Parms:      void buffer ^, buffer size
// Returns:    TRUE if OKee
// Note:       JSON Object:
//             {'data': {'battery': 'CHARGING_FROM_IN', 'powerInput5vIo': 'NOT_PRESENT', 'isFault': True, 
//             'isButton': False, 'powerInput': 'PRESENT'}, 'error': 'NO_ERROR'}
//
static bool pj_Get5V(void *pvResBuf, int iResLen)
{
   bool     fCc=FALSE;
   char    *pcObj;
   FILE    *ptFp;

   pcObj = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, 1024);
   ptFp  = popen(pcPyCmdStat, "r");
   //
   if(ptFp)
   {
      PRINTF("pj_Get5V():Pipe open:%s" CRLF, pcPyCmdStat);
      pcObj = fgets(pcObj, 1023, ptFp);
      fCc   = pj_GetJsonData(pcObj, PJ_TYPE_STR, Input5vIo, pvResBuf, iResLen);
      pclose(ptFp);
   }
   else
   {
      PRINTF("pj_Get5V():ERROR Pipe open:%s" CRLF, pcPyCmdStat);
   }
   safefree(pcObj);
   return(fCc);
}

//
// Function:   pj_GetFault
// Purpose:    Retrieve the FAULT status
//
// Parms:      void buffer ^, buffer size
// Returns:    TRUE if OKee
// Note:       JSON Object:
//             {'data': {'battery': 'CHARGING_FROM_IN', 'powerInput5vIo': 'NOT_PRESENT', 'isFault': True, 
//             'isButton': False, 'powerInput': 'PRESENT'}, 'error': 'NO_ERROR'}
//
static bool pj_GetFault(void *pvResBuf, int iResLen)
{
   bool     fCc=FALSE;
   char    *pcObj;
   FILE    *ptFp;

   pcObj = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, 1024);
   ptFp  = popen(pcPyCmdStat, "r");
   //
   if(ptFp)
   {
      PRINTF("pj_GetFault():Pipe open:%s" CRLF, pcPyCmdStat);
      pcObj = fgets(pcObj, 1023, ptFp);
      fCc   = pj_GetJsonData(pcObj, PJ_TYPE_BOOL, pcFault, pvResBuf, iResLen);
      pclose(ptFp);
   }
   else
   {
      PRINTF("pj_GetFault():ERROR Pipe open:%s" CRLF, pcPyCmdStat);
   }
   safefree(pcObj);
   return(fCc);
}

//
// Function:   pj_GetButton
// Purpose:    Retrieve the BUTTON status
//
// Parms:      void buffer ^, buffer size
// Returns:    TRUE if OKee
// Note:       JSON Object:
//             {'data': {'battery': 'CHARGING_FROM_IN', 'powerInput5vIo': 'NOT_PRESENT', 'isFault': True, 
//             'isButton': False, 'powerInput': 'PRESENT'}, 'error': 'NO_ERROR'}
//
static bool pj_GetButton(void *pvResBuf, int iResLen)
{
   bool     fCc=FALSE;
   char    *pcObj;
   FILE    *ptFp;

   pcObj = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, 1024);
   ptFp  = popen(pcPyCmdStat, "r");
   //
   if(ptFp)
   {
      PRINTF("pj_GetButton():Pipe open:%s" CRLF, pcPyCmdStat);
      pcObj = fgets(pcObj, 1023, ptFp);
      fCc   = pj_GetJsonData(pcObj, PJ_TYPE_BOOL, pcButton, pvResBuf, iResLen);
      pclose(ptFp);
   }
   else
   {
      PRINTF("pj_GetButton():ERROR Pipe open:%s" CRLF, pcPyCmdStat);
   }
   safefree(pcObj);
   return(fCc);
}

//
// Function:   pj_GetPower
// Purpose:    Retrieve the Power input status
//
// Parms:      void buffer ^, buffer size
// Returns:    TRUE if OKee
// Note:       JSON Object:
//             {'data': {'battery': 'CHARGING_FROM_IN', 'powerInput5vIo': 'NOT_PRESENT', 'isFault': True, 
//             'isButton': False, 'powerInput': 'PRESENT'}, 'error': 'NO_ERROR'}
//
static bool pj_GetPower(void *pvResBuf, int iResLen)
{
   bool     fCc=FALSE;
   char    *pcObj;
   FILE    *ptFp;

   pcObj = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, 1024);
   ptFp  = popen(pcPyCmdStat, "r");
   //
   if(ptFp)
   {
      PRINTF("pj_GetPower():Pipe open:%s" CRLF, pcPyCmdStat);
      pcObj = fgets(pcObj, 1023, ptFp);
      fCc   = pj_GetJsonData(pcObj, PJ_TYPE_STR, pcPower, pvResBuf, iResLen);
      pclose(ptFp);
   }
   else
   {
      PRINTF("pj_GetPower():ERROR Pipe open:%s" CRLF, pcPyCmdStat);
   }
   safefree(pcObj);
   return(fCc);
}

//
// Function:   pj_GetError
// Purpose:    Retrieve the Power input status
//
// Parms:      void buffer ^, buffer size
// Returns:    TRUE if OKee
// Note:       JSON Object:
//             {'data': {'battery': 'CHARGING_FROM_IN', 'powerInput5vIo': 'NOT_PRESENT', 'isFault': True, 
//             'isButton': False, 'powerInput': 'PRESENT'}, 'error': 'NO_ERROR'}
//
static bool pj_GetError(void *pvResBuf, int iResLen)
{
   bool     fCc=FALSE;
   char    *pcObj;
   FILE    *ptFp;

   pcObj = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, 1024);
   ptFp  = popen(pcPyCmdStat, "r");
   //
   if(ptFp)
   {
      PRINTF("pj_GetError():Pipe open:%s" CRLF, pcPyCmdStat);
      pcObj = fgets(pcObj, 1023, ptFp);
      fCc   = pj_GetJsonData(pcObj, PJ_TYPE_STR, pcError, pvResBuf, iResLen);
      pclose(ptFp);
   }
   else
   {
      PRINTF("pj_GetError():ERROR Pipe open:%s" CRLF, pcPyCmdStat);
   }
   safefree(pcObj);
   return(fCc);
}

//
// Function:   pj_GetJsonData
// Purpose:    Retrieve the JSON object data
//
// Parms:      JSON Object, Type, JSON Fieldname, void buffer ^, buffer size
// Returns:    TRUE if OKee
// Note:       JSON Object:
//             {'data': {'battery': 'CHARGING_FROM_IN', 'powerInput5vIo': 'NOT_PRESENT', 'isFault': True, 
//             'isButton': False, 'powerInput': 'PRESENT'}, 'error': 'NO_ERROR'}
//
static bool pj_GetJsonData(char *pcObj, PJTYPE tType, const char *pcField, void *pvResBuf, int iResLen)
{
   bool     fCc=FALSE;
   bool    *pfRes;
   int     *piRes;
   char    *pcRes;
   double  *pflRes;

   if(pcObj)
   {
      pcRes = GEN_STRSTR(pcObj, pcField);
      if(pcRes)
      {
         pcRes = GEN_FindDelimiter(pcRes, DELIM_COLON);
         if(pcRes) 
         {
            PRINTF("pj_GetJsonData():ObjVar=%s" CRLF, pcObj);
            switch(tType)
            {
               default:
                  break;

               case PJ_TYPE_STR:
                  //
                  // result is:
                  //    {'data': {'battery': 'CHARGING_FROM_IN', 'powerInput5vIo': 'NOT_PRESENT', 'isFault': True, 'isButton': False, 'powerInput': 'PRESENT'}, 'error': 'NO_ERROR'}
                  //                 pcRes ^
                  //
                  pcRes++;
                  pcRes = GEN_FindDelimiter(pcRes, DELIM_QUOTE);
                  if(pcRes)
                  {
                     pcRes++;
                     PRINTF("pj_GetJsonData():STR=%s" CRLF, pcRes);
                     GEN_CopyToDelimiter(cDelimiter, (char *)pvResBuf, pcRes, iResLen);
                     fCc = TRUE;
                  }
                  else PRINTF("pj_GetJsonData():value not found in %s" CRLF, pcObj);
                  break;

               case PJ_TYPE_INT:
                  //
                  // result is:
                  //     {'data': 78, 'error':'NO_ERROR'}
                  //      pcRes ^
                  //
                  pcRes++;
                  pcRes = GEN_FindDelimiter(pcRes, DELIM_NUMERIC);
                  if(pcRes)
                  {
                     PRINTF("pj_GetJsonData():INT=%s" CRLF, pcRes);
                     piRes  = (int *)pvResBuf;
                     *piRes = atoi(pcRes);
                     fCc = TRUE;
                  }
                  else
                  {
                     PRINTF("pj_GetJsonData():value not found in %s" CRLF, pcObj);
                  }
                  break;

               case PJ_TYPE_BOOL:
                  //
                  // result is:
                  //    {'data': {'battery': 'CHARGING_FROM_IN', 'powerInput5vIo': 'NOT_PRESENT', 'isFault': True, 'isButton': False, 'powerInput': 'PRESENT'}, 'error': 'NO_ERROR'}
                  //                                                                                 pcRes ^
                  //
                  pcRes++;
                  PRINTF("pj_GetJsonData():BOOL=%s" CRLF, pcRes);
                  pcRes = GEN_FindDelimiter(pcRes, DELIM_NOSPACE);
                  if(pcRes)
                  {
                     pfRes  = (bool *)pvResBuf;
                     if(GEN_STRNCMPI(pcRes, pcTrue, 4) == 0)
                     {
                        *pfRes = TRUE;
                        fCc    = TRUE;
                     }
                     else 
                     {
                        if( GEN_STRNCMPI(pcRes, pcFalse, 5) == 0)   
                        {
                           *pfRes = FALSE;
                           fCc    = TRUE;
                        }
                     }
                  }
                  break;

               case PJ_TYPE_FLT:
                  //
                  // result is:
                  //     {'data': 78.897, 'error':'NO_ERROR'}
                  //      pcRes ^
                  //
                  pcRes++;
                  pcRes = GEN_FindDelimiter(pcRes, DELIM_NUMERIC);
                  if(pcRes)
                  {
                     PRINTF("pj_GetJsonData():FLT=%s" CRLF, pcRes);
                     pflRes  = (double *)pvResBuf;
                     *pflRes = atof(pcRes);
                     fCc     = TRUE;
                  }
                  else
                  {
                     PRINTF("pj_GetJsonData():value not found in %s" CRLF, pcObj);
                  }
                  break;
            }
         }
         else PRINTF("pj_GetJsonData():delimiter not found in %s" CRLF, pcObj);
      }
      else PRINTF("pj_GetJsonData():Field[%s] not found in %s" CRLF, pcField, pcObj);
   }
   return(fCc);
}

#ifdef COMMENT
/* ======   Local Functions separator ===========================================
void ___COMMENT_OUT_(){}
==============================================================================*/

//
// Function:   pj_GetChargeLevel
// Purpose:    Retrieve the chargelevel of the battery
//
// Parms:      void buffer ^, buffer size
// Returns:    TRUE if OKee
// Note:       JSON Object:
//             {'data':78, 'error':'NO_ERROR'}
//
static bool pj_GetChargeLevel(void *pvResBuf, int iResLen)
{
   bool     fCc=FALSE;
   bool     fFound=FALSE;
   char    *pcLine;
   char    *pcRes;
   int     *piRes;
   FILE    *ptFp;

   pcLine = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, 1024);
   ptFp   = popen(pcPyCmdBat, "r");
   //
   if(ptFp)
   {
      PRINTF("pj_GetChargeLevel():Pipe open:%s" CRLF, pcPyCmdBat);
      do
      {
         pcRes = fgets(pcLine, 1023, ptFp);
         if(pcRes)
         {
            GEN_RemoveChar(pcRes, 0x0a);
            GEN_RemoveChar(pcRes, 0x0d);
            PRINTF("pj_GetChargeLevel():%s" CRLF, pcRes);
            pcRes = GEN_STRSTR(pcRes, "data");
            if(pcRes)
            {
               //
               // result is:
               //    "{'data': 78, 'error':'NO_ERROR'}"
               // pcRes ^
               //
               pcRes = GEN_FindDelimiter(pcRes, DELIM_COLON);
               if(pcRes) 
               {
                  pcRes++;
                  pcRes = GEN_FindDelimiter(pcRes, DELIM_NUMERIC);
                  if(pcRes)
                  {
                     PRINTF("pj_GetChargeLevel():Charge level=%s" CRLF, pcRes);
                     piRes  = (int *)pvResBuf;
                     *piRes = atoi(pcRes);
                     fCc = TRUE;
                  }
                  else
                  {
                     PRINTF("pj_GetChargeLevel():value not found in %s" CRLF, pcLine);
                  }
               }
               else
               {
                  PRINTF("pj_GetChargeLevel():delimiter not found in %s" CRLF, pcLine);
               }
               fFound = TRUE;
            }
            else
            {
               PRINTF("pj_GetChargeLevel():data not found in %s" CRLF, pcLine);
            }
         }
         else fFound = TRUE;
      }
      while(!fFound);
      pclose(ptFp);
   }
   else
   {
      PRINTF("pj_GetChargeLevel():ERROR Pipe open:%s" CRLF, pcPyCmdBat);
   }
   safefree(pcLine);
   return(fCc);
}

//
// Function:   pj_GetStatusBattery
// Purpose:    Retrieve the battery status
//
// Parms:      void buffer ^, buffer size
// Returns:    TRUE if OKee
// Note:       JSON Object:
//             {'data': {'battery': 'CHARGING_FROM_IN', 'powerInput5vIo': 'NOT_PRESENT', 'isFault': True, 
//             'isButton': False, 'powerInput': 'PRESENT'}, 'error': 'NO_ERROR'}
//
static bool pj_GetStatusBattery(void *pvResBuf, int iResLen)
{
   bool     fCc=FALSE;
   bool     fFound=FALSE;
   char    *pcLine;
   char    *pcRes;
   FILE    *ptFp;

   pcLine = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, 1024);
   ptFp   = popen(pcPyCmdStat, "r");
   //
   if(ptFp)
   {
      PRINTF("pj_GetStatusBattery():Pipe open:%s" CRLF, pcPyCmdStat);
      do
      {
         pcRes = fgets(pcLine, 1023, ptFp);
         if(pcRes)
         {
            GEN_RemoveChar(pcRes, 0x0a);
            GEN_RemoveChar(pcRes, 0x0d);
            PRINTF("pj_GetStatusBattery():%s" CRLF, pcRes);
            pcRes = GEN_STRSTR(pcRes, "battery");
            if(pcRes)
            {
               //
               // result is:
               //    {'data': {'battery': 'CHARGING_FROM_IN', 'powerInput5vIo': 'NOT_PRESENT', 'isFault': True, 
               //                                              'isButton': False, 'powerInput': 'PRESENT'}, 'error': 'NO_ERROR'}
               //         pcRes ^
               //
               PRINTF("pj_GetStatusBattery():battery = %s" CRLF, pcRes);
               pcRes = GEN_FindDelimiter(pcRes, DELIM_COLON);
               if(pcRes) 
               {
                  pcRes++;
                  PRINTF("pj_GetStatusBattery():battery: = %s" CRLF, pcRes);
                  pcRes = GEN_FindDelimiter(pcRes, DELIM_QUOTE);
                  if(pcRes)
                  {
                     pcRes++;
                     PRINTF("pj_GetStatusBattery():battery status = %s" CRLF, pcRes);
                     GEN_CopyToDelimiter(cDelimiter, (char *)pvResBuf, pcRes, iResLen);
                     fCc = TRUE;
                  }
                  else
                  {
                     PRINTF("pj_GetStatusBattery():value not found in %s" CRLF, pcLine);
                  }
               }
               else
               {
                  PRINTF("pj_GetStatusBattery():delimiter not found in %s" CRLF, pcLine);
               }
               fFound = TRUE;
            }
            else
            {
               PRINTF("pj_GetStatusBattery():battery not found in %s" CRLF, pcLine);
            }
         }
         else fFound = TRUE;
      }
      while(!fFound);
   }
   pclose(ptFp);
   safefree(pcLine);
   return(fCc);
}



#endif