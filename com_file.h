/*  (c) Copyright:  2005..2024  Patrn, Confidential Data
 *
 *  Workfile:           com_file.h
 *  Purpose:            Offer the API as an abstraction layer for library calls
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Changes:       
 *    02 Jun 2013:      Created
 *    06 Jun 2021:      Add GEN_FileCopy()
 *    06 Nov 2021:      Add GEN_ChangeOwner(), GEN_FileCopyAttributes()
 *    14 Dec 2021:      Add GEN_FileCopyRecords()
 *    19 May 2023:      Change FINFO_GetFileInfo() to return text AND numeric data
 *    02 Jun 2023:      Add GetDirEntriesTimestamp()
 *    28 Mar 2024:      Add GEN_DirectoryCopy()
 *    24 Apr 2024:      Add Dir/File functions
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _COM_FILE_H_
#define _COM_FILE_H_

#define  GEN_RECORD_LEN             1023
#define  GEN_RECORD_LENZ            GEN_RECORD_LEN+1
#define  GEN_BUFFER_LEN             32768
//
#define  FI_TYPE_INT                -1       // Return int     value
#define  FI_TYPE_INT64              -2       // Return int64   value
#define  FI_TYPE_UINT32             -3       // Return u_int32 value
#define  FI_TYPE_UINT64             -4       // Return u_int64 value
//
// Directory/File Attributes
//
#define  ATTR_RW_R__R__       (S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH)
#define  ATTR_RW_RW_R__       (S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH)
#define  ATTR_RWXR_XR_X       (S_IRWXU|S_IRGRP|S_IXGRP|S_IROTH|S_IXOTH)
#define  ATTR_RWXRWXR_X       (S_IRWXU|S_IRWXG|S_IROTH|S_IXOTH)
//
typedef enum PIKS
{
   PIK_ALPHA = 0,
   PIK_DANDT_LH,
   PIK_DANDT_HL,
}  PIKS;
//
typedef enum FINFO
{
   FI_NONE = 0,
   FI_FILEDIR,
   FI_SIZE,
   FI_MDATE,
   FI_UID,
   FI_GUI,
   FI_FILENAME,
   FI_FILEEXT
}  FINFO;
//
typedef struct PIKDIR
{
   bool           fDir;                      // IsDir
   int            iDandT;                    // File Timestamp
   int64          llSize;                    // Size in bytes
   char          *pcName;                    // Node name
   struct PIKDIR *pstNext;                   // Next entry
   struct PIKDIR *pstPrev;                   // Next entry
}  PIKDIR;
//
// Global prototypes
//
int      GEN_AddPid                    (char *);
bool     GEN_ChangeOwner               (const char *, const char *, const char *, mode_t, bool);
bool     GEN_DirectoryExists           (const char *);
int      GEN_DirectoryCopy             (char *, char *);
bool     GEN_DirectoryCreate           (const char *, mode_t, bool);
bool     GEN_FileCopy                  (char *, char *, bool, int64 *);
bool     GEN_FileCopyAttributes        (char *, char *, int);
bool     GEN_FileCopyRecords           (char *, char *, bool, int64 *);
bool     GEN_FileExists                (const char *);
int      GEN_FileTruncate              (char *, int);
void     GEN_FreeDirEntries            (PIKDIR *);
PIKDIR  *GEN_GetDirEntries             (const char *, const char *, int *, int *, PIKS);
bool     GEN_GetFilename               (const char *, const char *, int, char *, int, PIKS);
int      GEN_GetNumberOfFiles          (const char *, const char *);
int      GEN_RemovePid                 (char *);
//
void    *FINFO_GetDirEntries           (char *, const char *, int *);
bool     FINFO_GetFilename             (void *, int, char *, int);
bool     FINFO_GetFileInfo             (char *, FINFO, void *, int);
int      FINFO_GetLineCount            (char *);
int64    FINFO_GetTotalSpace           (char *);
int64    FINFO_GetFreeSpace            (char *);
double   FINFO_GetFreeSpacePercentage  (char *);
void     FINFO_ReleaseDirEntries       (void *, int);
char    *FINFO_SplitPathname           (FINFO, char *);
 
#endif  /*_COM_FILE_H_ */

