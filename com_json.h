/*  (c) Copyright:  2017...2021  Patrn ESS, Confidential Data
 *
 *  Workfile:           com_json.h
 *  Purpose:            Basic JSON Parser functions
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    09 Aug 2017:      Created
 *    11 Oct 2021:      Add GEN_JsonGetDouble{}
 *                          GEN_JsonParseObject()
 *                          GEN_JsonSetLevel()
 *    21 Oct 2021:      Add GEN_JsonGetElementByName()
 *                          GEN_JsonGetArrayEntry()
 *                          GEN_JsonDumpObject()
 *    04 Mar 2022:      Add GEN_JsonGetArrayCopy()
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _COM_JSON_H_
#define _COM_JSON_H_

#define EXCLUDE_DQ   TRUE              // Exclude "" in element names
#define INCLUDE_DQ   FALSE             // Include "" in element names
//
typedef enum JSONT
{
   JSON_TYPE_NONE    = 0,
   JSON_TYPE_OBJECT,
   JSON_TYPE_ARRAY,
   JSON_TYPE_BOOL,
   JSON_TYPE_STRING,
   JSON_TYPE_INTEGER,
   JSON_TYPE_FLOAT,
   //
   JSON_NUMTYPES
}  JSONT;
//
bool     GEN_JsonCheckObject           (char *, int *);
char    *GEN_JsonFindObject            (char *);
JSONT    GEN_JsonGetObjectType         (char *, int);
char    *GEN_JsonGetElementCopy        (char *, int, bool);
int      GEN_JsonGetElementSize        (char *);
char    *GEN_JsonGetObjectCopy         (char *, bool);
int      GEN_JsonGetObjectSize         (char *);
bool     GEN_JsonGetStringCopy         (char *, const char *, char *, int);
//
bool     GEN_JsonGetArrayCopy          (char *, char *, int);
void    *GEN_JsonGetArrayEntry         (char *, int);
JSONT    GEN_JsonGetArrayType          (char *, int);
int      GEN_JsonGetNumberOfElements   (char *);
char    *GEN_JsonGetElement            (char *, int);
char    *GEN_JsonGetElementByName      (char *, const char *);
bool     GEN_JsonGetElementName        (char *, int, char *, int, bool);
char    *GEN_JsonGetElementObject      (char *, int, int);
JSONT    GEN_JsonGetElementType        (char *);
//
bool     GEN_JsonGetElementBoolean     (char *, bool *);
bool     GEN_JsonGetElementInteger     (char *, int *);
bool     GEN_JsonGetElementString      (char *, char *, int);
//
bool     GEN_JsonGetBoolean            (char *, const char *, bool *);
bool     GEN_JsonGetDouble             (char *, const char *, double *);
bool     GEN_JsonGetInteger            (char *, const char *, int *);
bool     GEN_JsonGetString             (char *, const char *, char **);
bool     GEN_JsonGetUnsignedLong       (char *, const char *, u_int32 *);


#endif /* _COM_JSON_H_ */
