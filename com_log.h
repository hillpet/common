/*  (c) Copyright:  2017..2021  Patrn, Confidential Data
 *
 * Workfile:            com_log.h
 * Purpose:             Headerfile for Log
 *                      
 *
 * Compiler/Assembler:  Raspbian Linux GNU gcc
 * Ext Packages:
 * Note:               
 * 
 * Author:              Peter Hillen
 * Changes:
 *    17 Jul 2012       Created
 *    09 Nov 2019       Change Global LOG struct
 *    22 Nov 2020:      Add LOG_DumpData() to stdout
 *    29 May 2021:      Add LOG_ListData() to log-file
 *    14 Sep 2023:      LOG_Report() is const char *
 *
 *
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _COM_LOG_H_
#define _COM_LOG_H_

#define LOG_DEF_RECORD_LEN      256
#define LOG_MAX_RECORD_LEN      2047
#define LOG_MAX_RECORD_LENZ     (LOG_MAX_RECORD_LEN + 1)
#define LOG_MAX_PATHNAME        32
#define FORMATTED_LONG_SIZE     24                      // 123.456.789.012

//
// LOG the assert :
//
// Add to the makefile
// -D DEBUG_ASSERT
//
#if      defined(DEBUG_ASSERT)
void     LOG_Assert                 (char *, bool);
#define  LOG_ASSERT(a,b)            LOG_Assert(a,b)
#define  LOG_DEBUG1(a,b)            LOG_debug(a,b)
#define  LOG_DEBUG2(a,b,c)          LOG_debug(a,b,c)
#define  LOG_DEBUG3(a,b,c,d)        LOG_debug(a,b,c,d)
#define  LOG_DEBUG4(a,b,c,d,e)      LOG_debug(a,b,c,d,e)
#define  LOG_DEBUG5(a,b,c,d,e,f)    LOG_debug(a,b,c,d,e,f)
#else
#define  LOG_ASSERT(a,b)
#define  LOG_DEBUG1(a,b)
#define  LOG_DEBUG2(a,b,c)
#define  LOG_DEBUG3(a,b,c,d)
#define  LOG_DEBUG4(a,b,c,d,e)
#define  LOG_DEBUG5(a,b,c,d,e,f)
#endif
//
// LOG_debug output to file
//
#define  DEBUG_FILE              0x80000000L
//
typedef struct _glog_
{
   pthread_mutex_t   tLogMutex;
   bool              fLogOpen;
   bool              fLogRedirected;
   //
   pthread_mutex_t   tTraceMutex;
   bool              fTraceOpen;
   bool              fTraceRedirected;
   //
   char              cLog[DATE_TIME_SIZE];
}  GLOG;
//
bool     LOG_Init                   (void);
char   **LOG_SegmentationFault      (const char *, int);
//
int      LOG_Report                 (int, const char *, const char *, ...);
void     LOG_ReportCloseFile        (void);
void     LOG_ReportFlushFile        (void);
int64    LOG_ReportFilePosition     (void);
void     LOG_ReportLockFile         (void);
bool     LOG_ReportOpenFile         (char *);
void     LOG_ReportUnlockFile       (void);
int      LOG_debug                  (u_int32, const char *, ...);
int      LOG_printf                 (const char *, ...);
//
bool     LOG_TraceOpenFile          (char *);
int      LOG_Trace                  (const char *, ...);
void     LOG_TraceFlushFile         (void);
void     LOG_TraceCloseFile         (void);
void     LOG_DumpData               (const char *, char *, int);
void     LOG_ListData               (const char *, char *, int);
 
#endif  /*_COM_LOG_H_ */

