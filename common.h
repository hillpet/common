/*  (c) Copyright:  2017..2019  Patrn, Confidential Data
 *
 * Workfile:            common.h
 * Purpose:             Headerfile for libcommon.a
 *                      
 *
 * Compiler/Assembler:  Raspbian Linux GNU gcc
 * Ext Packages:
 * Note:               
 * 
 * Author:              Peter Hillen
 * Changes:
 *    05 Sep 2017       Created
 *    09 Nov 2019       Change Global LOG struct
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _COMMON_H_
#define _COMMON_H_

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <semaphore.h>

#include "typedefs.h"
#include "com_file.h"
#include "com_func.h"
#include "com_juice.h"
#include "com_json.h"
#include "com_rtc.h"
#include "com_log.h"
#include "com_safe.h"
#include "com_http.h"
#include "com_nmap.h"

#endif /* _COMMON_H_ */
