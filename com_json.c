/*  (c) Copyright:  2017...2021  Patrn ESS, Confidential Data
 *
 *  Workfile:           com_json.c
 *  Purpose:            Basic JSON Parser functions
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    09 Aug 2017:      Created
 *    11 Oct 2021:      Add GEN_JsonGetDouble{}
 *                          GEN_JsonParseObject()
 *                          GEN_JsonSetLevel()
 *    21 Oct 2021:      Add GEN_JsonGetElementByName()
 *                          GEN_JsonGetArrayEntry()
 *    19 Nov 2021:      Add Variadic macro's for PRINTF()
 *    11 Feb 2022:      Make sure to find correct element name using GEN_STRSTR(..)
 *    07 Mar 2022:      Replace atoi() by strtol()
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>

#include "typedefs.h"
#include "config.h"
#include "com_safe.h"
#include "com_func.h"
#include "com_rtc.h"
#include "com_log.h"
#include "com_json.h"

//#define USE_PRINTF
#include "printx.h"

//
// Local prototypes
//
static bool    gen_JsonCopyElementName    (char *, char *, int, bool);
static char   *gen_JsonGetElementObject   (char *, int);
static JSONT   gen_JsonGetType            (char *);

/*------  Local functions separator -------------------------------------------
__OBJECT_FUNCTIONS________________(){};
--------------------------------------;---------------------------------------*/

//  
// Function:   GEN_JsonCheckObject
// Purpose:    Parse JSON object and return the max levels
// 
// Parameters: JSON object, Result^
// Returns:    TRUE if object is OK
// Note:       
//    pcObject-> 0    { 
//               1       "1":
//               1       {
//               2          "state":
//               2          {
//               3             "on":true,"bri":100,"hue":12000,"sat":254,"effect":"none",
//               3             "xy":[0.4400,0.5228],"ct":342,"alert":"select","colormode":"hs",
//               3             "mode":"homeautomation","reachable":true
//               3          },
//               2          "swupdate":
//               2          {
//               3             "state":"notupdatable","lastinstall":"2021-10-07T18:42:30"
//               3          },
//               2          "type":"Extended color light","name":"Tint nr 2","modelid":"ZBT-ExtendedColor",
//               2          "manufacturername":"MLI","productname":"Extended color light","capabilities":
//               2          {
//               3             "certified":false,"control":
//               3             {
//               4                "colorgamuttype":"other","colorgamut":[[0.6800,0.3100],[0.1100,0.8200],[0.1300,0.0400]],
//               4                "ct":
//               4                {
//               5                   "min":153,"max":556
//               5                }
//               4             },
//               3             "streaming":
//               3             {
//               4                "renderer":false,"proxy":false
//               4             }
//               3          },
//               2          "config":
//               2          {
//               3             "archetype":"classicbulb","function":"mixed","direction":"omnidirectional"
//               3          },
//               2          "uniqueid":"00:15:8d:00:02:cb:c1:ac-01","swversion":"2.0"
//               2       }
//               1    }
//               0
//             Level
//  
bool GEN_JsonCheckObject(char *pcObject, int *piMaxLevel)
{
   int   iMax=0, iCbLev=0, iSqLev=0;

   if(pcObject == NULL) return(FALSE);
   //
   while(*pcObject)
   {
      switch(*pcObject++)
      {
         case '{':
            iCbLev++;
            if(iCbLev > iMax) iMax = iCbLev;
            break;

         case '}':
            iCbLev--;
            break;

         case '[':
            iSqLev++;
            break;

         case ']':
            iSqLev--;
            break;

         default:
            break;
      }
   }
   if(piMaxLevel) *piMaxLevel = iMax;
   return( (iCbLev==0) && (iSqLev==0) );
}

//  
// Function:   GEN_JsonGetObjectType
// Purpose:    Parse JSON object and return correct element type
// 
// Parameters: JSON (sub)object, Level
// Returns:    Object ptr or NULL
// Note:       
//  pcObject-> { 
//                "1":
//                {
//                   "state":
//                   {
//                      "on":true,"bri":100,"hue":12000,"sat":254,"effect":"none",
//                      "xy":[0.4400,0.5228],"ct":342,"alert":"select","colormode":"hs",
//                      "mode":"homeautomation","reachable":true
//                   },
//                   "swupdate":
//                   ......
//                }
//             }
//  
JSONT GEN_JsonGetObjectType(char *pcObject, int iIndex)
{
   JSONT tType=JSON_TYPE_NONE;

   pcObject = GEN_JsonGetElement(pcObject, iIndex);
   pcObject = GEN_FindDelimiter(pcObject, DELIM_COLON);
   tType    = gen_JsonGetType(pcObject);
   return(tType);
}

//
//  Function:  GEN_JsonFindObject
//  Purpose:   Find the JSON object in a HTTP reply
//
//  Parms:     HTTP Reply
//  Returns:   JSON Object ptr
//  Note:      HTTP Reply header is terminated with 2x CRLF.
//
char *GEN_JsonFindObject(char *pcHttp)
{
   bool  fSearching=TRUE;
   int   iNumCrLfs=0;
   char *pcObj=NULL;

   while(fSearching)
   {
      switch(*pcHttp++)
      {
         case '\0':
            fSearching = FALSE;
            break;

         case '\r':
         case '\n':
            if(++iNumCrLfs == 4) 
            {
               pcObj      = pcHttp;
               fSearching = FALSE;
            }
            break;

         default:
            iNumCrLfs = 0;
            break;
      }
   }
   return(pcObj);
}

//  
// Function:   GEN_JsonGetArrayCopy
// Purpose:    Parse JSON object and copy the whole array
// 
// Parameters: JSON object, Elementname, Buffer, size
// Returns:    TRUE if parameter found and converted
// Note:       
//   pcObject->{ 
//                "1":
//                {
//                   "state":
//                   {
//                      "on":true,"bri":100,"hue":12000,"sat":254,"effect":"none",
//                      "xy":[0.4400,0.5228],"ct":342,"alert":"select",
//                      "colormode":"hs","mode":"homeautomation","reachable":true
//                   },
//                   "swupdate":
//                   ......
//                }
//             }
//  
bool GEN_JsonGetArrayCopy(char *pcSrc, char *pcDst, int iSize)
{
   bool  fCc=FALSE;
   
   //
   // "xy":[0.4400,0.5228],"ct":342,"alert":"select",
   //       ^pcSrc
   //
   while(iSize && pcSrc)
   {
      switch(*pcSrc)
      {
         case '\0':
            PRINTF("GEN-JsonGetArrayCopy():Premature EOS!" CRLF);
            // FALLTHROUGH
         case ']':
            *pcDst = '\0';
            iSize  = 0;
            fCc    = TRUE;
            break;

         default:
            *pcDst++ = *pcSrc++;
            *pcDst   = '\0';
            iSize--;
            break;
      }
   }
   return(fCc);    
}

//  
// Function:   GEN_JsonGetElementCopy
// Purpose:    Get a copy of a complete JSON element
//             Print it if not to be stored
// Parameters: JSON (sub)object, Idx, Opt Store yes/no
// Returns:    Ptr to opt dest or NULL
// Note:       Caller MUST free the element memory if Store=YES
//             { 
//                "state":
//   pcObject->   {
//                   "xyz":[0.44,  0.5228, 0.1234],
//        Elm->      "abd":[44,    28,     1234],
//                   "efg":["aap", "noot", "mies"],
//                },
//                ..
//             }    
//
char *GEN_JsonGetElementCopy(char *pcObj, int iIdx, bool fStore)
{
   int   x, iSize;
   char *pcBuf=NULL;
   char *pcRes;

   pcRes = GEN_JsonGetElement(pcObj, iIdx);
   if(pcRes)
   {
      iSize = GEN_JsonGetElementSize(pcRes);
      pcBuf = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, iSize+1);
      //
      //PRINTF("GEN-JsonGetElementCopy():Size=%d bytes." CRLF, iSize);
      //
      for(x=0; x<iSize; x++)
      {
         pcBuf[x] = *pcRes++;
      }
      pcBuf[x] = 0;
      //
      // Either delete buffer or pass to caller
      //
      if(!fStore) 
      {
         GEN_Printf("%s" CRLF2, pcBuf);
         pcBuf = SAFEFREEX(__PRETTY_FUNCTION__, pcBuf);
      }
   }
   return(pcBuf);
}

//  
// Function:   GEN_JsonGetElementSize
// Purpose:    Determine the JSON element size in bytes
// 
// Parameters: JSON (sub)object
// Returns:    Size
// Note:       
//             "state":
//             {
//                "xyz":[0.44,  0.5228, 0.1234],
//     pcElm->    "abd":[44,    28,     1234],
//                "efg":["aap", "noot", "mies"],
//             },
//             ..
//
int GEN_JsonGetElementSize(char *pcElm)
{
   bool  fSearch=TRUE;
   int   iActSize=0,iSize=0;
   int   iCbLevel=0;
   int   iSqLevel=0;

   if(pcElm == NULL) return(0);
   //
   pcElm = GEN_FindDelimiter(pcElm, DELIM_DQUOTE);
   if(pcElm == NULL) return(0);
   //
   while(fSearch)
   {
      switch(*pcElm++)
      {
         case '\0':
            fSearch = FALSE;
            break;

         case '[':
            iSqLevel++;
            iSize++;
            break;

         case ']':
            iSqLevel--;
            iSize++;
            break;

         case '{':
            iCbLevel++;
            iSize++;
            break;

         case '}':
            iCbLevel--;
            if( (iSqLevel == 0) && (iCbLevel < 0) )
            {
               // Done
               iActSize = iSize;
               fSearch  = FALSE;
            }
            else iSize++;
            break;

         case ',':
            if(iSqLevel == 0)
            {
               // Done
               iActSize = iSize;
               fSearch  = FALSE;
            }
            else iSize++;
            break;

         default:
            iSize++;
            break;
      }
   }
   return(iActSize);
}

//  
// Function:   GEN_JsonGetObjectCopy
// Purpose:    Get a copy of a complete JSON object
//             Print it if not to be stored
// Parameters: JSON (sub)object, Opt Store yes/no
// Returns:    Ptr to opt dest or NULL
// Note:       Caller MUST free the object memory if Store=YES
//             { 
//                "state":
//   pcObject->   {
//                   "xyz":[0.44,  0.5228, 0.1234],
//                   "abd":[44,    28,     1234],
//                   "efg":["aap", "noot", "mies"],
//                },        |      |
//                ......    |      |
//             }            +------+--return pointer
//
char *GEN_JsonGetObjectCopy(char *pcObject, bool fStore)
{
   int   x, iSize;
   char *pcBuffer;

   iSize    = GEN_JsonGetObjectSize(pcObject);
   pcBuffer = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, iSize+1);
   //
   //PRINTF("GEN_JsonGetObjectCopy():Size=%d bytes." CRLF, iSize);
   //
   for(x=0; x<iSize; x++)
   {
      pcBuffer[x] = *pcObject++;
   }
   pcBuffer[x] = 0;
   //
   // Either delete buffer or pass to caller
   //
   if(!fStore) 
   {
      GEN_Printf("%s" CRLF2, pcBuffer);
      pcBuffer = SAFEFREEX(__PRETTY_FUNCTION__, pcBuffer);
   }
   return(pcBuffer);
}

//  
// Function:   GEN_JsonGetObjectSize
// Purpose:    Determine the JSON object size in bytes
// 
// Parameters: JSON (sub)object
// Returns:    Size
// Note:       
//  pcObject-> { 
//                "state":
//                {
//                   "xyz":[0.44,  0.5228, 0.1234],
//                   "abd":[44,    28,     1234],
//                   "efg":["aap", "noot", "mies"],
//                },
//                ..
//             } 
//
int GEN_JsonGetObjectSize(char *pcObject)
{
   bool  fSearch=TRUE;
   int   iSize=0;
   int   iCbLevel=0;

   if(pcObject == NULL) return(0);

   while(fSearch)
   {
      switch(*pcObject++)
      {
         case '\0':
            fSearch = FALSE;
            break;

         case '{':
            iCbLevel++;
            iSize++;
            break;

         case '}':
            iSize++;
            if(--iCbLevel == 0)
            {
               // Done
               fSearch = FALSE;
            }
            break;

         default:
            iSize++;
            break;
      }
   }
   return(iSize);
}

//  
// Function:   GEN_JsonGetStringCopy
// Purpose:    Parse JSON object and copy the string value
// 
// Parameters: JSON object, Elementname, Buffer, size
// Returns:    TRUE if parameter found and converted
// Note:       
//   pcObject->{ 
//                "1":
//                {
//                   "state":
//                   {
//                      "on":true,"bri":100,"hue":12000,"sat":254,"effect":"none",
//                      "xy":[0.4400,0.5228],"ct":342,"alert":"select",
//   pcElement->        "colormode":"hs","mode":"homeautomation","reachable":true
//                   },
//                   "swupdate":
//                   ......
//                }
//             }
//  
bool GEN_JsonGetStringCopy(char *pcObject, const char *pcElement, char *pcDst, int iSize)
{
   bool  fCc=FALSE;
   char *pcSrc;
   
   //
   // "colormode":"hs","mode":"homeautomation","reachable":true
   //  ^pcElement
   // Make sure to include the "" in order to find the exact element name, not just part of it !
   //
   PRINTF("GEN-JsonGetStringCopy():Element=%s" CRLF, pcElement);
   pcSrc = GEN_STRSTR((const char *)pcObject, pcElement); 
   if(pcSrc)
   {
      pcSrc = GEN_FindDelimiter(pcSrc, DELIM_COLON);
      if(pcSrc)
      {
         pcSrc = GEN_FindDelimiter(pcSrc, DELIM_DQUOTE);
         PRINTF("GEN-JsonGetStringCopy():Element value=%s" CRLF, pcSrc);
         while(iSize && pcSrc)
         {
            pcSrc++;
            switch(*pcSrc)
            {
               case '\0':
                  PRINTF("GEN-JsonGetStringCopy():Premature EOS!" CRLF);
                  // FALLTHROUGH
               case '\"':
                  *pcDst = '\0';
                  iSize  = 0;
                  fCc    = TRUE;
                  break;

               default:
                  *pcDst++ = *pcSrc;
                  *pcDst   = '\0';
                  iSize--;
                  break;
            }
         }
      }
      else PRINTF("GEN-JsonGetStringCopy():Colon not found!" CRLF);
   }
   else PRINTF("GEN-JsonGetStringCopy():Element %s not found!" CRLF, pcElement);
   return(fCc);    
}


/*------  Local functions separator -------------------------------------------
__ELEMENT_FUNCTIONS___________________(){};
--------------------------------------;---------------------------------------*/

//  
// Function:   GEN_JsonGetArrayEntry
// Purpose:    Get the array element ptr
// 
// Parameters: JSON (sub)object
// Returns:    Ptr to array element
// Note:       { 
//                "state":
//     Object->   {
//                   "xyz":[0.44,  0.5228, 0.1234],
//                   "abd":[44,    28,     1234],
//                   "efg":["aap", "noot", "mies"],
//                },        |      |
//                ......    |      |
//             }            +------+--return pointer
//
void *GEN_JsonGetArrayEntry(char *pcObject, int iIndex)
{
   pcObject = GEN_JsonGetElement(pcObject, iIndex);
   pcObject = GEN_FindDelimiter(pcObject, DELIM_SQRB);
   return(++pcObject);
}

//  
// Function:   GEN_JsonGetArrayType
// Purpose:    Parse JSON object and return correct array type
// 
// Parameters: JSON (sub)object, Level
// Returns:    JSONT Type
// Note:       .....
//   pcObject->   {
//                   "xyz":[0.44,  0.5228, 0.1234],
//                   "abd":[44,    28,     1234],
//                   "efg":["aap", "noot", "mies"],
//                },        |      |
//                ......    |      |
//             }            +------+--return pointer
//
JSONT GEN_JsonGetArrayType(char *pcObject, int iIndex)
{
   JSONT tType=JSON_TYPE_NONE;

   pcObject = GEN_JsonGetElement(pcObject, iIndex);
   pcObject = GEN_FindDelimiter(pcObject, DELIM_SQRB);
   tType    = gen_JsonGetType(pcObject);
   return(tType);
}

//  
// Function:   GEN_JsonGetNumberOfElements
// Purpose:    Get number of JSON elements at a this object level
// 
// Parameters: JSON (sub)object
// Returns:    Nr, -1 is bad format.
// Note:       
//  pcObject-> { 
//                "1":
//                {
//                   "state":
//                   {
//                      "on":true,"bri":100,"hue":12000,"sat":254,"effect":"none",
//                      "xy":[0.4400,0.5228],"ct":342,"alert":"select","colormode":"hs",
//                      "mode":"homeautomation","reachable":true
//                   },
//                   "swupdate":
//                   ......
//                }
//             }
//
int GEN_JsonGetNumberOfElements(char *pcObject)
{
   bool  fSearch=TRUE;
   bool  fDquote=FALSE;
   bool  fEscape=FALSE;
   int   iNr=0, iCbLevel=0, iSbLevel=0;

   if(pcObject == NULL) return(-1);
   //
   //
   // Count all elements "xxx" until corresponding "}"
   //
   while(fSearch)
   {
      switch(*pcObject++)
      {
         case '\0':
            // Unexpected EOF
            PRINTF("gen-JsonGetNumberOfElements():ERROR" CRLF);
            fSearch  = FALSE;
            iNr = -1;
            break;

         case '[':
            // Embedded array: skip
            iSbLevel++;
            fEscape = FALSE;
            break;
         
         case ']':
            // End of Embedded array
            iSbLevel--;
            fEscape = FALSE;
            break;
         
         case '\\':
            fEscape = TRUE;
            break;

         case '\"':
            if(fDquote == FALSE) fDquote = TRUE;
            else                 fDquote = FALSE;
            fEscape = FALSE;
            break;

         case ':':
            if(!fEscape)
            {
               if(!fDquote)
               {
                  // 1st quote
                  if( (iSbLevel == 0) && (iCbLevel == 1) )
                  {
                     iNr++;
                  }
               }
            }
            fEscape = FALSE;
            break;
         
         case '{':
            iCbLevel++;
            fEscape = FALSE;
            break;

         case '}':
            if(--iCbLevel == 0)
            {
               // Done
               fSearch  = FALSE;
            }
            fEscape = FALSE;
            break;

         default:
            fEscape = FALSE;
            break;
      }
   }
   return(iNr);
}

//  
// Function:   GEN_JsonGetElement
// Purpose:    Get JSON element at this object level
// 
// Parameters: JSON (sub)object, Index 0..?
// Returns:    Element ptr or NULL
// Note:       pcObject->  { 
//             Element(0)->   "1":
//                            {
//                               "state":
//                               {
//                                  "on":true,"bri":100,"hue":12000,"sat":254,"effect":"none",
//                                  "xy":[0.4400,0.5228],"ct":342,"alert":"select","colormode":"hs",
//                                  "mode":"homeautomation","reachable":true
//                               },
//                               "swupdate":
//                               ......
//                            },
//             Element(1)->   "2":
//                            {
//                               ......
//                         }
//  
char *GEN_JsonGetElement(char *pcObject, int iIndex)
{
   bool  fSearch=TRUE;
   int   iCurIdx=0, iCbLevel=0, iSbLevel=0;
   char *pcElm=NULL;

   if(pcObject == NULL) return(NULL);
   //
   //
   // Skip all element at this level until we have the right one
   //
   while(fSearch)
   {
      switch(*pcObject)
      {
         case '\0':
            // Unexpected EOF
            PRINTF("GEN-JsonGetElement():ERROR:Idx=%d" CRLF, iIndex);
            fSearch  = FALSE;
            break;

         case ',':
            if( (iSbLevel == 0) && (iCbLevel == 1) )
            {
               iCurIdx++;
            }
            break;

         case '[':
            // Embedded array: skip
            iSbLevel++;
            break;
         
         case ']':
            // End of Embedded array
            iSbLevel--;
            break;
         
         case '\"':
            if( (iSbLevel == 0) && (iCbLevel == 1) && (iCurIdx == iIndex) )
            {
               pcElm   = pcObject;
               fSearch = FALSE;
            }
            break;

         case '{':
            iCbLevel++;
            break;

         case '}':
            if(--iCbLevel == 0)
            {
               // End of Object
               fSearch  = FALSE;
            }
            break;

         default:
            break;
      }
      pcObject++;
   }
   return(pcElm);
}

//  
// Function:   GEN_JsonGetElementByName
// Purpose:    Get JSON element by name
// 
// Parameters: JSON (sub)object, element name
// Returns:    Element ptr or NULL
// Note:       Caller MUST free element memory!
//  pcObject-> { 
//                "on":true,
//                "bri":100,
//                "hue":12000,
//                "sat":254,
//  pcElm->       "effect":"none",
//                "xy":[0.4400,0.5228],
//                "ct":342,
//                ......
//             }
//
char *GEN_JsonGetElementByName(char *pcObj, const char *pcName)
{
   int   iIdx, iNr, iLen;
   char *pcElm=NULL;

   iLen = GEN_STRLEN(pcName);
   iNr  = GEN_JsonGetNumberOfElements(pcObj);
   //PRINTF("GEN-JsonGetElementByName():Nr of Elm=%d, Search for [%s]" CRLF, iNr, pcName);
   //
   for(iIdx=0; iIdx<iNr; iIdx++)
   {
      pcElm = GEN_JsonGetElementCopy(pcObj, iIdx, TRUE);
      if(pcElm)
      {
         if(GEN_STRNCMP(&pcElm[1], pcName, iLen) == 0) 
         {
            PRINTF("GEN-JsonGetElementByName():Found:%d %s" CRLF, iIdx, pcElm);
            break;
         }
         pcElm = SAFEFREEX(__PRETTY_FUNCTION__, pcElm);
      }
   }
   return(pcElm);
}

//  
// Function:   GEN_JsonGetElementName
// Purpose:    Parse JSON object and return element name
// 
// Parameters: JSON (sub)object, element index 0...?, buffer, size, in/exclude double quotes
// Returns:    TRUE if OK
// Note:       
//  pcObject-> { 
//                "1":
//                {
//                   "state":
//                   {
//                      "on":true,"bri":100,"hue":12000,"sat":254,"effect":"none",
//                      "xy":[0.4400,0.5228],"ct":342,"alert":"select","colormode":"hs",
//                      "mode":"homeautomation","reachable":true
//                   },
//                   "swupdate":
//                   ......
//                }
//             }
//  
bool GEN_JsonGetElementName(char *pcObject, int iIndex, char *pcName, int iSize, bool fExclude)
{
   bool  fCc=FALSE;
   char *pcElm;
   
   if(pcObject == NULL) return(FALSE);
   //
   pcElm = GEN_JsonGetElement(pcObject, iIndex);
   fCc   = gen_JsonCopyElementName(pcElm, pcName, iSize, fExclude);
   return(fCc);
}

//  
// Function:   GEN_JsonGetElementObject
// Purpose:    Find an nested element object 
// 
// Parameters: JSON object, Element index, Level
// Returns:    Object ptr or NULL
// Note:       Returned Json Object ptr:
//             Object->
//             Obj(L=0)->  { 
//                            "1":
//             Obj(L=1)->     {
//                               "state":
//             Obj(L=2)          {
//                         I=0      "on":true,"bri":100,"hue":12000,"sat":254,"effect":"none",
//                         I=1      "xy":[0.4400,0.5228],"ct":342,"alert":"select","colormode":"hs",
//                         I=2      "mode":"homeautomation","reachable":true
//                               },
//                               "swupdate":
//                               ......
//                            }
//                         },
//                         ...... next elements
//
char *GEN_JsonGetElementObject(char *pcObject, int iIndex, int iLevel)
{
   char *pcElm;

   pcElm = GEN_JsonGetElement(pcObject, iIndex);
   pcElm = gen_JsonGetElementObject(pcElm, iLevel);
   return(pcElm);
}

//  
// Function:   GEN_JsonGetElementType
// Purpose:    Parse JSON object and return correct element type
// 
// Parameters: JSON (sub)object, Level
// Returns:    Object ptr or NULL
// Note:       
//             { 
//                "1":
//                {
//                   "state":
//                   {
//                      "on":true,"bri":100,"hue":12000,"sat":254,"effect":"none",
//       pcElm-->       "xy":[0.4400,0.5228],"ct":342,"alert":"select","colormode":"hs",
//                      "mode":"homeautomation","reachable":true
//                   },
//                   "swupdate":
//                   ......
//                }
//             }
//  
JSONT GEN_JsonGetElementType(char *pcElm)
{
   JSONT tType=JSON_TYPE_NONE;

   pcElm = GEN_FindDelimiter(pcElm, DELIM_COLON);
   tType = gen_JsonGetType(pcElm);
   return(tType);
}


/*------  Local functions separator -------------------------------------------
__VALUE_FUNCTIONS___________________(){};
--------------------------------------;---------------------------------------*/

//  
// Function:   GEN_JsonGetBoolean
// Purpose:    Parse JSON object and return a bool value
// 
// Parameters: JSON object ptr, Elementname, Result^
// Returns:    TRUE if bool found
// Note:       
//  pcObject-> { 
//                "1":
//                {
//                   "state":
//                   {
//                      "on":true,"bri":100,"hue":12000,"sat":254,"effect":"none",
//  pcElement->         "xy":[0.4400,0.5228],"ct":342,"alert":"select","colormode":"hs",
//                      "mode":"homeautomation","reachable":true
//                   },
//                   "swupdate":
//                   ......
//                }
//             }
//  
bool GEN_JsonGetBoolean(char *pcObject, const char *pcElement, bool *pfResult)
{
   bool  fCc=FALSE;
   char *pcSrc;
   
   //
   // "colormode":"hs","mode":"homeautomation","reachable":true
   //  ^pcElement
   // Make sure to include the "" in order to find the exact element name, not just part of it !
   //
   pcSrc = GEN_STRSTR((const char *)pcObject, pcElement); 
   if(pcSrc)
   {
      //PRINTF("GEN_JsonGetBoolean(): Obj=%s: Parm=%s Found:%s" CRLF, pcObject, pcElement, pcSrc);
      pcSrc = GEN_FindDelimiter(pcSrc, DELIM_COLON);
      if(pcSrc)
      {
         pcSrc++;
         pcSrc = GEN_FindDelimiter(pcSrc, DELIM_NOSPACE);
         if(pcSrc)
         {
            
            if     (GEN_STRNCMPI(pcSrc, "false", 5) == 0) { *pfResult = FALSE; fCc = TRUE; }
            else if(GEN_STRNCMPI(pcSrc, "true",  4) == 0) { *pfResult = TRUE;  fCc = TRUE; }
            //else PRINTF("GEN_JsonGetBoolean(): Obj=%s: Parm=%s Found:%s NO BOOLEAN" CRLF, pcObject, pcElement, pcSrc);
         }
         //else PRINTF("GEN_JsonGetBoolean(): Obj=%s: Parm=%s Found:%s NO DELIM_NOSPACE" CRLF, pcObject, pcElement, pcSrc);
      }
      //else PRINTF("GEN_JsonGetBoolean(): Obj=%s: Parm=%s Found:%s NO DELIM_COLON" CRLF, pcObject, pcElement, pcSrc);
   }
   //else PRINTF("GEN_JsonGetBoolean(): Obj=%s: Parm %s Not found" CRLF, pcObject, pcElement);
   return(fCc);    
}

//  
// Function:   GEN_JsonGetElementBoolean
// Purpose:    Return the bool value of an element
// 
// Parameters: JSON element ptr, Result^
// Returns:    TRUE if bool found
// Note:       
//             { 
//                "1":
//                {
//                   "state":
//                   {
//            pcElm->   "on":true,"bri":100,"hue":12000,"sat":254,"effect":"none",
//                      "xy":[0.4400,0.5228],"ct":342,"alert":"select","colormode":"hs",
//                      "mode":"homeautomation","reachable":true
//                   },
//                   "swupdate":
//                   ......
//                }
//             }
//  
bool GEN_JsonGetElementBoolean(char *pcElm, bool *pfResult)
{
   bool  fCc=FALSE;
   char *pcSrc;
   
   pcSrc = GEN_FindDelimiter(pcElm, DELIM_COLON);
   if(pcSrc)
   {
      pcSrc++;
      pcSrc = GEN_FindDelimiter(pcSrc, DELIM_NOSPACE);
      if(pcSrc)
      {
         
         if     (GEN_STRNCMPI(pcSrc, "false", 5) == 0) { *pfResult = FALSE; fCc = TRUE; }
         else if(GEN_STRNCMPI(pcSrc, "true",  4) == 0) { *pfResult = TRUE;  fCc = TRUE; }
      }
   }
   return(fCc);    
}

//  
// Function:   GEN_JsonGetDouble
// Purpose:    Parse JSON object and return a double float value
// 
// Parameters: JSON object, Elementname, result^
// Returns:    TRUE if parameter found and converted
// Note:       
//  pcObject-> { 
//                "1":
//                {
//                   "state":
//                   {
//                      "on":true,"bri":100,"hue":12000,"sat":254,"effect":"none",
//   pcElement->        "xy":[0.4400,0.5228],"ct":342,"alert":"select","colormode":"hs",
//                      "mode":"homeautomation","reachable":true
//                   },
//                   "swupdate":
//                   ......
//                }
//             }
//  
bool GEN_JsonGetDouble(char *pcObject, const char *pcElement, double *pflValue)
{
   bool  fCc=FALSE;
   char *pcSrc;
   
   //
   // "colormode":"hs","mode":"homeautomation","reachable":true
   //  ^pcElement
   // Make sure to include the "" in order to find the exact element name, not just part of it !
   //
   pcSrc = GEN_STRSTR((const char *)pcObject, pcElement); 
   if(pcSrc)
   {
      pcSrc = GEN_FindDelimiter(pcSrc, DELIM_INTEGER);
      if(pcSrc)
      {
         errno = 0;
         *pflValue = strtod(pcSrc, NULL);
         fCc = (errno == 0);
      }
   }
   return(fCc);    
}

//  
// Function:   GEN_JsonGetInteger
// Purpose:    Parse JSON object and return a int value
// 
// Parameters: JSON object, Elementname, result^
// Returns:    TRUE if parameter found and converted
// Note:       
//  pcObject-> { 
//                "1":
//                {
//                   "state":
//                   {
//                      "on":true,"bri":100,"hue":12000,"sat":254,"effect":"none",
//  pcElement->         "xy":[0.4400,0.5228],"ct":342,"alert":"select","colormode":"hs",
//                      "mode":"homeautomation","reachable":true
//                   },
//                   "swupdate":
//                   ......
//                }
//             }
//  
bool GEN_JsonGetInteger(char *pcObject, const char *pcElement, int *piValue)
{
   bool  fCc=FALSE;
   char *pcSrc;
   
   //
   // "colormode":"hs","mode":"homeautomation","reachable":true
   //  ^pcElement
   // Make sure to include the "" in order to find the exact element name, not just part of it !
   //
   pcSrc = GEN_STRSTR((const char *)pcObject, pcElement); 
   if(pcSrc)
   {
      pcSrc = GEN_FindDelimiter(pcSrc, DELIM_INTEGER);
      if(pcSrc)
      {
         *piValue = (int)strtol(pcSrc, NULL, 0);
         fCc = TRUE;
      }
   }
   return(fCc);    
}

//  
// Function:   GEN_JsonGetElementInteger
// Purpose:    Return the int value of the element
// 
// Parameters: JSON Element ptr, result^
// Returns:    TRUE if parameter found and converted
// Note:       
//             { 
//                "1":
//                {
//                   "state":
//                   {
//        pcElm->       "bri":100,"hue":12000,"sat":254,"effect":"none",
//                      "xy":[0.4400,0.5228],"ct":342,"alert":"select","colormode":"hs",
//                      "mode":"homeautomation","reachable":true
//                   },
//                   "swupdate":
//                   ......
//                }
//             }
//  
bool GEN_JsonGetElementInteger(char *pcElm, int *piValue)
{
   bool  fCc=FALSE;
   char *pcSrc;
   
   pcSrc = GEN_FindDelimiter(pcElm, DELIM_INTEGER);
   if(pcSrc)
   {
      *piValue = (int)strtol(pcSrc, NULL, 0);
      fCc = TRUE;
   }
   return(fCc);    
}

//  
// Function:   GEN_JsonGetString
// Purpose:    Parse JSON object and return a string ptr
// 
// Parameters: JSON object, Elementname, result^
// Returns:    TRUE if parameter found and converted
// Note:    
//  pcObject-> { 
//                "1":
//                {
//                   "state":
//                   {
//                      "on":true,"bri":100,"hue":12000,"sat":254,"effect":"none",
//                      "xy":[0.4400,0.5228],"ct":342,"alert":"select","colormode":"hs",
//         pcElement->  "mode":"homeautomation","reachable":true
//                   },
//                   "swupdate":
//                   ......
//                }
//             }
//  
bool GEN_JsonGetString(char *pcObject, const char *pcElement, char **pcResult)
{
   bool  fCc=FALSE;
   char *pcSrc;
   
   //
   // "colormode":"hs","mode":"homeautomation","reachable":true
   //  ^pcElement
   // Make sure to include the "" in order to find the exact element name, not just part of it !
   //
   pcSrc = GEN_STRSTR((const char *)pcObject, pcElement); 
   if(pcSrc)
   {
      pcSrc = GEN_FindDelimiter(pcSrc, DELIM_COLON);
      if(pcSrc)
      {
         pcSrc = GEN_FindDelimiter(pcSrc, DELIM_DQUOTE);
         if(pcSrc++)
         {
            *pcResult = pcSrc;
            fCc = TRUE;
         }
      }
   }
   return(fCc);    
}

//  
// Function:   GEN_JsonGetElementString
// Purpose:    Copy element string value
// 
// Parameters: Element^, Dest^, size
// Returns:    TRUE
// Note:    
//  pcObject-> { 
//                "1":
//                {
//                   "state":
//                   {
//                      "on":true,"bri":100,"hue":12000,"sat":254,"effect":"none",
//                      "xy":[0.4400,0.5228],"ct":342,"alert":"select","colormode":"hs",
//         pcElement->  "mode":"homeautomation","reachable":true
//                   },
//                   "swupdate":
//                   ......
//                }
//             }
//  
bool GEN_JsonGetElementString(char *pcElm, char *pcDst, int iSize)
{
   bool  fCc=FALSE;
   char *pcSrc;

   pcSrc = GEN_FindDelimiter(pcElm, DELIM_COLON);
   if(pcSrc)
   {
      pcSrc = GEN_FindDelimiter(pcSrc, DELIM_DQUOTE);
      if(pcSrc)
      {
         // Element found: skip double-quotes
         pcSrc++;
         while(iSize && pcSrc)
         {
            pcSrc++;
            switch(*pcSrc)
            {
               case '\0':
                  PRINTF("GEN-JsonGetElementString():Premature EOS!" CRLF);
                  // FALLTHROUGH
               case '\"':
                  *pcDst = '\0';
                  iSize  = 0;
                  fCc    = TRUE;
                  break;

               default:
                  *pcDst++ = *pcSrc;
                  *pcDst   = '\0';
                  iSize--;
                  break;
            }
         }
      }
   }
   return(fCc);    
}

//  
// Function:   GEN_JsonGetUnsignedLong
// Purpose:    Parse JSON object and return a long value
// 
// Parameters: JSON object, Elementname, result^
// Returns:    TRUE if parameter found and converted
// Note:       
//  pcObject-> { 
//                "1":
//                {
//                   "state":
//                   {
//                      "on":true,"bri":100,"hue":12000,"sat":254,"effect":"none",
//                      "xy":[0.4400,0.5228],"ct":342,"alert":"select","colormode":"hs",
//  pcElement->         "mode":"homeautomation","reachable":true
//                   },
//                   "swupdate":
//                   ......
//                }
//             }
//  
bool GEN_JsonGetUnsignedLong(char *pcObject, const char *pcElement, u_int32 *pulValue)
{
   bool  fCc=FALSE;
   char *pcSrc;
   
   //
   // "colormode":"hs","mode":"homeautomation","reachable":true
   // ^pcElement
   // Make sure to include the "" in order to find the exact element name, not just part of it !
   //
   pcSrc = GEN_STRSTR((const char *)pcObject, pcElement); 
   if(pcSrc)
   {
      pcSrc = GEN_FindDelimiter(pcSrc, DELIM_INTEGER);
      if(pcSrc)
      {
         errno = 0;
         *pulValue = strtoul(pcSrc, NULL, 10);
         fCc = (errno == 0);
      }
   }
   return(fCc);    
}


/*------  Local functions separator -------------------------------------------
__LOCAL_FUNCTIONS_________________(){};
--------------------------------------;---------------------------------------*/

//  
// Function:   gen_JsonCopyElementName
// Purpose:    Copy element name to dest
// 
// Parameters: JSON object, Dest, size, in/exclude double quotes
// Returns:    TRUE if OK copied
// Note:       
//  pcObject-> { 
//                "1":
//                {
//                   "state":
//                   {
//                      "on":true,"bri":100,"hue":12000,"sat":254,"effect":"none",
//                      "xy":[0.4400,0.5228],"ct":342,"alert":"select","colormode":"hs",
//                      "mode":"homeautomation","reachable":true
//                   },
//                   "swupdate":
//                   ......
//                }
//             }
//  
static bool gen_JsonCopyElementName(char *pcObject, char *pcDst, int iSize, bool fExclude)
{
   bool  fCc=FALSE;
   char *pcSrc;

   if(pcObject == NULL) return(FALSE);

   if( (pcSrc = GEN_FindDelimiter(pcObject, DELIM_DQUOTE)) == NULL) return(FALSE);
   if(!fExclude) 
   {
      // Include the double quotes in the destination
      *pcDst++ = *pcSrc;
      iSize--;
   }
   pcSrc++;
   while(iSize > 0)
   {
      switch(*pcSrc)
      {
         case '\0':
         case '\n':
            *pcDst = '\0';
            iSize  = 0;
            break;

         case '\"':
            if(!fExclude) *pcDst++ = *pcSrc++;
            *pcDst = '\0';
            iSize = 0;
            fCc   = TRUE;
            break;

         default:
            *pcDst++ = *pcSrc++;
            *pcDst   = '\0';
            iSize--;
            break;
      }
   }
   return(fCc);
}

//  
// Function:   gen_JsonGetType
// Purpose:    Get the type of this object/element
// 
// Parameters: JSON object
// Returns:    JSONT
// Note:       pcObj points to ':' in the JSON object
//             { 
//                "1":
//                {
//                   "state":
//                   {
//                      "on":true,"bri":100,"hue":12000,"sat":254,"effect":"none",
//                      "xy":[0.4400,0.5228],"ct":342,"alert":"select","colormode":"hs",
//                      "mode":"homeautomation","reachable":true
//                   },
//                   "swupdate":
//                   ......
//                }
//             }
//  
static JSONT gen_JsonGetType(char *pcObj)
{
   bool  fSearching=TRUE;
   JSONT tType=JSON_TYPE_NONE;

   if(pcObj)
   {
      pcObj++;
      //
      // Skip ':'
      // Element value has several types:
      //   :"Xxxx xx"      STRING
      //   :100.0          FLOAT
      //   :false          BOOL false/true
      //   :{"Xxxx":       OBJECT
      //   :[....]         ARRAY
      // Others end with
      //   :12345,         DOUBLE
      //   :12345}         INTEGER
      //
      while(fSearching)
      {
         switch(*pcObj)
         {
            case '{':
               tType = JSON_TYPE_OBJECT;
               fSearching = FALSE;
               break;

            case '\"':
               tType = JSON_TYPE_STRING;
               fSearching = FALSE;
               break;

            case '.':
               tType = JSON_TYPE_FLOAT;
               fSearching = FALSE;
               break;

            case '\0':
            case ',':
            case '}':
            case ']':
               // End Of Element: must be a number
               tType = JSON_TYPE_INTEGER;
               fSearching = FALSE;
               break;

            case 't':
            case 'f':
               tType = JSON_TYPE_BOOL;
               fSearching = FALSE;
               break;

            case '[':
               tType = JSON_TYPE_ARRAY;
               fSearching = FALSE;
               break;

            default:
               // Ignore all other
               pcObj++;
               break;
         }
      }
   }
   return(tType);
}

//  
// Function:   gen_JsonGetElementObject
// Purpose:    Find an nested element object 
// 
// Parameters: JSON ELEMENT ptr, Level
// Returns:    OBJECT ptr or NULL
// Note:       
//                      { 
//             pcElm->     "1":
//             Obj(0)->    {
//                            "state":
//             Obj(1)         {
//                               "on":true,"bri":100,"hue":12000,"sat":254,"effect":"none",
//                               "xy":[0.4400,0.5228],"ct":342,"alert":"select","colormode":"hs",
//                               "mode":"homeautomation","reachable":true
//                            },
//                            "swupdate":
//                            ......
//                         }
//                      },
//                      ...... next elements
//
static char *gen_JsonGetElementObject(char *pcElm, int iLevel)
{
   bool  fSearch=TRUE;
   int   iCurLevel=0;

   while(fSearch)
   {
      switch(*pcElm)
      {
         case '\0':
            PRINTF("gen-JsonGetElementObject():ERROR:Lev=%d" CRLF, iLevel);
            pcElm    = NULL;
            fSearch  = FALSE;
            break;

         case '{':
            if(iLevel == iCurLevel)
            {
               fSearch  = FALSE;
            } 
            else 
            {
               pcElm++;
               iCurLevel++;
            }
            break;

         case '}':
            if(--iLevel == 0)
            {
               // End of Element 
               fSearch  = FALSE;
            } 
            else 
            {
               pcElm++;
            }
            break;

         default:
            pcElm++;
            break;
      }
   }
   return(pcElm);
}

#ifdef   FEATURE_DUMP_ELEMENT
//  
// Function:   gen_JsonDumpValue
// Purpose:    Dump Element value
// 
// Parameters: Element ptr, Idx
// Returns:    
// Note:             "ElementName"
//             pcElm ->:<ElementValue>
//  
static void gen_JsonDumpValue(char *pcElm, int iIdx)
{
   int   i=0;
   char  cTemp[NUM_DUMPZ];

   while(i<NUM_DUMP)
   {
      switch(*pcElm)
      {
         // Stop copy on any possible Value end; Drop CrLfs
         case '}':
         case ',':
         case '\0':
            cTemp[i] = '\0';
            i = NUM_DUMP;
            break;

         case '\r':
         case '\n':
            break;

         default:
            cTemp[i++] = *pcElm;
            cTemp[i]   = '\0';
            break;
      }
      pcElm++;

   }
   GEN_Printf("gen-JsonDumpValue():Element %02d=%s" CRLF, iIdx, cTemp);
}
#endif


