/*  (c) Copyright:  2005..2019  Patrn, Confidential Data
 *
 *  Workfile:           com_file.c
 *  Purpose:            Offer the API as an abstraction layer for library calls
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Changes:       
 *    02 Jun 2013:      Created
 *    06 Jun 2021:      Add GEN_FileCopy()
 *    19 Nov 2021:      Add Variadic macro's for PRINTF()
 *    14 Dec 2021:      Add GEN_FileCopyRecords()
 *    19 May 2023:      Change FINFO_GetFileInfo() to return text AND numeric data
 *    24 Apr 2024:      Add Dir/File functions
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <sys/types.h>
#include <dirent.h>
#include <fnmatch.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <pwd.h>
#include <grp.h>
//
#include "typedefs.h"
#include "config.h"
#include "com_rtc.h"
#include "com_log.h"
#include "com_safe.h"
#include "com_func.h"
//
#include "com_file.h"

//#define USE_PRINTF
#include "printx.h"

//
// Static functions
//
static char   *pcFileFilter = NULL;  
//
static int     finfo_FileFilter        (const struct dirent *);
static void    finfo_RemoveCrLf        (char *);
static PIKDIR *gen_SortDirEntries      (PIKDIR *, PIKS);


/*----------------------------------------------------------------------
_______________GLOBAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

//
//  Function:   GEN_AddPid
//  Purpose:    Add PID to VAR dir
//              
//  Parms:      Pathname of PID file
//  Returns:    1 = error
//
int GEN_AddPid(char *pcFile)
{
   int   iCc = 1;
   FILE *pstFile;
        
   remove(pcFile);
   if( (pstFile = safefopen(pcFile, "w")) != NULL )
   {
      GEN_FPRINTF(pstFile, "%i", getpid());
      safefclose(pstFile);
      iCc = 0;
   }
   return(iCc);   
}

/*
 * Function    : GEN_ChangeOwner
 * Description : Change owner/group and priv for a dir/file
 *
 * Parameters  : Path, User, Group, Priv, log yesno
 * Returns     : TRUE if OK
 *                
 */
bool GEN_ChangeOwner(const char *pcPath, const char *pcUser, const char *pcGroup, mode_t tPriv, bool fLog) 
{
   uid_t          tUid;
   gid_t          tGid;
   struct passwd *pstPwd;
   struct group  *pstGrp;

   errno  = 0;
   pstPwd = getpwnam(pcUser);
   if (pstPwd == NULL) 
   {
      if(fLog) LOG_Report(errno, "COM", "GEN-ChangeOwner(): Error using <%s>", pcUser);
      return(FALSE);
   }
   tUid = pstPwd->pw_uid;
   //
   pstGrp = getgrnam(pcGroup);
   if (pstGrp == NULL) 
   {
      if(fLog) LOG_Report(errno, "COM", "GEN-ChangeOwner(): Error using <%s>", pcGroup);
      return(FALSE);
   }
   tGid = pstGrp->gr_gid;
   //
   if (chown(pcPath, tUid, tGid) == -1) 
   {
      if(fLog) LOG_Report(errno, "COM", "GEN-ChangeOwner(): Error change owner <%s>", pcPath);
      return(FALSE);
   }
   if(tPriv)
   {
      if(chmod(pcPath, tPriv) != 0)
      {
         if(fLog) LOG_Report(errno, "COM", "GEN-ChangeOwner(): Error change priviledge <%s>", pcPath);
      }
   }
   return(TRUE);
}

//
//  Function:   GEN_DirectoryCopy
//  Purpose:    Copy wildcard directories
//              
//  Parms:      Dest path, source path
//  Returns:    Cc
//
int GEN_DirectoryCopy(char *pcDst, char *pcSrc)
{
   int   iCc, iLen;
   char *pcCall;

   iLen   = GEN_STRLEN(pcSrc) + GEN_STRLEN(pcDst) + 32;
   pcCall = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, iLen);
   GEN_SNPRINTF(pcCall, iLen, "cp %s %s", pcSrc, pcDst);
   iCc = system(pcCall);
   SAFEFREE(__PRETTY_FUNCTION__ , pcCall);
   return(iCc);
}

// 
// Function:   GEN_DirectoryCreate
// Purpose:    Create directory
// 
// Parameters: Dir path, Permissions, log yesno
// Returns:    TRUE if okee
// Note:       
// 
bool GEN_DirectoryCreate(const char *pcDir, mode_t tPerm, bool fLog)
{
   bool  fCc=TRUE;
   int   iRes;

   if((iRes = mkdir(pcDir, tPerm)) != 0)
   {
      if(fLog)
      {
         PRINTF("GEN-DirectoryCreate():ERROR creating directory [%s]:%s" CRLF, pcDir, strerror(errno));
         LOG_Report(errno, "COM", "GEN-DirectoryCreate():ERROR creating directory [%s]:", pcDir);
      }
      fCc = FALSE;
   }
   else 
   {
      if(fLog)
      {
         LOG_Report(0, "COM", "GEN-DirectoryCreate():Dir [%s] created [0%o]:", pcDir, tPerm);
         PRINTF("GEN-DirectoryCreate():Dir [%s] created [0%o]" CRLF, pcDir, tPerm);
      }
   }
   return(fCc);
}

// 
// Function:   GEN_DirectoryExists
// Purpose:    Check if directory exists
// 
// Parameters: Dir path
// Returns:    TRUE if exists
// Note:       
// 
bool GEN_DirectoryExists(const char *pcDir)
{
   bool        fCc=FALSE;
   int         iRes;
   struct stat stStat;

   iRes = stat(pcDir, &stStat);
   if(iRes == 0)
   {
      if(S_ISDIR(stStat.st_mode)) 
      {
         // Fine: Dir exists
         fCc = TRUE;
      }
   }
   return(fCc);
}

//
//  Function:   GEN_FileCopy
//  Purpose:    Copy file
//              
//  Parms:      Dest, Source, Source filepos, Dest append, source filepos ptr
//  Returns:    TRUE if OKee
//
bool GEN_FileCopy(char *pcDst, char *pcSrc, bool fDstApp, int64 *pllSrcPos)
{
   bool     fCc=FALSE;
   char    *pcBuffer;
   size_t  tRd, tWr;
   FILE    *ptSrc;
   FILE    *ptDst;

   ptSrc = safefopen(pcSrc, "r");
   //
   if(fDstApp) ptDst = safefopen(pcDst, "a+");
   else        ptDst = safefopen(pcDst, "w");
   //
   if(ptSrc && ptDst)
   {
      PRINTF("GEN-FileCopy():Copy from %s" CRLF, pcSrc);
      PRINTF("GEN-FileCopy():Copy to   %s" CRLF, pcDst);
      //
      pcBuffer = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, GEN_BUFFER_LEN);
      if(pllSrcPos) fseek(ptSrc, *pllSrcPos, SEEK_SET);
      //
      while(!safefeof(ptSrc))
      {
         tRd = safefread(pcBuffer, 1, GEN_BUFFER_LEN, ptSrc);
         if(tRd)
         {
            tWr = safefwrite(pcBuffer, 1, tRd, ptDst);
            if(tWr != tRd)
            {
               LOG_Report(errno, "GEN", "GEN-FileCopy():ONLY Written %d bytes i.o %d bytes!", tWr, tRd);
               PRINTF("GEN-FileCopy():ONLY Written %d bytes i.o %d bytes!" CRLF, tWr, tRd);
            }
         }
      }
      if(pllSrcPos) *pllSrcPos = ftell(ptDst);
      SAFEFREEX(__PRETTY_FUNCTION__, pcBuffer);
      fCc = TRUE;
   }
   else
   {
      if(!ptSrc) LOG_Report(errno, "GEN", "GEN-FileCopy():Error opening source:%s", pcSrc);
      if(!ptDst) LOG_Report(errno, "GEN", "GEN-FileCopy():Error opening Destination:%s", pcDst);
   }
   if(ptSrc) safefclose(ptSrc);
   if(ptDst) safefclose(ptDst);
   return(fCc);
}

//
//  Function:   GEN_FileCopyAttributes
//  Purpose:    Copy file with specific attributes
//              
//  Parms:      Dest, Source
//  Returns:    TRUE if OKee
//
bool GEN_FileCopyAttributes(char *pcDst, char *pcSrc, int iAttr)
{
   bool     fCc=FALSE;
   char    *pcBuffer;
   int      iNrRd, iNrWr;
   int      iSrc, iDst=0;

   if( (iSrc = safeopen(pcSrc, O_RDONLY)) > 0)
   {
      if( (iDst = safeopen2(pcDst, O_RDWR|O_CREAT|O_TRUNC, (mode_t)iAttr)) > 0)
      {
         PRINTF("GEN-FileCopyAttributes():Copy from %s" CRLF, pcSrc);
         PRINTF("GEN-FileCopyAttributes():Copy to   %s" CRLF, pcDst);
         //
         pcBuffer = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, GEN_BUFFER_LEN);
         //
         do
         {
            iNrRd = saferead(iSrc, pcBuffer, GEN_BUFFER_LEN);
            if(iNrRd)
            {
               iNrWr = safewrite(iDst, pcBuffer, iNrRd);
               if(iNrWr != iNrRd)
               {
                  LOG_Report(errno, "GEN", "GEN-FileCopyAttributes():ONLY Written %d bytes i.o %d bytes!", iNrWr, iNrRd);
                  PRINTF("GEN-FileCopyAttributes():ONLY Written %d bytes i.o %d bytes!" CRLF, iNrWr, iNrRd);
               }
            }
         }
         while(iNrRd);
         SAFEFREEX(__PRETTY_FUNCTION__, pcBuffer);
         fCc = TRUE;
      }
      else
      {
         LOG_Report(errno, "GEN", "GEN-FileCopyAttributes():Error opening Destination:%s", pcDst);
      }
   }
   else
   {
      LOG_Report(errno, "GEN", "GEN-FileCopyAttributes():Error opening source:%s", pcSrc);
   }
   if(iSrc > 0) safeclose(iSrc);
   if(iDst > 0) safeclose(iDst);
   //
   return(fCc);
}

//
//  Function:   GEN_FileCopyRecords
//  Purpose:    Copy file (by record)
//              
//  Parms:      Dest, Source, Source filepos, Dest append, source filepos ptr
//  Returns:    TRUE if OKee
//
bool GEN_FileCopyRecords(char *pcDst, char *pcSrc, bool fDstApp, int64 *pllSrcPos)
{
   bool     fCc=FALSE;
   char    *pcBuffer;
   char    *pcRd;
   size_t   tRd, tWr;
   FILE    *ptSrc;
   FILE    *ptDst;

   ptSrc = safefopen(pcSrc, "r");
   //
   if(fDstApp) ptDst = safefopen(pcDst, "a+");
   else        ptDst = safefopen(pcDst, "w");
   //
   if(ptSrc && ptDst)
   {
      PRINTF("GEN-FileCopy():Copy from %s" CRLF, pcSrc);
      PRINTF("GEN-FileCopy():Copy to   %s" CRLF, pcDst);
      //
      pcBuffer = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, GEN_RECORD_LENZ);
      if(pllSrcPos) fseek(ptSrc, *pllSrcPos, SEEK_SET);
      //
      do
      {
         pcRd = fgets(pcBuffer, GEN_RECORD_LEN, ptSrc);
         if(pcRd)
         {
            tRd = GEN_STRLEN(pcBuffer);
            tWr = safefwrite(pcBuffer, 1, tRd, ptDst);
            if(tWr != tRd)
            {
               LOG_Report(errno, "GEN", "GEN-FileCopy():ONLY Written %d bytes i.o %d bytes!", tWr, tRd);
               PRINTF("GEN-FileCopy():ONLY Written %d bytes i.o %d bytes!" CRLF, tWr, tRd);
            }
         }
      }
      while(pcRd);
      //
      if(pllSrcPos) *pllSrcPos = ftell(ptSrc);
      SAFEFREEX(__PRETTY_FUNCTION__, pcBuffer);
      fCc = TRUE;
   }
   else
   {
      if(!ptSrc) LOG_Report(errno, "GEN", "GEN-FileCopy():Error opening source:%s", pcSrc);
      if(!ptDst) LOG_Report(errno, "GEN", "GEN-FileCopy():Error opening Destination:%s", pcDst);
   }
   if(ptSrc) safefclose(ptSrc);
   if(ptDst) safefclose(ptDst);
   return(fCc);
}

// 
// Function:   GEN_FileExists
// Purpose:    Check if file exists
// 
// Parameters: File path
// Returns:    TRUE if exists
// Note:       
// 
bool GEN_FileExists(const char *pcFilePath)
{
   bool        fCc=FALSE;
   int         iRes;
   struct stat stStat;

   iRes = stat(pcFilePath, &stStat);
   if(iRes == 0)
   {
      if(S_ISREG(stStat.st_mode)) 
      {
         // Fine: File exists
         fCc = TRUE;
      }
   }
   return(fCc);
}

//
// Function:   GEN_FreeDirEntries
// Purpose:    Free node struct
//
// Parms:      Node struct
// Returns:    
// Note:       
//             
void GEN_FreeDirEntries(PIKDIR *pstTops)
{
   PIKDIR  *pstCurr;

   while(pstTops)
   {
      pstCurr = pstTops->pstNext;
      if(pstTops->pcName) SAFEFREE(__PRETTY_FUNCTION__, pstTops->pcName);
      SAFEFREE(__PRETTY_FUNCTION__, pstTops);
      pstTops = pstCurr;
   }
}

//
//  Function:   GEN_FileTruncate
//  Purpose:    Truncate file by tail lines
//              
//  Parms:      File
//  Returns:    EXIT_CC_OKEE, EXIT_CC_GEN_ERROR
//
static const char *pcTail   = "tail -%d %s 2>/dev/null >%s.temp";
static const char *pcMove   = "mv %s.temp %s";
static const char *pcRemove = "rm -f %s.temp";
//
int GEN_FileTruncate(char *pcFile, int iLines)
{
   int   iCc;
   char *pcCall;

   pcCall = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, 256);
   GEN_SNPRINTF(pcCall, 255, pcTail, iLines, pcFile, pcFile);
   //
   // Drop all but last 1000 lines:
   // $ tail -1000 /mnt/rpicache/aap.log /mnt/rpicache/aap.log.temp
   //
   iCc = system(pcCall);
   PRINTF("GEN-FileTruncate():%s=%d" CRLF, pcCall, iCc);
   if(iCc == 0)
   {
      //
      // OK: Move temp to original file
      // $ mv /mnt/rpicache/aap.log.temp /mnt/rpicache/aap.log
      //
      GEN_SNPRINTF(pcCall, 255, pcMove, pcFile, pcFile);
      iCc = system(pcCall);
      PRINTF("GEN-FileTruncate():%s=%d" CRLF, pcCall, iCc);
   }
   else
   {
      //
      // ERROR: delete temp file
      //
      GEN_SNPRINTF(pcCall, 255, pcRemove, pcFile);
      iCc = system(pcCall);
      PRINTF("GEN-FileTruncate():%s=%d" CRLF, pcCall, iCc);
   }
   SAFEFREE(__PRETTY_FUNCTION__ , pcCall);
   return(iCc);
}

//
// Function:   GEN_GetDirEntries
// Purpose:    Get number of files and directories
//
// Parms:      Dir, Filter, Num Files ptr, Num Dirs ptr, Sorting
// Returns:    Result struct ptr or NULL
// Note:       FINFO_GetDirEntries() are sorted on alphabetic order!
//             
PIKDIR *GEN_GetDirEntries(const char *pcDirPath, const char *pcFilter, int *piNumFiles, int *piNumDirs, PIKS tSort)
{
   bool     fDoStore;
   int      x, iNum, iSize, iDandT;
   char     pcTemp[32];
   char    *pcFileName;
   char    *pcFilePath;
   void    *pvData;
   PIKDIR  *pstTops=NULL;
   PIKDIR  *pstLast=NULL;
   PIKDIR  *pstCurr=NULL;

   if(piNumDirs ) *piNumDirs  = 0;
   if(piNumFiles) *piNumFiles = 0;
   //
   pcFileName  = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, 256);
   pcFilePath  = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, 256);
   pvData      = FINFO_GetDirEntries((char *)pcDirPath, pcFilter, &iNum);
   //
   if(iNum > 0)
   {
      for (x=0; x<iNum; x++)
      {
         FINFO_GetFilename(pvData, x, pcFileName, 255);
         GEN_SPRINTF(pcFilePath, "%s/%s", pcDirPath, pcFileName);
         FINFO_GetFileInfo(pcFilePath, FI_FILEDIR, pcTemp, 31);
         if(*pcTemp == 'F')
         {
            if(piNumFiles) (*piNumFiles)++;
            fDoStore = TRUE;
         }
         else if( (*pcTemp == 'D') && (*pcFileName != '.'))
         {
            if(piNumDirs) (*piNumDirs)++;
            fDoStore = TRUE;
         }
         else
         {
            fDoStore = FALSE;
         }
         if(fDoStore)
         {
            pstCurr = (PIKDIR *) SAFEMALLOC(__PRETTY_FUNCTION__, sizeof(PIKDIR));
            //
            // Fill struct with info
            //
            iSize = GEN_STRLEN(pcFileName) + 32;
            pstCurr->pcName = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, iSize);
            GEN_STRNCPY(pstCurr->pcName, pcFileName, iSize-1);
            pstCurr->fDir = (*pcTemp == 'D');
            //
            // Get file size and timestamp
            //
            FINFO_GetFileInfo(pcFilePath, FI_SIZE, pcTemp, 31);
            FINFO_GetFileInfo(pcFilePath, FI_MDATE, &iDandT, FI_TYPE_INT);
            pstCurr->llSize = strtoll(pcTemp, NULL, 10); 
            pstCurr->iDandT = iDandT;
            //
            // Keep the top of the linked list
            //
            if(pstTops == NULL) 
            {
               pstTops = pstCurr;
               pstLast = pstCurr;
            }
            else
            {
               pstCurr->pstPrev = pstLast;
               pstLast->pstNext = pstCurr;
               pstLast          = pstCurr;
            }
         }
      }
   }
   if(pvData) FINFO_ReleaseDirEntries(pvData, iNum);
   //
   // Sort DIR entries alpha or Timestamp
   //
   pstTops = gen_SortDirEntries(pstTops, tSort);
   SAFEFREE(__PRETTY_FUNCTION__, pcFilePath);
   SAFEFREE(__PRETTY_FUNCTION__, pcFileName);
   return(pstTops);
}

// 
// Function:   GEN_GetNumberOfFiles
// Purpose:    Get number of files in this dir with this filter
//             
// Parameters: Dir path, Filter
//             
// Returns:    Nr of files
// Note:       
// 
int GEN_GetNumberOfFiles(const char *pcDir, const char *pcFilter)
{
   int      iNumFiles=0;
   PIKDIR  *pstTop;

   pstTop = GEN_GetDirEntries(pcDir, pcFilter, &iNumFiles, NULL, PIK_ALPHA);
   GEN_FreeDirEntries(pstTop);
   return(iNumFiles);
}

//
//  Function:   GEN_RemovePid
//  Purpose:    Remove PID file
//              
//  Parms:      Pathname of PID file
//  Returns:    
//
int GEN_RemovePid(char *pcFile)
{
  return(remove(pcFile));
}

/*----------------------------------------------------------------------
____________FILE_INFO_FUNCTIONS(){}
------------------------------x----------------------------------------*/

/*
 * Function    : FINFO_GetDirEntries
 * Description : Retrieve all info inside a single directory
 *
 * Parameters  : Dir path, filter string, result ptr
 * Returns     : dirent struct prt
 *               piNum has the number of entries in the Dir 
 *                or -1 on error (errno)
 * Note        : Entries are sorted on alphabetic order if alphasort
 *               is being used. If not (NULL), natural order.
 */
void *FINFO_GetDirEntries(char *pcPath, const char *pcFilter, int *piNum)
{
   struct dirent **pstDir=NULL;

   pcFileFilter = (char *)pcFilter;
   //
   *piNum = scandir(pcPath, &pstDir, finfo_FileFilter, alphasort);
   return(pstDir);
}

/*
 * Function    : FINFO_ReleaseDirEntries
 * Description : Release the dir info data struct
 *
 * Parameters  : Dir info prt, num entries
 * Returns     : 
 *
 */
void FINFO_ReleaseDirEntries(void *pvData, int iNr)
{
   struct dirent **pstDir = pvData;

   if(pstDir)
   {
      while(iNr > 0)
      {
         iNr--;
         free(pstDir[iNr]);
      }
      free(pstDir);
   }
}

/*
 * Function    : FINFO_GetFilename
 * Description : Get filename from current dir/file
 *
 * Parameters  : dirent dir info, entry nr, buffer and size
 * Returns     : TRUE if OKee
 *
 */
bool FINFO_GetFilename(void *pvData, int iIdx, char *pcBuffer, int iLen)
{
   struct dirent **pstDir = pvData;

   GEN_STRNCPY(pcBuffer, pstDir[iIdx]->d_name, (size_t)iLen);
   return(TRUE);
}

// 
// Function    : FINFO_GetFileInfo
// Description : Retrieve info about a file in this dir
// 
// Parameters  : Dir path, info type, buffer, buffer size
// Returns     : TRUE if OKee
// Note        : Also called from startup and shutdown: Can NOT use G_xxx vars !
// 
//               struct stat:
//                  dev_t           st_dev;        ID of device containing file     
//                  ino_t           st_ino;        Inode number
//                  mode_t          st_mode;       File type and mode               FI_FILEDIR
//                  nlink_t         st_nlink;      Number of hard links
//                  uid_t           st_uid;        User ID of owner                 FI_UID
//                  gid_t           st_gid;        Group ID of owner                FI_GUI
//                  dev_t           st_rdev;       Device ID (if special file)
//                  off_t           st_size;       Total size, in bytes             FI_SIZE
//                  blksize_t       st_blksize;    Block size for filesystem I/O
//                  blkcnt_t        st_blocks;     Number of 512B blocks allocated
//                  struct timespec st_atime;      Time of last access
//                  struct timespec st_mtime;      Time of last modification        FI_MDATE
//                  struct timespec st_ctime;      Time of last status change
//
bool FINFO_GetFileInfo(char *pcPath, FINFO iFinfo, void *pvData, int iLen)
{
   bool        fCc=TRUE;
   char       *pcBuffer;
   struct stat stStat;

   if(stat(pcPath, &stStat) == -1) return(FALSE);

   pcBuffer = (char *)pvData;
   //
   switch(iFinfo)
   {
      case FI_FILEDIR:
         if(iLen > 0)
         {
            // Text based requested
            PRINTF("FINFO-GetFileInfo():[%s]=0x%08X" CRLF, pcPath, stStat.st_mode);
            switch(stStat.st_mode & S_IFMT)
            {
               case S_IFDIR: 
                  *pcBuffer = 'D';
                  break;

               case S_IFREG:
                  *pcBuffer = 'F';
                  break;

               default:
                  *pcBuffer = '?';
                  break;
            }
         }
         else fCc = FALSE;
         break;

      case FI_SIZE:
         switch(iLen)
         {
            case FI_TYPE_INT64:
            case FI_TYPE_UINT64:
               *(int64 *)pvData = (int64)stStat.st_size;
               break;

            case FI_TYPE_INT:
            case FI_TYPE_UINT32:
               *(u_int32 *)pvData = (u_int32)stStat.st_size;
               break;

            default:
               // Text based requested
               GEN_SNPRINTF(pcBuffer, iLen, "%lld", (long long) stStat.st_size);
               break;
         }
         break;

      case FI_MDATE:
         switch(iLen)
         {
            case FI_TYPE_INT:
               *(int *)pvData = (int)stStat.st_mtime;
               break;

            case FI_TYPE_INT64:
               *(int64 *)pvData = (int64)stStat.st_mtime;
               break;

            case FI_TYPE_UINT32:
               *(u_int32 *)pvData = (u_int32)stStat.st_mtime;
               break;

            case FI_TYPE_UINT64:
               *(u_int64 *)pvData = (u_int64)stStat.st_mtime;
               break;

            default:
               // Text based requested
               GEN_SNPRINTF(pcBuffer, iLen, "%s", ctime(&stStat.st_mtime));
               finfo_RemoveCrLf(pcBuffer);
               break;
         }
         break;

      case FI_UID:
         switch(iLen)
         {
            case FI_TYPE_INT:
               *(int *)pvData = (int)stStat.st_uid;
               break;

            case FI_TYPE_INT64:
               *(int64 *)pvData = (int64)stStat.st_uid;
               break;

            case FI_TYPE_UINT32:
               *(u_int32 *)pvData = (u_int32)stStat.st_uid;
               break;

            case FI_TYPE_UINT64:
               *(u_int64 *)pvData = (u_int64)stStat.st_uid;
               break;

            default:
               // Text based requested
               GEN_SNPRINTF(pcBuffer, iLen, "%ld", (long)stStat.st_uid);
               break;
         }
         break;

      case FI_GUI:
         switch(iLen)
         {
            case FI_TYPE_INT:
               *(int *)pvData = (int)stStat.st_gid;
               break;

            case FI_TYPE_INT64:
               *(int64 *)pvData = (int64)stStat.st_gid;
               break;

            case FI_TYPE_UINT32:
               *(u_int32 *)pvData = (u_int32)stStat.st_gid;
               break;

            case FI_TYPE_UINT64:
               *(u_int64 *)pvData = (u_int64)stStat.st_gid;
               break;

            default:
               // Text based requested
               GEN_SNPRINTF(pcBuffer, iLen, "%ld", (long)stStat.st_gid);
               break;
         }
         break;

      default:
         fCc = FALSE;
         // Fallthrough
      case FI_NONE:
         *pcBuffer = 0;
         break;
   }
   return(fCc);
}

// 
// Function    : FINFO_GetTotalSpace
// Description : Retrieve drive total space
// 
// Parameters  : Dir path
// Returns     : Total space
// Note        : 
// 
int64 FINFO_GetTotalSpace(char *pcPath)
{
   struct statvfs stStat;
  
   if (statvfs(pcPath, &stStat) != 0) 
   {
      // error happens, just quits here
      return(-1);
   }
   //
   // the total size is f_bsize * f_blocks
   //
   return((int64)stStat.f_bsize * stStat.f_blocks);
}

// 
// Function    : FINFO_GetFreeSpace
// Description : Retrieve drive free space
// 
// Parameters  : Dir path
// Returns     : Free space
// Note        : 
// 
int64 FINFO_GetFreeSpace(char *pcPath)
{
   struct statvfs stStat;
  
   if (statvfs(pcPath, &stStat) != 0) 
   {
      // error happens, just quits here
      return(-1);
   }
   //
   // the available size is f_bsize * f_bavail
   //
   return((int64)stStat.f_bsize * stStat.f_bavail);
}

// 
// Function    : FINFO_GetFreeSpacePercentage
// Description : Retrieve drive free space in percentage
// 
// Parameters  : Dir path
// Returns     : Free space percentage
// Note        : 
// 
double FINFO_GetFreeSpacePercentage(char *pcPath)
{
   struct statvfs stStat;
   u_int64        llTotal, llFree;
   double         flFree;
  
   if(statvfs(pcPath, &stStat) != 0) 
   {
      // error happens, just quits here
      return -1;
   }
   //
   // the total     size is f_bsize * f_blocks
   // the available size is f_bsize * f_bavail
   //
   llFree  = (u_int64) stStat.f_bsize * stStat.f_bavail;
   llTotal = (u_int64) stStat.f_bsize * stStat.f_blocks;
   flFree  = (double) ((llFree * 100) / llTotal);
   return(flFree);
}

//
// Function:   FINFO_GetLineCount
// Purpose:    Retrieve the linecount in a document
//
// Parms:      Filename
// Returns:    Cound or -1 on error
// Note:       
//
int FINFO_GetLineCount(char *pcName)
{
   int      iNr=0;
   char    *pcBuffer;
   char    *pcRead;
   FILE    *ptFile;

   ptFile = safefopen(pcName, "r");
   if(ptFile)
   {
      pcBuffer = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, 1024);
      //
      do
      {
         pcRead = fgets(pcBuffer, 1023, ptFile);
         if(pcRead) iNr++;
      }
      while(pcRead);
      safefclose(ptFile);
      SAFEFREE(__PRETTY_FUNCTION__ , pcBuffer);
   }
   else
   {
      LOG_Report(errno, "COM", "FINFO-GetLineCount(): Error open file <%s>", pcName);
      PRINTF("FINFO-GetLineCount(): Error open file <%s>" CRLF, pcName);
   }
   return(iNr);
}

// 
// Function    : FINFO_SplitPathname
// Description : Split pathname
// 
// Parameters  : Type of operation, Dir path
// Returns     : Pointer to result
// Note        : Reseult points into pcPath
//               "/media/hddint/public/audio/Mark Knopfler - Easy Listening/11. True Love Will Never Fade.mp3"
//                                           ^                              ^
//                                           |                              |
//                                       FI_FILEDIR                     FI_FILENAME
//
// 
char *FINFO_SplitPathname(FINFO tInfo, char *pcPath)
{
   int   iLen, iNr=1;
   char *pcRes;

   if(!pcPath) return(NULL);

   iLen = GEN_STRLEN(pcPath);
   //
   switch(tInfo)
   {
      case FI_FILEDIR:
         // Find 2nd '/'
         iNr++;
         // Fallthrough
      case FI_FILENAME:
         pcRes = &pcPath[iLen-1];
         while(iLen)
         {
            if(*pcRes == '/') 
            {
               iNr--;
               if(iNr == 0)
               {
                  pcRes++;
                  break;
               }
            }
            pcRes--;
            iLen--;
         }
         if(iLen == 0) pcRes = NULL;
         break;

      case FI_FILEEXT:
         pcRes = &pcPath[iLen-1];
         while(iLen)
         {
            if(*pcRes == '.') break;
            pcRes--;
            iLen--;
         }
         if(iLen == 0) pcRes = NULL;
         break;

      default:
         pcRes = NULL;
         break;
   }
   return(pcRes);
}

/*----------------------------------------------------------------------
________________LOCAL_FUNCTIONS(){}
------------------------------x----------------------------------------*/

/*
 * Function    : finfo_FileFilter
 * Description : Filefilterfunction
 *
 * Parameters  : Dirent struct
 * Returns     : 0 to discard file entry
 *
 */
static int finfo_FileFilter(const struct dirent *pstDirent)
{
   int   iCc=0;

   if(pcFileFilter)
   {
      //
      // We have a filter: check if there is a match
      //
      #ifdef FINFO_NO_REGEX
      if(GEN_STRNCMP(pcFileFilter, pstDirent->d_name, GEN_STRLEN(pcFileFilter)) == 0) 
      {
         PRINTF("finfo-FileFilter(): Pass: Filter=[%s]-File=[%s]" CRLF, pcFileFilter, pstDirent->d_name);
         iCc = 1;
      }
      else
      {
         PRINTF("finfo-FileFilter(): Fail: Filter=[%s]-File=[%s]" CRLF, pcFileFilter, pstDirent->d_name);
      }
      #else    //FINFO_NO_REGEX
      if( (fnmatch(pcFileFilter, pstDirent->d_name, 0)) == 0)
      {
         PRINTF("finfo-FileFilter(): Pass: Regex Filter=[%s]-File=[%s]" CRLF, pcFileFilter, pstDirent->d_name);
         iCc = 1;
      }
      else
      {
         PRINTF("finfo-FileFilter(): Fail: Regex Filter=[%s]-File=[%s]" CRLF, pcFileFilter, pstDirent->d_name);
      }
      #endif   //FINFO_NO_REGEX
   }
   else
   {
      //
      // No filter
      //
      iCc = 1;
      PRINTF("finfo-FileFilter(): Pass: Filter=[None]-File=[%s]" CRLF, pstDirent->d_name);
   }
   return(iCc);
}

/*
 * Function    : finfo_RemoveCrLf
 * Description : Remove any line-ends from the buffer
 *
 * Parameters  : Buffer
 * Returns     : 
 *
 */
static void finfo_RemoveCrLf(char *pcBuffer)
{
   while(*pcBuffer)
   {
      if( (*pcBuffer == '\r') || (*pcBuffer == '\n') ) 
      {
         *pcBuffer = '\0';
         break;
      }
      pcBuffer++;
   }
}

// 
// Function:   gen_SortDirEntries
// Purpose:    Sort the new dir list
//             
// Parameters: Dir struct, Sort
//             
// Returns:    
// Note:       
// 
static PIKDIR *gen_SortDirEntries(PIKDIR *pstTop, PIKS tSort)
{
   bool     fSorted, fCc;
   PIKDIR  *pstA;
   PIKDIR  *pstB;
   PIKDIR  *pstX;
   PIKDIR  *pstY;

   //
   // DIR entries are default sorting AlphaNumeric
   //
   if( pstTop && (tSort != PIK_ALPHA))
   {
      //
      // Bubble sort
      //
      do
      {
         PRINTF("rpi-SortDirEntries():Top" CRLF);
         pstA    = pstTop;
         fSorted = FALSE;
         //
         while(pstA)
         {
            pstB = pstA->pstNext;
            if(pstB)
            {
               switch(tSort)
               {
                  case PIK_DANDT_LH:
                     // Timestmp Old --> New
                     fCc = (pstA->iDandT > pstB->iDandT);
                     break;

                  case PIK_DANDT_HL:
                     // Timestmp New --> Old
                     fCc = (pstB->iDandT > pstA->iDandT);
                     break;

                  default:
                     case PIK_ALPHA:
                     fCc = FALSE;
                     break;
               }
               if(fCc)
               {
                  // Swap A and B
                  pstX          = pstA->pstPrev;
                  pstY          = pstB->pstNext;
                  //
                  pstA->pstNext = pstB->pstNext;
                  pstB->pstNext = pstA;
                  pstB->pstPrev = pstA->pstPrev;
                  pstA->pstPrev = pstB;
                  //
                  if(pstX) pstX->pstNext = pstB;
                  else     pstTop        = pstB;
                  if(pstY) pstY->pstPrev = pstA;
                  fSorted = TRUE;
                  break;
               }
            }
            pstA = pstB;
         }
      }
      while(fSorted);
   }
   PRINTF("rpi-SortDirEntries():Done" CRLF);
   return(pstTop);
}
