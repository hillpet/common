/*  (c) Copyright:  2005..2019  CvB, Confidential Data
 *
 *  Workfile:           com_safe.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            This module contains wrappers around a number of system calls and
 *                      library functions so that a default error behavior can be defined.
 *                      Initially by John Goerzen, Linux Programming Bible
 * 
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:       
 *    20 Jul 2005       Created
 *    29 Aug 2019       Add SAFEFREE() macro to pass caller's function
 *    05 Oct 2019       Change GLOBAL_PutMallocs() handling to in/decremental
 *    08 Oct 2019       Add safeferror() to open generic error file
 *    07 Jun 2021:      Add safefeof()
 *    19 Nov 2021:      Add Variadic macro's for PRINTF()
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <malloc.h>
#include <signal.h>
#include <errno.h>
#include <stdarg.h>

#include "typedefs.h"
#include "config.h"
#include "com_rtc.h"
#include "com_log.h"

#define __SAFECALLS_C__
#include "com_safe.h"

//#define USE_PRINTF
#include "printx.h"

int   GLOBAL_GetMallocs    (void);
void  GLOBAL_PutMallocs    (int);

//
// The first two are automatically set by save_HandleError.  The third you can
// set to be the file handle to which error messages are written.
// If NULL take stderr.
//
int         SafeLibErrno     = 0;
const char *SafeLibErrorLoc  = NULL;
FILE       *SafeLibErrorDest = NULL;
//
static void save_HandleError  (int, const char *const, const char *, ...);

/*------  Local functions separator -----------------------------------------
__GLOBAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
//  Function:   safestrdup
//  Purpose:    safe strdup(s)
//              
//  Parms:      string ptr
//  Returns:    
//
char *safestrdup(const char *s)
{
  char *retval;

  retval = strdup(s);
  if (!retval)
  {
    LOG_Report(0, "SAF", "strdup [%s] failed", s);
  }
  return retval;
}

//
//  Function:   safestrncpy
//  Purpose:    safe strncpy(d, s, len)
//              
//  Parms:      
//  Returns:    
//
char *safestrncpy(char *dest, const char *src, size_t n)
{
  if (GEN_STRLEN(src) >= n)
  {
    LOG_Report(0, "SAF", "strncpy: attempt to copy string [%s] to buffer %d bytes long", src, (int) n);
  }
  return GEN_STRNCPY(dest, src, (size_t)n);
}

//
//  Function:   safestrcat
//  Purpose:    safe strcat(d, s, len)
//              
//  Parms:      
//  Returns:    
//
char *safestrcat(char *dest, const char *src, size_t n)
{
  if ((GEN_STRLEN(src) + GEN_STRLEN(dest)) >= n)
  {
    LOG_Report(0, "SAF", "strcat [%s][%s]:too big a string, max=%d ", dest, src, n);
  }
  return strncat(dest, src, n - 1);
}


//
//  Function:   safekill
//  Purpose:    safe kill(p, s)
//              
//  Parms:      process ID, sig
//  Returns:    
//
int safekill(pid_t pid, int sig)
{
  int retval;

  retval = kill(pid, sig);
  if (retval == -1)
  {
    LOG_Report(errno, "SAF", "kill (pid %d, sig %d) failed", (int) pid, sig);
  }
  return retval;
}


//
//  Function:   safegetenv
//  Purpose:    safe getenv(s)
//              
//  Parms:      
//  Returns:    
//
char *safegetenv(const char *name)
{
  char *retval;

  retval = getenv(name);
  if (!retval)
  {
    LOG_Report(errno, "SAF", "getenv on [%s] failed", name);
  }
  return retval;
}

//
//  Function:   safechdir
//  Purpose:    safe chdir(s)
//              
//  Parms:      
//  Returns:    
//
int safechdir(const char *path)
{
  int retval;

  retval = chdir(path);
  if (retval == -1)
  {
    LOG_Report(errno, "SAF", "chdir to [%s] failed", path);
  }
  return retval;
}

//
//  Function:   safemkdir
//  Purpose:    safe mkdir(s, m)
//              
//  Parms:      
//  Returns:    
//
int safemkdir(const char *path, mode_t mode)
{
  int retval;

  retval = mkdir(path, mode);
  if (retval == -1)
  {
    LOG_Report(errno, "SAF", "mkdir [%s] failed", path);
  }
  return retval;
}

//
//  Function:   safestat
//  Purpose:    safe stat(s, b)
//              
//  Parms:      
//  Returns:    
//
int safestat(const char *file_name, struct stat *buf)
{
   int retval;

   retval = stat(file_name, buf);
   if (retval == -1)
   {
       LOG_Report(errno, "SAF", "Couldn't stat [%s]", file_name);
   }
   return retval;
}  

//
//  Function:   safeopen
//  Purpose:    safe open(s, f)
//              
//  Parms:      
//  Returns:    
//
int safeopen(const char *pathname, int flags)
{
  int retval;

  if ((retval = open(pathname, flags)) == -1) 
  {
    LOG_Report(errno, "SAF", "open [%s] failed", pathname);
  }
  return retval;
}

//
//  Function:   safeopen2
//  Purpose:    safe open(s, f, m)
//              
//  Parms:      
//  Returns:    
//
int safeopen2(const char *pathname, int flags, mode_t mode)
{
  int retval;

  retval = open(pathname, flags, mode);
  if (retval == -1)
  {
    LOG_Report(errno, "SAF", "open2 [%s] failed", pathname);
  }
  return retval;
}

//
//  Function:   safepipe
//  Purpose:    safe pipe(d)
//              
//  Parms:      
//  Returns:    
//
int safepipe(int filedes[2])
{
  int retval;

  retval = pipe(filedes);
  if (retval == -1)
  {
    LOG_Report(errno, "SAF", "pipe failed");
  }
  return retval;
}

//
//  Function:   safedup2
//  Purpose:    safe dup2(o, n)
//              
//  Parms:      
//  Returns:    
//
int safedup2(int oldfd, int newfd)
{
  int retval;

  retval = dup2(oldfd, newfd);
  if (retval == -1)
  {
    LOG_Report(errno, "SAF", "dup2 failed"); 
  }
  return retval;
}

//
//  Function:   safeexecvp
//  Purpose:    safe execvp(s, s)
//              
//  Parms:      
//  Returns:    
//
int safeexecvp(const char *file, char *const argv[])
{
  int retval;

  retval = execvp(file, argv);
  if (retval == -1)
  {
    LOG_Report(errno, "SAF", "execvp [%s] failed", file);
  }
  return retval;
}

//
//  Function:   safeseek
//  Purpose:    Seek position in file
//              
//  Parms:      Fd, Position
//  Returns:    position or -1 (+ errno)
//
size_t safeseek(int iFd, size_t tPos)
{
  size_t tNewPos;

  tNewPos = lseek(iFd, tPos, SEEK_SET);
  if (tNewPos != tPos)
  {
     LOG_Report(errno, "SAF", "safeseek %lld from fd %d failed", tPos, iFd);
     tNewPos = -1;
  }
  return(tNewPos);
}

//
//  Function:   saferead
//  Purpose:    
//              
//  Parms:      
//  Returns:    
//
size_t saferead(int iFd, void *pvBuffer, size_t tNr)
{
  size_t tRead;

  tRead = read(iFd, pvBuffer, tNr);
  if (tRead == -1)
  {
     LOG_Report(errno, "SAF", "read %d bytes from fd %lld failed", tNr, iFd);
  }
  return(tRead);
}

//
//  Function:   safewrite
//  Purpose:    Write to file
//              
//  Parms:      Fd, Buffer, size
//  Returns:    Cc
//
size_t safewrite(int iFd, const char *pcBuffer, size_t tNr)
{
  size_t tWrite;

  tWrite = write(iFd, pcBuffer, tNr);
  if (tWrite == -1)
  {
     LOG_Report(errno, "SAF", "write %lld bytes to fd %d failed", tNr, iFd);
  }
  return(tWrite);
}

//
//  Function:   safeclose
//  Purpose:    
//              
//  Parms:      
//  Returns:    
//
int safeclose(int iFd)
{
  int iCc;

  iCc = close(iFd);

  if (iCc == -1) 
  {
    LOG_Report(errno, "SAF", "Possible serious problem: close failed");
  }
  return(iCc);
}

//
//  Function:   safeferror
//  Purpose:    safe open error file
//              
//  Parms:      Error file path
//  Returns:    TRUE if opened OKee 
//
bool safeferror(char *pcPath)
{
   bool  fCc=FALSE;

   if( (SafeLibErrorDest = safefopen(pcPath, "a+")) ) fCc = TRUE;
   return(fCc);
}

//
//  Function:   safefopen
//  Purpose:    
//              
//  Parms:      
//  Returns:    
//
FILE *safefopen(char *path, char *mode)
{
  FILE *retval;

  retval = fopen(path, mode);
  if (!retval)
  {
    LOG_Report(errno, "SAF", "fopen [%s] failed", path);
  }
  return retval;
}

//
//  Function:   safefread
//  Purpose:    
//              
//  Parms:      
//  Returns:    
//
size_t safefread(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
  size_t retval;

  retval = fread(ptr, size, nmemb, stream);
  if (ferror(stream))
  {
    LOG_Report(errno, "SAF", "fread failed");
  }
  return retval;
}

//
//  Function:   safefgets
//  Purpose:    
//              
//  Parms:      
//  Returns:    
//
char *safefgets(char *s, int size, FILE *stream) 
{
  char *retval;

  retval = fgets(s, size, stream);
  if (!retval) 
  {
    LOG_Report(errno, "SAF", "fgets failed");
  }
  return retval;
}

//
//  Function:   safefwrite
//  Purpose:    
//              
//  Parms:      
//  Returns:    
//
size_t safefwrite(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
  size_t retval;

  retval = fwrite(ptr, size, nmemb, stream);
  if (ferror(stream))
  {
    LOG_Report(errno, "SAF", "fwrite failed");
  }
  return retval;
}

//
//  Function:   safefclose
//  Purpose:    
//              
//  Parms:      
//  Returns:    
//
int safefclose(FILE *stream)
{
  int retval;

  retval = fclose(stream);
  if (retval != 0)
  {
    LOG_Report(errno, "SAF", "Possibly serious error: fclose failed");
  }
  return retval;
}

//
//  Function:   safefflush
//  Purpose:    
//              
//  Parms:      
//  Returns:    
//
int safefflush(FILE *stream)
{
  int retval;

  retval = fflush(stream);
  if (retval != 0)
  {
    LOG_Report(errno, "SAF", "fflush failed");
  }
  return retval;
}

//
//  Function:  safefeof
//  Purpose:   Check stream EOF
//              
//  Parms:      
//  Returns:   +1 if EOF marker is set
//              0 No EOF
//             -1 No stream
//
int safefeof(FILE *stream)
{
   if(stream) return(feof(stream));
   //
   LOG_Report(0, "SAF", "feof has no stream");
   return(-1);
}

//
//  Function:   safemalloccount
//  Purpose:    return the malloc/free sequences
//              
//  Parms:      void
//  Returns:    malloc/free balance
//
int safemalloccount(void)
{
   return(GLOBAL_GetMallocs());
}

//
//  Function:   safemalloc
//  Purpose:    Safely allocate memory, keeping track of the malloc/free sequences
//              
//  Parms:      
//  Returns:    
//
void *safemalloc(size_t size)
{
  void *pvData;

  pvData = malloc(size);
  if (!pvData)
  {
    save_HandleError(ENOMEM, "SAF", "malloc failed");
  }
  else
  {
     //
     // Count the allocation and reset the data
     //
     GLOBAL_PutMallocs(1);
     bzero(pvData, size);
  }
  return(pvData);
}

//
// Function:   saferemalloc
// Purpose:    Re-malloc memory, copy data over in new buffer
//             
// Parms:      old buffer, new size
// Returns:    new buffer
//
void *saferemalloc(void *pvData, size_t iSize)
{
   void *pvNewData;

   if(pvData == NULL)
   {
      //
      // This is a new malloc: treat as one
      //
      GLOBAL_PutMallocs(1);
   }
   pvNewData = realloc(pvData, iSize);
   if (pvNewData == NULL)
   {
      //
      // Big problems re-allocating memory: need to free old memory to prevent
      // memory leaks
      //
      if(pvData) SAFEFREE(__PRETTY_FUNCTION__ , pvData);
      save_HandleError(ENOMEM, "SAF", "saferemalloc() failed, size=", "%d", iSize);
      //
      // We cannot go back since safemalloc relies on a non-NULL pointer return !
      // We raise an SIGTERM already.
      //
      while(1) sleep(2);
   }
   return(pvNewData);
}

//
//  Function:   safefree
//  Purpose:    Free allocated memory
//              
//  Parms:      Memory ptr
//  Returns:    NULL
//
void *safefree(void *pvData)
{
  if(pvData == NULL)
  {
    save_HandleError(ENOMEM, "SAF", "Free NULL ptr!");
  }
  else 
  {
     GLOBAL_PutMallocs(-1);
     free(pvData);
  }
  return(NULL);
}

//
//  Function:   safefreex
//  Purpose:    Free allocated memory, debug version diaplaying caller's function
//              
//  Parms:      Func-name, memory ptr
//  Returns:    NULL
//
void *safefreex(const char *pcFunc, void *pvData)
{
  if(pvData == NULL)
  {
    save_HandleError(ENOMEM, "SAF", "Free NULL ptr, called from [%s()]!", pcFunc);
  }
  else 
  {
     GLOBAL_PutMallocs(-1);
     free(pvData);
  }
  return(NULL);
}


//
//  Function:   safefork
//  Purpose:    
//              
//  Parms:      
//  Returns:    
//
pid_t safefork(void)
{
  int retval;

  retval = fork();

  if (retval == -1)
  {
    LOG_Report(errno, "SAF", "fork failed");
  }
  return retval;
}

/*------  Local functions separator -----------------------------------------
__LOCAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/


//
//  Function:   save_HandleError
//  Purpose:    Handle the error exit of these functions
//              
//  Parms:      Errno, Caller
//  Returns:    
//
static void save_HandleError(int iErrNo, const char *const pcCaller, const char *fmt, ...) 
{
   va_list           fmtargs;
   struct sigaction  sastruct;
   const char       *pcPrevCaller=SafeLibErrorLoc;
   FILE             *ptFile=SafeLibErrorDest;

   if(ptFile == NULL) ptFile = stderr;
   //
   // Safe these into global variables for any possible signal handler.
   //
   SafeLibErrorLoc = pcCaller;
   SafeLibErrno    = iErrNo;
   //
   // Print the error message(s)
   //
   va_start(fmtargs, fmt);
   //
   GEN_FPRINTF(ptFile, "Com_Safe(): Error in [%s]: ", pcCaller);
   GEN_VFPRINTF(ptFile, fmt, fmtargs);
   va_end(fmtargs);
   GEN_FPRINTF(ptFile, CRLF);
   //
   if(iErrNo) 
   {
       GEN_FPRINTF(ptFile, "Com_Safe(): Error cause: [%s]" CRLF, strerror(iErrNo));
   }
   //
   // Exit if no signal handler.  Otherwise, raise a signal (only ONCE!)
   //
   sigaction(SIGTERM, NULL, &sastruct);
   if (sastruct.sa_handler != SIG_DFL) 
   {
      if(!pcPrevCaller) raise(SIGTERM);
   } 
}
