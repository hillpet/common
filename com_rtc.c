/*  (c) Copyright:  2005..2019  Patrn, Confidential Data
 *
 *  Workfile:           com_rtc.c
 *  Purpose:            This module handles all real-time clock functions
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    20 Aug 2005:      Created
 *    26 Jan 2019:      Add DST_AUTO
 *    07 Oct 2019:      Add TIME_FORMAT_WW_DD_MMM_YYYY_HH_MM_SS
 *    21 Oct 2019:      Add TIME_FORMAT_MM_SS
 *    18 Oct 2020:      Remove option DST (handled by Linux)
 *    01 May 2021:      CSV formats have no leading zero's (%d i.o %02d)
 *    05 Jul 2021:      Add TIME_FORMAT_PIKRELLCAM
 *    19 Nov 2021:      Add Variadic macro's for PRINTF()
 *    28 Mar 2022:      Real CLOCK related calls should take DST into account !
 *    03 Apr 2023:      RTC_GetMidnight(): DST_YES
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *    RTC and DST:
 *    
 *    All SECS_SINCE_1970 do NOT take DST into account. Only when converting SECS_SINCE_1970
 *    back to any CLOCK related value, such as:
 *
 *    RTC_ConvertDateTime()   if DST: +3600 Secs
 *    RTC_GetYear()           if DST: +3600 Secs
 *    RTC_GetMonth()          if DST: +3600 Secs
 *    RTC_GetDay()            if DST: +3600 Secs
 *    RTC_GetHour()           if DST: +3600 Secs
 *    RTC_GetMidnight()       if DST: +3600 Secs
 *
 *    Reworking CLOCK date/time back to SECS_SINCE_1970, take DST back into account:
 *
 *    RTC_GetSecs()           if DST: -3600 Secs
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <semaphore.h>

#include "typedefs.h"
#include "config.h"
#include "com_func.h"
#include "com_rtc.h"
#include "com_log.h"

//#define USE_PRINTF
#include "printx.h"

//
// DST handled by OS or not
//
#define DST_HANDLED_BY_OS

//
// Local prototypes
//
static void    rtc_ClockFormat                 (int, u_int32, CLOCK_DAY_TIME *);
static void    rtc_ClockMJDtoYMD               (u_int32, u_int32 *, u_int32 *, u_int32 *);
static void    rtc_ClockMJDtoDofW              (u_int32, u_int32 *);
static void    rtc_ClockUTCtoHMS               (u_int32, u_int8 *, u_int8 *, u_int8 *);
static bool    rtc_DaylightSavingTime          (u_int32);

#ifndef DST_HANDLED_BY_OS
static u_int32 rtc_FindNextDaylightSavingTime  (u_int32, u_int8);
//#define  FEATURE_TEST_TIME
#ifdef   FEATURE_TEST_TIME
#define  TIME_TEST()                           rtc_TestDST()
static   void    rtc_TestDST                   (void);
static   u_int32 rtc_TestDSTSkipYear           (u_int32, u_int8);

#else    //FEATURE_TEST_TIME
#define  TIME_TEST()

#endif   //FEATURE_TEST_TIME

#endif   //DST_HANDLED_BY_OS

static const char *cDaysOfTheWeekEn[]  = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };
static const char *cDaysOfTheWeek[]    = { "Zo",  "Ma",  "Di",  "Wo",  "Do",  "Vr",  "Za" };
//
static const int   iDaysinMonth[]      = {  0,     31,    28,    31,    30,    31,    30,    31,    31,    30,    31,    30,    31   };
static const int   iDaysinMonthLeap[]  = {  0,     31,    29,    31,    30,    31,    30,    31,    31,    30,    31,    30,    31   };
//
static const char *cMonthOfTheYearEn[] = { "---", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
static const char *cMonthOfTheYear[]   = { "---", "Jan", "Feb", "Mrt", "Apr", "Mei", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec" };
//
static const char *cDaylight[]         = { "W", "Z" };
//
// Linux will return the Secs_since_1970 in UTC.
// Amsterdam : GMT +1
//
static int         iTimeZone           = 1;

/**
 **  Name:         RTC_DaylightSavingTime
 **
 **  Description:  Return the daylight saving time (aka Summertime)
 **
 **  Arguments:    Secs since 1970-01-01
 **
 **  Returns:      TRUE if DST
 **/
bool RTC_DaylightSavingTime(u_int32 ulSecs)
{
  return(rtc_DaylightSavingTime(ulSecs));
}

/**
 **  Name:         RTC_GetSystemSecs
 **
 **  Description:  Get the current system time
 **
 **  Arguments:    time_t time(time_t *tloc);
 **                time() returns the time as the number of seconds since the Epoch,
 **                1970-01-01 00:00:00 +0000 (UTC).
 **                
 **                If tloc is non-NULL, the return value is also stored in the memory
 **                pointed to by tloc.
 **                
 **  Returns:      Current time in secs since 1970
 **                On success, the value of time in seconds since the Epoch is returned.
 **                On error, ((time_t) -1) is returned, and errno is set appropriately.
 **                
 **/
u_int32 RTC_GetSystemSecs(void)
{
  int  iSecs;

  iSecs  = (int) time(NULL);
  iSecs += (iTimeZone * ES_SECONDS_PER_HOUR);
  return( (u_int32) iSecs);
}

/**
 **  Name:        RTC_GetSecs
 **
 **  Description: Get the current system time in seconds since 1970 untill 00:00 of this day
 **
 **  Arguments:   Year, Month(1..12), Day((1..31)
 **
 **  Returns:     Time in seconds since 1970 untill 00:00 of iDay
 **  Note:        We are converting DST sensitive CLOCK values back to SECS-SINCE-1970.
 **               SECS-SINCE-1970 are NOT affected by DST. 
 **/
u_int32 RTC_GetSecs(int iYear, int iMonth, int iDay)
{
   int      iNr, iY, iM, iD;
   u_int32  ulSecs=1;

   //
   // We're at 1970-01-01_00:00:01
   //
   for(iY=1970; iY<iYear; iY++)
   {
      // Sum days for each year passed
      for(iM=1; iM<=12; iM++)
      {
         iNr     = RTC_GetDaysPerMonth(ulSecs);
         ulSecs += (iNr * ES_SECONDS_PER_DAY);
         //PRINTF("RTC-GetSecs():%04d-%02d has %d days" CRLF, iY, iM+1, iNr);
      }
   }
   //
   // We're at YEAR-01-01_00:00:01
   //
   for(iM=1; iM<iMonth; iM++)
   {
      iNr     = RTC_GetDaysPerMonth(ulSecs);
      ulSecs += (iNr * ES_SECONDS_PER_DAY);
   }
   //
   // We're at YEAR-MM-01_00:00:01
   //
   for(iD=1; iD<iDay; iD++)
   {
      ulSecs += ES_SECONDS_PER_DAY;
   }
   //
   // We're at YEAR-MM-DD_00:00:01
   // Take daylight saving into account.
   //
   if( rtc_DaylightSavingTime(ulSecs) )
   {
      ulSecs -= ES_SECONDS_PER_HOUR;
   }
   return(--ulSecs);
}

/**
 **  Name:         RTC_SetTimeZone
 **
 **  Description:  Set the current GMT timezone setting (-12...0...+12)
 **
 **  Arguments:    Offset in hours from GMT
 **
 **  Returns:      
 **/
void RTC_SetTimeZone(int iGmtOffset)
{
  if( (iGmtOffset <=12) || (iGmtOffset >= -12) )
  { 
     iTimeZone = iGmtOffset;
  }
  else
  {
     LOG_Report(0, "RTC", "Bad GMT offset %d", iGmtOffset);
  }
}

/**
 **  Name:         RTC_GetCurrentQuarter
 **
 **  Description:  Get the current quarter number
 **
 **  Arguments:    Optional time in secs (0 = now)
 **
 **  Returns:      Current quarter index  0 .. (24*4)-1
 **/
u_int32 RTC_GetCurrentQuarter(u_int32 ulSecs)
{
  CLOCK_DAY_TIME stTime;
  u_int32        ulQs;

  //
  // Get the date and time data from the global total-secs, if necessary
  //
  if(ulSecs == 0) ulSecs = RTC_GetSystemSecs();

  rtc_ClockFormat(DST_YES, ulSecs, &stTime);

  ulQs  = (((u_int32) stTime.ubHour) *  4);
  ulQs += (((u_int32) stTime.ubMin)  / 15);

  if(ulQs > ES_QUARTERS_PER_DAY - 1)
  {
     ulQs = ES_QUARTERS_PER_DAY - 1;
  }
  return(ulQs);
}

/**
 **  Name:         RTC_GetDateTime
 **
 **  Description:  Get the system date/time in this time zone. 
 **
 **  Arguments:    Buffer[+1] for date/time string
 **
 **  Returns:      Current time in secs
 **/
u_int32 RTC_GetDateTime(char *pcDateTime)
{
  u_int32 ulSecs;

  //
  // Get the ASCII date and time string from the global total-secs including THIS timezone
  //
  ulSecs = RTC_GetSystemSecs();
  //
  // Rework total secs to date/time, include daylight saving !!!
  //
  RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MM_YYYY_HH_MM, pcDateTime, ulSecs);
  return(ulSecs);
}

/**
 **  Name:         RTC_GetDateTimeSecs
 **
 **  Description:  Get the system date/time in hh:mm:ss in this time zone. 
 **
 **  Arguments:    Buffer[22+1] for date/time string 
 **
 **  Returns:      Current time in secs
 **/
u_int32 RTC_GetDateTimeSecs(char *pcDateTime)
{
  u_int32 ulSecs;

  //
  // Get the ASCII date and time string from the global total-secs including THIS timezone
  //
  ulSecs = RTC_GetSystemSecs();
  //
  // Rework total secs to date/time, include daylight saving !!!
  //
   RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MM_YYYY_HH_MM_SS, pcDateTime, ulSecs);
   return(ulSecs);
}

/**
 **  Name:         RTC_ConvertDateTime
 **
 **  Description:  Rework a seconds count to ascii date/time string
 ** 
 **  Arguments:    Format, Buffer for date/time string, base seconds
 **
 **  Returns:      void
 **  Note:         MAX_TIME_LENZ is the maximum buffer size used for all formats
 **/
void RTC_ConvertDateTime(TIMEFORMAT tFormat, char *pcDateTime, u_int32 ulSecs)
{
   CLOCK_DAY_TIME stTime;
   u_int8         ubDst;

   //
   //  Convert to string
   //
   if(pcDateTime)
   {
      rtc_ClockFormat(DST_YES, ulSecs, &stTime);
      //
      switch(tFormat)
      {
         case TIME_FORMAT_WW_DD_MM_YYYY_HH_MM:
            // size=20 <Mo|29-08-2005|10:23|>
            GEN_SPRINTF(pcDateTime, "%s|%02u-%02u-%u|%02u:%02u|",
                                    cDaysOfTheWeek[stTime.ubWeekDay],
                                    stTime.ubDay,
                                    stTime.ubMonth,
                                    stTime.ubYear + 1900,
                                    stTime.ubHour,
                                    stTime.ubMin);
            break;

         case TIME_FORMAT_WW_DD_MM_YYYY_HH_MM_SS:
            // size=22 <Mo|29-08-2005|14:56:34>
            GEN_SPRINTF(pcDateTime, "%s|%02u-%02u-%u|%02u:%02u:%02u",
                                    cDaysOfTheWeek[stTime.ubWeekDay],
                                    stTime.ubDay,
                                    stTime.ubMonth,
                                    stTime.ubYear + 1900,
                                    stTime.ubHour,
                                    stTime.ubMin,
                                    stTime.ubSec);
            break;

         case TIME_FORMAT_WW_S_DD_MM_YYYY_HH_MM:
            // size=21 <Mo(Z)29-08-2005 14:56>
            ubDst = rtc_DaylightSavingTime(ulSecs)?1:0;
            GEN_SPRINTF(pcDateTime, "%s(%s)%02u-%02u-%u %02u:%02u",
                                    cDaysOfTheWeek[stTime.ubWeekDay],
                                    cDaylight[ubDst],
                                    stTime.ubDay,
                                    stTime.ubMonth,
                                    stTime.ubYear + 1900,
                                    stTime.ubHour,
                                    stTime.ubMin);
            break;

         case TIME_FORMAT_WW_S_DD_MM_YYYY_HH_MM_SS:
            // size=21 <Mo(Z)29-08-2005 14:56:23>
            ubDst = rtc_DaylightSavingTime(ulSecs)?1:0;
            GEN_SPRINTF(pcDateTime, "%s(%s)%02u-%02u-%u %02u:%02u:%02u",
                                    cDaysOfTheWeek[stTime.ubWeekDay],
                                    cDaylight[ubDst],
                                    stTime.ubDay,
                                    stTime.ubMonth,
                                    stTime.ubYear + 1900,
                                    stTime.ubHour,
                                    stTime.ubMin,
                                    stTime.ubSec);
            break;

         case TIME_FORMAT_WW_DD_MMM_YYYY_HH_MM_SS:
            // size=23 <Mo 29 Aug 2005 14:56:44>
            GEN_SPRINTF(pcDateTime, "%s %02u %s %u %02u:%02u:%02u",
                                    cDaysOfTheWeek[stTime.ubWeekDay],
                                    stTime.ubDay,
                                    cMonthOfTheYear[stTime.ubMonth],
                                    stTime.ubYear + 1900,
                                    stTime.ubHour,
                                    stTime.ubMin,
                                    stTime.ubSec);
            break;

         case TIME_FORMAT_WW_DD_MMM_YYYY:
            // size=14 <Mo 29 Aug 2005>
            GEN_SPRINTF(pcDateTime, "%s %02u %s %u",
                                    cDaysOfTheWeek[stTime.ubWeekDay],
                                    stTime.ubDay,
                                    cMonthOfTheYear[stTime.ubMonth],
                                    stTime.ubYear + 1900);
            break;

         case TIME_FORMAT_WW_DD_MMM_YYYY_HH_MM:
            // size=20 <Mo 29 Aug 2005 14:56>
            GEN_SPRINTF(pcDateTime, "%s %02u %s %u %02u:%02u",
                                    cDaysOfTheWeek[stTime.ubWeekDay],
                                    stTime.ubDay,
                                    cMonthOfTheYear[stTime.ubMonth],
                                    stTime.ubYear + 1900,
                                    stTime.ubHour,
                                    stTime.ubMin);
            break;

         case TIME_FORMAT_WW_DD_MM_YYYY:
            // size=13 <Mo 29-08-2005>
            GEN_SPRINTF(pcDateTime, "%s %02u-%02u-%u",
                                    cDaysOfTheWeek[stTime.ubWeekDay],
                                    stTime.ubDay,
                                    stTime.ubMonth,
                                    stTime.ubYear + 1900);
            break;

         case TIME_FORMAT_DD_MM_YYYY_HH_MM:
            // size=16 <29-08-2005 14:56>
            GEN_SPRINTF(pcDateTime, "%02u-%02u-%u %02u:%02u",
                                    stTime.ubDay,
                                    stTime.ubMonth,
                                    stTime.ubYear + 1900,
                                    stTime.ubHour,
                                    stTime.ubMin);
            break;

         case TIME_FORMAT_DD_MM_YYYY:
            // size=10 <29-08-2005>
            GEN_SPRINTF(pcDateTime, "%02u-%02u-%u",
                                    stTime.ubDay,
                                    stTime.ubMonth,
                                    stTime.ubYear + 1900);
            break;

         case TIME_FORMAT_HH_MM_SS:
            // size=8 <14:56:38>
            GEN_SPRINTF(pcDateTime, "%02u:%02u:%02u",
                                    stTime.ubHour,
                                    stTime.ubMin,
                                    stTime.ubSec);
            break;

         case TIME_FORMAT_HH_MM:
            // size=5 <14:56>
            GEN_SPRINTF(pcDateTime, "%02u:%02u",
                                    stTime.ubHour,
                                    stTime.ubMin);
            break;

         case TIME_FORMAT_MM_SS:
            // size=5 <14:56>
            GEN_SPRINTF(pcDateTime, "%02u:%02u",
                                    stTime.ubMin,
                                    stTime.ubSec);
            break;

         case TIME_FORMAT_WW_YYYY_MM_DD:
            // size=10 <Mo20050829>
            GEN_SPRINTF(pcDateTime, "%s%04u%02u%02u",
                                    cDaysOfTheWeek[stTime.ubWeekDay],
                                    stTime.ubYear + 1900,
                                    stTime.ubMonth,
                                    stTime.ubDay);
            break;

         case TIME_FORMAT_AMM_YYYY:
            // size=8 <Aug 2005>
            GEN_SPRINTF(pcDateTime, "%s %04u",
                                    cMonthOfTheYear[stTime.ubMonth],
                                    stTime.ubYear + 1900);
            break;

         case TIME_FORMAT_YYYY_MM:
            // size=6 <200508>
            GEN_SPRINTF(pcDateTime, "%04u%02u",
                                    stTime.ubYear + 1900,
                                    stTime.ubMonth);
            break;

         case TIME_FORMAT_YYYY_MM_DD:
            // size=8 <20050829>
            GEN_SPRINTF(pcDateTime, "%04u%02u%02u",
                                    stTime.ubYear + 1900,
                                    stTime.ubMonth,
                                    stTime.ubDay);
            break;

         case TIME_FORMAT_YYYY_MM_DD_WW:
            // size=11 <20050829_Mo>
            GEN_SPRINTF(pcDateTime, "%04u%02u%02u_%s",
                                    stTime.ubYear + 1900,
                                    stTime.ubMonth,
                                    stTime.ubDay,
                                    cDaysOfTheWeek[stTime.ubWeekDay]);
            break;

         case TIME_FORMAT_YYYY_MM_DD_HH_MM_WW:
            // size=16 <20050829_1255_Mo>
            GEN_SPRINTF(pcDateTime, "%04u%02u%02u_%02u%02u_%s",
                                    stTime.ubYear + 1900,
                                    stTime.ubMonth,
                                    stTime.ubDay,
                                    stTime.ubHour,
                                    stTime.ubMin,
                                    cDaysOfTheWeek[stTime.ubWeekDay]);
            break;

         case TIME_FORMAT_YYYY_MM_DD_HH_MM_SS:
            // size=15 <20050829_125559>
            GEN_SPRINTF(pcDateTime, "%04u%02u%02u_%02u%02u%02u",
                                    stTime.ubYear + 1900,
                                    stTime.ubMonth,
                                    stTime.ubDay,
                                    stTime.ubHour,
                                    stTime.ubMin,
                                    stTime.ubSec);
            break;

         case TIME_FORMAT_CSV_YYYY_MM_DD_HH_MM_SS:
            // size=19 <2005,12,29,12,55,59>
            GEN_SPRINTF(pcDateTime, "%4u,%2u,%2u,%2u,%2u,%2u",
                                    stTime.ubYear + 1900,
                                    stTime.ubMonth,
                                    stTime.ubDay,
                                    stTime.ubHour,
                                    stTime.ubMin,
                                    stTime.ubSec);
            break;

         case TIME_FORMAT_CSV_YYYY_MM_DD:
            // size=10 <2005,12,29>
            GEN_SPRINTF(pcDateTime, "%4u,%2u,%2u",
                                    stTime.ubYear + 1900,
                                    stTime.ubMonth,
                                    stTime.ubDay);
            break;

         case TIME_FORMAT_CSV_HH_MM_SS:
            // size=8 <12,55,59>
            GEN_SPRINTF(pcDateTime, "%2u,%2u,%2u",
                                    stTime.ubHour,
                                    stTime.ubMin,
                                    stTime.ubSec);
            break;

         case TIME_FORMAT_CSV_HH_MM:
            // size=5 <12,55>
            GEN_SPRINTF(pcDateTime, "%2u,%2u",
                                    stTime.ubHour,
                                    stTime.ubMin);
            break;

         case TIME_FORMAT_MAIL:
            // size=31 <Mon, 29 Nov 2017 14:56:23 +0100>
            if(iTimeZone < 0)
            {
               GEN_SPRINTF(pcDateTime, "%s, %u %s %u %02u:%02u:%02u -%02d00",
                                          cDaysOfTheWeekEn[stTime.ubWeekDay],
                                          stTime.ubDay,
                                          cDaysOfTheWeekEn[stTime.ubMonth],
                                          stTime.ubYear + 1900,
                                          stTime.ubHour,
                                          stTime.ubMin,
                                          stTime.ubSec,
                                          abs(iTimeZone));
            }
            else
            {
               GEN_SPRINTF(pcDateTime, "%s, %u %s %u %02u:%02u:%02u +%02d00",
                                          cDaysOfTheWeekEn[stTime.ubWeekDay],
                                          stTime.ubDay,
                                          cMonthOfTheYearEn[stTime.ubMonth],
                                          stTime.ubYear + 1900,
                                          stTime.ubHour,
                                          stTime.ubMin,
                                          stTime.ubSec,
                                          abs(iTimeZone));
            }
            break;

         case TIME_FORMAT_DAY_OF_WEEK:
            // size=3 <Mon>
            GEN_SPRINTF(pcDateTime, "%s", cDaysOfTheWeekEn[stTime.ubWeekDay]);
            break;
         
         case TIME_FORMAT_PIKRELLCAM:
            // size=19 <2021-07-05_13.34.22>
            GEN_SPRINTF(pcDateTime, "%04u-%02u-%02u_%02u.%02u.%02u",
                                    stTime.ubYear + 1900,
                                    stTime.ubMonth,
                                    stTime.ubDay,
                                    stTime.ubHour,
                                    stTime.ubMin,
                                    stTime.ubSec);
            break;

         default:
            break;
      }
   }
}

/**
 **  Name:         RTC_ConvertDateTimeSzie
 **
 **  Description:  Return formsat size of ascii date/time string
 ** 
 **  Arguments:    Format
 **
 **  Returns:      Size in bytes (incl terminator)
 **/
int RTC_ConvertDateTimeSize(TIMEFORMAT tFormat)
{
   int iSize=0;

   switch(tFormat)
   {
      case TIME_FORMAT_WW_DD_MM_YYYY_HH_MM:
         // size=21 <Mo|29-08-2005|10:23>
         iSize = 21;
         break;

      case TIME_FORMAT_WW_DD_MM_YYYY_HH_MM_SS:
         // size=22 <Mo|29-08-2005|14:56:34>
         iSize = 22;
         break;

      case TIME_FORMAT_WW_S_DD_MM_YYYY_HH_MM:
         // size=21 <Mo(Z)29-08-2005 14:56>
         iSize = 21;
         break;

      case TIME_FORMAT_WW_DD_MMM_YYYY_HH_MM_SS:
         // size=23 <Mo 29 Aug 2005 14:56:44>
         iSize = 23;
         break;

      case TIME_FORMAT_WW_DD_MMM_YYYY:
         // size=14 <Mo 29 Aug 2005>
         iSize = 14;
         break;

      case TIME_FORMAT_WW_DD_MMM_YYYY_HH_MM:
         // size=20 <Mo 29 Aug 2005 14:56>
         iSize = 20;
         break;

      case TIME_FORMAT_WW_DD_MM_YYYY:
         // size=13 <Mo 29-08-2005>
         iSize = 13;
         break;

      case TIME_FORMAT_DD_MM_YYYY_HH_MM:
         // size=16 <29-08-2005 14:56>
         iSize = 16;
         break;

      case TIME_FORMAT_DD_MM_YYYY:
         // size=10 <29-08-2005>
         iSize = 10;
         break;

      case TIME_FORMAT_HH_MM_SS:
         // size=8 <14:56:38>
         iSize = 8;
         break;

      case TIME_FORMAT_HH_MM:
         // size=8 <14:56>
         iSize = 5;
         break;

      case TIME_FORMAT_MM_SS:
         // size=5 <56:38>
         iSize = 5;
         break;

      case TIME_FORMAT_WW_YYYY_MM_DD:
         // size=10 <Mo20050829>
         iSize = 10;
         break;

      case TIME_FORMAT_YYYY_MM:
         // size=6 <200508>
         iSize = 6;
         break;

      case TIME_FORMAT_AMM_YYYY:
         // size=8 <Aug 2005>
         iSize = 8;
         break;

      case TIME_FORMAT_YYYY_MM_DD:
         // size=8 <20050829>
         iSize = 8;
         break;

      case TIME_FORMAT_YYYY_MM_DD_WW:
         // size=11 <20050829_Mo>
         iSize = 11;
         break;

      case TIME_FORMAT_YYYY_MM_DD_HH_MM_WW:
         // size=16 <20050829_1255_Mo>
         iSize = 16;
         break;

      case TIME_FORMAT_YYYY_MM_DD_HH_MM_SS:
         // size=15 <20050829_125559>
         iSize = 15;
         break;

      case TIME_FORMAT_CSV_YYYY_MM_DD_HH_MM_SS:
         // size=19 <2005,08,29,12,55,59>
         iSize = 19;
         break;

      case TIME_FORMAT_CSV_YYYY_MM_DD:
         // size=10 <2005,08,29>
         iSize = 10;
         break;

      case TIME_FORMAT_CSV_HH_MM_SS:
         // size=8 <12,55,59>
         iSize = 8;
         break;

      case TIME_FORMAT_CSV_HH_MM:
         // size=5 <12,55>
         iSize = 5;
         break;

      case TIME_FORMAT_MAIL:
         // size=31 <Mon, 29 Nov 2017 14:56:23 +0100>
         iSize = 31;
         break;

      case TIME_FORMAT_DAY_OF_WEEK:
         // size=3 <Mon>
         iSize = 3;
         break;
      
      default:
         break;
   }
   return(iSize+1);
}

/**
 **  Name:        RTC_GetYear
 **
 **  Description: Rework a seconds count to current year
 ** 
 **  Arguments:   Optional time in secs (0 = now)
 **
 **  Returns:     Year (1970...20xx)
 **  Note:        SECS-SINCE-1970 are NOT affected by DST. 
 **               We are converting SECS-SINCE-1970 to DST sensitive CLOCK values, so adjust for DST.
 **/
int RTC_GetYear(u_int32 ulSecs)
{
   CLOCK_DAY_TIME stTime;

   if(ulSecs == 0) ulSecs = RTC_GetSystemSecs();

   rtc_ClockFormat(DST_YES, ulSecs, &stTime);
   return((int)stTime.ubYear + 1900);
}

/**
 **  Name:        RTC_GetMonth
 **
 **  Description: Rework a seconds count to current month
 ** 
 **  Arguments:   Optional time in secs (0 = now)
 **
 **  Returns:     Month (1..12)
 **  Note:        SECS-SINCE-1970 are NOT affected by DST. 
 **               We are converting SECS-SINCE-1970 to DST sensitive CLOCK values, so adjust for DST.
 **/
int RTC_GetMonth(u_int32 ulSecs)
{
   CLOCK_DAY_TIME stTime;

   if(ulSecs == 0) ulSecs = RTC_GetSystemSecs();

   rtc_ClockFormat(DST_YES, ulSecs, &stTime);
   return((int)stTime.ubMonth);
}

/**
 **  Name:        RTC_GetDay
 **
 **  Description: Rework a seconds count to current day
 ** 
 **  Arguments:   Optional time in secs (0 = now)
 **
 **  Returns:     Day (1..31)
 **  Note:        SECS-SINCE-1970 are NOT affected by DST. 
 **               We are converting SECS-SINCE-1970 to DST sensitive CLOCK values, so adjust for DST.
 **/
int RTC_GetDay(u_int32 ulSecs)
{
   CLOCK_DAY_TIME stTime;

   if(ulSecs == 0) ulSecs = RTC_GetSystemSecs();

   rtc_ClockFormat(DST_YES, ulSecs, &stTime);
   return((int)stTime.ubDay);
}

/**
 **  Name:        RTC_GetMidnight
 **
 **  Description: Rework a seconds count to the clock of 00:00:00 of any day
 ** 
 **  Arguments:   Optional time in secs (0 = now)
 **
 **  Returns:     SECS-SINCE-1970 of the current day's CLOCK 00:00:00 (Start of this day)
 **  Note:        SECS-SINCE-1970 are NOT affected by DST. 
 **               We are converting SECS-SINCE-1970 to DST sensitive CLOCK values, since it is the 
 **               midnight CLOCK 00:00:00 we want. Therefor we must take DST into account.
 **/
u_int32 RTC_GetMidnight(u_int32 ulSecs)
{
   CLOCK_DAY_TIME stTime;

   if(ulSecs == 0) ulSecs = RTC_GetSystemSecs();
   //
   // 03Apr2023: Replace DST_YES
   //
   rtc_ClockFormat(DST_YES, ulSecs, &stTime);
   //
   ulSecs -= ((int)stTime.ubHour) * 3600;
   ulSecs -= ((int)stTime.ubMin)  * 60;
   ulSecs -= ((int)stTime.ubSec);

   #ifdef COMMENT
   rtc_ClockFormat(DST_NO, ulSecs, &stTime);
   //
   ulSecs -= ((int)stTime.ubHour) * 3600;
   ulSecs -= ((int)stTime.ubMin)  * 60;
   ulSecs -= ((int)stTime.ubSec);
   //
   // Take daylight saving into account.
   //
   if( rtc_DaylightSavingTime(ulSecs) )
   {
      ulSecs -= ES_SECONDS_PER_HOUR;
      PRINTF("RTC-GetMidnight():DST: Secs=%d" CRLF, ulSecs);
   }
   else PRINTF("RTC-GetMidnight():NO DST: Secs=%d" CRLF, ulSecs);
   #endif   //COMMENT

   return(ulSecs);
}

/**
 **  Name:        RTC_GetHour
 **
 **  Description: Rework a seconds count to current hour
 ** 
 **  Arguments:   Optional time in secs (0 = now)
 **
 **  Returns:     Hour (0..23)
 **  Note:        SECS-SINCE-1970 are NOT affected by DST. 
 **               We are converting SECS-SINCE-1970 to DST sensitive CLOCK values, so adjust for DST.
 **/
int RTC_GetHour(u_int32 ulSecs)
{
   CLOCK_DAY_TIME stTime;

   if(ulSecs == 0) ulSecs = RTC_GetSystemSecs();

   rtc_ClockFormat(DST_YES, ulSecs, &stTime);
   return((int)stTime.ubHour);
}

/**
 **  Name:        RTC_GetMinute
 **
 **  Description: Rework a seconds count to current minute
 ** 
 **  Arguments:   Optional time in secs (0 = now)
 **
 **  Returns:     Minute (0..59)
 **  Note:        SECS-SINCE-1970 are NOT affected by DST. 
 **               We are converting SECS-SINCE-1970 to DST sensitive CLOCK values, so adjust for DST.
 **/
int RTC_GetMinute(u_int32 ulSecs)
{
   CLOCK_DAY_TIME stTime;

   if(ulSecs == 0) ulSecs = RTC_GetSystemSecs();

   rtc_ClockFormat(DST_YES, ulSecs, &stTime);
   return((int)stTime.ubMin);
}

/**
 **  Name:        RTC_GetSecond
 **
 **  Description: Rework a seconds count to current second
 ** 
 **  Arguments:   Optional time in secs (0 = now)
 **
 **  Returns:     Second (0..59)
 **  Note:        SECS-SINCE-1970 are NOT affected by DST. 
 **               We are converting SECS-SINCE-1970 to DST sensitive CLOCK values, so adjust for DST.
 **/
int RTC_GetSecond(u_int32 ulSecs)
{
   CLOCK_DAY_TIME stTime;

   if(ulSecs == 0) ulSecs = RTC_GetSystemSecs();

   rtc_ClockFormat(DST_YES, ulSecs, &stTime);
   return((int)stTime.ubSec);
}

/**
 **  Name:        RTC_GetDaysPerMonth
 **
 **  Description: Rework a seconds count to number of days in the month
 ** 
 **  Arguments:   Optional time in secs (0 = now)
 **
 **  Returns:     Number of days
 **  Note:        This formula takes account of the facts that all years that are evenly divisible 
 **               by 4 are leap years, but years that are evenly divisible by 100 are not leap years 
 **               unless they are also evenly divisible by 400, in which case they are leap years.
 *
 *                From 1970 onwards: Leap years are
 *                1972,1976,1980,1984,1988,1992,1996,2000,2004,2008,2012,2016,2020,2024,...
 *
 *                Seconds till 2022/01/01 00:00:00 
 *                   = ((2022 - 1970) * 365 * 24 * 3600) + ( 13 * 24 * 3600)
 *                   = 1640995200
 *
 *
 **/
int RTC_GetDaysPerMonth(u_int32 ulSecs)
{
   bool  fLeap=FALSE;
   int   iYear, iMonth, iDays;

   if(ulSecs == 0) ulSecs = RTC_GetSystemSecs();

   iYear  = RTC_GetYear(ulSecs);    // year
   iMonth = RTC_GetMonth(ulSecs);   // month 1..12! iDaysInMonth takes this into account !
   //
   if(iYear % 4 == 0)
   {                        
      // Maybe leap year
      if(iYear % 100 == 0)
      {
         if(iYear % 400 == 0) fLeap = TRUE;
      }                
      else fLeap = TRUE;
   } 
   //
   if(fLeap) 
   {
      iDays = iDaysinMonthLeap[iMonth];
      //PRINTF("RTC-GetDaysPerMonth(): %d-%d leap year" CRLF, iYear, iMonth);
   }
   else      
   {
      iDays = iDaysinMonth[iMonth];
      //PRINTF("RTC-GetDaysPerMonth(): %d-%d no leap year" CRLF, iYear, iMonth);
   }
   //
   return(iDays);
}

/*------  Local functions separator -------------------------------------------
_______________LOCAL_FUNCTIONS(){};
-----------------------------X--------------------------------------------------*/

/*
 ** rtc_ClockMJDtoYMD()
 *
 *  PARAMETERS:   ulMJD    - Modified Julian Date.
 *                pulYear  - Argument in which the year is set.
 *                           Argement value become related to ulMJD and is years since 1900.
 *                pulMonth - Argument in which the month is set.
 *                           Argement value becomes related to ulMJD, range: 1 - 12.
 *                pulDay   - Argument in which the day (related to ulMJD) is set.
 *                           Argement value becomes related to ulMJD, range: 1 - 31.
 *
 *  DESCRIPTION:  Converts MJD to year, month and day.
 *
 *  RETURNS:      None
 *
 */
static void rtc_ClockMJDtoYMD(u_int32 ulMJD, u_int32 *pulYear, u_int32 *pulMonth, u_int32 *pulDay)
{
   u_int32 ulV1, ulV2, ulV3;
   u_int32 ulY1, ulM1;
   u_int32 ulK = 0;

   // calculate using integer formulas
   // year calculation from MPEG specification:
   //   Y' = int([MJD - 15078.2] / 365.25)
   // method: (in 10 radix)
   //   Y' = MJD*8 - 15078.2*8
   //   Y' = Y'/ (365.25 * 8)
   //
   //
   ulY1 = (u_int32)(((ulMJD << 3) - 120626L)/2922);
   // month calculation from MPEG specification
   //   M' = int([MJD - 14956.1 - int(Y' * 365.25)] / 30.6001)
   // method: (in 10 radix)
   //   M' = MJD*8 - 14956.1*8 - int(Y' * 365.25*8)
   //   M' = (M'* 512) / (30.6001 * 512 * 8)
   //
   ulV1 = 2922L;

   ulM1 = (u_int32)((((ulMJD << 3) - 119649L - (0xFFFFFFF8l&(ulY1 * ulV1))) << 9) / 125338L);

   if ((ulM1 == 14) || (ulM1 == 15))
   {
        ulK = 1;
   }
   *pulYear = (u_int32)(ulY1 + ulK);

   *pulMonth = (u_int32)(ulM1 - 1 - ulK*12);


   //
   // day calculation from MPEG specification:
   //   D = MJD - 14956 - int(y' * 365.25) - int(m' * 30.6001)
   // method:  (in 10 radix)
   //   D = (MJD*16 - 14956*16 - int(y1*365.25*16))*256 - int(m1*30.6001*4096)
   //   D = D / 4096
   //      integer conversions are done by masking off all bits below the
   //      shift amount:     FFFF000 if term multiplied by 4096
   //                        FFFFFF0 if term multiplied by 16
   //                        FFFFFF8 if term multiplied by 8
   //
   ulV1 = 239296L;
   ulV2 = 5844L;
   ulV3 = 125338L;

   *pulDay = (u_int32)(((int32)((   ((int32) (ulMJD << 4)) - ulV1
                                -   ((int32)((0xFFFFFFF0L)&(ulY1*ulV2)))  ) << 8)
                                -   (int32)  (0xFFFFF000L &(ulM1*ulV3))   ) >> 12);

}


/*
 ** rtc_ClockMJDtoDofW()
 *
 *  PARAMETERS:   ulMJD       - Modified Julian Date.
 *                pulWeekDay  - Argument is set to the day of the week (related to ulMJD).
 *
 *  DESCRIPTION:  Converts MJD to the day of the week.
 *                Sunday = 0, ....., Saturday = 6.
 *
 *  RETURNS:      None
 *
 */
static void rtc_ClockMJDtoDofW(u_int32 ulMJD, u_int32 *pulWeekDay)
{
   //
   // day of week calculation from MPEG specification:
   //   WD = [ (MJD + 2)mod 7]
   // method:
   //   WD = ((MJD + 2) % 7)
   //   *wd = 0 (sunday) to 6 (saturday)
   //
   *pulWeekDay = (ulMJD + 3) % 7;
}

/*
 ** rtc_ClockUTCtoHMS()
 *
 *  PARAMETERS:   ulUTC          - Universal Time Co-ordinated
 *                pubHour (out)  - Number of Hours (0 .. 23)
 *                pubMin (out)   - Number of Minutes (0 .. 59)
 *                pubSec (out)   - Number of Seconds (0 .. 59)
 *
 *  DESCRIPTION:  Convertes an UTC represented time to HMS represented time.s
 *
 *  RETURNS:      None
 *
 */
static void rtc_ClockUTCtoHMS(u_int32 ulUTC, u_int8 *pubHour, u_int8 *pubMin, u_int8 *pubSec)
{
   u_int32 ulCurrentSeconds = ulUTC;

   // calculate total seconds into usable parts
   ulCurrentSeconds  = (u_int32)(ulCurrentSeconds % ES_SECONDS_PER_DAY    );
   *pubHour          = (u_int8 )(ulCurrentSeconds / ES_SECONDS_PER_HOUR   );
   ulCurrentSeconds  = (u_int32)(ulCurrentSeconds % ES_SECONDS_PER_HOUR   );
   *pubMin           = (u_int8 )(ulCurrentSeconds / ES_SECONDS_PER_MINUTE );
   *pubSec           = (u_int8 )(ulCurrentSeconds % ES_SECONDS_PER_MINUTE );
}

/******************************************************************************
 * Name:    rtc_ClockFormat
 *
 * Purpose: This function will convert a date/time specified in seconds since
 *          1-1-1970 to a formatted date/time.
 *
 * Input:   DST Auto-Yes-No, ulSeconds - Date specified in seconds since 1-1-1970
 *
 * Output:  pstDayTime - Formatted date/time
 *
 * Returns: ---
 *
 * Note:
******************************************************************************/
static void rtc_ClockFormat(int iDST, u_int32 ulSeconds, CLOCK_DAY_TIME *pstDayTime)
{
   u_int32 ulMJD;
   u_int32 ulUTC;
   u_int32 ulYear;
   u_int32 ulMonth;
   u_int32 ulDay;
   u_int32 ulWeekDay;

   switch(iDST)
   {
      default:
      case DST_NO:
         break;

      case DST_YES:
         //
         // Take daylight saving into account.
         //
         if( rtc_DaylightSavingTime(ulSeconds) )
         {
            ulSeconds += ES_SECONDS_PER_HOUR;
            PRINTF("rtc-ClockFormat():DST: Secs=%d" CRLF, ulSeconds);
         }
         else PRINTF("rtc-ClockFormat():NO DST: Secs=%d" CRLF, ulSeconds);
         break;
   }
   ulMJD  = ulSeconds / ES_SECONDS_PER_DAY;
   ulMJD += ES_MJD_1970_OFFSET;
   ulUTC  = ulSeconds % ES_SECONDS_PER_DAY;

   rtc_ClockMJDtoYMD (ulMJD, &ulYear, &ulMonth, &ulDay);
   rtc_ClockMJDtoDofW(ulMJD, &ulWeekDay);
   rtc_ClockUTCtoHMS (ulUTC, &pstDayTime->ubHour, &pstDayTime->ubMin, &pstDayTime->ubSec);

   pstDayTime->ubMonth   = (u_int8)ulMonth;
   pstDayTime->ubDay     = (u_int8)ulDay;
   pstDayTime->ubYear    = (u_int8)ulYear;   // + 1900;
   pstDayTime->ubWeekDay = (u_int8)ulWeekDay;
   //
   PRINTF("rtc-ClockFormat():%d:%d:%d" CRLF, ulYear, ulMonth, ulDay);
}

/*
 *
 *  Function:     rtc_DaylightSavingTime
 *  Description:  Handle daylight saving time
 *
 *  Arguments:    Current secs (since 1970)
 *  Returns:      TRUE if daylight saving time
 *  Note:         =======================================================================
 *                Do NOT call PRINTF macro's here since the LOG_xxx()  will also use the
 *                DST functions !!
 *                =======================================================================
 *
 *                From 1970 onwards: Leap years are
 *                1972,1976,1980,1984,1988,1992,1996,2000,2004,2008,2012,2016,2020,2024,...
 *                Secs till 2022/01/01 00:00:00 = 1640995200
 *
 */
static bool rtc_DaylightSavingTime(u_int32 ulSecs)
{
#ifdef DST_HANDLED_BY_OS

   bool         fCc=FALSE;
   struct tm   *pstTime;

   //
   // Have the DST handled by the OS
   //
   if(ulSecs == 0) ulSecs = RTC_GetSystemSecs();
   pstTime = localtime((time_t *)&ulSecs);
   if(pstTime->tm_isdst) fCc = TRUE;
   return(fCc);

#else    //DST_HANDLED_BY_OS

   static u_int32 ulDSTstart = ES_SECS_DST_2004_START;
   static u_int32 ulDSTend   = ES_SECS_DST_2004_END;

   bool fCC   = FALSE;
   bool fOnce = FALSE;

   while(ulSecs > ulDSTend)
   {
      //
      // we need a new start-end of the next Daylight saving:
      //
      //    Region         Start                         Stop
      //---------------------------------------------------------------------------------
      //    EU:            Last Sunday in March          Last Sunday in October 
      //                   at 03:00                      at 03:00
      //
      // DST-start-2004 = A (ES_SECS_DST_2004_START)
      // DST-end  -2004 = B (ES_SECS_DST_2004_END)
      // Do
      // {
      //      A += ES_SECONDS_PER_WEEK
      //      ThisMonth = MJDtoYMD(A)
      //      NextMonth = MJDtoYMD(A+ES_SECONDS_PER_WEEK)
      //      if( (ThisMonth == Mar) && (NextMonth = Apr) ) DST-Start-next = A;
      //      if( (ThisMonth == Oct) && (NextMonth = Dec) ) DST-End-next   = A;
      // }
      // While( (SecsNow > DST-Start-next) && (SecsNow < DST-End-next) )
      //
      //GEN_Printf("rtc-DaylightSavingTime():Secs=%lu, DST Start-End = %lu - %lu" CRLF, ulSecs, ulDSTstart, ulDSTend);
      
      ulDSTstart = rtc_FindNextDaylightSavingTime(ulDSTstart, 3);
      ulDSTend   = rtc_FindNextDaylightSavingTime(ulDSTend,  10);
      //
      fOnce      = TRUE;
   }

   if(fOnce)
   {
      TIME_TEST();
   }
   //
   // Check if the current time needs DST correction
   //
   if( (ulSecs > ulDSTstart) && (ulSecs < ulDSTend) ) 
   {
      //GEN_Printf("rtc-DaylightSavingTime():%d:DST: Yes" CRLF, ulSecs);
      fCC = TRUE;
   }
   //else GEN_Printf("rtc-DaylightSavingTime():%d:DST: No" CRLF, ulSecs);
   return(fCC);

#endif   //DST_HANDLED_BY_OS
}

/*------  Local functions separator -------------------------------------------
_____________PRIVATE_FUNCTIONS(){};
-----------------------------X------------------------------------------------*/

#ifndef DST_HANDLED_BY_OS
/*
 *
 *  Function:     rtc_FindNextDaylightSavingTime
 *  Description:  Find the next daylight saving time period
 *
 *  Arguments:    secs start, month 
 *  Returns:      second when the next DST period starts
 *
 */
static u_int32 rtc_FindNextDaylightSavingTime(u_int32 ulA, u_int8 ubMonth)
{
   bool           fMonthFound = FALSE;
   bool           fFound      = FALSE;
   u_int32        ulB         = ulA;
   CLOCK_DAY_TIME stTime;

   while(!fFound)
   {
      ulA += ES_SECONDS_PER_WEEK;               // Skip 1 week
      rtc_ClockFormat(DST_NO, ulA, &stTime);    // Get month (and the rest)
      if(fMonthFound)
      {                                         // Already correct month
         if(stTime.ubMonth == ubMonth)
         {                                      // Correct month found
            fFound = TRUE;                      // FOUND new time
         }
         else
         {                                      // Not right month
            ulB = ulA;                          // Save current time
         }
      }
      else                                      // Not right month
      {
         if(stTime.ubMonth == ubMonth)
         {
            ubMonth++;                          // Set for next month
            fMonthFound = TRUE;                 // MONTH found!
         }
         else
         {                                      // Not right month
            ulB = ulA;                          // Save current time
         }
      }
   }
   return(ulB);
}

#ifdef   FEATURE_TEST_TIME
/*
 *
 *  Function:     rtc_TestDST
 *  Description:  test the DST functions
 *
 *  Arguments:    void
 *  Returns:      void
 *
 */
static void rtc_TestDST(void)
{
   u_int16        usLoop  = 20;
   u_int32        ulA, ulB;
   CLOCK_DAY_TIME stTime;
   
   ulA = ES_SECS_DST_2004_START;
   ulB = ES_SECS_DST_2004_END;

   while(usLoop--)
   {
      ulA = rtc_FindNextDaylightSavingTime(ulA,  3);
      rtc_ClockFormat(DST_NO, ulA, &stTime);
      GEN_Printf("rtc-FindNextDaylightSavingTime():DST Start:%s %02u-%02u-%u %02u:%02u" CRLF,
                           cDaysOfTheWeek[stTime.ubWeekDay],
                           stTime.ubDay,
                           stTime.ubMonth,
                           stTime.ubYear + 1900,
                           stTime.ubHour,
                           stTime.ubMin);
      //
      ulB = rtc_FindNextDaylightSavingTime(ulB, 10);
      rtc_ClockFormat(DST_NO, ulB, &stTime);
      GEN_Printf("rtc-FindNextDaylightSavingTime():DST End:  %s %02u-%02u-%u %02u:%02u" CRLF,
                           cDaysOfTheWeek[stTime.ubWeekDay],
                           stTime.ubDay,
                           stTime.ubMonth,
                           stTime.ubYear + 1900,
                           stTime.ubHour,
                           stTime.ubMin);
      //
      // skip a year
      //
      ulA = rtc_TestDSTSkipYear(ulA,  3);
      ulB = rtc_TestDSTSkipYear(ulB, 10);
   }
}
/*
 *
 *  Function:     rtc_TestDSTSkipYear
 *  Description:  Skip a year to test the DST functions
 *
 *  Arguments:    secs
 *  Returns:      void
 *
 */
static u_int32 rtc_TestDSTSkipYear(u_int32 ulSecs, u_int8 ubMonth)
{
   CLOCK_DAY_TIME stTime;
   
   do
   {
      // skip till this month ends
      ulSecs += ES_SECONDS_PER_WEEK;               // Skip 1 week
      rtc_ClockFormat(DST_NO, ulSecs, &stTime);    // Get month (and the rest)
   }
   while(stTime.ubMonth == ubMonth);
   
   ubMonth++;
   
   do
   {
      // skip until month starts
      ulSecs += ES_SECONDS_PER_WEEK;               // Skip 1 week
      rtc_ClockFormat(DST_NO, ulSecs, &stTime);    // Get month (and the rest)
   }
   while(stTime.ubMonth != ubMonth);

   return(ulSecs);
}

#endif   //FEATURE_TEST_TIME
#endif   //DST_HANDLED_BY_OS
