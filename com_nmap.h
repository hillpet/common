/*  (c) Copyright:  2018..2019 Patrn, Confidential Data
 *
 *  Workfile:           com_nmap.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Library com_nmap.c header file for:
 *                      pijuice functions
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    26 Dec 2018       Created
 * 
 *
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _COM_NMAP_H_
#define _COM_NMAP_H_

typedef enum _nmap_command_
{
   NMAP_CMD_NONE           = 0,
   NMAP_CMD_NR_HOSTS,
   NMAP_CMD_HOST_IP,
   NMAP_CMD_HOST_MAC,
   NMAP_CMD_HOST_LATENCY,
   NMAP_CMD_HOST_NAME,
   NMAP_CMD_HOST_OS,
   //
   NUM_NMAP_CMDS
}  NMAPC;
//
#define  NMAP_IP_LEN          20
#define  NMAP_MAC_LEN         20
#define  NMAP_NAME_LEN        32
//
typedef struct _nmap_data_
{
   int                  iHost;
   double               flLatency;
   char                 cIp[NMAP_IP_LEN];
   char                 cMac[NMAP_MAC_LEN];
   char                 cNameMac[NMAP_NAME_LEN];
   char                 cNameOs[NMAP_NAME_LEN];
   char                 cNameHost[NMAP_NAME_LEN];
   struct _nmap_data_  *pstNext;
}  NMAPD;
//
typedef struct _nmap_header_
{
   pid_t    tPidNmap;
   int      iNumHosts;
   NMAPD   *pstTop;
}  NMAPH;

//
// Global prototypes
//
bool  NMAP_Init          (NMAPH *);
bool  NMAP_Get           (NMAPH *, NMAPC, void *, int);
void  NMAP_Exit          (NMAPH *);
 
#endif  /*_COM_NMAP_H_ */

