/*  (c) Copyright:  2017..2024  Patrn, Confidential Data
 *
 *  Workfile:           config.h
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            
 *
 *                     
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 * Changes:
 *     5 Sep 2017:      Created
 *    16 Dec 2018:      Added con_juice.* battery backup PY calls  
 *    05 Jan 2019:      cr003-com_net changes
 *    28 Jan 2019:      cr004-com_rtc changes
 *    05 Sep 2019:      Added VERBOSE log flags to NET_CON and NET_CL
 *    01 Jul 2021:      cr0005-Add LOG reports to determine spontaneous exits
 *    31 Mar 2022:      Fix DST issues RTC_xxx()
 *    02 Mar 2024:      *.txt and *.log urls are shown in a browser rather than downloaded
 *    04 Mar 2024:      Check for available client before accepting new connection
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _CONFIG_H_
#define _CONFIG_H_

//#define DEBUG_ASSERT                    // Define in makefile !
//#define FEATURE_USE_PRINTF              // Define in makefile !
//=============================================================================
// Feature switches
// Define TEST or RELEASE version
//=============================================================================
  #define FEATURE_ECHO_HTTP_OUT           // Enable verbose Log of HTTP PUT
  #define FEATURE_ECHO_HTTP_IN            // Enable verbose Log of HTTP GET/POST

#ifdef  DEBUG_ASSERT
#define COMMON_VERSION                    "Common-v1.01-cr061"
#else   //DEBUG_ASSERT
#define COMMON_VERSION                    "Common-v1.01.061"
#endif  //DEBUG_ASSERT

#endif /* _CONFIG_H_ */
