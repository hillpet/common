/*  (c) Copyright:  2005..2017  CvB, Confidential Data
 *
 *  Workfile:           gen_safe.h
 *  Revision:           
 *  Modtime:            
 *
 *  Purpose:            This module contains wrappers around a number of system calls and
 *                      library functions so that a default error behavior can be defined.
 *                      by John Goerzen, Linux Programming Bible
 * 
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    20 Jul 2005       Created
 *    29 Aug 2019       Add SAFEFREE() macro to pass caller's function
 *    08 Oct 2019       Add safeferror()
 *    07 Jun 2021:      Add safefeof()
 *    03 May 2023:      Add int64 file ops
 *    24 Jun 2023:      Add SAFEFREEX
 * 
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/
#ifndef __COM_SAFE_H__
#define __COM_SAFE_H__

#include <stdio.h>           /* required for FILE * stuff */
#include <sys/stat.h>        /* required for struct stat stuff */
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>

void    *safemalloc        (size_t size);
void    *saferemalloc      (void *pvData, size_t size);
void    *safefree          (void *pvData);
void    *safefreex         (const char *pcFunc, void *pvData);
//
#define  SAFEMALLOC(a,b)   safemalloc(b) 
#define  SAFEFREEX(a,b)    safefree(b) 
//
#ifdef   DEBUG_ASSERT
#define  SAFEFREE(a,b)     safefreex(a,b) 
#else    //DEBUG_ASSERT
#define  SAFEFREE(a,b)     safefree(b) 
#endif   //DEBUG_ASSERT

char    *safestrdup        (const char *);
char    *safestrncpy       (char *, const char *, size_t);
char    *safestrcat        (char *, const char *, size_t);
int      safekill          (pid_t, int);
char    *safegetenv        (const char *);
int      safechdir         (const char *);
int      safemkdir         (const char *, mode_t);
int      safestat          (const char *, struct stat *);
int      safeopen          (const char *, int);
int      safeopen2         (const char *, int, mode_t);
int      safepipe          (int filedes[2]);
int      safedup2          (int , int);
int      safeexecvp        (const char *, char *const argv[]);
size_t   safeseek          (int, size_t);
size_t   saferead          (int, void *, size_t);
size_t   safewrite         (int, const char *, size_t);
int      safeclose         (int);
FILE    *safefopen         (char *, char *);
bool     safeferror        (char *);
size_t   safefread         (void *, size_t, size_t, FILE *);
char    *safefgets         (char *, int, FILE *);
size_t   safefwrite        (void *, size_t, size_t, FILE *);
int      safefeof          (FILE *);
int      safefclose        (FILE *);
int      safefflush        (FILE *);
int      safemalloccount   (void);
pid_t    safefork          (void);

#endif   // __COM_SAFE_H__
