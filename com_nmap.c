/*  (c) Copyright:  2018...2019  Patrn, Confidential Data 
 *
 *  Workfile:           com_nmap.c
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Library for:
 *                      Nmap functions
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    26 Dec 2018       Created
 *    14 Dec 2021:      Switch to printx
 * 
 * 
 *
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include "typedefs.h"
#include "config.h"
#include "com_func.h"
#include "com_rtc.h"
#include "com_log.h"
#include "com_safe.h"
#include "com_nmap.h"

//#define USE_PRINTF
#include "printx.h"

static const char *pcNmapScan       = "scan report";
static const char *pcNmapHostUp     = "Host is up";
//static const char *pcNmapMac        = "MAC Address";
//
static const char *pcCmdPing        = "nmap -sP 192.168.178.1-254";
//
static int nmap_Get                 (NMAPH *);

/* ======   Local Functions separator ===========================================
void ___GLOBAL_FUNCTIONS(){}
==============================================================================*/


//
// Function:   NMAP_Init
// Purpose:    Init NMAP functions
//
// Parms:      Header struct
// Returns:    TRUE if OKee
// Note:       
//
bool NMAP_Init(NMAPH *pstHdr)
{
   bool  fCc=FALSE;
   int   iCc;
   pid_t tPid;

   tPid = fork();
   //
   switch(tPid)
   {
      default:
         // parent 
         pstHdr->tPidNmap  = tPid;
         pstHdr->iNumHosts = 0;
         pstHdr->pstTop    = NULL;
         fCc = TRUE;
         break;

      case 0:
         // Child: nmap exec
         iCc = nmap_Get(pstHdr);
         exit(iCc);
         break;

      case -1:
         // Error
         break;
   }

   return(fCc);
}

//
// Function:   NMAP_Get
// Purpose:    Get NMAP data
//
// Parms:      NMAPH header struct, NMAP command, Dest ptr, dest length
// Returns:    TRUE if cmd OKee
// Note:       
//
bool NMAP_Get(NMAPH *pstHdr, NMAPC tCmd, void *pvDest, int iLen)
{
   bool  fCc=FALSE;

   switch(tCmd)
   {
      default:
         break;
      
      case NMAP_CMD_NR_HOSTS:
         *(int *) pvDest = 0;
         break;
      
      case NMAP_CMD_HOST_IP:
         break;
      
      case NMAP_CMD_HOST_MAC:
         break;
      
      case NMAP_CMD_HOST_LATENCY:
         break;
      
      case NMAP_CMD_HOST_NAME:
         break;
      
      case NMAP_CMD_HOST_OS:
         break;
   }
   return(fCc);
}

//
// Function:   NMAP_Exit
// Purpose:    Exit NMAP functions
//
// Parms:      NMAPH header struct
// Returns:    
// Note:       Free all allocated memory
//
void NMAP_Exit(NMAPH *pstHdr)
{
   NMAPD   *pstData;
   NMAPD   *pstTemp;

   if(pstHdr)
   {
      pstData = pstHdr->pstTop;
      while(pstData)
      {
         pstTemp = pstData;
         pstData = pstData->pstNext;
         safefree(pstTemp);
      }
   }
}


/* ======   Local Functions separator ===========================================
void ___LOCAL_FUNCTIONS(){}
==============================================================================*/

//
// Function:   nmap_Get
// Purpose:    Retrieve the NMAP data
//
// Parms:      NMAPH header struct
// Returns:    Thread cc
// Note:       $ nmap -sP 192.168.178.1-254
//                >> "Nmap scan report for 192.168.178.xxx"
//                >> "Host is up (0.00234s latency)"
//                >> "MAC Address: xx:xx:xx:xx:xx:xx (bla bla)"
//
static int nmap_Get(NMAPH *pstHdr)
{
   bool     fEof=FALSE;
   int      iCc=EXIT_CC_OKEE;
   char    *pcObj;
   char    *pcRes;
   FILE    *ptFp;
   NMAPD   *pstDat=NULL;
   NMAPD   *pstTmp;

   pcObj = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, 1024);
   ptFp  = popen(pcCmdPing, "r");
   //
   if(ptFp)
   {
      PRINTF("nmap_Get():Pipe open:%s" CRLF, pcCmdPing);
      //
      while(!fEof)
      {
         pcRes = fgets(pcObj, 1023, ptFp);
         if(pcRes)
         {
            GEN_RemoveChar(pcRes, 0x0a);
            GEN_RemoveChar(pcRes, 0x0d);
            GEN_TrimRight(pcRes);
            PRINTF("nmap_Get(): Report: %s" CRLF, pcRes);
            if(GEN_STRSTRI(pcRes, pcNmapScan))
            {
               //
               // We have a responding host:
               // "Nmap scan report for 192.168.178.xxx"
               //
               pstTmp = pstDat;
               pstDat = (NMAPD *) SAFEMALLOC(__PRETTY_FUNCTION__, sizeof(NMAPD));
               if(pstHdr->pstTop == NULL) pstHdr->pstTop  = pstDat;
               else                       pstTmp->pstNext = pstDat;
               //
               pcRes   = GEN_FindDelimiter(pcRes, DELIM_NUMERIC);
               if(pcRes)
               {
                  GEN_STRNCPY(pstDat->cIp, pcRes, NMAP_IP_LEN);
                  pcRes = fgets(pcObj, 1023, ptFp);
                  if(pcRes)
                  {
                     if(GEN_STRSTRI(pcRes, pcNmapHostUp))
                     {
                        GEN_RemoveChar(pcRes, 0x0a);
                        GEN_RemoveChar(pcRes, 0x0d);
                        GEN_TrimRight(pcRes);
                        PRINTF("nmap_Get(): Up/Down: %s" CRLF, pcRes);
                     }
                  }
               }
            }
         }
         else 
         {
            PRINTF("nmap_Get():EOF" CRLF);
            fEof = TRUE;
         }
      }
      pclose(ptFp);
   }
   else
   {
      PRINTF("nmap_Get():ERROR Pipe open:%s" CRLF, pcCmdPing);
      iCc=EXIT_CC_GEN_ERROR;
   }
   safefree(pcObj);
   return(iCc);
}


#ifdef COMMENT
/* ======   Local Functions separator ===========================================
void ___COMMENT_OUT_(){}
==============================================================================*/


#endif