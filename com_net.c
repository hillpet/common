/*  (c) Copyright:  2017..2019  Patrn, Confidential Data 
 *
 *  Workfile:           com_net.c
 *  Purpose:            Library for:
 *                      general networking
 *                      sockets, pipes, etc.
 *                      unbuffered I/O
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 * 
 *  Author:             Peter Hillen
 *  Changes:
 *    02 Mar 2018:      Made common from rpi_net.*
 *    28 Mar 2018:      Handle server overload more generic
 *    30 Nov 2018:      Extend HTTP connection checks
 *    05 Jan 2019:      NET_FlushCache() from 1 --> 32 chars
 *    29 Jun 2109:      Add a few LOG_Report() and log Received data
 *    30 Jun 2019:      Change session API
 *    31 Aug 2019:      Reduce NET Logs (log file overflows on KodiRPC)
 *    03 Sep 2019:      Add Net VERBOSE level (0=OFF)
 *    17 Feb 2020:      Add LOG traces on NET errors
 *    09 Oct 2020:      Add port # to session traces; Add client/server request type HTTP, RAW, ..
 *    19 Nov 2021:      Add Variadic macro's for PRINTF()
 *    07 Mar 2022:      Replace atoi() by strtol()
 *    04 Mar 2024:      Check for available client before accepting new connection
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <ifaddrs.h>
#include <errno.h>
#include <netdb.h>
#include <semaphore.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include "typedefs.h"
#include "config.h"
#include "com_file.h"
#include "com_func.h"
#include "com_json.h"
#include "com_rtc.h"
#include "com_log.h"
#include "com_safe.h"
#include "com_http.h"
#include "com_net.h"

//#define USE_PRINTF
#include "printx.h"
//
// Enums STH
//
#define  EXTRACT_HTTP(a,b,c)   a,
enum
{
#include "com_stm.h"
   //
   NUM_HTTP_STMS
};
#undef EXTRACT_HTTP
//
//
// Generic HTTP data
//
extern const char *pcHttpResponseHeader;
extern const char *pcWebPageEnd;
//
//#define  FEATURE_NET_REPORT_PAGE
#ifdef   FEATURE_NET_REPORT_PAGE
static const char *pcWebPageTitle      = "<!doctype html public \"-//w3c//dtd html 4.01//en\"" NEWLINE \
                                         "<html>" NEWLINE \
                                         "<head>" NEWLINE \
                                         "<style type=\"text/css\">" NEWLINE \
                                         "thead {font-family:\"Tahoma\";font-size:\"100%%\";color:green}" NEWLINE \
                                         "tbody {font-family:\"Courier new\";font-size:\"25%%\";color:blue}" NEWLINE \
                                         "</style>" NEWLINE \
                                         "<meta http-equiv=\"content-type\" content=\"text/html; charset=windows-1252\">" NEWLINE \
                                         "<title>PATRN Embedded Software Services</title>" NEWLINE \
                                         "</head>" NEWLINE \
                                         "<body style=\"text-align:left\">" NEWLINE;
//
static const char *pcWebPageError      =  "<hr/>" NEWLINE \
                                          "<h2>PATRN ESS</h2>" NEWLINE \
                                          "<h3>Sorry, this server is too busy. Try again later!</h3>" NEWLINE \
                                          "<hr/>" NEWLINE;
//
static const char *pcWebPageLineBreak  =  "<br/>";
static const char *pcWebPageNotInUse   =  "Socket not in use";

#else    //FEATURE_NET_REPORT_PAGE
static const char *pcWebPageError      =  "Sorry, this server is too busy. Try again later!" NEWLINE NEWLINE;

#endif   //FEATURE_NET_REPORT_PAGE

static const char *pcWebPageRcv        =  "RCV:";
static const char *pcWebPageXmt        =  "XMT:";
//
static NETCL  *net_GetFreeClient          (NETCON *);
static bool    net_HandleNewSession       (NETCON *, char *, int);
static int     net_HandleSession          (NETCL *);
static bool    net_HasAlphanumerics       (const char *string);
static int     net_ProtocolType           (const char *protocol);
static int     net_ResolveProtocol        (const char *protocol);
static void    net_SocketAddressInit      (SOCKADDR_IN *);
static int     net_SocketAddressService   (SOCKADDR_IN *, const char *service, const char *proto);
static void    net_setnonblocking         (int);
static int     net_Session                (NETCL *);
static void    net_SessionClear           (NETCL *, NETSTART, NETTYPE);
static bool    net_LookupHost             (const char *, char *, int);
static void    net_ReportReceiveData      (NETCL *);
static void    net_ReportServerError      (NETCON *, int);
static void    net_ReportServerData       (NETCL *);
static void    net_DumpData               (const char *, char *, int);


/* ======   Local Functions separator ===========================================
void ___global_functions(){}
==============================================================================*/

//
// Function:   NET_Init
// Purpose:    Initialize the connection based API
//             
//             
// Parms:      HTTP dir and Cache dir
// Returns:    NetCon struct
// Note:       Default client/server to HTTP data. Overrule to RAW if requiered
//
NETCON *NET_Init(char *pcWww, char *pcCache)
{
   int      i;
   NETCON  *pstNet;

   PRINTF("NET-Init()" CRLF);
   pstNet = (NETCON *) SAFEMALLOC(__PRETTY_FUNCTION__, sizeof(NETCON));
   GEN_MEMSET(pstNet, 0, sizeof(NETCON));
   //
   pstNet->iConnections = NUM_CONNECTIONS;
   for(i=0; i<NUM_CONNECTIONS; i++)
   {
      pstNet->stClient[i].pcWww       = pcWww;
      pstNet->stClient[i].pcCache     = pcCache;
      pstNet->stClient[i].pcHdr       = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, HDR_BUFFER_SIZE);
      pstNet->stClient[i].pcUrl       = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, URL_BUFFER_SIZE);
      pstNet->stClient[i].pcRcvBuffer = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, RCV_BUFFER_SIZE);
      pstNet->stClient[i].pcXmtBuffer = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, XMT_BUFFER_SIZE);
   }
   //
   FD_ZERO(&pstNet->tFdRead);
   FD_ZERO(&pstNet->tFdWrite);
   FD_ZERO(&pstNet->tFdOoB);
   //
   return(pstNet);
}

//
//  Function:  NET_ServerConnect
//  Purpose:   Setup server connection:
//             socket()
//             bind()
//             listen()
//              
//  Parms:     Port number ("8080"), protocol("tcp")
//  Returns:   Socket ID or -1 on error
//
int NET_ServerConnect(NETCON *pstNet, const char *pcPort, const char *pcProto) 
{
   SOCKADDR_IN  stSocket;
   int          iSocket;
   int          iTrueval = 1;

   PRINTF("NET-ServerConnect(): Port=%s, Protocol=%s" CRLF, pcPort, pcProto);
   //
   net_SocketAddressInit(&stSocket);
   net_SocketAddressService(&stSocket, pcPort, pcProto);
   //
   iSocket = socket(AF_INET, SOCK_STREAM, 0);
   if (iSocket < 0) 
   {
      PRINTF("NET-ServerConnect(): Socket %d Error" CRLF, iSocket);
      LOG_Report(errno, "NET", "NET-ServerConnect():couldn't create socket");
      return(-1);
   }
   //
   setsockopt(iSocket, SOL_SOCKET, SO_REUSEADDR, &iTrueval, sizeof(iTrueval));
   net_setnonblocking(iSocket);
   //
   if (bind(iSocket, (struct sockaddr *)&stSocket, sizeof(stSocket)) < 0) 
   {
      PRINTF("NET-ServerConnect(): Bind Error socket %d" CRLF, iSocket);
      LOG_Report(errno, "NET", "NET-ServerConnect():bind to port %s failed", pcPort);
      return(-1);
   }
   //
   if (net_ProtocolType(pcProto) == SOCK_STREAM) 
   {
      PRINTF("NET-ServerConnect(): listen on port %s" CRLF, pcPort);
      if (listen(iSocket, 5) < 0) 
      {
         PRINTF("NET-ServerConnect(): Listen Error socket %d" CRLF, iSocket);
         LOG_Report(errno, "NET", "NET-ServerConnect():listen on port %s failed", pcPort);
         return(-1);
      }
      PRINTF("NET-ServerConnect(): Listening on port %s..." CRLF, pcPort);
   }
   LOG_Report(0, "NET", "NET-ServerConnect():Connected: Port %s, Socket %d", pcPort, iSocket);
   pstNet->iSocketConnect = iSocket;
   pstNet->iSocketPort    = (int)strtol(pcPort, NULL, 0);
   return(iSocket);
}

//
//  Function:  NET_ServerDisconnect
//  Purpose:   Disconnect to a Server
//              
//  Parms:     Net Client
//  Returns:   0=OKee, -1=Error(errno)
//
int NET_ServerDisconnect(NETCL *pstCl) 
{
   int   iCc=0;

   if(pstCl)
   {
      if(pstCl->iSocket) 
      {
         iCc = shutdown(pstCl->iSocket, SHUT_RDWR);
         pstCl->iSocket = 0;
      }
   }
   return(iCc);
}

//
//  Function:  NET_ServerTerminate
//  Purpose:   Terminate server connection
//              
//  Parms:     Connection^
//  Returns:   NULL
//
NETCON *NET_ServerTerminate(NETCON *pstNet) 
{
   int   x;

   if(pstNet)
   {
      for (x=0; x<NUM_CONNECTIONS; x++) 
      {
         if (pstNet->stClient[x].iSocket) 
         {
            if( (NET_ServerDisconnect(&pstNet->stClient[x])) < 0)
            {
               LOG_Report(errno, "NET", "NET-ServerTerminate():shutdown error:");
            }
         }
         SAFEFREE(__PRETTY_FUNCTION__ , pstNet->stClient[x].pcHdr);
         SAFEFREE(__PRETTY_FUNCTION__ , pstNet->stClient[x].pcUrl);
         SAFEFREE(__PRETTY_FUNCTION__ , pstNet->stClient[x].pcRcvBuffer);
         SAFEFREE(__PRETTY_FUNCTION__ , pstNet->stClient[x].pcXmtBuffer);
      }
      shutdown(pstNet->iSocketConnect, SHUT_RDWR);
      SAFEFREE(__PRETTY_FUNCTION__ , pstNet);
   }
   return(NULL);
}  

//
//  Function:   NET_BuildConnectionList
//  Purpose:    Build the network list of open connections
//              
//              
//  Parms:      Netcon struct
//  Returns:    Highest connection FD
//
int NET_BuildConnectionList(NETCON *pstNet)
{
   int x, iFd;

   // 
   // First put together fd_set for select(), which will
   // consist of the sock veriable in case a new connection
   // is coming in, plus all the sockets we have already
   // accepted.
   // 
   // FD_ZERO() clears out the fd_set called socks, so that
   //     it doesn't contain any file descriptors.
   // 
   FD_ZERO(&pstNet->tFdRead);
   // 
   // FD_SET() adds the file descriptor "sock" to the fd_set,
   // so that select() will return if a connection comes in
   // on that socket (which means you have to do accept(), etc.
   // 
   //PRINTF("NET-BuildConnectionList(): Add CNX-%d" CRLF, pstNet->iSocketConnect);
   FD_SET(pstNet->iSocketConnect, &pstNet->tFdRead);
   pstNet->iSocketMax = pstNet->iSocketConnect;
   // 
   // Loops through all the possible connections and adds
   // those sockets to the fd_set
   // 
   for (x=0; x<pstNet->iConnections; x++) 
   {
      iFd = pstNet->stClient[x].iSocket; 
      if (iFd != 0) 
      {
         PRINTF("NET-BuildConnectionList(): Add Client-%d" CRLF, iFd);
         FD_SET(iFd, &pstNet->tFdRead);
         if (iFd > pstNet->iSocketMax)
         {
            pstNet->iSocketMax = iFd;
         }
       }
   }
   //PRINTF("NET-BuildConnectionList(): Max fd=%d" CRLF, pstNet->iSocketMax);
   return(pstNet->iSocketMax);
}

//
//  Function:   NET_WaitForConnection
//  Purpose:    
//              
//              
//  Parms:      Netcon struct, timeout in mSecs
//  Returns:    Number of sockets with data
//
int NET_WaitForConnection(NETCON *pstNet, int iTimeout)
{
   int   iNumSocks, iSecs, iMicroSecs;

   iSecs      =  iTimeout / 1000;
   iMicroSecs = (iTimeout % 1000) * 1000;
   //
   pstNet->stTimeout.tv_sec  = iSecs;
   pstNet->stTimeout.tv_usec = iMicroSecs;

   // 
   // The first argument to select is the highest file
   // descriptor value plus 1. In most cases, you can
   // just pass FD_SETSIZE and you'll be fine.
   // 
   // The second argument to select() is the address of
   // the fd_set that contains sockets we are waiting
   // to be readable (including the listening socket).
   // 
   // The third parameter is an fd_set that you want to
   // know if you can write on -- this example doesn't
   // use it, so it passes 0, or NULL. The fourth parameter
   // is sockets you're waiting for out-of-band data for,
   // which usually, you're not.
   // 
   // The last parameter to select() is a time-out of how
   // long select() should block. If you want to wait forever
   // until something happens on a socket, you'll probably
   // want to pass NULL.
   // 
   iNumSocks = select(pstNet->iSocketMax+1, &pstNet->tFdRead, &pstNet->tFdWrite, &pstNet->tFdOoB, &pstNet->stTimeout);
   //
   // select() returns the number of sockets that had
   // things going on with them -- i.e. they're readable.
   //
   // Once select() returns, the original fd_set has been
   // modified so it now reflects the state of why select()
   // woke up. i.e. If file descriptor 4 was originally in
   // the fd_set, and then it became readable, the fd_set
   // contains file descriptor 4 in it.
   //
   return(iNumSocks);
}

//
//  Function:  NET_HandleSessions
//  Purpose:   There is data avilable in a socket connection
//              
//  Parms:     Netcon struct, handler function, IP-Addr buffer, size
//  Returns:   0: Session OKee completion
//             +: Session needs more data from the socket
//             -: Session error
//
int NET_HandleSessions(NETCON *pstNet, PFNET pfCallback, char *pcIpAddr, int iIpSize)
{
   int      iCc=0;
   int      iNr, x, iXmt=0;
   NETCL   *pstCl;

   //
   // OK, now socks will be set with whatever socket(s)
   // are ready for reading. Lets first check our
   // connection socket, and then check the sockets
   // in session.
   // 
   if(FD_ISSET(pstNet->iSocketConnect, &pstNet->tFdWrite))
   {
      //
      // Connection socket has room for more data: flush cache if needed
      //
   }
   // 
   // If a client is trying to connect() to our connection
   //  socket, select() will consider that as the socket
   //  being 'readable'. Thus, if the connection socket is
   //  part of the fd_set, we need to accept a new session.
   //
   if(FD_ISSET(pstNet->iSocketConnect, &pstNet->tFdRead))
   {
      //
      // Connection socket has new data: create new session
      //
      net_HandleNewSession(pstNet, pcIpAddr, iIpSize);
   }
   // 
   // Now check connectlist for available data
   // Run through our sockets and check to see if anything
   // happened with them, if so 'service' them.
   // 
   for(x=0; x<pstNet->iConnections; x++) 
   {
      if(FD_ISSET(pstNet->stClient[x].iSocket, &pstNet->tFdWrite))
      {
         //
         // Connection socket has room for more data: flush cache if needed
         //
         iXmt = NET_FlushCache(&pstNet->stClient[x]);
         if(iXmt < 0) 
         {
            iCc = -1;
            break;
         }
      }
      //
      PRINTF("NET-HandleSessions():Client#%d" CRLF, x);
      if(FD_ISSET(pstNet->stClient[x].iSocket, &pstNet->tFdRead))
      {
         PRINTF("NET-HandleSessions():Client#%d: FD_ISSET()=TRUE" CRLF, x);
         pstCl        = &pstNet->stClient[x]; 
         pstCl->iPort = pstNet->iSocketPort;
         //
         // Handle session:
         //
         iCc = net_HandleSession(pstCl);
         switch(pstCl->tSessionType)
         {
            case HTTP_RAW:
               //
               // Raw data packet mode: copy data packet to URL buffer
               //
               iNr = NET_RcvGet(pstCl, pstCl->pcUrl, URL_BUFFER_SIZE);
               pstCl->pcUrl[iNr] = 0;
               //FALLTHROUGH HTTP_DONE
            case HTTP_DONE:
               //
               // Session payload has completely arrived: handle it
               //
               if(pfCallback) 
               {
                  if( pfCallback(pstCl) )
                  {
                     //LOG_Report(0, "NET", "NET-HandleSessions(%d):Callback() OKee, reset STH", pstCl->iPort);
                     //
                     // This GET/PUT/POST session has completed correctly: 
                     // reset the internal state machine to handle the next transaction
                     //
                     net_SessionClear(pstCl, NET_WARMSTART, pstNet->tRequest);
                     PRINTF("NET-HandleSessions():Callback OKee" CRLF);
                  }
                  else
                  {
                     net_SessionClear(pstCl, NET_WARMSTART, pstNet->tRequest);
                     LOG_Report(0, "NET", "NET-HandleSessions(%d):Callback() ERROR!!", pstCl->iPort);
                  }
               }
               //
               // Terminate connection
               //
               pstCl->iSocket     = NET_ClientDisconnect(pstCl->iSocket);
               pstCl->ulTimestamp = 0;
               break;
         
            default:
               LOG_Report(0, "NET", "NET-HandleSessions(%d):Client-%d:Unknown Session type", pstCl->iPort, x);
               //
               // Terminate connection
               //
               pstCl->iSocket     = NET_ClientDisconnect(pstCl->iSocket);
               pstCl->ulTimestamp = 0;
               iCc = -1;
               break;

            case HTTP_ERROR:
               LOG_Report(0, "NET", "NET-HandleSessions(%d):Client-%d:Session ERROR", pstCl->iPort, x);
               //
               // Terminate connection
               //
               pstCl->iSocket     = NET_ClientDisconnect(pstCl->iSocket);
               pstCl->ulTimestamp = 0;
               iCc = -1;
               break;

            case HTTP_TPKT:
               if(pstNet->iVerbose & NET_VERBOSE_SESSION) 
               {
                  LOG_Report(0, "NET", "NET-HandleSessions(%d):Client-%d:Session is ISO Transport over TCP: Ignore", pstCl->iPort, x);
               }
               if(pstNet->iVerbose & NET_VERBOSE_TPKT) 
               {
                  net_ReportServerData(pstCl);
               }
               //
               // Terminate connection
               //
               pstCl->iSocket     = NET_ClientDisconnect(pstCl->iSocket);
               pstCl->ulTimestamp = 0;
               iCc = -1;
               break;

            case HTTP_MORE:
               //
               // Session receive buffer is handled, but session is not yet done: retrieve additional data from socket
               //
               pstCl->fFlags |= NET_SESSION_FRAG;
               if(pstNet->iVerbose & NET_VERBOSE_SESSION) LOG_Report(0, "NET", "NET-HandleSessions(%d):Client-%d:Session incomplete", pstCl->iPort, x);
               iCc = 1;
               break;
         }
      }
   }
   return(iCc);
}

//
//  Function:   NET_WriteString
//  Purpose:    Send a string, including terminating null.  NET_ReadDelimString() could be
//              perfect for reading it on the other end.  And in fact, NET_ReadString()
//              uses just that.
//  Parms:      Socket, String
//  Returns:    
//
int NET_WriteString(int iSocket, char *pcStr) 
{
   int   iLen=GEN_STRLEN(pcStr)+1;
   //
   return( NET_Write(iSocket, pcStr, iLen));
}

//
//  Function:   NET_ReadString
//  Purpose:    Reads a string from the network, terminated by a null.
//              
//              
//  Parms:      
//  Returns:    
//
int NET_ReadString(int iSocket, char *pcBuf, int iMaxlen) 
{
  return( NET_ReadDelimString(iSocket, pcBuf, iMaxlen, 0) );
}

//
//  Function:   NET_ReadNewlineString
//  Purpose:    Reads a string terminated by a newline
//              
//              
//  Parms:      Socket, Buffer, Buffer length
//  Returns:    
//
int NET_ReadNewlineString(int iSocket, char *pcBuf, int iMaxlen) 
{
  return( NET_ReadDelimString(iSocket, pcBuf, iMaxlen, '\n') );
}

//
//  Function:  NET_ReadDelimString
//  Purpose:   Reads a string with an arbitrary ending delimiter.
//              
//              
//  Parms:     Socket, Buffer, Buffer length, delimiter 
//  Returns:   Number read
//
int NET_ReadDelimString(int iSocket, char *pcBuf, int iMaxlen, char cDelim) 
{
  int iCount = 0, iRead;

  while(iCount <= iMaxlen) 
  {
    iRead = read(iSocket, pcBuf+iCount, 1);
    if (iRead < 0) return(iRead);
    if (iRead < 1) 
    {
      LOG_Report(errno, "NET", "NET-ReadDelimString():unexpected EOF from socket");
      return(iRead);
    }
    if (pcBuf[iCount] == cDelim) 
    {            
       /* Found the delimiter */
       pcBuf[iCount++] = 0;
       return(iCount);
    }
    iCount++;
  }
  return(0);
}

//
//  Function:   NET_Copy
//  Purpose:    Copies data from the in to the out file descriptor.  If numsize
//              is nonzero, specifies the maximum number of bytes to copy.  If
//              it is 0, data will continue being copied until in returns EOF. 
//  Parms:      Fd in and out, max size
//  Returns:    
//
int NET_Copy(int iFdIn, int iFdOut, int iMaxbytes) 
{
  char   pcBuffer[COPY_BUFSIZE];
  int    iRead, iRemaining;

  iRemaining = iMaxbytes;

  while(iRemaining || !iMaxbytes) 
  {
    iRead = read(iFdIn, pcBuffer, (!iRemaining || COPY_BUFSIZE < iRemaining) ? COPY_BUFSIZE : iRemaining);
    if (iRead < 1) return(iRead);
    NET_Write(iFdOut, pcBuffer, iRead);
    if (iMaxbytes) iRemaining -= iRead;
  }
  return(0);
}

//
//  Function:   NET_Write
//  Purpose:    This function will write a certain number of bytes from the buffer
//              to the descriptor fd.  The number of bytes written are returned.
//              This function will not return until all data is written or an error
//              occurs.
//  Parms:      Fd, buffer, count
//  Returns:    Number written, or -1 on error
//
int NET_Write(int iFd, char *pcBuffer, int iCount) 
{
   int  iWritten=0, iResult;

   if (iCount < 0) return(-1);
 
   while(iWritten != iCount) 
   {
      iResult = write(iFd, &pcBuffer[iWritten], iCount - iWritten);
      if (iResult < 0) 
      {
         PRINTF("NET-Write():Error %d=%d" CRLF, iResult, errno);
         switch(errno)
         {
            case EAGAIN:
               // send buffer is full: wait
               iResult = 0;
               break;

            default:
               return(iResult);
         }
      }
      else if(iResult != (iCount-iWritten)) 
      {
         PRINTF("NET-Write():W=%d, Act=%d" CRLF, iCount, iResult);
      }
      iWritten += iResult;
   }
   return(iWritten);
}

//
// Function:   NET_ReadAll
// Purpose:    
//             This function will read a number of bytes from the descriptor fd until an EOF
//             (Rd==0) has been encoutered. The number of bytes read are returned.  
//             In the event of an error, the error is returned.  
//
// Parms:      
// Returns:    Neg on read error
//             else number read
//
int NET_ReadAll(int iFd, char *pcBuffer, int iNrMax) 
{
   int   iNrTot=0;
   int   iNrRd;

   do
   {
      iNrRd = NET_Read(iFd, &pcBuffer[iNrTot], iNrMax);
      //LOG_Report(0, "NET", "NET-ReadAll(%d): %d bytes (tot=%d, max=%d)", pstCl->iPort, iNrRd, iNrTot, iNrMax);
      //LOG_printf("NET-ReadAll(): %d bytes (tot=%d, max=%d)" CRLF, iNrRd, iNrTot, iNrMax);
      if(iNrRd > 0)
      {
         iNrTot += iNrRd;
         iNrMax -= iNrRd;
      }
   }
   while(iNrRd > 0);
   //LOG_Report(0, "NET", "NET-ReadAll(%d): %d bytes read", pstCl->iPort, iNrRd);
   //LOG_printf("NET-ReadAll(): %d bytes read" CRLF, iNrRd);
   return(iNrTot);
}

//
//  Function:   NET_Read
//  Purpose:    
//              This function will read a number of bytes from the descriptor fd.  The
//              number of bytes read are returned.  In the event of an error, the
//              error handler is returned.  In the event of an EOF at the first read
//              attempt, 0 is returned.  In the event of an EOF after some data has
//              been received, the count of the already-received data is returned.
//  Parms:      
//  Returns:    Neg on read error
//              else number read
//
int NET_Read(int iFd, char *pcReadBuffer, int iMaxRd) 
{
   bool  fReading=TRUE;
   int   iCurRd=0;
   

   while(fReading)
   {
      iCurRd = read(iFd, pcReadBuffer, iMaxRd);
      //
      if(iCurRd == 0) fReading = FALSE;
      else 
      {
         if(iCurRd < 0)
         {
            switch(errno)
            {
               case EAGAIN:
                  // receive buffer is empty: exit
                  iCurRd = 0;
                  fReading = FALSE;
                  break;

               case EINTR:
                  // we were interrupted: continue reading
                  break;

               default:
                  //
                  // Error reading : exit
                  //
                  fReading = FALSE;
                  break;
            }
         }
         else 
         {
            fReading = FALSE;
         }
      }
   }
   return(iCurRd);
}

//
//  Function:   NET_ReadUlong
//  Purpose:    Reads a uint32 from the network in network byte order.
//              
//              A note on the implementation: because some architectures cannot
//              write to the memory of the integer except all at once, a character
//              buffer is used that is then copied into place all at once.
//  Parms:      
//  Returns:    
//
int NET_ReadUlong(int iFd, u_int32 *pulValue) 
{
  char   pcBuffer[sizeof(u_int32)];
  int    iStatus;

  iStatus = NET_Read(iFd, pcBuffer, sizeof(u_int32));
  if (iStatus != sizeof(u_int32)) 
  {
    LOG_Report(0, "NET", "NET-ReadUlong():unexpected EOF");
    return(-1);
  }
  bcopy(pcBuffer, (char *)pulValue, sizeof(u_int32));
  *pulValue = ntohl(*pulValue);
  return(0);
}

//
//  Function:   NET_WriteUlong
//  Purpose:    Write an unsigned long in network byte order
//              
//  Parms:      
//  Returns:    
//
int NET_WriteUlong(int iFd, u_int32 ulValue) 
{
  char     pcBuffer[sizeof(u_int32)];
  u_int32 ulTemp;
  int      iStatus;

   ulTemp = htonl(ulValue);
   bcopy((char *)&ulTemp, pcBuffer, sizeof(ulTemp));
   iStatus = NET_Write(iFd, pcBuffer, sizeof(ulTemp));
   if (iStatus != sizeof(ulTemp)) return(-1);
   return(0);
}

//
//  Function:   NET_GetMyFqdn
//  Purpose:    Returns the fully qualified domain name of the current host.
//              
//  Parms:      Host name, max-size
//  Returns:    
//
char *NET_GetMyFqdn(char *pcHostname, int iMaxSize) 
{

  gethostname(pcHostname, iMaxSize);
  return( NET_GetFqdn(pcHostname, iMaxSize) );
}

//
//  Function:   NET_GetFqdn
//  Purpose:    Returns the fully qualified domain name of an arbitrary host.
//              
//  Parms:      Buffer, max size
//  Returns:    
//
char *NET_GetFqdn(char *pcHostname, int iMaxSize) 
{
  struct hostent *pstHp;
 
  pstHp = gethostbyname(pcHostname);
  if (pstHp == NULL) return(NULL);
  safestrncpy(pcHostname, (pstHp->h_aliases[0]) ? pstHp->h_aliases[0] : pstHp->h_name, iMaxSize);
  return(pcHostname);
}

//
// Function:   NET_ResolveHostName
// Purpose:    Translate host name to IP
//
// Parms:      Dest ptr
// Returns:    Looked up IP addr
// Note:       Caller needs to free the memory
//
char *NET_ResolveHostName(char *pcName)
{
   char *pcIp;

   pcIp = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, 100);
   if(!net_LookupHost(pcName, pcIp, 100))
   {
      //
      // Not found: 
      //
      pcIp = SAFEFREE(__PRETTY_FUNCTION__ , pcIp);
   }
   return(pcIp);
}

//
//  Function:  NET_ClientConnect
//  Purpose:   Connect to a client
//             socket()
//             connect()
//             
//  Parms:     Host("192.168.178.207"), port(80), protocol("tcp")
//  Returns:   Socket ID or -1 on error
//
int NET_ClientConnect(const char *pcHost, int iPort, const char *pcProto) 
{
   SOCKADDR_IN  stSocket;
   int          iSocket;

   iSocket = socket(AF_INET, net_ProtocolType(pcProto), net_ResolveProtocol(pcProto));
   bzero(&stSocket, sizeof(stSocket));
   stSocket.sin_family = AF_INET;
   stSocket.sin_port = htons(iPort);
   inet_pton(AF_INET, pcHost, &stSocket.sin_addr);
   //
   if(connect(iSocket, (struct sockaddr *) &stSocket, sizeof(stSocket)) < 0)
   {
      LOG_Report(errno, "NET", "NET-ClientConnect(%d):connect failed", iPort);
      iSocket = -1;
   }
   return(iSocket);
}

//
//  Function:  NET_ClientDisconnect
//  Purpose:   Disconnect to a client
//             
//  Parms:     Fd
//  Returns:   Fd=0
//
int NET_ClientDisconnect(int iSocket) 
{
   if(iSocket)
   {
      if( close(iSocket) < 0)
      {
         LOG_Report(errno, "NET", "NET-ClientDisconnect():close error:");
      }
   }
   return(0);
}

//
//  Function:  NET_ServerAccept
//  Purpose:   Accept a connection from a client
//             accept()
//             
//  Parms:     Connection, IP-Addr buffer, size 
//  Returns:   Session Socket ID or -1 on error
//
int NET_ServerAccept(NETCON *pstNet, char *pcIpAddr, int iIpSize)
{
   int         iSocket, iCc;
   SOCKADDR_IN stSocket;
   socklen_t   tLength=sizeof(stSocket);
   
   iSocket = pstNet->iSocketConnect;
   iCc = accept(iSocket, (struct sockaddr *) &stSocket, &tLength);
   //
   // Save caller IP addr:
   //
   inet_ntop(AF_INET, &stSocket.sin_addr, pcIpAddr, iIpSize);
   PRINTF("NET-ServerAccept(): IP=%s" CRLF, pcIpAddr);
   //
   return(iCc);
}

//
//  Function:  NET_FlushCache
//  Purpose:   Flush the cached xmt buffer
//             
//  Parms:     NETCL *
//  Returns:   Bytes flushed
//
//
int NET_FlushCache(NETCL *pstCl)
{
   int   iNr, iWr=0;
   char  cChar[32];

   do
   {
      iNr = NET_XmtGet(pstCl, cChar, 32);
      if(iNr)
      {
         iNr  = NET_Write(pstCl->iSocket, cChar, iNr);
         if(iNr < 0)
         {
            LOG_Report(errno, "NET", "NET-FlushCache(%d):NetWrite Error. %d Written, write returns %d :", pstCl->iPort, iWr, iNr);
            iWr = iNr;
         }
         else iWr += iNr;
      }
   }
   while(iNr > 0);   //skip on write error
   //
   //LOG_Report(0, "NET", "NET-FlushCache(%d):Get=%4d Put=%4d Wrt=%4d", pstCl->iPort, pstCl->iXmtBufferGet, pstCl->iXmtBufferPut, iWr);
   //PRINTF("NET-FlushCache():Get=%4d Put=%4d Wrt=%4d" CRLF, pstCl->iXmtBufferGet, pstCl->iXmtBufferPut, iWr);
   return(iWr);
}

//
//  Function:  NET_GetIpInfo
//  Purpose:   Retrieve the IP address of the local interfaces
//             
//  Parms:     Buffer/len IP-Addr, buffer/size Interface
//  Returns:   TRUE if IP address found
//  Note:      struct ifaddrs 
//             {
//                 struct ifaddrs *  ifa_next;
//                 char *            ifa_name;
//                 u_int             ifa_flags;
//                 struct sockaddr * ifa_addr;
//                 struct sockaddr * ifa_netmask;
//                 struct sockaddr * ifa_dstaddr;
//                 void *            ifa_data;
//             }
//                
//
bool NET_GetIpInfo(char *pcIpAddr, int iIpSize, char *pcIfce, int iIfceSize)
{
   int               iFamily, s, n, iLen;
   struct ifaddrs   *pstIfAddr, *pstIf;

   if(getifaddrs(&pstIfAddr) == -1) 
   {
      PRINTF("NET-GetIpInfo():getifaddrs() failed" CRLF);
      return(FALSE);
   }
   //
   // Walk through linked list, maintaining head pointer so we
   // can free list later
   //
   for (pstIf=pstIfAddr, n=0; pstIf!=NULL; pstIf=pstIf->ifa_next, n++) 
   {
      if (pstIf->ifa_addr == NULL)
         continue;
      //
      // Ignore local Ip 127.0.0.1
      //
      if(GEN_STRCMP(pstIf->ifa_name, "lo") == 0)
         continue;

      PRINTF("NET-GetIpInfo(): Adapter %d" CRLF, n);
      PRINTF("NET-GetIpInfo(): ifa_name  = %s" CRLF, pstIf->ifa_name);
      PRINTF("NET-GetIpInfo(): ifa_flags = %x" CRLF, pstIf->ifa_flags);

      iFamily = pstIf->ifa_addr->sa_family;
      //
      //For an AF_INET* interface address, display the address
      //
      if (iFamily == AF_INET || iFamily == AF_INET6) 
      {
         s = getnameinfo(pstIf->ifa_addr,
                ( iFamily == AF_INET) ? sizeof(struct sockaddr_in) : sizeof(struct sockaddr_in6),
                  pcIpAddr, iIpSize, NULL, 0, NI_NUMERICHOST);
         //
         if (s != 0) 
         {
            PRINTF("NET-GetIpInfo():getnameinfo() failed:%s(=%d)" CRLF, gai_strerror(s), s);
         }
         GEN_STRNCPY(pcIfce, pstIf->ifa_name, iIfceSize);
         PRINTF("NET-GetIpInfo(): my IP=%s (%s)" CRLF, pcIpAddr, pcIfce);
      } 
   }
   freeifaddrs(pstIfAddr);
   iLen = GEN_STRLEN(pcIpAddr);
   return(iLen > 0);
}

//
//  Function:   NET_RcvGet
//  Purpose:    Retrieves data from the RCB buffer
//              
//  Parms:      Client, buffer^, max number to read
//  Returns:    TRUE if OKee
//
int NET_RcvGet(NETCL *pstCl, char *pcBuffer, int iNr)
{
   int   iAct=0;
   
   while(iNr--)
   {
      if(pstCl->iRcvBufferGet != pstCl->iRcvBufferPut)
      {
         if(pcBuffer) *pcBuffer++ = pstCl->pcRcvBuffer[pstCl->iRcvBufferGet];
         pstCl->iRcvBufferGet++;
         pstCl->iRcvBufferGet &= RCV_BUFFER_MASK;
         iAct++;
      }
      else break;
   }
   return(iAct);
}

//
//  Function:   NET_RcvPut
//  Purpose:    Stores a single char into the RCB buffer
//              
//  Parms:      Client, buffer^, int
//  Returns:    TRUE if OKee
//
int NET_RcvPut(NETCL *pstCl, char *pcBuffer, int iNr)
{
   int   iAct=0, iIdx;
   
   while(iNr--)
   {
      iIdx = (pstCl->iRcvBufferPut + 1) & RCV_BUFFER_MASK;
      if(iIdx != pstCl->iRcvBufferGet)
      {
         pstCl->pcRcvBuffer[iIdx] = *pcBuffer++;
         pstCl->iRcvBufferPut     = iIdx;
         iAct++;
      }
      else break;
   }
   return(iAct);
}

//
//  Function:   NET_XmtGet
//  Purpose:    Retrieves a data from the XMT buffer
//              
//  Parms:      Client, buffer^, max nr
//  Returns:    TRUE if OKee
//
int NET_XmtGet(NETCL *pstCl, char *pcBuffer, int iNr)
{
   int   iAct=0;
   
   while(iNr--)
   {
      if(pstCl->iXmtBufferGet != pstCl->iXmtBufferPut)
      {
         *pcBuffer++ = pstCl->pcXmtBuffer[pstCl->iXmtBufferGet++];
         pstCl->iXmtBufferGet &= XMT_BUFFER_MASK;
         iAct++;
      }
      else break;
   }
   return(iAct);
}

//
//  Function:   NET_XmtPut
//  Purpose:    Stores a single char into the XMT buffer
//              
//  Parms:      Client, buffer^, int
//  Returns:    TRUE if OKee
//
int NET_XmtPut(NETCL *pstCl, char *pcBuffer, int iNr)
{
   int   iAct=0, iIdx;
   
   while(iNr--)
   {
      iIdx = (pstCl->iXmtBufferPut + 1) & XMT_BUFFER_MASK;
      if(iIdx != pstCl->iXmtBufferGet)
      {
         //
         // Yep, there is still room in the circular buffer
         //
         pstCl->pcXmtBuffer[pstCl->iXmtBufferPut] = *pcBuffer++;
         pstCl->iXmtBufferPut                     = iIdx;
         iAct++;
      }
      else break;
   }
   return(iAct);
}

//
//  Function:  NET_ReportServerStatus
//  Purpose:   Report server status
//
//  Parms:     Net, mode
//  Returns:   Number of free sockets
//
int NET_ReportServerStatus(NETCON *pstNet, int iMode)
{
   int      x, iFree=0;
   u_int32  ulSecsNow, ulSecs;
   char    *pcBuffer=NULL;
   NETCL   *pstCl;

   //PRINTF("NET-ReportServerStatus()" CRLF);
   //
   if(iMode & NET_CHECK_LOG) pcBuffer = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, 200);
   //
   for (x=0; x<pstNet->iConnections; x++)
   {
      pstCl = &pstNet->stClient[x];
      //
      if(pcBuffer) 
      {
         LOG_Report(0, "NET", "NET-ReportServerStatus(%d):Cnx-%d", pstCl->iPort, x+1);
         PRINTF("NET-ReportServerStatus():Cnx-%d" CRLF, x+1);
      }
      if(pstCl->iSocket)
      {
         if(pcBuffer) 
         {
            LOG_Report(0, "NET", "NET-ReportServerStatus(%d):In use:", pstCl->iPort);
            RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MM_YYYY_HH_MM_SS, pcBuffer, pstCl->ulTimestamp);
            PRINTF("NET-ReportServerStatus():Last accessed at %s" CRLF, pcBuffer);
            LOG_Report(0, "NET", "NET-ReportServerStatus(%d):Last accessed at %s", pstCl->iPort, pcBuffer);
            net_ReportServerData(pstCl);
         }
         if(iMode & NET_CHECK_FREE)
         {
            //
            // Request to free connections if time exceeds max
            //
            ulSecsNow = RTC_GetSystemSecs();
            ulSecs    = ulSecsNow - pstCl->ulTimestamp;
            if(ulSecs > HTTP_CONNECTION_MAX_SECS)
            {
               //
               // The CNX is in perril: reset all connection with timestamp > HTTP_CONNECTION_MAX_SECS ago 
               //
               if( shutdown(pstCl->iSocket, SHUT_RDWR) == 0)
               {
                  LOG_Report(0, "NET", "NET-ReportServerStatus(%d):Shutdown Cnx-%d OKee", pstCl->iPort, x+1);
               }
               else
               {
                  LOG_Report(errno, "NET", "NET-ReportServerStatus(%d):Shutdown Cnx-%d ERROR:", pstCl->iPort, x+1);
               }
               iFree++;
               pstCl->iSocket     = 0;
               pstCl->ulTimestamp = 0;
               net_SessionClear(pstCl, NET_WARMSTART, pstNet->tRequest);
            }
         }
      }
      else
      {
         //
         // Report:not in use
         //
         iFree++;
         if(pcBuffer) 
         {
            LOG_Report(0, "NET", "NET-ReportServerStatus(%d):Cnx-%d Not in use:", pstCl->iPort, x+1);
            net_ReportServerData(pstCl);
         }
      }
   }
   if(pcBuffer) SAFEFREE(__PRETTY_FUNCTION__ , pcBuffer);
   return(iFree);
}

//
//  Function:  NET_Connections
//  Purpose:   Report number of connections
//
//  Parms:     Net
//  Returns:   Number of connections
//
int NET_Connections(NETCON *pstNet)
{
   int   x, iNr=0;

   for (x=0; x<pstNet->iConnections; x++)
   {
      if( pstNet->stClient[x].iSocket) iNr++;
   }
   return(iNr);
}

//
//  Function:  NET_ConnectedIp
//  Purpose:   Report connected IP address of this connection
//
//  Parms:     Net, Connection index 0..pstNet->iConnections
//  Returns:   IP address ptr or NULL
//
char *NET_ConnectedIp(NETCON *pstNet, int iIdx)
{
   char *pcIp=NULL;

   if(iIdx < pstNet->iConnections)
   {
      if( pstNet->stClient[iIdx].iSocket ) pcIp = pstNet->stClient[iIdx].cIpAddr;
   }
   return(pcIp);
}

/* ======   Local Functions separator ===========================================
void ___local_functions(){}
==============================================================================*/

//
//  Function:   net_GetFreeClient
//  Purpose:    Check if we have room for another client on this connection
//              
//  Parms:      Netcon struct
//              
//  Returns:    Client struct
//
static NETCL *net_GetFreeClient(NETCON *pstNet)
{
   int      x;
   NETCL   *pstCl=NULL;

   for (x=0; x<pstNet->iConnections; x++)
   {
      if(pstNet->stClient[x].iSocket == 0) 
      {
         //
         // Free slot !
         //
         pstCl = &pstNet->stClient[x];
         PRINTF("net-GetFreeClient():Slot=%d" CRLF, x);
         net_SessionClear(pstCl, NET_COLDSTART, pstNet->tRequest);
         break;
      }
   }
   return(pstCl);
}

//
//  Function:  net_HasAlphanumerics
//  Purpose:   a private function used only by this library.  It checks
//             the passed string.  
//  Parms:      
//  Returns:   TRUE if string contains non-numerics
//             FALSE if only 0..9
//
static bool net_HasAlphanumerics(const char *pcString) 
{
   int x, iLen;

   iLen = GEN_STRLEN(pcString);
   for (x=0; x<iLen; x++)
   {
      if (!(isdigit(pcString[x])))  return(TRUE);
   }
   return(FALSE);
}

//
// Function:   net_HandleSession
// Purpose:    Handle this session
//             
// Parms:      Netcl struct, Received bytes^
// Returns:     0: Session completed
//             +1: Session needs more data (Circular buffer or socket)
//             -1: Session error
// Note:       Receive buffer is circular: we can transfer iSize bytes from Socket to Buffer[Put]
//
//             0         1         2         3     RCV_BUFFER_SIZE
//             01234567890123456789012345678901    32
//     Start:  --------------------------------
//             G
//             P
//
//             --------------------------------
//     Case 1:                     G               
//                  Pxxxxxxxxxxxxxx                20 - 05 = 15
//
//             --------------------------------
//     Case 2:   G                                 
//                                       Pxxxxx    32 - 26 = 6
//
//             --------------------------------
//     Case 3: G                                  
//                                       Pxxxx     32 - 25 = 5 (Put cannot reach Get)
//
static int net_HandleSession(NETCL *pstCl)
{
   int   iCc=-1;
   int   iRcv, iSize;

   if(pstCl->iRcvBufferPut < pstCl->iRcvBufferGet) iSize = pstCl->iRcvBufferGet - pstCl->iRcvBufferPut;        // Case 1
   else if(pstCl->iRcvBufferGet > 0)               iSize = RCV_BUFFER_SIZE - pstCl->iRcvBufferPut;             // Case 2
   else                                            iSize = RCV_BUFFER_SIZE - pstCl->iRcvBufferPut - 1;         // Case 3
   //
   PRINTF("net-HandleSession(%d)" CRLF, pstCl->iPort);
   //LOG_Report(0, "NET", "net-HandleSession(%d):Get=%4d Put=%4d Size=%4d", pstCl->iPort, pstCl->iRcvBufferGet, pstCl->iRcvBufferPut, iSize);
   //
   // Read data from the socket
   //
   iRcv = NET_Read(pstCl->iSocket, &pstCl->pcRcvBuffer[pstCl->iRcvBufferPut], iSize);
   //LOG_Report(0, "NET", "net-HandleSession(%d):Read %d bytes", pstCl->iPort, iRcv);
   //
   if(iRcv < 0) LOG_Report(errno, "NET", "net-HandleSession(%d):NetRead error:", pstCl->iPort);
   else if(iRcv > 0)
   {
      //
      // Data is available
      //
      if(pstCl->fFlags & NET_SESSION_FRAG)
      {
         pstCl->fFlags &= ~NET_SESSION_FRAG;
         if(pstCl->iVerbose & NET_VERBOSE_SESSION)
         { 
            LOG_Report(0, "NET", "net-HandleSession(%d):Incomplete session continued", pstCl->iPort);
         }
      }
      PRINTF("net-HandleSession(%d):Read %d bytes" CRLF, pstCl->iPort, iRcv);
      //
      pstCl->iRcvBufferPut += iRcv;
      pstCl->iRcvBufferPut &= RCV_BUFFER_MASK;
      //
      if(pstCl->iVerbose & NET_VERBOSE_DUMP_RCV) 
      {
         net_ReportReceiveData(pstCl);
      }
      iCc = net_Session(pstCl);
   }
   else
   {
      //
      // No more data is available: POST could need padding zero's until the content length
      //
      switch(pstCl->tHttpType)
      {
         default:
            if(pstCl->iVerbose & NET_VERBOSE_DUMP_RCV) 
            {
               LOG_Report(0, "NET", "net-HandleSession(%d):HTTP_GET:Expecting more data, none available !", pstCl->iPort);
               net_ReportReceiveData(pstCl);
            }
            pstCl->tSessionType = HTTP_ERROR;
            break;

         case HTTP_POST:
            if(pstCl->iContLength)
            {
               //
               // Pad extra zero's from the content length
               //
               if(pstCl->iVerbose & NET_VERBOSE_DUMP_RCV) 
               {
                  LOG_Report(0, "NET", "net-HandleSession(%d):HTTP_POST:Pad %d zero bytes", pstCl->iPort, pstCl->iContLength);
                  net_ReportReceiveData(pstCl);
               }
               pstCl->tSessionType = HTTP_DONE;
               iCc = 0;
            }
            else
            {
               if(pstCl->iVerbose & NET_VERBOSE_DUMP_RCV) 
               {
                  LOG_Report(0, "NET", "net-HandleSession(%d):HTTP_POST:Expecting more data, none available !", pstCl->iPort);
                  net_ReportReceiveData(pstCl);
               }
               pstCl->tSessionType = HTTP_ERROR;
            }
            break;
      }
   }
   return(iCc);
}

//
//  Function:   net_HandleNewSession
//  Purpose:    There is someone knocking on the door
//              
//  Parms:      Netcon struct, new connection, Ip-Addr buffer, Ip-Size
//              
//  Returns:    TRUE if added
//
static bool net_HandleNewSession(NETCON *pstNet, char *pcIpAddr, int iIpSize)
{
   int      iFd;
   bool     fCc=FALSE;
   NETCL   *pstCl;

   //
   // We have a new connection coming in!  We'll try to find a spot for it in connectlist
   //
   pstCl = net_GetFreeClient(pstNet);
   if(pstCl == NULL) return(FALSE);
   //
   // We do have a free client slot, proceed !
   //
   iFd = NET_ServerAccept(pstNet, pcIpAddr, iIpSize);
   if (iFd < 0) 
   {
       PRINTF("net-HandleNewSession(): Accept error FD=%d" CRLF, iFd);
       LOG_Report(errno, "NET", "net-HandleNewSession()-Accept error:");
       return(FALSE);
   }
   net_setnonblocking(iFd);
   //
   if(pstCl)
   {
      PRINTF("net-HandleNewSession():Free client: FD=%d" CRLF, iFd);
      pstCl->iSocket  = iFd;
      pstCl->iVerbose = pstNet->iVerbose;
      GEN_STRNCPY(pstCl->cIpAddr, pcIpAddr, INET_ADDRSTRLEN);
      fCc = TRUE;
   }
   else
   {
      //
      // No room left in the network queue!
      //
      PRINTF("net-HandleNewSession(): No room left for new client." CRLF);
      net_ReportServerError(pstNet, iFd);
      NET_ClientDisconnect(iFd);
   }
   return(fCc);
}

//
//  Function:   net_ProtocolType
//  Purpose:    Return the protocol ID
//              
//  Parms:      protocol name ptr
//  Returns:    SOCK_STREAM, SOCK_DGRAM
//
static int net_ProtocolType(const char *pcProtocol) 
{
  if (GEN_STRCMP(pcProtocol, "tcp") == 0) return SOCK_STREAM;
  if (GEN_STRCMP(pcProtocol, "udp") == 0) return SOCK_DGRAM;
  if (GEN_STRCMP(pcProtocol, "TCP") == 0) return SOCK_STREAM;
  if (GEN_STRCMP(pcProtocol, "UDP") == 0) return SOCK_DGRAM;
  return(-1);
}

//
//  Function:   net_ResolveProtocol
//  Purpose:    
//              
//              
//  Parms:      
//  Returns:    
//
static int net_ResolveProtocol(const char *pcProtocol) 
{
  struct protoent *pstProtocol;

  pstProtocol = getprotobyname(pcProtocol);
  if (!pstProtocol) 
  {
    PRINTF("net-ResolveProtocol(): Error protocol = %s" CRLF, pcProtocol);
    LOG_Report(errno, "NET", "net-ResolveProtocol():getprotobyname failed for %s", pcProtocol);
    return(-1);
  }
  return(pstProtocol->p_proto);
}

//
//  Function:   net_SocketAddressInit
//  Purpose:    Clear the socket structure
//              
//  Parms:      
//  Returns:    
//
static void net_SocketAddressInit(SOCKADDR_IN *pstSocketAddr) 
{
  bzero((char *) pstSocketAddr, sizeof(SOCKADDR_IN));
  //
  pstSocketAddr->sin_family      = AF_INET;
  pstSocketAddr->sin_addr.s_addr = INADDR_ANY;
}

//
//  Function:   net_SocketAddressService
//  Purpose:    
//              
//        
//  getservent()     reads the next line from the file /etc/services and returns a structure servent containing 
//                   the broken out fields from the line. The /etc/services file is opened if necessary.
//  getservbyname()  returns a servent structure for the line from /etc/services that matches the service name 
//                   using protocol proto. If proto is NULL, any protocol will be matched.
//  getservbyport()  returns a servent structure for the line that matches the port given in network byte 
//                   order using protocol proto. If proto is NULL, any protocol will be matched.
//  setservent()     opens and rewinds the /etc/services file. If stayopen is true (1), then the file will not 
//                   be closed between calls to getservbyname() and getservbyport().
//  endservent()     closes /etc/services.
//
//  servent structure is defined in <netdb.h> as follows:
//    struct servent 
//    {
//       char    *s_name;        /* official service name */
//       char   **s_aliases;     /* alias list */
//       int      s_port;        /* port number */
//       char    *s_proto;       /* protocol to use */
//    }
//    The members of the servent structure are:
//       s_name
//          The official name of the service. 
//       s_aliases
//          A zero terminated list of alternative names for the service. 
//       s_port
//          The port number for the service given in network byte order. 
//       s_proto
//          The name of the protocol to use with this service. 
//        
//          RETURN VALUE
//          The getservent(), getservbyname() and getservbyport() functions return the servent structure, 
//          or a NULL pointer if an error occurs or the end of the file is reached.  
//        
//  Parms:      
//  Returns:    0=OKee, -1=ERROR
//
static int net_SocketAddressService(SOCKADDR_IN *pstSocketAddr, const char *pcService, const char *pcProtocol)
{
   struct servent *pstServiceAddr;
   int             iPort;

   /* Need to allow numeric as well as textual data. */
   /* 0: pass right through. */

   PRINTF("net-SocketAddressService(): Service=%s, protocol=%s " CRLF, pcService, pcProtocol);

   if (GEN_STRCMP(pcService, "0") == 0)
   {
      pstSocketAddr->sin_port = 0;
   }
   else 
   {                           
      /* nonzero port */
      PRINTF("net-SocketAddressService(): lookup port in services list" CRLF);
      pstServiceAddr = getservbyname(pcService, pcProtocol);
      if(pstServiceAddr) 
      {
         PRINTF("net-SocketAddressService(): Translates to port %d" CRLF, pstServiceAddr->s_port);
         pstSocketAddr->sin_port = pstServiceAddr->s_port;
      } 
      else 
      {
         /* name did not resolve, try number */
         PRINTF("net-SocketAddressService(): no service list found" CRLF);
         if (net_HasAlphanumerics(pcService)) 
         { 
            /* and it's a text name, fail. */
            PRINTF("net-SocketAddressService(): Error service address %s" CRLF, pcService);
            LOG_Report(errno, "NET", "net-SocketAddressService():no lookup for %s/%s", pcService, pcProtocol);
            return(-1);
         }
         //
         iPort = (int)strtol(pcService, NULL, 0);
         PRINTF("net-SocketAddressService(): Port=%d" CRLF, iPort);
         //
         if ((pstSocketAddr->sin_port = htons(iPort)) == 0) 
         {
            PRINTF("net-SocketAddressService():Numeric conversion failed on port %d (%s)" CRLF, iPort, pcService);
            LOG_Report(0, "NET", "net-SocketAddressService():Numeric conversion failed on port %d (%s)", iPort, pcService);
            return(-1);
         }
      }
   }
   return(0);
}

//
//  Function:   net_setnonblocking
//  Purpose:    
//              
//  Parms:      Socket
//  Returns:    
//
static void net_setnonblocking(int iSock)
{
   int iOpts;
    
   iOpts = fcntl(iSock, F_GETFL);
   if (iOpts < 0) 
   {
      LOG_Report(errno, "NET", "net-setnonblocking(F_GETFL): Socket=%d", iSock);
   }
   iOpts = (iOpts | O_NONBLOCK);
   if (fcntl(iSock, F_SETFL, iOpts) < 0) 
   {
      LOG_Report(errno, "NET", "net-setnonblocking(F_SETFL): Socket=%d", iSock);
   }
   return;
}

//
//  Function:  net_Session
//  Purpose:   Handle the incoming data from this session
//
//  Parms:     Session
//  Returns:    0: all session bytes handled
//             +1: More data needed 
//             -1: Error
//  Note:      Additional bytes from the POST Content-Length may still be en-route !
//
static int net_Session(NETCL *pstCl)
{
   int   iCc=0;
   bool  fParsing=TRUE;
   RTYPE tType;

   //PRINTF("net-Session(%d): Get=%d, Put=%d" CRLF, pstCl->iPort, pstCl->iRcvBufferGet, pstCl->iRcvBufferPut);
   // 
   // The session Rcv buffer :
   //
   //         +------------------------------------------------------------------+
   //         |GET /index.html HTTP/1.1 ........                                 |
   //         +------------------------------------------------------------------+
   //          ^iRcvBufferGet                                               RCV_BUFFER_SIZE
   //                                  ^iRcvBufferPut
   //
   //
   while(fParsing)
   {
      //
      // Parse the incoming data for this client
      //
      tType = HTTP_CollectRequest(pstCl);
      switch(tType)
      {
         case HTTP_CHAR:
            break;

         case HTTP_MORE:
            //
            // Rcv buffer empty: wait for more data from the socket 
            //
            pstCl->tSessionType = tType;
            fParsing            = FALSE;
            iCc                 = 1;
            break;

         case HTTP_POST:
         case HTTP_GET:
         case HTTP_PUT:
            pstCl->tHttpType    = tType;
            break;

         case HTTP_DONE:
            //
            // All data in this session have been processed
            //
            pstCl->tSessionType = tType;
            fParsing            = FALSE;
            break;

         default:
            LOG_Report(0, "NET", "net-Session(%d): Unknown Session type %d", pstCl->iPort, tType);
            pstCl->tSessionType = tType;
            fParsing            = FALSE;
            iCc                 = -1;
            break;

         case HTTP_ERROR:
            LOG_Report(0, "NET", "net-Session(%d): HTTP buffer overflow", pstCl->iPort);
            PRINTF("net-Session(%d): HTTP buffer overflow" CRLF, pstCl->iPort);
            pstCl->tSessionType = tType;
            fParsing            = FALSE;
            iCc                 = -1;
            break;

         case HTTP_TPKT:
            // Correct end of the TPKT packet
            pstCl->tSessionType = tType;
            fParsing            = FALSE;
            break;

         case HTTP_RAW:
            // Raw packet
            pstCl->tSessionType = tType;
            fParsing            = FALSE;
            break;
      }
   }
   return(iCc);
}

//
//  Function:  net_SessionClear
//  Purpose:   Clear buffers for this session
//
//  Parms:     Session, NETSTART type, Request type
//  Returns:   
//
static void net_SessionClear(NETCL *pstCl, NETSTART tStart, NETTYPE tRequest)
{
   switch(tStart)
   {
      default:
      case NET_COLDSTART:
         //PRINTF("net-SessionClear():Cold" CRLF);
         GEN_MEMSET(pstCl->pcHdr,       0x00, HDR_BUFFER_SIZE);
         GEN_MEMSET(pstCl->pcUrl,       0x00, URL_BUFFER_SIZE);
         GEN_MEMSET(pstCl->pcRcvBuffer, 0x00, RCV_BUFFER_SIZE);
         GEN_MEMSET(pstCl->pcXmtBuffer, 0x00, XMT_BUFFER_SIZE);
         GEN_MEMSET(pstCl->cIpAddr,     0x00, INET_ADDRSTRLEN);
         //
         if(tRequest == NET_HTTP) pstCl->iReqState = NETSTM_START;
         else                     pstCl->iReqState = NETSTM_RAW;
         //
         pstCl->iSubState     = -1;
         pstCl->iPort         = 0;
         pstCl->iVerbose      = 0;
         pstCl->iRcvBufferGet = pstCl->iRcvBufferPut;
         pstCl->iXmtBufferGet = pstCl->iXmtBufferPut;
         pstCl->iUrlIdx       = 0;
         pstCl->iHdrIdx       = 0;
         pstCl->iPaylIdx      = 0;
         pstCl->iNextIdx      = 0;
         pstCl->iFileIdx      = 0;
         pstCl->iExtnIdx      = 0;
         //
         // Mark new session with timestamp it was initiated !
         //
         pstCl->ulTimestamp   = RTC_GetSystemSecs();
         //
         pstCl->tSessionType  = 0;
         pstCl->tHttpType     = 0;
         pstCl->tContType     = 0;
         pstCl->iContLength   = 0;
         break;

      case NET_WARMSTART:
         //PRINTF("net-SessionClear():Warm" CRLF);
         if(tRequest == NET_HTTP) pstCl->iReqState = NETSTM_START;
         else                     pstCl->iReqState = NETSTM_RAW;
         //
         pstCl->iSubState     = -1;
         pstCl->iUrlIdx       = 0;
         pstCl->iHdrIdx       = 0;
         pstCl->iPaylIdx      = 0;
         pstCl->iNextIdx      = 0;
         pstCl->iFileIdx      = 0;
         pstCl->iExtnIdx      = 0;
         //
         pstCl->tSessionType  = 0;
         pstCl->tHttpType     = 0;
         pstCl->tContType     = 0;
         pstCl->iContLength   = 0;
         break;
   }
}

//
//  Function:  net_LookupHost
//  Purpose:   Lookup host IP address
//
//  Parms:     Host URL, Dest, dest len
//  Returns:   TRUE if found
//
static bool net_LookupHost(const char *pcHost, char *pcAddr, int iIpLen)
{
   struct addrinfo stHints, *pstRes, *pstTmp;
   int             iErr;
   void           *pvData;

   GEN_MEMSET(&stHints, 0, sizeof (stHints));
   stHints.ai_family   = PF_UNSPEC;
   stHints.ai_socktype = SOCK_STREAM;
   stHints.ai_flags   |= AI_CANONNAME;

   iErr = getaddrinfo (pcHost, NULL, &stHints, &pstRes);
   if (iErr != 0)
   {
      return(FALSE);
   }
   PRINTF("net-LookupHost(): Host=%s" CRLF, pcHost);
   pstTmp = pstRes;
   while(pstRes)
   {
      inet_ntop (pstRes->ai_family, pstRes->ai_addr->sa_data, pcAddr, iIpLen);
      //
      switch (pstRes->ai_family)
      {
         default:
         case AF_INET:
            pvData = &((struct sockaddr_in *) pstRes->ai_addr)->sin_addr;
            break;

         case AF_INET6:
            pvData = &((struct sockaddr_in6 *) pstRes->ai_addr)->sin6_addr;
            break;
      }
      inet_ntop (pstRes->ai_family, pvData, pcAddr, iIpLen);
      PRINTF("net-LookupHost(): IPv%d address: %s (%s)" CRLF, pstRes->ai_family == PF_INET6 ? 6 : 4, pcAddr, pstRes->ai_canonname);
      pstRes = pstRes->ai_next;
   }
   if(pstTmp) freeaddrinfo(pstTmp);
   return(TRUE);
}


//
//  Function:  net_ReportServerError
//  Purpose:   Report server error : too many connections
//
//  Parms:     Net, Fd
//  Returns:   
//
static void net_ReportServerError(NETCON *pstNet, int iFd)
{
   int      x;
   u_int32  ulSecs, ulSecsNow;
   char    *pcBuffer;
   NETCL   *pstCl;

   PRINTF("net-ReportServerError()" CRLF);
   pcBuffer  = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, 200);
   ulSecsNow = RTC_GetSystemSecs();
   //
   if(pstNet->iVerbose & NET_VERBOSE_SERVER) LOG_Report(0, "NET", "net-ReportServerError():No connections available:Close expired ones.");
   
   #ifdef   FEATURE_NET_REPORT_PAGE
   //
   // Send back full HTML page with error data
   //
   NET_WriteString(iFd, (char *)pcHttpResponseHeader);
   NET_WriteString(iFd, (char *)pcWebPageTitle);
   NET_WriteString(iFd, (char *)pcWebPageLineBreak);
   NET_WriteString(iFd, (char *)pcWebPageError);
   NET_WriteString(iFd, (char *)pcWebPageLineBreak);
   //
   for (x=0; x<pstNet->iConnections; x++)
   {
      GEN_SNPRINTF(pcBuffer, 200, "Cnx-%2d:", x+1);
      //
      NET_WriteString(iFd, (char *)pcWebPageLineBreak);
      NET_WriteString(iFd, pcBuffer);
      NET_WriteString(iFd, (char *)pcWebPageLineBreak);
      //
      PRINTF("net-ReportServerError():%s" CRLF, pcBuffer);
      if(pstNet->iVerbose & NET_VERBOSE_SERVER) LOG_Report(0, "NET", "net-ReportServerError():%s", pcBuffer);
      //
      pstCl = &pstNet->stClient[x];
      if(pstCl->iSocket)
      {
         RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MM_YYYY_HH_MM_SS, pcBuffer, pstCl->ulTimestamp);
         NET_WriteString(iFd, (char *)pcBuffer);
         NET_WriteString(iFd, (char *)pcWebPageLineBreak);
         if(pstCl->iVerbose & NET_VERBOSE_SERVER) net_ReportServerData(pstCl);
         //
         ulSecs = ulSecsNow - pstCl->ulTimestamp;
         if(ulSecs > HTTP_CONNECTION_MAX_SECS)
         {
            //
            // The CNX is in perril: reset all connection with timestamp > HTTP_CONNECTION_MAX_SECS ago 
            //
            if( shutdown(pstCl->iSocket, SHUT_RDWR) == 0)
            {
               if(pstCl->iVerbose & NET_VERBOSE_SERVER) LOG_Report(0, "NET", "net-ReportServerError():Shutdown Cnx-%d OKee", x+1);
            }
            else
            {
               if(pstCl->iVerbose & NET_VERBOSE_SERVER) LOG_Report(errno, "NET", "net-ReportServerError():Shutdown Cnx-%d ERROR:", x+1);
            }
            pstCl->iSocket = 0;
         }
         else
         {
            if(pstCl->iVerbose & NET_VERBOSE_SERVER) LOG_Report(0, "NET", "net-ReportServerError():Cnx-%d no Shutdown yet (%d secs)", x+1, ulSecs);
         }
      }
      else
      {
         //
         // Not likely: we came here because no socket was available. Anyway.
         //
         RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MM_YYYY_HH_MM_SS, pcBuffer, pstCl->ulTimestamp);
         if(pstCl->iVerbose & NET_VERBOSE_SERVER) 
         {
            LOG_Report(0, "NET", "net-ReportServerError(%d):Cnx-%d not in use", pstCl->iPort, x+1);
            LOG_Report(0, "NET", "net-ReportServerError(%d):Last accessed at %s", pstCl->iPort, pcBuffer);
         }
         NET_WriteString(iFd, (char *)pcWebPageNotInUse);
         NET_WriteString(iFd, (char *)pcWebPageLineBreak);
         NET_WriteString(iFd, (char *)pcBuffer);
         NET_WriteString(iFd, (char *)pcWebPageLineBreak);
         PRINTF("net-ReportServerError():Last accessed at %s" CRLF, pcBuffer);
      }
   }
   NET_WriteString(iFd, (char *)pcWebPageLineBreak);
   NET_WriteString(iFd, (char *)pcWebPageEnd);

   #else    //FEATURE_NET_REPORT_PAGE
   //
   // Only send back single-line error msg
   //
   NET_WriteString(iFd, (char *)pcWebPageError);
   //
   for (x=0; x<pstNet->iConnections; x++)
   {
      PRINTF("net-ReportServerError():Cnx-%d" CRLF, x+1);
      //
      pstCl = &pstNet->stClient[x];
      if(pstCl->iSocket)
      {
         if(pstCl->iVerbose & NET_VERBOSE_SERVER) 
         {
            LOG_Report(0, "NET", "net-ReportServerError(%d):Cnx-%d", pstCl->iPort, x+1);
            net_ReportServerData(pstCl);
         }
         ulSecs = ulSecsNow - pstCl->ulTimestamp;
         if(ulSecs > HTTP_CONNECTION_MAX_SECS)
         {
            //
            // The CNX is in perril: reset all connection with timestamp > HTTP_CONNECTION_MAX_SECS ago 
            //
            if( shutdown(pstCl->iSocket, SHUT_RDWR) == 0)
            {
               if(pstCl->iVerbose & NET_VERBOSE_SERVER) LOG_Report(0, "NET", "net-ReportServerError(%d):Shutdown Cnx-%d OKee", pstCl->iPort, x+1);
            }
            else
            {
               if(pstCl->iVerbose & NET_VERBOSE_SERVER) LOG_Report(errno, "NET", "net-ReportServerError(%d):Shutdown Cnx-%d ERROR:", pstCl->iPort, x+1);
            }
            pstCl->iSocket = 0;
         }
         else
         {
            if(pstCl->iVerbose & NET_VERBOSE_SERVER) LOG_Report(0, "NET", "net-ReportServerError(%d):Cnx-%d no Shutdown yet (%d secs)", pstCl->iPort, x+1, ulSecs);
         }
      }
      else
      {
         //
         // Not likely: we came here because no socket was available. Anyway.
         //
         if(pstNet->iVerbose & NET_VERBOSE_SERVER) 
         {
            LOG_Report(0, "NET", "net-ReportServerError(%d):Cnx-%d has no socket", pstCl->iPort, x+1);
            net_ReportServerData(pstCl);
            RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MM_YYYY_HH_MM_SS, pcBuffer, pstCl->ulTimestamp);
            PRINTF("net-ReportServerError():Cnx-%d last accessed at %s" CRLF, x+1, pcBuffer);
            LOG_Report(0, "NET", "net-ReportServerError(%d):Cnx-%d last accessed at %s", pstCl->iPort, x+1, pcBuffer);
         }
      }
   }
   #endif   //FEATURE_NET_REPORT_PAGE
   
   SAFEFREE(__PRETTY_FUNCTION__ , pcBuffer);
}

//
//  Function:  net_ReportServerData
//  Purpose:   Report server data
//
//  Parms:     Net client
//  Returns:   
//
static void net_ReportServerData(NETCL *pstCl)
{
   char  pcBuffer[32];

   LOG_Report(0, "NET", "net-ReportServerData():State handler state      : %d",     pstCl->iReqState);
   LOG_Report(0, "NET", "net-ReportServerData():Net Client flags         : 0x%X",   pstCl->fFlags);
   LOG_Report(0, "NET", "net-ReportServerData():Content length           : %d",     pstCl->iContLength);
   LOG_Report(0, "NET", "net-ReportServerData():Session Socket           : %d",     pstCl->iSocket);
   LOG_Report(0, "NET", "net-ReportServerData():Port                     : %d",     pstCl->iPort);
   LOG_Report(0, "NET", "net-ReportServerData():HTTP www path            : %s",     pstCl->pcWww);
   LOG_Report(0, "NET", "net-ReportServerData():HTTP cache path          : %s",     pstCl->pcCache);
   LOG_Report(0, "NET", "net-ReportServerData():RCV index get            : %d",     pstCl->iRcvBufferGet);
   LOG_Report(0, "NET", "net-ReportServerData():RCV index put            : %d",     pstCl->iRcvBufferPut);
   LOG_Report(0, "NET", "net-ReportServerData():RCV index URL            : %d",     pstCl->iUrlIdx);
   LOG_Report(0, "NET", "net-ReportServerData():RCV index request header : %d",     pstCl->iHdrIdx);
   LOG_Report(0, "NET", "net-ReportServerData():RCV index first payload  : %d",     pstCl->iPaylIdx);
   LOG_Report(0, "NET", "net-ReportServerData():RCV index next  payload  : %d",     pstCl->iNextIdx);
   LOG_Report(0, "NET", "net-ReportServerData():RCV index filename       : %d",     pstCl->iFileIdx);
   LOG_Report(0, "NET", "net-ReportServerData():RCV index extension      : %d",     pstCl->iExtnIdx);
   LOG_Report(0, "NET", "net-ReportServerData():XMT index get            : %d",     pstCl->iXmtBufferGet);
   LOG_Report(0, "NET", "net-ReportServerData():XMT index put            : %d",     pstCl->iXmtBufferPut);
   LOG_Report(0, "NET", "net-ReportServerData():Session type             : %d",     pstCl->tSessionType);
   LOG_Report(0, "NET", "net-ReportServerData():Get/Post type            : %d",     pstCl->tHttpType);
   LOG_Report(0, "NET", "net-ReportServerData():Content type             : %d",     pstCl->tContType);
   //
   RTC_ConvertDateTime(TIME_FORMAT_WW_DD_MM_YYYY_HH_MM_SS, pcBuffer, pstCl->ulTimestamp);
   LOG_Report(0, "NET", "net-ReportServerData():Last timestamp           : %s",     pcBuffer);
   //
   net_DumpData(pcWebPageRcv, pstCl->pcRcvBuffer, RCV_BUFFER_SIZE);
   net_DumpData(pcWebPageXmt, pstCl->pcXmtBuffer, XMT_BUFFER_SIZE);
}

//
//  Function:  net_ReportReceiveData
//  Purpose:   Report received data
//
//  Parms:     Net client
//  Returns:   
//
static void net_ReportReceiveData(NETCL *pstCl)
{
   LOG_Report(0, "NET", "net-ReportReceiveData(%d):RCV index get           : %d", pstCl->iPort, pstCl->iRcvBufferGet);
   LOG_Report(0, "NET", "net-ReportReceiveData(%d):RCV index put           : %d", pstCl->iPort, pstCl->iRcvBufferPut);
   //
   net_DumpData(pcWebPageRcv, pstCl->pcRcvBuffer, RCV_BUFFER_SIZE);
}

//
// Function:   net_DumpData
// Purpose:    Dump server data
//
// Parms:      Title, Buffer, size
// Returns:   
// Note:       iAllZero =
//             0: Init
//             1: Non-zero item in buffer
//             2: Zero buffer has been displayed
//
#define NET_DUMP_COLS         32
#define NET_DUMP_COLS_LAST    NET_DUMP_COLS - 1
//
#define NET_DUMP_TEXT       ((NET_DUMP_COLS * 3) + 4)
//
#define NET_DUMP_SIZE_HEX   ((NET_DUMP_COLS * 4) + 16)
#define NET_DUMP_SIZE_ASC   ((NET_DUMP_COLS * 1) + 16)
//
static void net_DumpData(const char *pcTitle, char *pcData, int iLength)
{
   int      iAllZero=0;
   int      x=0, y=0, i, iNr;
   char     cChar;
   char    *pcBuffer;
   char    *pcAscii;

   //
   // Alloc and clear
   //
   pcBuffer = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, NET_DUMP_SIZE_HEX);
   pcAscii  = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, NET_DUMP_SIZE_ASC);
   GEN_MEMSET(pcBuffer, 0x20, NET_DUMP_COLS * 3);
   GEN_MEMSET(pcAscii,  0x20, NET_DUMP_COLS * 1);
   //
   LOG_Report(0, "NET", "net-DumpData():%s", pcTitle);
   PRINTF("net-DumpData():%s" CRLF, pcTitle);
   for(i=0; i<iLength; i++)
   {
      if( (cChar = pcData[i]) != 0) iAllZero = 1;
      GEN_SNPRINTF(&pcBuffer[x], 4, "%02X", cChar);
      pcBuffer[x+2] = ' ';
      //
      // Print ASCII 
      //
      if(isprint((int)cChar)) pcAscii[y] = cChar;
      else                    pcAscii[y] = '.';
      //
      if(i % NET_DUMP_COLS == NET_DUMP_COLS_LAST) 
      {
         iNr = (i / NET_DUMP_COLS) * NET_DUMP_COLS;
         switch(iAllZero)
         {
            case 0:
               // This row has all zeros
               LOG_Report(0, "NET", "%04d 00 -->", iNr);
               iAllZero = 2;
               break;

            default:
            case 1:
               // This row has actual data
               LOG_Report(0, "NET", "%04d %s  (%s)", iNr, pcBuffer, pcAscii);
               iAllZero = 0;
               break;

            case 2:
               // Previous content was all zeros: summary is enough for now
               break;
         }
         // Restart new row
         GEN_MEMSET(pcBuffer, 0x20, NET_DUMP_COLS * 3);
         GEN_MEMSET(pcAscii,  0x20, NET_DUMP_COLS * 1);
         x = 0;
         y = 0;
      }
      else 
      {
         x += 3;
         y += 1;
      }
   }
   if(x) 
   {
      LOG_Report(0, "NET", "%s  (%s)", pcBuffer, pcAscii);
   }
   SAFEFREE(__PRETTY_FUNCTION__ , pcAscii);
   SAFEFREE(__PRETTY_FUNCTION__ , pcBuffer);
}