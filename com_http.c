/*  (c) Copyright:  2017..2020  Patrn, Confidential Data
 *
 *  Workfile:           com_http.cpp
 *  Revision:          
 *  Modtime:           
 *
 *  Purpose:            Dynamic HTTP page for RPi webserver
 *
 *
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:
 *
 *  Author:             Peter Hillen
 *  Date Created:       2 Mar 2018 Made common from rpi_http.*
 *
 *  Revisions:
 *  Entry Points:       HTTP_RequestStaticPage()
 *                      HTTP_CollectGet()
 *                      HTTP_CollectGetParameter()
 *
 *  History: 
 *    22 Oct 2017:      http_HandleRequest()
 *                      Fix issue to handle full pathnames from the URL.
 *    25 Mar 2018:      Add %f to HTTP_BuildGeneric() for basic float/double support
 *    08 Apr 2019:      Add start page with meta refresh tags
 *    29 Jun 2019:      Add TPKT parsing
 *    01 Jul 2019:      Pass HTTP_ERROR on syntax errors; Fix POST acquisition; Check popen() fp
 *    07 Oct 2019:      Add GEN_GetParameter()
 *    09 Oct 2020:      Add Client/Server request type (HTTP, RAW,...)
 *    21 May 2021:      Add HTTP Delete (for csv files)
 *    19 Nov 2021:      Add Variadic macro's for PRINTF()
 *    07 Mar 2022:      Replace atoi() by strtol()
 *    29 Feb 2024:      Add text file viewer
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include <semaphore.h>
#include <unistd.h>

#include "typedefs.h"
#include "config.h"
#include "com_file.h"
#include "com_func.h"
#include "com_json.h"
#include "com_rtc.h"
#include "com_log.h"
#include "com_safe.h"
#include "com_http.h"

//#define USE_PRINTF
#include "printx.h"

//
// Enums STH
//
#define  EXTRACT_HTTP(a,b,c)   a,
enum
{
#include "com_stm.h"
   //
   NUM_HTTP_STMS
};
#undef EXTRACT_HTTP

//
// Externals
//
bool  GLOBAL_CheckDelete(char *);
//
// Generic GLOBAL HTTP data
//
const char *pcHttpResponseHeader       = "HTTP/1.0 200 OK" NEWLINE "Content-Type=text/html" NEWLINE NEWLINE;
const char *pcHttpResponseHeaderJson   = "HTTP/1.0 200 OK" NEWLINE "Content-Type=application/json" NEWLINE NEWLINE;
const char *pcHttpResponseHeaderBad    = "HTTP/1.0 400 Bad Request" NEWLINE NEWLINE;
//
const char *pcWebPageStart       = "<!doctype html public \"-//w3c//dtd html 4.01//en\"" NEWLINE \
                                   "<html>" NEWLINE \
                                   "<head>" NEWLINE \
                                   "<style type=\"text/css\">" NEWLINE \
                                   "thead {font-family:\"Tahoma\";font-size:\"100%%\";color:green}" NEWLINE \
                                   "tbody {font-family:\"Courier new\";font-size:\"25%%\";color:blue}" NEWLINE \
                                   "</style>" NEWLINE \
                                   "<meta http-equiv=\"content-type\" content=\"text/html; charset=windows-1252\">" NEWLINE \
                                   "<title>%s</title>" NEWLINE \
                                   "</head>" NEWLINE \
                                   "<body style=\"text-align:center\">" NEWLINE;
//
const char *pcWebPageStartRefresh= "<!doctype html public \"-//w3c//dtd html 4.01//en\"" NEWLINE \
                                   "<html>" NEWLINE \
                                   "<head>" NEWLINE \
                                   "<style type=\"text/css\">" NEWLINE \
                                   "thead {font-family:\"Tahoma\";font-size:\"100%%\";color:green}" NEWLINE \
                                   "tbody {font-family:\"Courier new\";font-size:\"25%%\";color:blue}" NEWLINE \
                                   "</style>" NEWLINE \
                                   "<meta http-equiv=\"content-type\" content=\"text/html; charset=windows-1252\">" NEWLINE \
                                   "<title>%s</title>" NEWLINE \
                                   "<meta http-equiv=\"refresh\"" NEWLINE \
                                   "content=\"%s\">"
                                   "</head>" NEWLINE \
                                   "<body style=\"text-align:center\">" NEWLINE;
//
const char *pcWebPageTitle       = "PATRN Embedded Software Services";
//
const char *pcWebPageCenter      = "<body style=\"text-align:center\">" NEWLINE;
const char *pcWebPageDefault     = "<hr/>" NEWLINE \
                                   "<h2>PATRN ESS</h2>" NEWLINE \
                                   "<h3>Raspberry Pi HTTP server</h3>" NEWLINE \
                                   "<hr/>" NEWLINE;
//
const char *pcWebPageLineBreak   = "<br/>";
const char *pcWebPagePreStart    = "<div align=\"left\"><pre>";
const char *pcWebPagePreEnd      = "</pre></div> ";
const char *pcWebPageEnd         = "</body></html>" NEWLINE NEWLINE;
//
const char *pcRpiArp             = "arp -ne";
const char *pcRpiMac             = "b8:27:eb";
//
// Local prototypes
//
static FTYPE   http_CollectHandleExtension   (NETCL *);
static bool    http_HandleHtml               (NETCL *);
static bool    http_HandleJs                 (NETCL *);
static bool    http_HandleJpg                (NETCL *);
static bool    http_HandleBmp                (NETCL *);
static bool    http_HandleGif                (NETCL *);
static bool    http_HandlePng                (NETCL *);
static bool    http_HandleTxt                (NETCL *);
static bool    http_HandleMp3                (NETCL *);
static bool    http_HandleHdtv               (NETCL *);
static bool    http_HandleJson               (NETCL *);
static bool    http_HandleRequest            (NETCL *);
static bool    http_HandleViewer             (NETCL *);
//
static bool    http_VerifyPath               (NETCL *, char *, int);
static bool    http_Xmt                      (NETCL *, char);
//
static RTYPE   http_ParseRequestHeader       (NETCL *, char *);
static int     http_ReqHdrGetValue           (NETCL *, char *);
static RTYPE   http_ReqHdrAcceptEncoding     (NETCL *, char *, int);
static RTYPE   http_ReqHdrContentType        (NETCL *, char *, int);
static RTYPE   http_ReqHdrContentLength      (NETCL *, char *, int);
//
// HTTP acquisition state machine functions
//
static RTYPE   http_StmIdle                  (NETCL *);
static RTYPE   http_StmStart                 (NETCL *);
static RTYPE   http_StmGet1                  (NETCL *);
static RTYPE   http_StmGet2                  (NETCL *);
static RTYPE   http_StmPost1                 (NETCL *);
static RTYPE   http_StmPost2                 (NETCL *);
static RTYPE   http_StmPost3                 (NETCL *);
static RTYPE   http_StmSpace                 (NETCL *);
static RTYPE   http_StmSlash                 (NETCL *);
static RTYPE   http_StmEscape1               (NETCL *);
static RTYPE   http_StmEscape2               (NETCL *);
static RTYPE   http_StmUrl                   (NETCL *);
static RTYPE   http_StmContentHeaders        (NETCL *);
static RTYPE   http_StmPayload               (NETCL *);
static RTYPE   http_StmTpktRes               (NETCL *);
static RTYPE   http_StmTpktLenMsb            (NETCL *);
static RTYPE   http_StmTpktLenLsb            (NETCL *);
static RTYPE   http_StmTpktPacket            (NETCL *);
static RTYPE   http_StmRawPacket             (NETCL *);
//
static const char pcHttpContentTypeHtml[]        = "text/html";
static const char pcHttpContentTypeShtml[]       = "text/shtml";
static const char pcHttpContentTypeJs[]          = "application/javascript";
static const char pcHttpContentTypeCgi[]         = "application/cgi";
static const char pcHttpContentTypeJpg[]         = "image/jpeg";
static const char pcHttpContentTypeBmp[]         = "image/bmp";
static const char pcHttpContentTypePng[]         = "image/png";
static const char pcHttpContentTypeGif[]         = "image/gif";
static const char pcHttpContentTypeMp3[]         = "image/mp3";
static const char pcHttpContentTypeMp4[]         = "image/mp4";
static const char pcHttpContentTypeH264[]        = "image/h264";
static const char pcHttpContentTypeTxt[]         = "text/html";
static const char pcHttpContentTypeZip[]         = "application/zip";
static const char pcHttpContentTypeCsv[]         = "application/csv";
static const char pcHttpContentTypeJson[]        = "application/json";

static const char pcHttpRequestHeaderGeneric[]   = "HTTP/1.0 200 OK\r\nContent-Type:html\r\n";
static const char pcHttpRequestHeaderOKee[]      = "HTTP/1.0 200 OK\r\nContent-Type:%s\r\n";
static const char pcHttpRequestHeaderBad[]       = "HTTP/1.0 400 Bad Request\r\n";
static const char pcHttpRequestHeaderForbidden[] = "HTTP/1.0 403 Forbidden\r\n";
static const char pcHttpRequestHeaderError[]     = "HTTP/1.0 404 Not Found\r\n";
static const char pcHttpRequestHeaderConnect[]   = "Connection:Closed\r\n\r\n";

static const HTTPTYPE stHttpTypes[] =
{
//   pcExt           tType          pcType                  pfExt   
   { "gen",          HTTP_GEN,      pcHttpContentTypeHtml,  0                       },       // generic files
   { "shtml",        HTTP_SHTML,    pcHttpContentTypeShtml, 0                       },       // ssi    shtml and files without ext.
   { "html",         HTTP_HTML,     pcHttpContentTypeHtml,  http_HandleHtml         },       // html   html pages
   { "htm",          HTTP_HTML,     pcHttpContentTypeHtml,  http_HandleHtml         },       // html   html pages
   { "js",           HTTP_JS,       pcHttpContentTypeJs,    http_HandleJs           },       // js     JavaScript files
   { "cgi",          HTTP_CGI,      pcHttpContentTypeCgi,   0                       },       // cgi    cgi script files
   { "gif",          HTTP_GIF,      pcHttpContentTypeGif,   http_HandleGif          },       // gif    images
   { "jpg",          HTTP_JPG,      pcHttpContentTypeJpg,   http_HandleJpg          },       // jpg    images
   { "bmp",          HTTP_BMP,      pcHttpContentTypeBmp,   http_HandleBmp          },       // bmp    images
   { "png",          HTTP_PNG,      pcHttpContentTypePng,   http_HandlePng          },       // Png    images
   { "mp3",          HTTP_MP3,      pcHttpContentTypeMp3,   http_HandleMp3          },       // mp3    audio
   { "mp4",          HTTP_MP4,      pcHttpContentTypeMp4,   http_HandleHdtv         },       // mp4    HDTV Video
   { "h264",         HTTP_H264,     pcHttpContentTypeH264,  http_HandleHdtv         },       // h264   HDTV Video
   { "txt",          HTTP_TXT,      pcHttpContentTypeTxt,   http_HandleTxt          },       // txt    Unformatted
   { "log",          HTTP_TXT,      pcHttpContentTypeTxt,   http_HandleTxt          },       // log    Unformatted
   { "json",         HTTP_JSON,     pcHttpContentTypeJson,  http_HandleJson         },       // json   JSON files
   { "zip",          HTTP_ZIP,      pcHttpContentTypeZip,   http_HandleRequest      },       // zip    files
   { "csv",          HTTP_CSV,      pcHttpContentTypeCsv,   http_HandleRequest      },       // csv    files
   { "css",          HTTP_CSS,      pcHttpContentTypeHtml,  0                       },       // css    files
   { "ico",          HTTP_ICO,      pcHttpContentTypeHtml,  0                       }        // ico    files
};
static const int iNumHttpTypes = sizeof(stHttpTypes)/sizeof(HTTPTYPE);

//
// HTTP request headers
//
static const HTTPREQ stHttpRequestHeaders[] =
{
   { "Accept-Encoding",    http_ReqHdrAcceptEncoding        },
   { "Content-Type",       http_ReqHdrContentType           },
   { "Content-Length",     http_ReqHdrContentLength         },
   {  NULL,                NULL                             }
};

#define  EXTRACT_HTTP(a,b,c)   {a,b,c},
static const HTTPSTM stHttpStateMachine[] =
{
#include "com_stm.h"
};
static const int iNumHttpStateMachines = sizeof(stHttpStateMachine)/sizeof(HTTPSTM);
#undef EXTRACT_HTTP

/*------  Local functions separator ------------------------------------
__GLOBAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
// Function    : HTTP_CollectRequest
// Description : Collect all incoming HTTP data and single out the pathname/parameters
//
// Parameters  : Client
// Returns     : Request type
// Note        : Received data from the socket (firefox browser):
//
//    "GET /cam.json?snapshot&Height=576&Ext=bmp&Preview=860,0,860,540&Rotation=180&Width=1024 HTTP/1.0"
//    "Host: www.patrn.nl"
//    "User-Agent: Mozilla/5.0 (Windows; U; WinGecko/20100722 Firefox/3.6.8 (.NET CLR 3.5.30729)"
//    "Accept: image/png,image/*;q=0.8,*/*;q=0.5"
//    "Accept-Encoding: gzip,deflate"
//    "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7"
//    "Keep-Alive: 115"
//    "Connection: keep-alive"
//    ""
//
//    "GET //all/public/www/pix/20170312_133243.bmp HTTP/1.0"
//    "Host: www.patrn.nl"
//    "User-Agent: Mozilla/5.0 (Windows; U; WinGecko/20100722 Firefox/3.6.8 (.NET CLR 3.5.30729)"
//    "Accept: image/png,image/*;q=0.8,*/*;q=0.5"
//    "Accept-Encoding: gzip,deflate"
//    "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7"
//    "Keep-Alive: 115"
//    "Connection: keep-alive"
//    ""
//
//    "POST /cam.json HTTP/1.0"
//    "Host: www.patrn.nl"
//    "User-Agent: Mozilla/5.0 (Windows; U; WinGecko/20100722 Firefox/3.6.8 (.NET CLR 3.5.30729)"
//    "Accept: image/png,image/*;q=0.8,*/*;q=0.5"
//    "Accept-Encoding: gzip,deflate"
//    "Content-Type: application-json"
//    "Content-Length: 42"
//    "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7"
//    "Keep-Alive: 115"
//    "Connection: keep-alive"
//    ""
//    "*** This is the 42 bytes port payload  ***"
//
//    Acquire data:
//    =============================================================================================
//    State:   0..3:    Wait for "GET " or "POST "
//             4:       Start filename collection
//             5..7:    %AB escape sequence collection
//             8:       Wait/Store Filename.ext?parameter1=x&parameter2=y (handle http escape chars)
//             9..11:   Wait for "POST " 
//             12..13:  Parse request headers
//             etc.
//             (see com_stm.h)
//
//         GET:
//         pcUrl -> "/all/public/www/cam.json?command=snapshot&Height=576&Ext=bmp&Preview=860,0,860,540&Rotation=180&Width=1024"
//                   |-->>>          |   |    |
//    iPathIdx = ----+               |   |    |
//    iFileIdx = --------------------+   |    |
//    iExtnIdx = ------------------------+    |
//    iPaylIdx = -----------------------------+
//                                            |
//         POST: Add Payload------------------|
//                                            |
//         pcUrl -> "/all/public/www/cam.json command=snapshot&Height=576&Ext=bmp&Preview=860,0,860,540&Rotation=180&Width=1024"
//
//      RcvBuffer-> "GET /cam.json?snapshot&Height=576&Ext=bmp&Preview=860,0,860,540&Rotation=180&Width=1024 HTTP/1.0\r\n"
//                      "Host: www.patrn.nl\r\nContent-Type: application-json\r\nContent-Length: 42\r\n\r\n"
//                       |
//     iHdrIdx = --------+
//
//      RcvBuffer-> "POST /cam.json HTTP/1.0\r\n"
//                      "Host: www.patrn.nl\r\nContent-Type: application-json\r\nContent-Length: 42\r\n"
//                      "\r\n" 
//                      "snapshot&Height=576&Ext=bmp&Preview=860,0,860,540&Rotation=180&Width=1024"
//
RTYPE HTTP_CollectRequest(NETCL *pstCl)
{
   RTYPE    tType = HTTP_CHAR;

   if(pstCl->iRcvBufferGet == pstCl->iRcvBufferPut)
   {
      //
      // Hmmmm, no more data in the middle of the STM, maybe there is more in the socket
      //
      PRINTF("HTTP_CollectRequest():Buffer empty" CRLF);
      tType = HTTP_MORE;
   }
   else
   {
      //
      // NET_VERBOSE_HTTP_GET:
      // Echo all HTTP request data from the client
      // Call HTTP State handler
      //
      if(pstCl->iVerbose & NET_VERBOSE_HTTP_GET) putchar((int)pstCl->pcRcvBuffer[pstCl->iRcvBufferGet]);
      if( (pstCl->iReqState >= 0) && (pstCl->iReqState < iNumHttpStateMachines) )
      {
         //PRINTF("HTTP_CollectRequest():%s" CRLF, stHttpStateMachine[pstCl->iReqState].pcName);
         tType = stHttpStateMachine[pstCl->iReqState].pfHttpStm(pstCl);
         //
         // Keep buffers in check
         //
         if(pstCl->iUrlIdx >= URL_BUFFER_SIZE-1)
         {
            pstCl->iUrlIdx = URL_BUFFER_SIZE-2;
            pstCl->pcUrl[pstCl->iUrlIdx] = 0;
            LOG_Report(0, "NET", "HTTP_CollectRequest():URL Buffer overflow (%d)", pstCl->iUrlIdx);
            LOG_Report(0, "NET", "URL=%s", pstCl->pcUrl);
            LOG_Report(0, "NET", "HDR=%s", pstCl->pcHdr);
            tType = HTTP_ERROR;
         }
         if(pstCl->iHdrIdx >= HDR_BUFFER_SIZE-1)
         {
            pstCl->iHdrIdx = HDR_BUFFER_SIZE-2;
            pstCl->pcHdr[pstCl->iHdrIdx] = 0;
            LOG_Report(0, "NET", "HTTP_CollectRequest():HDR Buffer overflow (%d)", pstCl->iHdrIdx);
            LOG_Report(0, "NET", "URL=%s", pstCl->pcUrl);
            LOG_Report(0, "NET", "HDR=%s", pstCl->pcHdr);
            tType = HTTP_ERROR;
         }
      }
      else
      {
         LOG_Report(0, "NET", "HTTP_CollectRequest(): BAD STM state %d", pstCl->iReqState);
         pstCl->iReqState = NETSTM_IDLE;              // Skip all next chars
      }
   }
   return(tType);
}

//
// Function    : HTTP_CollectGetParameter
// Description : Collect incoming HTTP parameters
//
// Parameters  : Client
// Returns     : NULL if no parameter is found, 
//                else asciiz pointer to parameter --> "parameter1=xxxx"
// 
//  Note       : Called to retrieve subsequent parameters from the pcUrl buffer in 
//               the client struct.
//
//               "/dir1/dir2/filename.ext?parameter1=xxx&parameter2=yyy&...&parameterX=zzz"
//                                    ^  ^^             ^
//                                    |  ||             +---- /0 terminator after call
//                                    |  |+--- iPaylIdx 
//                                    |  +---- /0 terminator
//                                    +------- iExtnIdx  
//
// 
char *HTTP_CollectGetParameter(NETCL *pstCl)
{
   bool     fSearching=TRUE;
   char    *pcUrl=pstCl->pcUrl;
   int      iPaylIdx=pstCl->iNextIdx;
   char    *pcParm=NULL;
   
   PRINTF("HTTP_CollectGetParameter():Payl Idx=%d" CRLF, iPaylIdx);
   if(iPaylIdx)
   {
      pcParm = &pcUrl[iPaylIdx];
      PRINTF("HTTP_CollectGetParameter():<%s>" CRLF, pcParm);
      //
      while(fSearching)
      {
         switch(pcUrl[iPaylIdx])
         {
            default:
               iPaylIdx++;
               break;

            case 0:
               // This is the last parameter
               iPaylIdx = 0;
               fSearching = FALSE;
               break;

            case '&':
               // This is the next parameter
               iPaylIdx++;
               fSearching = FALSE;
               break;
         }
      }
      pstCl->iNextIdx = iPaylIdx;
   }
   return(pcParm);
}

//
// Function    : HTTP_GetParameter
// Description : Get single HTTP parameter
//
// Parameters  : Client, Parm number 1..?, Dest buffer, Dest buffer length
// Returns     : NULL if no parameter is found, 
//                    else asciiz pointer to parameter --> "parameter1=xxxx"
// 
//  Note       : Called to retrieve a parameter from the pcUrl buffer in the client struct.
//
//               "/dir1/dir2/filename.ext?parameter1=xxx&parameter2=yyy&...&parameterX=zzz"
//                                    ^  ^^ 
//                                    |  || 
//                                    |  |+--- iPaylIdx 
//                                    |  +---- /0 terminator
//                                    +------- iExtnIdx  
//
//                If pcDest[iMax] is provided, the requested parameter will be copied
//                into the buffer.
// 
char *HTTP_GetParameter(NETCL *pstCl, int iNr, char *pcDest, int iMax)
{
   bool     fSearching=TRUE;
   int      iIdx, iLen;
   char    *pcParm=NULL;
   char    *pcParms=NULL;
   char    *pcParme=NULL;
   
   if(iNr == 0) return(NULL);
   //
   iIdx = pstCl->iPaylIdx;
   //
   PRINTF("HTTP-GetParameter():Payload Idx=%d" CRLF, iIdx);
   if(iIdx)
   {
      pcParms = pcParme = &(pstCl->pcUrl[iIdx]);
      //
      while(fSearching)
      {
         switch(*pcParme)
         {
            case 0:
               // This is the last parameter
               if(iNr == 1) pcParm = pcParms;
               fSearching = FALSE;
               break;

            case '&':
               // This is the next parameter
               if(iNr > 1) 
               {
                  pcParme++;
                  pcParms = pcParme;
                  iNr--;
               }
               else 
               {
                  pcParm     = pcParms;
                  fSearching = FALSE;
               }
               break;

            default:
               pcParme++;
               break;
         }
      }
      //
      // Ready: maybe copy result to dest
      //
      if(pcDest && pcParm)
      {
         iLen = pcParme - pcParms;
         if(iLen >= iMax) iLen = iMax -1 ;
         GEN_MEMCPY(pcDest, pcParm, iLen);
         pcDest[iLen] = 0;
      }
      PRINTF("HTTP-GetParameter():<%s>" CRLF, pcParm);
   }
   return(pcParm);
}

//
// Function    : HTTP_RequestStaticPage
// Description : Request access to a file
//
// Parameters  : Client
// Returns     : TRUE if OKee
//
bool HTTP_RequestStaticPage(NETCL *pstCl)
{
   PRINTF("HTTP_RequestStaticPage():<%s>" CRLF, pstCl->pcUrl);

   http_CollectHandleExtension(pstCl);

   return(TRUE);
}

//
// Function    : HTTP_BuildGeneric
// Description : Build a generic http page
//
// Parameters  : Client, string^, parameters
// Returns     : TRUE if OKee
//
bool HTTP_BuildGeneric(NETCL *pstCl, const char *pcPage, ...)
{
   va_list  tParms;
   bool     fParsing=1;
   bool     fCc=TRUE;
   int      i=0, iVal;
   int32    lVal;
   double   flVal;
   char    *pcData, cChar, cBuffer[20];
   
   va_start(tParms, pcPage);
   //
   while(fParsing)
   {
      cChar = pcPage[i];
      switch(cChar)
      {
         case 0:
            fParsing = 0;
            break;
            
         case '%':
            i++;
            cChar = pcPage[i];
            switch(cChar)
            {
               case '%':
                  http_Xmt(pstCl, cChar);
                  break;
               
               case 'd':
                  iVal = va_arg(tParms, int);
                  GEN_SPRINTF (cBuffer, "%d", iVal);
                  HTTP_SendString(pstCl, cBuffer);
                  break;
               
               case 'f':
                  flVal = va_arg(tParms, double);
                  GEN_SPRINTF (cBuffer, "%f", flVal);
                  HTTP_SendString(pstCl, cBuffer);
                  break;
               
               case 'F':
                  flVal = va_arg(tParms, double);
                  GEN_SPRINTF (cBuffer, "%5.3f", flVal);
                  HTTP_SendString(pstCl, cBuffer);
                  break;
               
               case 'x':
               case 'X':
                  iVal = va_arg(tParms, int);
                  GEN_SPRINTF (cBuffer, "%x", iVal);
                  HTTP_SendString(pstCl, cBuffer);
                  break;
               
               case 'l':
                  lVal = va_arg(tParms, int32);
                  GEN_SPRINTF (cBuffer, "%ld", lVal);
                  HTTP_SendString(pstCl, cBuffer);
                  break;
               
               case 'S':
               case 's':
                  pcData = va_arg(tParms, char *);
                  HTTP_SendString(pstCl, pcData);
                  break;
               
               default:
                  fCc = FALSE;
                  break;
            }
            break;
            
         default:
            http_Xmt(pstCl, cChar);
            break;
      }
      i++;
   }
   va_end(tParms);
   return(fCc);
}

//
// Function:   HTTP_GetFromNetwork
// Purpose:    Retrieve (specific) data from the network
//
// Parms:      Url, target string, Dest Buffer, length, read timeout in secs
// Returns:    TRUE if actual address returned
// Note:       Target = "" will return any string
//             Dest   = NULL will discard the data but report TRUE if target is found
//
bool HTTP_GetFromNetwork(char *pcUrl, char *pcFind, char *pcBuffer, int iLen, int iTimeout)
{
   bool     fFound=FALSE;
   char    *pcLine;
   char    *pcName;
   char    *pcRes;
   FILE    *ptFp;
   
   pcLine = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, READ_LEN+1);
   pcName = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, NAME_LEN+1);
   //
   GEN_SNPRINTF(pcName, NAME_LEN, "wget http://%s -T %d -O - -q", pcUrl, iTimeout);
   if( (ptFp = popen(pcName, "r")) == NULL)
   {
      // Error
      LOG_Report(errno, "GEN", "HTTP-GetFromNetwork():Pipe %s open ERROR:", pcName);
      PRINTF("HTTP-GetFromNetwork():Pipe %s open ERROR %s" CRLF, pcName, strerror(errno));
   }
   else
   {
      while(1)
      {
         pcRes = fgets(pcLine, READ_LEN, ptFp);
         if(pcRes)
         {
            GEN_RemoveChar(pcRes, 0x0d);
            GEN_RemoveChar(pcRes, 0x0a);
            GEN_TrimRight(pcRes);
            PRINTF("HTTP_GetFromNetwork(): %s" CRLF, pcRes);
            if(GEN_STRSTRI(pcRes, pcFind))
            {
               if(pcBuffer) GEN_STRNCPY(pcBuffer, pcRes, (size_t)iLen);
               fFound = TRUE;
               break;
            }
         }
         else 
         {
            PRINTF("HTTP_GetFromNetwork():EOF" CRLF);
            break;
         }
      }
      pclose(ptFp);
   }
   SAFEFREE(__PRETTY_FUNCTION__ , pcName);
   SAFEFREE(__PRETTY_FUNCTION__ , pcLine);
   return(fFound);
}

//
// Function:   HTTP_RetrieveRpi
// Purpose:    Retrieve all raspberries from the local network
//
// Parms:      
// Returns:    RPI Struct ^
// Note:       
//
RPILOC *HTTP_RetrieveRpi()
{
   char    *pcLine;
   char    *pcName;
   char    *pcTemp;
   char    *pcRes;
   RPILOC  *pstRpi=NULL;
   RPILOC  *pstNew;
   FILE    *ptFp;
   
   pcLine = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, READ_LEN+1);
   pcName = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, NAME_LEN+1);
   pcTemp = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, NAME_LEN+1);
   //
   GEN_SNPRINTF(pcName, NAME_LEN, pcRpiArp);
   if( (ptFp = popen(pcName, "r")) == NULL)
   {
      // Error
      LOG_Report(errno, "GEN", "HTTP-GetRaspberry():Pipe %s open ERROR:", pcName);
      PRINTF("HTTP-GetRaspberry():Pipe %s open ERROR %s" CRLF, pcName, strerror(errno));
   }
   else
   {
      while(1)
      {
         pcRes = fgets(pcLine, READ_LEN, ptFp);
         if(pcRes)
         {
            PRINTF("HTTP-GetRaspberry():%s", pcRes);
            if(GEN_STRSTRI(pcRes, pcRpiMac))
            {
               // This entry has the RPI Mac
               pstNew = (RPILOC *) SAFEMALLOC(__PRETTY_FUNCTION__, sizeof(RPILOC));
               if(pstRpi) pstRpi->pstNext = pstNew;
               pstRpi = pstNew;
               GEN_SSCANF(pcRes, "%15s %16s %17s %40[^\n]", pstRpi->cIp, pcTemp, pstRpi->cMac, pcTemp);
               PRINTF("HTTP-GetRaspberry():IP=%s, MAC=%s" CRLF, pstRpi->cIp, pstRpi->cMac);
            }
         }
         else 
         {
            PRINTF("HTTP-GetRaspberry():EOF" CRLF);
            break;
         }
      }
      pclose(ptFp);
   }
   SAFEFREE(__PRETTY_FUNCTION__ , pcTemp);
   SAFEFREE(__PRETTY_FUNCTION__ , pcName);
   SAFEFREE(__PRETTY_FUNCTION__ , pcLine);
   return(pstRpi);
}

//
// Function:   HTTP_ReleaseRpi
// Purpose:    Release report
//
// Parms:      RPI struct
// Returns:    NULL
// Note:       
//
RPILOC *HTTP_ReleaseRpi(RPILOC *pstRpi)
{
   RPILOC  *pstNew;

   while(pstRpi)
   {
      pstNew = pstRpi->pstNext;
      SAFEFREE(__PRETTY_FUNCTION__ , pstRpi);
      pstRpi = pstNew;
   }
   return(NULL);
}

//
// Function    : HTTP_SendData
// Description : Put out Client buffer
//
// Parameters  : Client
// Returns     : 
//
void HTTP_SendData(NETCL *pstCl, char *pcData, int iLen)
{
   int   iIdx=0;

   while(iIdx<iLen)
   {
      http_Xmt(pstCl, pcData[iIdx]);
      iIdx++;
   }
}

//
// Function    : HTTP_SendString
// Description : Display http string
//
// Parameters  : Client, string^
// Returns     : 
//
void HTTP_SendString(NETCL *pstCl, char *pcData)
{
   char    cChar;
   int     i=0;

   while(1)
   {
      cChar = pcData[i];
      //
      if(cChar)
      {
         http_Xmt(pstCl, cChar);
         i++;
      }
      else break;
   }
}

// 
// Function:   HTTP_SendTextFile
// Purpose:    Send a textfile from fs to socket
// 
// Parameters: Client, filename
// Returns:    TRUE if OKee
// Note:       
//
bool HTTP_SendTextFile(NETCL *pstCl, char *pcFile)
{
   bool     fCc=FALSE;
   int      iRead, iTotal=0;
   FILE    *ptFile;
   char    *pcBuffer;
   char    *pcRead;

   ptFile = fopen(pcFile, "r");

   if(ptFile)
   {
      pcBuffer = (char *) SAFEMALLOC(__PRETTY_FUNCTION__, RCV_BUFFER_SIZE);
      //
      PRINTF("HTTP-SendTextFile(): File=<%s>" CRLF, pcFile);
      do
      {
         pcRead = fgets(pcBuffer, RCV_BUFFER_SIZE, ptFile);
         if(pcRead)
         {
            iRead = GEN_STRLEN(pcBuffer);
            //PRINTF("HTTP-SendTextFile(): %s(%d)" CRLF, pcBuffer, iRead);
            HTTP_SendData(pstCl, pcBuffer, iRead);
            iTotal += iRead;
         }
         else
         {
            //PRINTF("HTTP-SendTextFile(): EOF" CRLF);
         }
      }
      while(pcRead);
      //PRINTF("HTTP-SendTextFile(): %d bytes written." CRLF, iTotal);
      fclose(ptFile);
      SAFEFREE(__PRETTY_FUNCTION__, pcBuffer);
      fCc = TRUE;
   }
   else
   {
      PRINTF("HTTP-SendTextFile(): Error open file <%s>" CRLF, pcFile);
   }
   return(fCc);
}


/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

//
// Function    : http_CollectHandleExtension
// Description : Return the filename extension
//
// Parameters  : Client
// Returns     : FTYPE
//
static FTYPE http_CollectHandleExtension(NETCL *pstCl)
{
   const char *pcExtType;
   char       *pcUrl=pstCl->pcUrl;
   int         iUrlIdx=pstCl->iExtnIdx;
   FTYPE       tFileType;
   PFNET       pfExtCb;
   
   if(iUrlIdx)
   {
      char *pcExt = &pcUrl[iUrlIdx];

      for(iUrlIdx=0; iUrlIdx<iNumHttpTypes; iUrlIdx++)
      {
         pcExtType = stHttpTypes[iUrlIdx].pcExt;
         PRINTF("http_CollectHandleExtension():<%s> --> <%s>" CRLF, pcExt, pcExtType);
         if(GEN_STRCMP(pcExt, pcExtType) == 0)
         {
            //PRINTF(" Match !" CRLF);
            tFileType = stHttpTypes[iUrlIdx].tType;
            //
            // Reply the correct http header
            //
            // "HTTP/1.0 200 OK"          CRLF
            // "Content-Length:12456"     CRLF
            // "Content-Type:text/html"   CRLF
            // "Connection:Close"         CRLF CRLF
            //
            pcExtType  = stHttpTypes[iUrlIdx].pcType;
            if(pcExtType)
            {
               HTTP_BuildGeneric(pstCl, pcHttpRequestHeaderOKee, pcExtType);
            }
            HTTP_SendString(pstCl, (char *)pcHttpRequestHeaderConnect);
            //
            pfExtCb = (PFNET) stHttpTypes[iUrlIdx].pfExt;
            if(pfExtCb) pfExtCb(pstCl);
            return(tFileType);
         }
      }
      //
      // No valid Content type found
      //
      //PRINTF("http_CollectHandleExtension():No Match !" CRLF);
      HTTP_BuildGeneric(pstCl, pcHttpRequestHeaderGeneric);
      HTTP_SendString(pstCl,   (char *)pcHttpRequestHeaderConnect);
      HTTP_SendString(pstCl,   (char *)pcHttpRequestHeaderForbidden); 
      HTTP_SendString(pstCl,   (char *)pcHttpRequestHeaderConnect);
   }
   else
   {
      //PRINTF("http_CollectHandleExtension():No ext in URL" CRLF);
      HTTP_BuildGeneric(pstCl, pcHttpRequestHeaderGeneric);
      HTTP_SendString(pstCl,   (char *)pcHttpRequestHeaderConnect);
      HTTP_SendString(pstCl,   (char *)pcHttpRequestHeaderBad);
      HTTP_SendString(pstCl,   (char *)pcHttpRequestHeaderConnect);
   }
   return(HTTP_GEN);
}

//
// Function    : http_HandleHtml
// Description : Handle the request for HTML type files
//
// Parameters  : Client
// Returns     : TRUE if OKee
//
static bool http_HandleHtml(NETCL *pstCl)
{
   return( http_HandleRequest(pstCl) );
}

//
// Function    : http_HandleJs
// Description : Handle the request for JS files
//
// Parameters  : Client
// Returns     : TRUE if OKee
//
static bool http_HandleJs(NETCL *pstCl)
{
   return( http_HandleRequest(pstCl) );
}

//
// Function    : http_HandleJpg
// Description : Handle the request for JPG type files
//
// Parameters  : Client
// Returns     : TRUE if OKee
//
static bool http_HandleJpg(NETCL *pstCl)
{
   return( http_HandleRequest(pstCl) );
}

//
// Function    : http_HandleBmp
// Description : Handle the request for BMP type files
//
// Parameters  : Client
// Returns     : TRUE if OKee
//
static bool http_HandleBmp(NETCL *pstCl)
{
   return( http_HandleRequest(pstCl) );
}

//
// Function    : http_HandleGif
// Description : Handle the request for GIF type files
//
// Parameters  : Client
// Returns     : TRUE if OKee
//
static bool http_HandleGif(NETCL *pstCl)
{
   return( http_HandleRequest(pstCl) );
}

//
// Function    : http_HandlePng
// Description : Handle the request for PNG type files
//
// Parameters  : Client
// Returns     : TRUE if OKee
//
static bool http_HandlePng(NETCL *pstCl)
{
   return( http_HandleRequest(pstCl) );
}

//
// Function    : http_HandleTxt
// Description : Handle the request for TXT type files
//
// Parameters  : Client
// Returns     : TRUE if OKee
//
static bool http_HandleTxt(NETCL *pstCl)
{
   return( http_HandleViewer(pstCl) );
}

//
// Function    : http_HandleMp3
// Description : Handle the request for mp3 audio files
//
// Parameters  : Client
// Returns     : TRUE if OKee
//
static bool http_HandleMp3(NETCL *pstCl)
{
   return( http_HandleRequest(pstCl) );
}

//
// Function    : http_HandleHdtv
// Description : Handle the request for HDTV type files
//
// Parameters  : Client
// Returns     : TRUE if OKee
//
static bool http_HandleHdtv(NETCL *pstCl)
{
   return( http_HandleRequest(pstCl) );
}

//
// Function    : http_HandleJson
// Description : Handle the request for JSON files
//
// Parameters  : Client
// Returns     : TRUE if OKee
//
static bool http_HandleJson(NETCL *pstCl)
{
   return( http_HandleRequest(pstCl) );
}

//
// Function    : http_HandleRequest
// Description : Handle the request: 
//               pcUrl-->"/www/dir1/dir2/the_filename.ext"
//                                       ^iFileIdx    ^iFileExt
// OR
//               pcUrl-->"the_filename.ext"
//                        ^iFileIdx    ^iFileExt
// Parameters  : Client
// Returns     : TRUE if OKee
// Note        : Use the HTTP Header buffer pcHdr to handle the file transfer !
//
static bool http_HandleRequest(NETCL *pstCl)
{
   bool     fCc=FALSE, fAccess;
   int      iFd, iRead, iTotal=0;
   char    *pcUrl=pstCl->pcUrl;

   //
   // First verify that the caller has access to the full pathname:
   //    STATIC   : pcUrl->"cam.json" 
   //    DYNAMIC  : pcUrl->"/all/public/www/20170828_123456.jpg" 
   //    DYNAMIC  : pcUrl->RPI_WORK_DIR "aap.bmp" 
   //    DYNAMIC  : pcUrl->"thumbnails/picam_fotos.jpg" 
   //
   PRINTF("http_HandleRequest{}: Static file=<%s>" CRLF, pcUrl);
   //
   // CHECK legal pathnameS
   //
   if( (fAccess = http_VerifyPath(pstCl, pcUrl, 0)) )
   {
      // If URL has the full path, this is a legal pathname
      GEN_SNPRINTF(pstCl->pcHdr, HDR_BUFFER_SIZE, "%s", pcUrl);
   }
   else if((fAccess = http_VerifyPath(pstCl, pcUrl, 1)) )
   {
      // If URL has the full path, but missing leading '/' : this is a legal pathname too
      GEN_SNPRINTF(pstCl->pcHdr, HDR_BUFFER_SIZE, "/%s", pcUrl);
   }
   else
   {
      //
      // Page is STATIC but has no full path: build legal pathname
      //
      GEN_SNPRINTF(pstCl->pcHdr, HDR_BUFFER_SIZE, "%s%s", pstCl->pcWww, pcUrl);
   }
   //
   // Try to upload the file to caller
   //
   iFd = safeopen(pstCl->pcHdr, O_RDONLY);
   if(iFd > 0)
   {
      PRINTF("http_HandleRequest{}: File=<%s> opended OKee" CRLF, pstCl->pcHdr);
      
      #ifdef FEATURE_ECHO_BIN_OUT
      //
      // Echo all HTTP request data from the client
      //
      printf("http_HandleRequest{}: File=<%s>" CRLF, pstCl->pcHdr);
      #endif //FEATURE_ECHO_BIN_OUT
      
      do
      {
         iRead = read(iFd, pstCl->pcRcvBuffer, RCV_BUFFER_SIZE);
         if(iRead == 0) fCc = TRUE;
         if(iRead > 0)
         {
            HTTP_SendData(pstCl, pstCl->pcRcvBuffer, iRead);
            iTotal += iRead;
            //PRINTF("http_HandleRequest(): Send %d bytes" CRLF, iRead);
         }
      }
      while(iRead > 0);
      
      #ifdef FEATURE_ECHO_BIN_OUT
      //
      // Echo all HTTP request data from the client
      //
      printf("http_HandleRequest(): Send %d bytes" CRLF, iTotal);
      #endif //FEATURE_ECHO_BIN_OUT
      
      safeclose(iFd);
      GLOBAL_CheckDelete(pstCl->pcHdr);
   }
   else
   {
      LOG_Report(errno, "COM", "http_HandleRequest{}: Error open file <%s>", pstCl->pcHdr);
      PRINTF("http_HandleRequest{}: Error open file <%s>:%s" CRLF, pstCl->pcHdr, strerror(errno));
      //
      HTTP_SendString(pstCl, (char *)pcHttpRequestHeaderError);
      HTTP_SendString(pstCl, (char *)pcHttpRequestHeaderConnect);
   }
   return(fCc);
}

//
// Function    : http_HandleViewer
// Description : Handle the file viewer: 
//               pcUrl-->"/www/dir1/dir2/the_filename.ext"
//                                       ^iFileIdx    ^iFileExt
// OR
//               pcUrl-->"the_filename.ext"
//                        ^iFileIdx    ^iFileExt
// Parameters  : Client
// Returns     : TRUE if OKee
// Note        : Use the HTTP Header buffer pcHdr to handle the file transfer !
//
static bool http_HandleViewer(NETCL *pstCl)
{
   bool     fCc=FALSE, fAccess;
   char    *pcUrl=pstCl->pcUrl;

   //
   // First verify that the caller has access to the full pathname:
   //    STATIC   : pcUrl->"cam.json" 
   //    DYNAMIC  : pcUrl->"/all/public/www/20170828_123456.jpg" 
   //    DYNAMIC  : pcUrl->RPI_WORK_DIR "aap.bmp" 
   //    DYNAMIC  : pcUrl->"thumbnails/picam_fotos.jpg" 
   //
   // CHECK legal pathnameS
   //
   if( (fAccess = http_VerifyPath(pstCl, pcUrl, 0)) )
   {
      // If URL has the full path, this is a legal pathname
      GEN_SNPRINTF(pstCl->pcHdr, HDR_BUFFER_SIZE, "%s", pcUrl);
      PRINTF("http-HandleViewer(): Static(0) file=<%s>" CRLF, pstCl->pcHdr);
   }
   else if((fAccess = http_VerifyPath(pstCl, pcUrl, 1)) )
   {
      // If URL has the full path, but missing leading '/' : this is a legal pathname too
      GEN_SNPRINTF(pstCl->pcHdr, HDR_BUFFER_SIZE, "/%s", pcUrl);
      PRINTF("http-HandleViewer{}: Static(1) file=<%s>" CRLF, pstCl->pcHdr);
   }
   else
   {
      //
      // Page is STATIC but has no full path: build legal pathname
      //
      GEN_SNPRINTF(pstCl->pcHdr, HDR_BUFFER_SIZE, "%s%s", pstCl->pcWww, pcUrl);
      PRINTF("http-HandleViewer{}: Static(2) file=<%s>" CRLF, pstCl->pcHdr);
   }
   //
   // Try to view the file
   //
   HTTP_BuildGeneric(pstCl, pcWebPageStart, pcWebPageTitle);
   HTTP_BuildGeneric(pstCl, pcWebPageCenter);
   HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
   HTTP_BuildGeneric(pstCl, pcWebPageDefault);
   HTTP_BuildGeneric(pstCl, pcWebPagePreStart);
   HTTP_BuildGeneric(pstCl, pcWebPageLineBreak);
   //
   fCc = HTTP_SendTextFile(pstCl, pstCl->pcHdr);
   //
   HTTP_BuildGeneric(pstCl, pcWebPagePreEnd);
   HTTP_BuildGeneric(pstCl, pcWebPageEnd);
   return(fCc);
}

//
// Function    : http_VerifyPath
// Description : Check if the requested URL has a valid path.
//
// Parameters  : Net Client, The URL, the offset for the destination path
// Returns     : TRUE if valid URL
// Note        : Incoming URL = "GET /cam.json
//                         or   "GET /mnt/rpicache/filename.jpg" 
//                                    ^
//                                    pcUrl
//               The HTTP acquisition state machine drops the initial '/' from the URL by default
//
//
static bool http_VerifyPath(NETCL *pstCl, char *pcUrl, int iOffset)
{
   bool     fCc=FALSE;
   int      iLen;

   iLen = GEN_STRLEN(&(pstCl->pcWww[iOffset]));
   PRINTF("http_VerifyPath{}:<%d> <%s><%s>" CRLF, iLen, pcUrl, &(pstCl->pcWww[iOffset]));
   //
   if( GEN_STRNCMP(pcUrl, &(pstCl->pcWww[iOffset]), iLen) == 0)    fCc = TRUE;
   else 
   {
      iLen = GEN_STRLEN(&(pstCl->pcCache[iOffset]));
      PRINTF("http_VerifyPath{}:<%d> <%s><%s>" CRLF, iLen, pcUrl, &(pstCl->pcCache[iOffset]));
      if( GEN_STRNCMP(pcUrl, &(pstCl->pcCache[iOffset]), iLen) == 0) fCc = TRUE;
   }
   return(fCc);
}

// 
// Function:   http_Xmt 
// Purpose:    Send char to transmit buffer
// 
// Parameters: Client data, char
// Returns:    TRUE if OKee
// Note:       
//
static bool http_Xmt(NETCL *pstCl, char cChar)
{
   bool  fCc=TRUE;

#ifdef   FEATURE_ECHO_HTTP_OUT
   //
   // Echo all HTTP request data from the client
   //
   if(pstCl->iVerbose & NET_VERBOSE_HTTP_PUT)
   {
      putchar((int)cChar);
//    if(cChar == '\n')            putchar((int)cChar);
//    else if(isprint((int)cChar)) putchar((int)cChar);
//    else                         putchar((int)'.');
   }
#endif   //FEATURE_ECHO_HTTP_OUT

   //
   // Write the char into the XMT buffer. 
   // If the buffer is full, flush the cache to the real world
   //
   if( NET_XmtPut(pstCl, &cChar, 1) == 0)
   {
      //
      // XMT cache buffer is full: try to empty the cache first
      //
      if( NET_FlushCache(pstCl) <= 0)              fCc = FALSE;
      else if( NET_XmtPut(pstCl, &cChar, 1) == 0)  fCc = FALSE;
   }
   return(fCc);
}

/*------  Local functions separator ------------------------------------
__HTTP_HEADER_PARSER_________(){};
----------------------------------------------------------------------------*/

// 
// Function    : http_ParseRequestHeader
// Description : Parse the received http request for header info we need
// 
// Parameters  : Client
// Returns     : HTTP request type
// Note        :
//
static RTYPE http_ParseRequestHeader(NETCL *pstCl, char *pcReqHdr)
{
   RTYPE          tType=HTTP_CHAR;
   int            iLen;
   PFHTTP         pfAddr;
   const HTTPREQ *pstReqHdr=stHttpRequestHeaders;

   PRINTF("http_ParseRequestHeader():[%s]" CRLF, pcReqHdr);
   //
   while(pstReqHdr->pcHeader)
   {
      iLen = GEN_STRLEN(pstReqHdr->pcHeader);
      if(GEN_STRNCMPI(pstReqHdr->pcHeader, pcReqHdr, iLen) == 0)
      {
         //
         // Request header found
         //
         pfAddr = (PFHTTP) pstReqHdr->pfHttpCb;
         if(pfAddr)
         {
            //
            // Execute HTTP request header handler
            //
            tType = pfAddr(pstCl, pcReqHdr, iLen);
         }
         break;
      }
      else
      {
         pstReqHdr++;
      }
   }
   return(tType);
}

// 
// Function    : http_ReqHdrGetValue
// Description : Get a value from a request header
// 
// Parameters  : Client, request header
// Returns     : int
// 
//
static int http_ReqHdrGetValue(NETCL *pstCl, char *pcBuffer)
{
   int   iVal;

   iVal = (int)strtol(pcBuffer, NULL, 0);
   PRINTF("http_ReqHdrGetValue():Val=%d" CRLF, iVal);
   return(iVal);
}

// 
// Function    : http_ReqHdrAcceptEncoding
// Description : RequestHeader: Accept-Encoding
// 
// Parameters  : Client
// Returns     : Request header type
// 
//
static RTYPE http_ReqHdrAcceptEncoding(NETCL *pstCl, char *pcHdr, int iLen)
{
   RTYPE tType=HTTP_CHAR;

   PRINTF("http_ReqHdrAcceptEncoding():%s" CRLF, pcHdr);
   return(tType);
}

// 
// Function    : http_ReqHdrContentLength
// Description : RequestHeader: content-length: xxxx
// 
// Parameters  : Client
// Returns     : Request header type
// 
//
static RTYPE http_ReqHdrContentLength(NETCL *pstCl, char *pcHdr, int iLen)
{
   RTYPE tType=HTTP_CHAR;

   pstCl->iContLength = http_ReqHdrGetValue(pstCl, &pcHdr[iLen+1]);
   PRINTF("http_ReqHdrContentLength():%s (-->%d)" CRLF, pcHdr, pstCl->iContLength);
   //
   return(tType);
}

// 
// Function    : http_ReqHdrContentType
// Description : RequestHeader: content-type
// 
// Parameters  : Client
// Returns     : HTTP_CHAR
// Note        : client net connection holds the actual content type 
//
static RTYPE http_ReqHdrContentType(NETCL *pstCl, char *pcHdr, int iLen)
{
   int      iTypeLen, iIdx=0;
   FTYPE    tType=HTTP_TXT;
   char    *pcType=&pcHdr[iLen];

   PRINTF("http_ReqHdrContentType():%s" CRLF, pcHdr);
   //
   // pcHdr -> "Application-Type: application/json"
   //
   pcType   = GEN_FindDelimiter(pcType, DELIM_COLON);
   pcType++;
   pcType   = GEN_FindDelimiter(pcType, DELIM_NOSPACE);
   iTypeLen = GEN_STRLEN(pcType);
   //
   while(iIdx<iNumHttpTypes)
   {
      if(GEN_STRNCMPI(stHttpTypes[iIdx].pcType, pcType, iTypeLen) == 0)
      {
         //
         // Content header found
         //
         tType = stHttpTypes[iIdx].tType;
         break;
      }
      else iIdx++;
   }
   //
   // Store actual Content-Type
   //
   pstCl->tContType = tType;
   PRINTF("http_ReqHdrContentType():%s" CRLF, stHttpTypes[iIdx].pcType);
   return(HTTP_CHAR);
}


/*------  Local functions separator ------------------------------------
__HTTP_STATE_MACHINE_________(){};
----------------------------------------------------------------------------*/

//
// Function:   http_StmIdle
// Purpose:    STM is idle: discard all input
//             
// Parms:      Netclient struct
//             
// Returns:    Char type (HTTP_xxx)
//
static RTYPE http_StmIdle(NETCL *pstCl)
{
   NET_RcvGet(pstCl, NULL, 1);
   return(HTTP_CHAR);
}

//
// Function:   http_StmStart
// Purpose:    STM start, wait for GET/POST start
//             
// Parms:      Netclient struct
//             
// Returns:    Char type (HTTP_xxx)
//
static RTYPE http_StmStart(NETCL *pstCl)
{
   RTYPE tType=HTTP_CHAR;
   char  cChar;

   NET_RcvGet(pstCl, &cChar, 1);
   //
   // CircularRcv:
   // pcRcvBuffer-> "GET /cam.json?snapshot&Height=576&Ext=bmp&Preview=860,0,860,540&Rotation=180&Width=1024 "
   //               "HTTP/1.0\r\n"
   //               "Host: www.patrn.nl\r\n"
   //               "Content-Type: application-json\r\nContent-Length: 42\r\n"
   //               "\r\n"
   // or
   //
   // pcRcvBuffer-> "POST /cam.json"
   //               "HTTP/1.0\r\n"
   //               "Host: www.patrn.nl\r\n"
   //               "Content-Type: application-text\r\n"
   //               "Content-Length: 73\r\n"
   //               "\r\n"
   //               "snapshot&Height=576&Ext=bmp&Preview=860,0,860,540&Rotation=180&Width=1024"
   // 
   // pcRcvBuffer-> 03 00 00 2f 2a ... ISO Transport Service over TCP
   //
   PRINTF("http_Stm(%02x):%s" CRLF, cChar, stHttpStateMachine[pstCl->iReqState].pcName);
   //
        if(cChar == 'G') pstCl->iReqState = NETSTM_GET_1;
   else if(cChar == 'P') pstCl->iReqState = NETSTM_POST1;
   else if(cChar == 0x3) pstCl->iReqState = NETSTM_ISO;
   else tType = HTTP_ERROR;
   return(tType);
}

//
// Function:   http_StmGet1
// Purpose:    
//             
// Parms:      Netclient struct
//             
// Returns:    Char type (HTTP_xxx)
//
static RTYPE http_StmGet1(NETCL *pstCl)
{
   RTYPE tType=HTTP_CHAR;
   char  cChar;

   NET_RcvGet(pstCl, &cChar, 1);

   if(cChar == 'E') pstCl->iReqState = NETSTM_GET_2;
   else             
   {  
      pstCl->iReqState = NETSTM_START;
      tType            = HTTP_ERROR;
   }
   return(tType);
}

//
// Function:   http_StmGet2
// Purpose:    
//             
// Parms:      Netclient struct
//             
// Returns:    Char type (HTTP_xxx)
//
static RTYPE http_StmGet2(NETCL *pstCl)
{
   RTYPE tType=HTTP_CHAR;
   char  cChar;

   NET_RcvGet(pstCl, &cChar, 1);

   if(cChar == 'T') 
   {
      // HTTP GET Request
      tType            = HTTP_GET;
      pstCl->iReqState = NETSTM_SPACE;
   }
   else
   {
      pstCl->iReqState = NETSTM_START;
      tType            = HTTP_ERROR;
   }
   return(tType);
}

//
// Function:   http_StmPost1
// Purpose:    
//             
// Parms:      Netclient struct
//             
// Returns:    Char type (HTTP_xxx)
//
static RTYPE http_StmPost1(NETCL *pstCl)
{
   RTYPE tType=HTTP_CHAR;
   char  cChar;

   NET_RcvGet(pstCl, &cChar, 1);

   if(cChar == 'O') pstCl->iReqState = NETSTM_POST2;
   else
   {
      pstCl->iReqState = NETSTM_START;
      tType            = HTTP_ERROR;
   }
   return(tType);
}

//
// Function:   http_StmPost2
// Purpose:    
//             
// Parms:      Netclient struct
//             
// Returns:    Char type (HTTP_xxx)
//
static RTYPE http_StmPost2(NETCL *pstCl)
{
   RTYPE tType=HTTP_CHAR;
   char  cChar;

   NET_RcvGet(pstCl, &cChar, 1);

   if(cChar == 'S') pstCl->iReqState = NETSTM_POST3;
   else
   {
      pstCl->iReqState = NETSTM_START;
      tType            = HTTP_ERROR;
   }
   return(tType);
}

//
// Function:   http_StmPost3
// Purpose:    
//             
// Parms:      Netclient struct
//             
// Returns:    Char type (HTTP_xxx)
//
static RTYPE http_StmPost3(NETCL *pstCl)
{
   RTYPE tType = HTTP_CHAR;
   char  cChar;

   NET_RcvGet(pstCl, &cChar, 1);

   if(cChar == 'T') 
   {
      // HTTP POST Request
      pstCl->iReqState = NETSTM_SPACE;
      tType            = HTTP_POST;
   }
   else
   {
      pstCl->iReqState = NETSTM_START;
      tType            = HTTP_ERROR;
   }
   return(tType);
}

//
// Function:   http_StmSpace
// Purpose:    Acquire the obligatory ' '
//             
// Parms:      Netclient struct
//             
// Returns:    Char type (HTTP_xxx)
//
static RTYPE http_StmSpace(NETCL *pstCl)
{
   RTYPE tType=HTTP_CHAR;
   char  cChar;

   NET_RcvGet(pstCl, &cChar, 1);

   //
   // CircularRcv
   // pcRcvBuffer-> "GET" 
   // We are here-> " /cam.json?snapshot&Height=576&Ext=bmp&Preview=860,0,860,540&Rotation=180&Width=1024 "
   //               "HTTP/1.0\r\n"
   //               "Host: www.patrn.nl\r\n"
   //               "Content-Type: application-text\r\n"
   //               "Content-Length: 42\r\n\r\n"
   //
   if(cChar == ' ') 
   {
      pstCl->iReqState = NETSTM_SLASH;
   }
   else
   {
      pstCl->iReqState = NETSTM_START;
      tType            = HTTP_ERROR;
   }
   return(tType);
}

//
// Function:   http_StmSlash
// Purpose:    Acquire the obligatory '/'
//             
// Parms:      Netclient struct
//             
// Returns:    Char type (HTTP_xxx)
//
static RTYPE http_StmSlash(NETCL *pstCl)
{
   RTYPE tType=HTTP_CHAR;
   char  cChar;

   NET_RcvGet(pstCl, &cChar, 1);

   //
   // CircularRcv:
   // pcRcvBuffer-> "GET " 
   // We are here-> "/cam.json?snapshot&Height=576&Ext=bmp&Preview=860,0,860,540&Rotation=180&Width=1024 "
   //               "HTTP/1.0\r\n"
   //               "Host: www.patrn.nl\r\nContent-Type: application-json\r\nContent-Length: 42\r\n\r\n"
   //
   if(cChar == '/') 
   {
      pstCl->iFileIdx  = 0;                              // Mark Url start
      pstCl->iUrlIdx   = 0;                              // Reset acq index
      pstCl->iReqState = NETSTM_URL;                     // Acquire URL/Pathname
   }
   else
   {
      pstCl->iReqState = NETSTM_START;
      tType            = HTTP_ERROR;
   }
   return(tType);
}

//
// Function:   http_StmEscape1
// Purpose:    %AB escape sequence handler
//             
// Parms:      Netclient struct
//             
// Returns:    Char type (HTTP_xxx)
// Note:       Keep the escape seq handler ajacent in the list !
//
static RTYPE http_StmEscape1(NETCL *pstCl)
{
   char  cEscapeChar;
   char  cChar;

   NET_RcvGet(pstCl, &cChar, 1);

   cEscapeChar = pstCl->pcUrl[pstCl->iUrlIdx];
   //
   if( (cChar>='0') && (cChar<='9') )
   {
      cEscapeChar <<= 4;
      cEscapeChar += (cChar - 48);
   }
   else if( (cChar>='A') && (cChar<='F') )
   {
      cEscapeChar <<= 4;
      cEscapeChar += (cChar - 55);
   }
   else if( (cChar>='a') && (cChar<='f') )
   {
      cEscapeChar <<= 4;
      cEscapeChar += (cChar - 87);
   }
   //
   pstCl->pcUrl[pstCl->iUrlIdx] = cEscapeChar;
   pstCl->iReqState             = NETSTM_ESC_2;
   return(HTTP_CHAR);
}

//
// Function:   http_StmEscape2
// Purpose:    %AB escape sequence handler
//             
// Parms:      Netclient struct
//             
// Returns:    Char type (HTTP_xxx)
//
static RTYPE http_StmEscape2(NETCL *pstCl)
{
   char  cEscapeChar;
   char  cChar;

   NET_RcvGet(pstCl, &cChar, 1);

   cEscapeChar = pstCl->pcUrl[pstCl->iUrlIdx];
   //
   if( (cChar>='0') && (cChar<='9') )
   {
      cEscapeChar <<= 4;
      cEscapeChar += (cChar - 48);
   }
   else if( (cChar>='A') && (cChar<='F') )
   {
      cEscapeChar <<= 4;
      cEscapeChar += (cChar - 55);
   }
   else if( (cChar>='a') && (cChar<='f') )
   {
      cEscapeChar <<= 4;
      cEscapeChar += (cChar - 87);
   }
   //
   pstCl->pcUrl[pstCl->iUrlIdx++] = cEscapeChar;
   pstCl->iReqState               = pstCl->iSubState;
   pstCl->iSubState               = -1;
   return(HTTP_CHAR);
}

//
// Function:   http_StmUrl
// Purpose:    Start URL collector: Store URL until the space char. Then collect and parse
//             the headers. The headers end with an empty line (/r/n/r/n) sequence. 
//             
// Parms:      Netclient struct
//             
// Returns:    Char type (HTTP_xxx)
//
static RTYPE http_StmUrl(NETCL *pstCl)
{
   char  cChar;

   NET_RcvGet(pstCl, &cChar, 1);
   //
   // CircularRcv:
   // pcRcvBuffer-> "GET /" 
   // We are here-> "cam.json?snapshot&Height=576&Ext=bmp&Preview=860,0,860,540&Rotation=180&Width=1024 "
   //               "HTTP/1.0\r\n"
   //               "Host: www.patrn.nl\r\nContent-Type: application-json\r\nContent-Length: 42\r\n\r\n"
   // OR
   //
   // CircularRcv:
   // pcRcvBuffer-> "GET /" 
   // We are here-> "/all/public/www/20170827-123456.jpg "
   //               "HTTP/1.0\r\n"
   //               "Host: www.patrn.nl\r\nContent-Type: application-json\r\nContent-Length: 42\r\n\r\n"
   //
   if(cChar == '%') 
   {
      pstCl->pcUrl[pstCl->iUrlIdx] = 0;                  // clear TEMP %char accumalator
      pstCl->iSubState             = pstCl->iReqState;   // Save current state
      pstCl->iReqState             = NETSTM_ESC_1;       // %AB escape sequence
   }
   else if(cChar == ' ') 
   {
      //
      // URL end.
      //
      pstCl->pcUrl[pstCl->iUrlIdx++] = 0;                // Terminate url 
      pstCl->iHdrIdx                 = 0;                // Reset content-header buffer index
      pstCl->iReqState               = NETSTM_CHDRS;     // Acquire content-headers
      pstCl->fFlags &= ~NET_CONT_HDR_END;                // Reset /r/n/r/n End-of-ContHdr trigger
      //
      PRINTF("http_StmUrl():URL End: File=%s" CRLF, pstCl->pcUrl);
   }
   else
   {
      //
      //  pcUrl : [/dir1/dir2/filename.ext?parameter1=xxx&parameter2=yyy&...&parameterX=zzz]
      //                      ^        ^  ^^
      //                      |        |  ||
      //                      |        |  |+--- iPaylIdx 
      //                      |        |  +---- /0 terminator
      //                      |        +------- iExtnIdx  
      //                      +---------------- iFileIdx  
      //
      if((cChar == '/') && (pstCl->iPaylIdx == 0))       // Directory separator
      {
         pstCl->pcUrl[pstCl->iUrlIdx++] = cChar;
         pstCl->iFileIdx = pstCl->iUrlIdx;               // Overwrite filename index
      }
      else if((cChar == '.') && (pstCl->iPaylIdx == 0))  // Start filename extension
      {
         pstCl->pcUrl[pstCl->iUrlIdx++] = cChar;
         pstCl->iExtnIdx = pstCl->iUrlIdx;               // Overwrite index to extension
      }
      else if((cChar == '?') && (pstCl->iPaylIdx == 0))  // Start parameters
      {
         pstCl->pcUrl[pstCl->iUrlIdx++] = 0;             // terminate filename.ext
         pstCl->iPaylIdx = pstCl->iUrlIdx;               // store index to payload
         pstCl->iNextIdx = pstCl->iUrlIdx;               // copy
      }
      else pstCl->pcUrl[pstCl->iUrlIdx++] = cChar;
   }
   return(HTTP_CHAR);
}

//
// Function:   http_StmContentHeaders
// Purpose:    
//             
// Parms:      Netclient struct
//             
// Returns:    Char type (HTTP_xxx)
//
static RTYPE http_StmContentHeaders(NETCL *pstCl)
{
   RTYPE tType = HTTP_CHAR;
   char  cChar;

   NET_RcvGet(pstCl, &cChar, 1);

   //
   // CircularRcv:
   // pcRcvBuffer-> "GET /cam.json?snapshot&Height=576&Ext=bmp&Preview=860,0,860,540&Rotation=180&Width=1024 "
   // We are here-> "HTTP/1.0\r\n"
   //               "Host: www.patrn.nl\r\nContent-Type: application-json\r\nContent-Length: 42\r\n\r\n"
   //                |
   // iHdrIdx--------+
   //
   if(cChar == '\n') 
   {
      if(pstCl->fFlags & NET_CONT_HDR_END)
      {
         //
         // Two consecutive /r/n : End of Content headers
         // If we have a POST: collect payload too (separate url and payload with '?' as would the GET request)
         //
         if(pstCl->tHttpType == HTTP_POST)
         {
            pstCl->iPaylIdx  = pstCl->iUrlIdx;           // store index to 1st parameter
            pstCl->iNextIdx  = pstCl->iUrlIdx;           // store index to 1st parameter
            pstCl->iReqState = NETSTM_PAYLD;             //
            //
            //LOG_Report(0, "NET", "http_StmContentHeaders():POST Idx=%d, Size=%d", pstCl->iPaylIdx, pstCl->iContLength);
            PRINTF("http_StmContentHeaders():POST Idx=%d, Size=%d" CRLF, pstCl->iPaylIdx, pstCl->iContLength);
         }
         else
         {
            tType            = HTTP_DONE;                // HTTP_GET
            pstCl->iReqState = NETSTM_IDLE;              //
            //
            PRINTF("http_StmContentHeaders():GET request End" CRLF);
         }
      }
      else
      {
         //
         // Start new content header assembly
         //
         pstCl->fFlags |= NET_CONT_HDR_END;              // Setup /r/n/r/n End-of-ContHdr trigger
         pstCl->pcHdr[pstCl->iHdrIdx] = 0;               // Discard previous content header
      }
   }
   else if(cChar == '\r') 
   {
      if(pstCl->iHdrIdx)
      {
         //
         // End of this content header
         //
         pstCl->pcHdr[pstCl->iHdrIdx] = 0;                  // Terminate content-header
         //
         // Handle each content header
         //
         tType = http_ParseRequestHeader(pstCl, pstCl->pcHdr);
         //
         pstCl->iHdrIdx = 0;                                // Reset content-header buffer index      
         pstCl->fFlags &= ~NET_CONT_HDR_END;                // Reset /r/n/r/n End-of-ContHdr trigger
      }
   }
   else 
   {
      //
      // Continue assembling this content header
      //
      pstCl->pcHdr[pstCl->iHdrIdx++] = cChar;            // Store content-header char
   }
   return(tType);
}

//
// Function:   http_StmPayload
// Purpose:    POST: collect and store payload data
//             
// Parms:      Netclient struct
//             
// Returns:    Char type (HTTP_xxx)
//
static RTYPE http_StmPayload(NETCL *pstCl)
{
   RTYPE tType = HTTP_CHAR;
   char  cChar;

   NET_RcvGet(pstCl, &cChar, 1);
   
   //
   // CircularRcv:
   // pcRcvBuffer-> "POST /cam.json HTTP/1.0\r\n"
   //               "Host: www.patrn.nl\r\nContent-Type: application-json\r\nContent-Length: 42\r\n"
   //               "\r\n" 
   // We are here-> "snapshot&Height=576&Ext=bmp&Preview=860,0,860,540&Rotation=180&Width=1024"
   //                |
   // iPaylIdx-------+
   //
   if(pstCl->iContLength >=0)
   {
      if(cChar == '%') 
      {
         pstCl->iContLength -= 3;                           // "%AB" from buffer
         pstCl->pcUrl[pstCl->iUrlIdx] = 0;                  // clear TEMP %char accumalator
         pstCl->iSubState             = pstCl->iReqState;   // Save current state
         pstCl->iReqState             = NETSTM_ESC_1;       // %AB escape sequence
      }
      else
      {
         pstCl->iContLength--;
         pstCl->pcUrl[pstCl->iUrlIdx++] = cChar;            //
         pstCl->pcUrl[pstCl->iUrlIdx]   = 0;                // Terminate buffer
         //
         if(pstCl->iContLength)
         {
            tType = HTTP_CHAR;
         }
         else                   
         {
            tType            = HTTP_DONE;
            pstCl->iReqState = NETSTM_IDLE; 
            //
            PRINTF("http_StmPayload():Payload %s" CRLF, &pstCl->pcUrl[pstCl->iPaylIdx]);
         }
      }
   }
   else 
   {
      tType            = HTTP_DONE;
      pstCl->iReqState = NETSTM_IDLE; 
      //
      PRINTF("http_StmPayload():Payload end" CRLF);
   }
   return(tType);
}

//
// Function:   http_StmTpktRes
// Purpose:    TPKT: ISO Transport Service over TCP
//             
// Parms:      Netclient struct
//             
// Returns:    Char type (HTTP_xxx)
//
static RTYPE http_StmTpktRes(NETCL *pstCl)
{
   //
   // CircularRcv:
   //  pcRcvBuffer-> 03 
   //                00 
   //                00 2F    // Packet size
   //                2A 
   //                E0 00 ... "Cookie:xxxxx"
   //                "\r\n" 
   //                .....
   //
   pstCl->iReqState = NETSTM_ISOLM; 
   return(HTTP_CHAR);
}

//
// Function:   http_StmTpktLenMsb
// Purpose:    TPKT: ISO Transport Service over TCP
//             
// Parms:      Netclient struct
//             
// Returns:    Char type (HTTP_xxx)
//
static RTYPE http_StmTpktLenMsb(NETCL *pstCl)
{
   char  cChar;

   NET_RcvGet(pstCl, &cChar, 1);
   //
   // CircularRcv:
   //  pcRcvBuffer-> 03 
   //                00 
   //                00 2F 
   //                2A 
   //                E0 00 ... "Cookie:xxxxx"
   //                "\r\n" 
   //                .....
   //
   pstCl->iContLength = (int)cChar * 256;
   pstCl->iReqState = NETSTM_ISOLL; 
   return(HTTP_CHAR);
}

//
// Function:   http_StmTpktLenLsb
// Purpose:    TPKT: ISO Transport Service over TCP
//             
// Parms:      Netclient struct
//             
// Returns:    Char type (HTTP_xxx)
//
static RTYPE http_StmTpktLenLsb(NETCL *pstCl)
{
   RTYPE tType=HTTP_CHAR;
   int   iSize;
   char  cChar;

   NET_RcvGet(pstCl, &cChar, 1);
   
   //
   // CircularRcv:
   //  pcRcvBuffer-> 03 
   //                00 
   //                00 2F 
   //                2A 
   //                E0 00 ... "Cookie:xxxxx"
   //                "\r\n" 
   //                .....
   //
   // Calc remaining packet length
   //
   iSize = pstCl->iContLength + (int)cChar;
   if(iSize > 0)
   {
      pstCl->iContLength = iSize - 4;
      pstCl->iReqState   = NETSTM_TPKT; 
      LOG_Report(0, "NET", "http_StmTpkt():TPKT packet size = %d", iSize);
   }
   else
   {
      pstCl->iContLength = 0;
      pstCl->iReqState   = NETSTM_IDLE; 
      LOG_Report(0, "NET", "http_StmTpkt():Unknown TPKT packet");
      tType = HTTP_ERROR;
   }
   return(tType);
}

//
// Function:   http_StmTpktPacket
// Purpose:    TPKT: ISO Transport Service over TCP
//             Acquire the rest of the packet
// Parms:      Netclient struct
//             
// Returns:    Char type (HTTP_xxx)
//
static RTYPE http_StmTpktPacket(NETCL *pstCl)
{
   RTYPE tType=HTTP_CHAR;

   //
   // CircularRcv:
   //  pcRcvBuffer-> 03 
   //                00 
   //                00 2F 
   //                2A 
   //                E0 00 ... "Cookie:xxxxx"
   //                "\r\n" 
   //                .....
   //
   // Receive remaining packet length
   //
   if( --pstCl->iContLength == 0)
   {
      tType = HTTP_TPKT;
   }
   return(tType);
}

//
// Function:   http_StmRawPacket
// Purpose:    Raw data: Transport raw data over TCP
//             Copy all data into the pcUrl buffer for retrieval.
// Parms:      Netclient struct
//             
// Returns:    Char type (HTTP_xxx)
//
static RTYPE http_StmRawPacket(NETCL *pstCl)
{
   //
   // CircularRcv:
   //  pcRcvBuffer-> "...."
   //                "\r\n" 
   //                .....
   return(HTTP_RAW);
}



/*------  Local functions separator ------------------------------------
__Comment_________(){};
----------------------------------------------------------------------------*/


